Foxda
=====

Foxda is a German language tool to manage small associations and
companies, helping with book keeping, membership management and more.


History
-------

Foxda is a very old program that I created in 2001.  Despite its age,
it is still actively used and has been receiving a steady stream of
small improvements over the years and decades.

The program was initially written when I still went to school, and
while it does work well and does serve its purpose, it does not
reflect the state of the art of programming in any way.

On the surface, this is easily seen: Look at the old-fashioned
comments which form huge text block.  Look at the many variables and
class names which are in German language.  And notice the choice of
the programming language.

On a second look, you might find peculiarities such as CopyPrintJob
and CopyGraphics which apply the decorator pattern to PrintJob and
Graphics.  All that effort was done just to enable the implementation
of JPrintPreview, a custom print preview dialog that served its
purpose but never worked very well.  Years later I noticed the correct
solution to this problem: Just print into a PDF file and open it in
a PDF viewer.

The most important flaw, however, was the hard-coding of personal
information in the code.  This is why I can't publish the whole
project history here.  Instead, I'm providing the cleaned code as a
single initial commit here, after having moved all personal
information into the foxda.ini configuration file.

Foxda has always been Free Software and was licensed under the GPL,
but I never felt comfortable to actually release it.  Although I did
release some part of it as a library named [VoJI](http://voji.sourceforge.net/),
to my knowledge this library has never been used by anyone else.

In 2019, on its kind-of 18th aniversary, I finally released the whole
Foxda package to the public.


Get started
-----------

Install Ghostscript, Git, Java, PDFtk, PostgreSQL and Netpbm:

    sudo apt-get install default-jdk ghostscript git pdftk postgresql libpostgresql-jdbc-java netpbm

Build and install Foxda for the current user:

    git clone https://gitlab.com/v0g/foxda.git ~/.local/share/foxda
    make -C ~/.local/share/foxda
    mkdir -p ~/.local/bin
    ln -s ../share/foxda/foxda ~/.local/bin/

Create PostgreSQL user for the current user and set database password:

    sudo su - postgres -c "createuser -P $(id -nu)"

Create PostgreSQL database for Foxda:

    sudo su - postgres -c "createdb -O $(id -nu) foxda"

Adjust Foxda configuration and set `voji.db.user` to the current user and `voji.db.password` to the password:

    xdg-open ~/.local/share/foxda/foxda.ini

Run Foxda:

    ~/.local/bin/foxda

Within Foxda,

- select tab "DB",
- click on "Alles erstellen" and
- go back to the "Start" tab.

Note: For technical reasons, you need to add a local printer
(e.g. via CUPS), even if you only want to save the PDF exports
without printing.

Have fun!
