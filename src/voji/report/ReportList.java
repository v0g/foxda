/*****************************************************************************\
*                                                                             *
*                                                                             *
*                         V       V         JJ  IIIII                         *
*                          V     V           J    I                           *
*                           V   V   OO       J    I                           *
*                            V V   O  O  J   J    I                           *
*                             V     OO    JJJ   IIIII                         *
*                                                                             *
*                         ---------------------------                         *
*                        |Volker's  Java Improvements|                        *
*                         ---------------------------                         *
*                                                                             *
*                                                                             *
*******************************************************************************
*                   *                                     *                   *
*                   *   FILE:           ReportList.java   *                   *
*                   *                                     *                   *
*                    *************************************                    *
*                                                                             *
*                                                                             *
*  This library is free software; you can redistribute it and/or              *
*  modify it under the terms of the GNU Lesser General Public                 *
*  License as published by the Free Software Foundation; either               *
*  version 2.1 of the License, or (at your option) any later version.         *
*                                                                             *
*  This library is distributed in the hope that it will be useful,            *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU          *
*  Lesser General Public License for more details.                            *
*                                                                             *
*  You should have received a copy of the GNU Lesser General Public           *
*  License along with this library; if not, write to the Free Software        *
*  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA  *
*                                                                             *
*                                                                             *
\*****************************************************************************/
package voji.report;
import java.lang.*;
import java.util.*;
import javax.swing.*;
import voji.utils.*;

/**
 * This report component draws its sub components
 * vertically one under another.
 */
public class ReportList extends ReportComponent
{
    /**
     * Constructs a new empty <code>ReportList</code> instance
     */
    public ReportList()
    {
    }

    /**
     * Returns the total height of this component.
     * That means the sum of the sub components' heights,
     * or this component's own height
     * - which one is bigger.
     *
     * @param g the graphical context where the height has to refer to
     */
    public int getHeight(PageGraphics g)
    {
        int h=0;
        for (Iterator<ReportComponent> i=iterator();i.hasNext();)
            h+=i.next().getHeight(g);
        return Math.max(h,getOwnHeight(g));
    }

    /**
     * Draws this component.
     *
     * This method draws all sub components one under another.
     *
     * @param g the graphical context where to draw to
     *
     * @see #getHeight(PageGraphics)
     */
    protected void draw(PageGraphics g)
    {
        int pos=0;
        for (Iterator i=iterator();i.hasNext();)
            {
                ReportComponent comp=(ReportComponent)i.next();
                int h=comp.getHeight(g);
                pos+=h;

                comp.draw(g,h);
                g.translate(0,h);
            }
        g.translate(0,-pos);
    }
}
