/*****************************************************************************\
*                                                                             *
*                                                                             *
*                         V       V         JJ  IIIII                         *
*                          V     V           J    I                           *
*                           V   V   OO       J    I                           *
*                            V V   O  O  J   J    I                           *
*                             V     OO    JJJ   IIIII                         *
*                                                                             *
*                         ---------------------------                         *
*                        |Volker's  Java Improvements|                        *
*                         ---------------------------                         *
*                                                                             *
*                                                                             *
*******************************************************************************
*                   *                                     *                   *
*                   *   FILE:          ReportColor.java   *                   *
*                   *                                     *                   *
*                    *************************************                    *
*                                                                             *
*                                                                             *
*  This library is free software; you can redistribute it and/or              *
*  modify it under the terms of the GNU Lesser General Public                 *
*  License as published by the Free Software Foundation; either               *
*  version 2.1 of the License, or (at your option) any later version.         *
*                                                                             *
*  This library is distributed in the hope that it will be useful,            *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU          *
*  Lesser General Public License for more details.                            *
*                                                                             *
*  You should have received a copy of the GNU Lesser General Public           *
*  License along with this library; if not, write to the Free Software        *
*  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA  *
*                                                                             *
*                                                                             *
\*****************************************************************************/
package voji.report;
import java.lang.*;
import java.awt.*;
import voji.utils.*;

/**
 * This report component changes the current color for
 * its sub components.
 */
public class ReportColor extends ReportComponent
{
    /**
     * Constructs a new <code>ReportColor</code> instance
     *
     * @param color the color which should be used for this component's
     *              sub components
     */
    public ReportColor(Color color)
    {
        this.color=color;
    }

    /**
     * Constructs a new <code>ReportColor</code> instance
     * containing the given sub component
     *
     * @param color the color which should be used for this component's
     *              sub components
     * @param component a sub component to add
     */
    public ReportColor(Color color,ReportComponent component)
    {
        this(color);
        add(component);
    }

    /**
     * The color of the sub components
     */
    public Color color;

    /**
     * Draws this component
     *
     * @param g the graphical context where to draw to
     */
    public void draw(PageGraphics g)
    {
        Color o=g.getColor();
        g.setColor(color);
        super.draw(g);
        g.setColor(o);
    }
}
