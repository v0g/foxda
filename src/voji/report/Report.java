/*****************************************************************************\
*                                                                             *
*                                                                             *
*                         V       V         JJ  IIIII                         *
*                          V     V           J    I                           *
*                           V   V   OO       J    I                           *
*                            V V   O  O  J   J    I                           *
*                             V     OO    JJJ   IIIII                         *
*                                                                             *
*                         ---------------------------                         *
*                        |Volker's  Java Improvements|                        *
*                         ---------------------------                         *
*                                                                             *
*                                                                             *
*******************************************************************************
*                   *                                     *                   *
*                   *   FILE:               Report.java   *                   *
*                   *                                     *                   *
*                    *************************************                    *
*                                                                             *
*                                                                             *
*  This library is free software; you can redistribute it and/or              *
*  modify it under the terms of the GNU Lesser General Public                 *
*  License as published by the Free Software Foundation; either               *
*  version 2.1 of the License, or (at your option) any later version.         *
*                                                                             *
*  This library is distributed in the hope that it will be useful,            *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU          *
*  Lesser General Public License for more details.                            *
*                                                                             *
*  You should have received a copy of the GNU Lesser General Public           *
*  License along with this library; if not, write to the Free Software        *
*  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA  *
*                                                                             *
*                                                                             *
\*****************************************************************************/
package voji.report;
import java.lang.*;
import java.util.*;
import java.io.*;
import java.awt.*;
import voji.log.Log;
import voji.utils.*;

/**
 * This class holds a complete report and is able to print it
 * out in various ways.
 */
public class Report
{
    /**
     * Constructs a new empty <code>Report</code> instance
     *
     * @param orientation the page orientation (portrait or landscape) of the whole report
     */
    public Report(PageAttributes.OrientationRequestedType orientation)
    {
        this.orientation=orientation;
    }

    /**
     * Page orientation
     */
    private PageAttributes.OrientationRequestedType orientation;

    /**
     * Get page orientation
     */
    public PageAttributes.OrientationRequestedType getOrientation()
    {
        return orientation;
    }

    /**
     * Contains all sub components
     */
    private Vector<ReportComponent> components=new Vector<ReportComponent>();

    /**
     * Adds a new component
     *
     * @param component the component to add
     */
    public void add(ReportComponent component)
    {
        components.add(component);
    }

    /**
     * Contains all header components
     */
    private Vector<ReportComponent> headerComponents=new Vector<ReportComponent>();

    /**
     * Adds a new header component.
     * These components will be shown at the top of each page
     *
     * @param component the component to add
     */
    public void addHeader(ReportComponent component)
    {
        headerComponents.add(component);
    }

    /**
     * Contains all bottom components
     */
    private Vector<ReportComponent> bottomComponents=new Vector<ReportComponent>();

    /**
     * Adds a new bottom component.
     * These components will be shown at the bottom of each page
     *
     * @param component the component to add
     */
    public void addBottom(ReportComponent component)
    {
        bottomComponents.add(component);
    }

    /**
     * Prints this report to a printer
     *
     * @param printJob the <code>CopyPrintJob</code> where to draw to
     */
    public void print(CopyPrintJob printJob)
    {
        for (ListIterator comps=components.listIterator();comps.hasNext();)
            {
                draw(comps,printJob.getPageGraphics());
            }
    }

    /**
     * Prints this report to a printer
     *
     * @param printJob the <code>PrintJob</code> where to draw to
     */
    public void print(PrintJob printJob)
    {
        PageGraphics pageGraphics=new PageGraphics(printJob);

        for (ListIterator comps=components.listIterator();comps.hasNext();)
            {
                pageGraphics.setGraphics(printJob.getGraphics());
                draw(comps,pageGraphics);
            }
    }

    /**
     * Run the ps2pdf command line tool
     */
    private static void ps2pdf(File inputPsFile, File outputPdfFile)
        throws IOException, InterruptedException
    {
        ProcessBuilder pb = new ProcessBuilder("ps2pdf", inputPsFile.getAbsolutePath(), outputPdfFile.getAbsolutePath());
        pb.redirectOutput(ProcessBuilder.Redirect.INHERIT);
        pb.redirectError(ProcessBuilder.Redirect.INHERIT);
        Process p = pb.start();
        int exitCode = p.waitFor();
        if (exitCode != 0) {
            throw new RuntimeException("ps2pdf returned with exit code " + exitCode);
        }
    }

    /**
     * Run the pdftk command line tool to concatenate multiple PDF files
     */
    private static void pdfcat(Vector<File> inputPdfFiles, File outputPdfFile)
        throws IOException, InterruptedException
    {
        Vector<String> command = new Vector<String>();
        command.add("pdftk");
        for (File inputPdfFile : inputPdfFiles) {
            command.add(inputPdfFile.getAbsolutePath());
        }
        command.add("cat");
        command.add("output");
        command.add(outputPdfFile.getAbsolutePath());
        ProcessBuilder pb = new ProcessBuilder(command);
        pb.redirectOutput(ProcessBuilder.Redirect.INHERIT);
        pb.redirectError(ProcessBuilder.Redirect.INHERIT);
        Process p = pb.start();
        int exitCode = p.waitFor();
        if (exitCode != 0) {
            throw new RuntimeException("pdftk returned with exit code " + exitCode);
        }
    }

    /**
     * Prints this report to a Postscript file
     */
    public void printPs(Frame frame, File outputPsFile)
        throws IOException
    {
        String title = "";
        JobAttributes jobAttr = new JobAttributes();
        jobAttr.setDialog(JobAttributes.DialogType.NONE);
        jobAttr.setDestination(JobAttributes.DestinationType.FILE);
        jobAttr.setFileName(outputPsFile.getAbsolutePath());
        PageAttributes pageAttr = new PageAttributes();
        pageAttr.setOrientationRequested(orientation);
        PrintJob printJob = frame.getToolkit().getPrintJob(frame, title, jobAttr, pageAttr);
        print(printJob);
        printJob.end();
    }

    public void printPdf(Frame frame, File outputPdfFile)
        throws IOException, InterruptedException
    {
        File psFile = File.createTempFile("temp-print-", ".ps");
        psFile.deleteOnExit();
        printPs(frame, psFile);
        ps2pdf(psFile, outputPdfFile);
    }

    public static void printPdf(Frame frame, File outputPdfFile, Iterable<Report> reports)
        throws IOException, InterruptedException
    {
        Vector<File> pdfFiles = new Vector<File>();
        for (Report report : reports) {
            File pdfFile = File.createTempFile("temp-print-", ".pdf");
            pdfFile.deleteOnExit();
            report.printPdf(frame, pdfFile);
            pdfFiles.add(pdfFile);
        }
        pdfcat(pdfFiles, outputPdfFile);
    }

    /**
     * Run PDF viewer in background
     */
    public static void runPdfViewer(File pdfFile)
        throws IOException
    {
        ProcessBuilder pb = new ProcessBuilder("xdg-open", pdfFile.getAbsolutePath());
        pb.redirectInput(ProcessBuilder.Redirect.PIPE);
        pb.redirectOutput(ProcessBuilder.Redirect.INHERIT);
        pb.redirectError(ProcessBuilder.Redirect.INHERIT);
        Process p = pb.start();
        p.getInputStream().close();
    }

    /**
     * Concat multiple reports, then print into temporary PDF file and open that file in the PDF viewer
     */
    public static void printAndPreviewPdf(Frame frame, Iterable<Report> reports, String name)
    {
        try {
            final File pdfFile = File.createTempFile(name + " - ", ".pdf");
            pdfFile.deleteOnExit();
            printPdf(frame, pdfFile, reports);
            runPdfViewer(pdfFile);
        }
        catch (IOException e) { Log.log(e); }
        catch (InterruptedException e) { Log.log(e); }
    }

    /**
     * Print into temporary PDF file and open that file in the PDF viewer
     */
    public static void printAndPreviewPdf(Frame frame, Report report, String name)
    {
        Vector<Report> reports = new Vector<Report>();
        reports.add(report);
        printAndPreviewPdf(frame, reports, name);
    }

    /**
     * Draws the given components onto the given graphics context.
     * It also prints the header and bottom components.
     * After drawing the components <code>Iterator</code>
     * will point to the first component of the next page.
     *
     * @param components an <code>Iterator</code> containing the
     *                   report components to draw
     * @param graphics the graphics context where to draw to
     */
    protected void draw(ListIterator components,PageGraphics graphics)
    {
        int pos=0;

        // calculate bottom space
        int max=graphics.getPrintJob().getPageDimension().height;
        for (Iterator i=bottomComponents.iterator();i.hasNext();)
            max-=((ReportComponent)i.next()).getHeight(graphics);

        // header
        for (Iterator i=headerComponents.iterator();i.hasNext();)
            {
                ReportComponent comp=(ReportComponent)i.next();
                int height=comp.getHeight(graphics);

                comp.draw(graphics);
                graphics.translate(0,height);
                pos+=height;
            }

        // main part
        for (boolean retry=false;components.hasNext();retry=true)
            {
                ReportComponent comp=(ReportComponent)components.next();

                int height=comp.getHeight(graphics);
                if (retry && pos+height>max)
                    {
                        components.previous();
                        break;
                    }

                comp.draw(graphics);
                graphics.translate(0,height);
                pos+=height;
            }

        // bottom
        graphics.translate(0,max-pos);
        for (Iterator i=bottomComponents.iterator();i.hasNext();)
            {
                ReportComponent comp=(ReportComponent)i.next();

                comp.draw(graphics);
                graphics.translate(0,comp.getHeight(graphics));
            }
    }
}
