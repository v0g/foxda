/*****************************************************************************\
*                                                                             *
*                                                                             *
*                         V       V         JJ  IIIII                         *
*                          V     V           J    I                           *
*                           V   V   OO       J    I                           *
*                            V V   O  O  J   J    I                           *
*                             V     OO    JJJ   IIIII                         *
*                                                                             *
*                         ---------------------------                         *
*                        |Volker's  Java Improvements|                        *
*                         ---------------------------                         *
*                                                                             *
*                                                                             *
*******************************************************************************
*                   *                                     *                   *
*                   *   FILE:          ReportImage.java   *                   *
*                   *                                     *                   *
*                    *************************************                    *
*                                                                             *
*                                                                             *
*  This library is free software; you can redistribute it and/or              *
*  modify it under the terms of the GNU Lesser General Public                 *
*  License as published by the Free Software Foundation; either               *
*  version 2.1 of the License, or (at your option) any later version.         *
*                                                                             *
*  This library is distributed in the hope that it will be useful,            *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU          *
*  Lesser General Public License for more details.                            *
*                                                                             *
*  You should have received a copy of the GNU Lesser General Public           *
*  License along with this library; if not, write to the Free Software        *
*  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA  *
*                                                                             *
*                                                                             *
\*****************************************************************************/
package voji.report;
import java.lang.*;
import java.awt.*;
import javax.swing.*;
import voji.utils.*;

/**
 * This report component draws an image
 */
public class ReportImage extends ReportComponent
{
    /**
     * Constructs a new <code>ReportImage</code> instance
     * representing the given image
     *
     * @param image the image to draw
     * @param x the X-coordinate of the upper left of the image
     * @param y the Y-coordinate of the upper left of the image
     * @param width the width where to scale the image to
     * @param height the height where to scale the image to
     */
    public ReportImage(Image image,double x,double y,
                       double width,double height)
    {
        this.image=image;
        this.x=x;
        this.y=y;
        this.width=width;
        this.height=height;
    }

    /**
     * Constructs a new <code>ReportImage</code> instance
     * representing the given icon
     *
     * @param icon the ImageIcon to draw
     * @param x the X-coordinate of the upper left of the image
     * @param y the Y-coordinate of the upper left of the image
     * @param width the width where to scale the image to
     * @param height the height where to scale the image to
     */
    public ReportImage(ImageIcon icon,double x,double y,
                       double width,double height)
    {
        this(icon.getImage(),x,y,width,height);
    }

    /**
     * The current image
     */
    public Image image;

    /**
     * The X-coordinate of the upper left of the image
     */
    public double x;

    /**
     * The Y-coordinate of the upper left of the image
     */
    public double y;

    /**
     * The width where to scale the image to
     */
    public double width;

    /**
     * The height where to scale the image to
     */
    public double height;

    /**
     * Returns the local height of this component
     *
     * @param g a graphical context to determine font sizes etc.
     */
    protected int getOwnHeight(PageGraphics g)
    {
        return g.toPixels(y+height);
    }

    /**
     * Draws this component
     *
     * @param g the graphical context where to draw to
     */
    public void draw(PageGraphics g)
    {
        super.draw(g);
        while (!g.drawImage(image,g.toPixels(x),g.toPixels(y),
                            g.toPixels(width),g.toPixels(height),null))
            {
                try { Thread.sleep(10); }
                catch (InterruptedException e) {};
            }
    }
}
