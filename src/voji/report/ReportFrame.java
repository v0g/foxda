/*****************************************************************************\
*                                                                             *
*                                                                             *
*                         V       V         JJ  IIIII                         *
*                          V     V           J    I                           *
*                           V   V   OO       J    I                           *
*                            V V   O  O  J   J    I                           *
*                             V     OO    JJJ   IIIII                         *
*                                                                             *
*                         ---------------------------                         *
*                        |Volker's  Java Improvements|                        *
*                         ---------------------------                         *
*                                                                             *
*                                                                             *
*******************************************************************************
*                   *                                     *                   *
*                   *   FILE:          ReportFrame.java   *                   *
*                   *                                     *                   *
*                    *************************************                    *
*                                                                             *
*                                                                             *
*  This library is free software; you can redistribute it and/or              *
*  modify it under the terms of the GNU Lesser General Public                 *
*  License as published by the Free Software Foundation; either               *
*  version 2.1 of the License, or (at your option) any later version.         *
*                                                                             *
*  This library is distributed in the hope that it will be useful,            *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU          *
*  Lesser General Public License for more details.                            *
*                                                                             *
*  You should have received a copy of the GNU Lesser General Public           *
*  License along with this library; if not, write to the Free Software        *
*  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA  *
*                                                                             *
*                                                                             *
\*****************************************************************************/
package voji.report;
import java.lang.*;
import java.awt.*;
import voji.utils.*;

/**
 * This report component draws a frame around its sub components
 */
public class ReportFrame extends ReportComponent
{
    /**
     * Constructs a new <code>ReportFrame</code> instance
     * of the specified type
     *
     * @param left the left extension of the text
     * @param right the right extension of the text
     * @param type the {@link #type type} of the frame
     */
    public ReportFrame(double left,double right,int type)
    {
        this.left=left;
        this.right=right;
        this.type=type;
    }

    /**
     * Constructs a new <code>ReportFrame</code> instance
     * of the type {@link #SINGLE SINGLE}
     *
     * @param left the left extension of the text
     * @param right the right extension of the text
     */
    public ReportFrame(double left,double right)
    {
        this(left,right,SINGLE);
    }

    /**
     * The left extension of the frame
     */
    public double left;

    /**
     * The right extension of the frame
     */
    public double right;

    /**
     * The type of the frame.
     * It's either
     * {@link #NONE   NONE},
     * {@link #SINGLE SINGLE},
     * {@link #DOUBLE DOUBLE},
     * {@link #BOLD   BOLD} or
     * {@link #OVAL   OVAL}.
     */
    public int type;

    /**
     * Invisible Frame
     */
    public static final int NONE=0;

    /**
     * Frame with a single thin border line
     */
    public static final int SINGLE=1;

    /**
     * Frame with two thin border lines
     */
    public static final int DOUBLE=2;

    /**
     * Frame with a bold border line
     */
    public static final int BOLD=3;

    /**
     * Frame with an oval border line
     */
    public static final int OVAL=4;

    /**
     * The space between the two lines of a {@link #DOUBLE DOUBLE}
     */
    protected static final int DOUBLE_SPACE=1;

    /**
     * The width of a {@link #BOLD BOLD} line
     */
    protected static final int BOLD_WIDTH=2;

    /**
     * Draws this component
     *
     * @param g the graphical context where to draw to
     */
    public void draw(PageGraphics g)
    {
        super.draw(g);

        int x1=g.toPixels(left);
        int x2=g.toPixels(right);
        int h=getHeight(g);
        switch (type)
            {
            case DOUBLE:
                g.drawRect(x1+DOUBLE_SPACE+1,DOUBLE_SPACE+1,
                           x2-x1-2*DOUBLE_SPACE-2,h-2*DOUBLE_SPACE-2);

            case SINGLE:
                g.drawRect(x1,0,x2-x1,h);
                break;

            case BOLD:
                g.fillRect(x1-BOLD_WIDTH/2,-BOLD_WIDTH/2,
                           x2-x1+BOLD_WIDTH,BOLD_WIDTH);
                g.fillRect(x1-BOLD_WIDTH/2,-BOLD_WIDTH/2,
                           BOLD_WIDTH,h+BOLD_WIDTH);
                g.fillRect(x1-BOLD_WIDTH/2,h-BOLD_WIDTH/2,
                           x2-x1+BOLD_WIDTH,BOLD_WIDTH);
                g.fillRect(x2-BOLD_WIDTH/2,-BOLD_WIDTH/2,
                           BOLD_WIDTH,h+BOLD_WIDTH);
                break;

            case OVAL:
                g.drawOval(x1,0,x2-x1,h);
                break;
            }
    }
}
