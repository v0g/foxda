/*****************************************************************************\
*                                                                             *
*                                                                             *
*                         V       V         JJ  IIIII                         *
*                          V     V           J    I                           *
*                           V   V   OO       J    I                           *
*                            V V   O  O  J   J    I                           *
*                             V     OO    JJJ   IIIII                         *
*                                                                             *
*                         ---------------------------                         *
*                        |Volker's  Java Improvements|                        *
*                         ---------------------------                         *
*                                                                             *
*                                                                             *
*******************************************************************************
*                   *                                     *                   *
*                   *   FILE:      ReportComponent.java   *                   *
*                   *                                     *                   *
*                    *************************************                    *
*                                                                             *
*                                                                             *
*  This library is free software; you can redistribute it and/or              *
*  modify it under the terms of the GNU Lesser General Public                 *
*  License as published by the Free Software Foundation; either               *
*  version 2.1 of the License, or (at your option) any later version.         *
*                                                                             *
*  This library is distributed in the hope that it will be useful,            *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU          *
*  Lesser General Public License for more details.                            *
*                                                                             *
*  You should have received a copy of the GNU Lesser General Public           *
*  License along with this library; if not, write to the Free Software        *
*  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA  *
*                                                                             *
*                                                                             *
\*****************************************************************************/
package voji.report;
import java.lang.*;
import java.util.*;
import javax.swing.*;
import voji.utils.*;

/**
 * This is the common base class of all components
 * which can be part of a report.
 */
public class ReportComponent implements SwingConstants
{
    /**
     * Constructs a new empty <code>ReportComponent</code> instance
     */
    public ReportComponent()
    {
    }

    /**
     * The vertical position of the component.
     * It's either
     * <code>TOP</code>,
     * <code>BOTTOM</code> or
     * <code>CENTER</code>.
     * Default is <code>TOP</code>.
     */
    public int position=TOP;

    /**
     * Contains all sub components
     */
    private Vector<ReportComponent> components=new Vector<ReportComponent>();

    /**
     * Adds a new component
     *
     * @param component the component to add
     */
    public void add(ReportComponent component)
    {
        components.add(component);
    }

    /**
     * Adds a new component and sets its vertical position
     *
     * @param component the component to add
     * @param position the {@link #position vertical position}
     *                 of <code>component</code>
     */
    public void add(ReportComponent component,int position)
    {
        component.position=position;
        add(component);
    }

    /**
     * Removes a component
     *
     * @param component the component to remove from this container
     */
    public void remove(ReportComponent component)
    {
        components.remove(component);
    }

    /**
     * Returns an iterator of all sub components
     */
    public Iterator<ReportComponent> iterator()
    {
        return components.iterator();
    }

    /**
     * Returns the total height of this component.
     * That means the maximum of this component's own height
     * and its sub components' total heights.
     *
     * @param g the graphical context where the height has to refer to
     *
     * @see #getOwnHeight(PageGraphics)
     * @see #draw(PageGraphics)
     */
    public int getHeight(PageGraphics g)
    {
        int h=getOwnHeight(g);
        for (Iterator i=iterator();i.hasNext();)
            h=Math.max(h,((ReportComponent)i.next()).getHeight(g));
        return h;
    }

    /**
     * Returns the local height of this component.
     * Overwrite this if your component really draws something.
     *
     * @param g a graphical context for you to determine font sizes etc.
     *
     * @see #getHeight(PageGraphics)
     */
    protected int getOwnHeight(PageGraphics g)
    {
        return 0;
    }

    /**
     * Draws this component
     *
     * @param g the graphical context where to draw to
     * @param height the height of the surrounding component
     */
    public void draw(PageGraphics g,int height)
    {
        int space=height-getHeight(g);
        int p=0;

        switch (position)
            {
            case TOP:    p=0;       break;
            case BOTTOM: p=space;   break;
            case CENTER: p=space/2; break;
            }

        g.translate(0,p);
        draw(g);
        g.translate(0,-p);
    }

    /**
     * Draws this component.
     *
     * Per default, this method just draws all sub components.
     *
     * @param g the graphical context where to draw to
     *
     * @see #getHeight(PageGraphics)
     */
    protected void draw(PageGraphics g)
    {
        int height=getHeight(g);

        for (Iterator i=iterator();i.hasNext();)
            ((ReportComponent)i.next()).draw(g,height);
    }
}
