/*****************************************************************************\
*                                                                             *
*                                                                             *
*                         V       V         JJ  IIIII                         *
*                          V     V           J    I                           *
*                           V   V   OO       J    I                           *
*                            V V   O  O  J   J    I                           *
*                             V     OO    JJJ   IIIII                         *
*                                                                             *
*                         ---------------------------                         *
*                        |Volker's  Java Improvements|                        *
*                         ---------------------------                         *
*                                                                             *
*                                                                             *
*******************************************************************************
*                   *                                     *                   *
*                   *   FILE:           ReportText.java   *                   *
*                   *                                     *                   *
*                    *************************************                    *
*                                                                             *
*                                                                             *
*  This library is free software; you can redistribute it and/or              *
*  modify it under the terms of the GNU Lesser General Public                 *
*  License as published by the Free Software Foundation; either               *
*  version 2.1 of the License, or (at your option) any later version.         *
*                                                                             *
*  This library is distributed in the hope that it will be useful,            *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU          *
*  Lesser General Public License for more details.                            *
*                                                                             *
*  You should have received a copy of the GNU Lesser General Public           *
*  License along with this library; if not, write to the Free Software        *
*  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA  *
*                                                                             *
*                                                                             *
\*****************************************************************************/
package voji.report;
import java.lang.*;
import java.util.*;
import java.awt.*;
import java.awt.geom.*;
import javax.swing.*;
import voji.utils.*;

/**
 * This report component draws a text
 */
public class ReportText extends ReportComponent
{
    /**
     * Constructs a new <code>ReportText</code> instance
     * representing the given string in the given font
     *
     * @param text the text to draw
     * @param left the left extension of the text
     * @param right the right extension of the text
     * @param font the font of the text
     * @param align the {@link #align horizontal alignment} of the text
     * @param type the {@link #type break type} of the text
     * @param hasInsets whether the text should have insets
     */
    public ReportText(String text,double left,double right,Font font,
                      int align,int type,boolean hasInsets)
    {
        this.text=text;
        this.left=left;
        this.right=right;
        this.font=font;
        this.align=align;
        this.type=type;
        this.hasInsets=hasInsets;
    }

    /**
     * Constructs a new <code>ReportText</code> instance
     * representing the given string in the given font.
     * It uses a {@link #WORD WORD} break type and
     * no insets
     * for text.
     *
     * @param text the text to draw
     * @param left the left extension of the text
     * @param right the right extension of the text
     * @param font the font of the text
     * @param align the {@link #align horizontal alignment} of the text
     */
    public ReportText(String text,double left,double right,Font font,int align)
    {
        this(text,left,right,font,align,WORD,false);
    }

    /**
     * Constructs a new <code>ReportText</code> instance
     * representing the given string in the given font.
     * It uses a {@link #WORD WORD} break type,
     * no insets and
     * a {@link #LEFT LEFT} alignment
     * for text.
     *
     * @param text the text to draw
     * @param left the left extension of the text
     * @param right the right extension of the text
     * @param font the font of the text
     */
    public ReportText(String text,double left,double right,Font font)
    {
        this(text,left,right,font,LEFT);
    }

    /**
     * The displayed text
     */
    public String text;

    /**
     * The left extension of the text
     */
    public double left;

    /**
     * The right extension of the text
     */
    public double right;

    /**
     * The font of the text
     */
    public Font font;

    /**
     * Says whether the text should have insets
     */
    public boolean hasInsets;

    /**
     * The horizontal alignment of the text.
     * It's either
     * <code>LEFT</code>,
     * <code>RIGHT</code> or
     * <code>CENTER</code>.
     */
    public int align;

    /**
     * The break type of the text.
     * It's either
     * {@link #NEWLINE NEWLINE},
     * {@link #WORD    WORD} or
     * {@link #CHAR    CHAR}.
     */
    public int type;

    /**
     * Break only at '\n'
     */
    public static final int NEWLINE=0;

    /**
     * Break at words and newlines
     */
    public static final int WORD=1;

    /**
     * Break at any character
     */
    public static final int CHAR=2;

    /**
     * Fix the font height in case it is 0.
     * This is a workaround for a really stange bug.
     */
    protected static int fixFontHeight(Font font, int fontHeight)
    {
        if (fontHeight != 0) {
            return fontHeight;
        }
        // TODO: different values for bold/non-bold fonts?
        switch (font.getSize()) {
        case  8: return  9;
        case 10: return 12;
        case 14: return 16;
        case 24: return 30;
        default: return  0;
        }
    }

    /**
     * Fix the font ascent in case it is 0.
     * This is a workaround for a really stange bug.
     */
    protected static int fixFontAscent(Font font, int fontAscent)
    {
        if (fontAscent != 0) {
            return fontAscent;
        }
        // TODO: different values for bold/non-bold fonts?
        switch (font.getSize()) {
        case  8: return  8;
        case 10: return 10;
        case 14: return 13;
        case 24: return 24;
        default: return  0;
        }
    }

    /**
     * Returns the local height of this component
     *
     * @param g a graphical context to determine font sizes etc.
     */
    protected int getOwnHeight(PageGraphics g)
    {
        updateText(g);
        Insets insets=getInsets(g);
        return
            insets.top+
            getLines(g).size()*fixFontHeight(font, g.getFontMetrics(font).getHeight())+
            insets.bottom;
    }

    /**
     * Draws this component
     *
     * @param g the graphical context where to draw to
     */
    public void draw(PageGraphics g)
    {
        super.draw(g);

        updateText(g);
        Insets insets=getInsets(g);
        FontMetrics metrics=g.getFontMetrics(font);
        int x1=g.toPixels(left)+insets.left;
        int x2=g.toPixels(right)-insets.right;
        int y=insets.top+fixFontAscent(font, metrics.getAscent());
        g.setFont(font);
        for (String line : getLines(g))
            {
                drawLine(g,line,x1,x2,y);
                y+=fixFontHeight(font, metrics.getHeight());
            }
    }

    /**
     * Draws a single line
     *
     * @param g the graphical context where to draw to
     * @param text the text to draw
     * @param x1 the left border
     * @param x2 the right border
     * @param y the baseline
     */
    protected void drawLine(Graphics g,String text,int x1,int x2,int y)
    {
        int width=(int)g.getFontMetrics().getStringBounds(text,g).getWidth();

        switch (align)
            {
            case LEFT:
                g.drawString(text,x1,y);
                break;

            case RIGHT:
                g.drawString(text,x2-width,y);
                break;

            case CENTER:
                g.drawString(text,(x1+x2-width)/2,y);
                break;
            }
    }

    /**
     * Returns the insets of this text component
     *
     * @param g a graphical context to determine font sizes etc.
     */
    protected Insets getInsets(PageGraphics g)
    {
        if (!hasInsets) return new Insets(0,0,0,0);

        Rectangle2D r=g.getFontMetrics(font).getMaxCharBounds(g);
        return new Insets((int)r.getHeight()/4,(int)r.getWidth()/4,
                          (int)r.getHeight()/4,(int)r.getWidth()/4);
    }

    /**
     * Splits the current text into lines
     *
     * @param g a graphical context to determine font sizes etc.
     * @return a <code>Vector</code> of strings containing the lines
     */
    protected Vector<String> getLines(PageGraphics g)
    {
        Insets insets=getInsets(g);
        int width=g.toPixels(right-left)-(insets.left+insets.right);
        FontMetrics metrics=g.getFontMetrics(font);
        String s=text;
        String line="";
        String space="";
        String append="";
        Vector<String> v=new Vector<String>();

        // avoid problems with the last line
        if (s.endsWith("\n")) s=s.substring(0,s.length()-1);
        if (s.length()>0) s+=" \n";

        // split
        for (int i=0;i<s.length();i++)
            {
                char c=s.charAt(i);
                boolean is_space=true;
                switch (c)
                    {
                    case '\r':
                        break;

                    case '\n':
                        if (append.length()>0)
                            {
                                if (type>=WORD &&
                                    line.length()>0 &&
                                    metrics.getStringBounds
                                    (line+space+append,g).getWidth()>width)
                                    {
                                        v.add(line);
                                        v.add(append);
                                    }
                                else
                                    {
                                        v.add(line+space+append);
                                    }
                            }
                        else
                            {
                                v.add(line);
                            }
                        line=space=append="";
                        break;

                    case '.':
                    case ',':
                    case '!':
                    case '?':
                    case ':':
                    case ';':
                    case '-':
                    case '_':
                    case '|':
                    case '/':
                    case '\\':
                    case '=':
                    case '+':
                    case '*':
                        is_space=false;
                        append+=c;
                    case ' ':
                    case '\t':
                        if (append.length()>0)
                            {
                                if (type>=WORD &&
                                    line.length()>0 &&
                                    metrics.getStringBounds
                                    (line+space+append,g).getWidth()>width)
                                    {
                                        v.add(line);
                                        line=append;
                                    }
                                else
                                    {
                                        line+=space+append;
                                    }
                                space="";
                                append="";
                            }
                        if (is_space)
                            space+=c;
                        break;

                    default:
                        if (type>=CHAR &&
                            metrics.getStringBounds(line+space+append+c,g).
                            getWidth()>width)
                            {
                                v.add(append.length()>0 ?
                                      line+space+append : line);
                                line=space=append="";
                            }
                        append+=c;
                    }
            }

        return v;
    }

    /**
     * Updates the text.
     * This method is useful for changing text report components.
     * It is called immediately
     * before determining the text height and
     * before drawing it.
     *
     * @param g a graphical context to determine font sizes etc.
     */
    protected void updateText(PageGraphics g) {}
}
