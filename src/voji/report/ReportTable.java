/*****************************************************************************\
*                                                                             *
*                                                                             *
*                         V       V         JJ  IIIII                         *
*                          V     V           J    I                           *
*                           V   V   OO       J    I                           *
*                            V V   O  O  J   J    I                           *
*                             V     OO    JJJ   IIIII                         *
*                                                                             *
*                         ---------------------------                         *
*                        |Volker's  Java Improvements|                        *
*                         ---------------------------                         *
*                                                                             *
*                                                                             *
*******************************************************************************
*                   *                                     *                   *
*                   *   FILE:          ReportTable.java   *                   *
*                   *                                     *                   *
*                    *************************************                    *
*                                                                             *
*                                                                             *
*  This library is free software; you can redistribute it and/or              *
*  modify it under the terms of the GNU Lesser General Public                 *
*  License as published by the Free Software Foundation; either               *
*  version 2.1 of the License, or (at your option) any later version.         *
*                                                                             *
*  This library is distributed in the hope that it will be useful,            *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU          *
*  Lesser General Public License for more details.                            *
*                                                                             *
*  You should have received a copy of the GNU Lesser General Public           *
*  License along with this library; if not, write to the Free Software        *
*  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA  *
*                                                                             *
*                                                                             *
\*****************************************************************************/
package voji.report;
import java.lang.*;
import java.awt.*;
import voji.utils.*;

/**
 * This report component draws a table row
 */
public class ReportTable extends ReportComponent
{
    /**
     * Constructs a new <code>ReportTable</code> instance
     * using the given strings and coordinates
     *
     * @param columns the strings to draw in the columns
     * @param left the left extension of the table
     * @param widths an array containing the width of each column
     * @param font the font to use
     * @param aligns the {@link ReportText#align alignment} of each column
     * @param textType the {@link ReportText#type break type} of the text
     * @param frameType the {@link ReportFrame#type type} of the frame
     * @param positions the {@link ReportComponent#position positions}
     *                  of each column
     */
    public ReportTable(String[] columns,double left,
                       double[] widths,Font font,
                       int[] aligns,int textType,
                       int frameType,int[] positions)
    {
        init(columns,left,widths,font,aligns,textType,frameType,positions);
    }

    /**
     * Constructs a new <code>ReportTable</code> instance
     * using the given strings and coordinates
     * It uses the same align and position for each column.
     *
     * @param columns the strings to draw in the columns
     * @param left the left extension of the table
     * @param widths an array containing the width of each column
     * @param font the font to use
     * @param aligns the {@link ReportText#align alignment} of each column
     * @param textType the {@link ReportText#type break type} of the text
     * @param frameType the {@link ReportFrame#type type} of the frame
     * @param position the {@link ReportComponent#position position}
     *                 of each column
     */
    public ReportTable(String[] columns,double left,
                       double[] widths,Font font,
                       int[] aligns,int textType,
                       int frameType,int position)
    {
        int[] positions=new int[columns.length];
        for (int i=0;i<positions.length;i++) positions[i]=position;

        init(columns,left,widths,font,aligns,textType,frameType,positions);
    }

    /**
     * Constructs a new <code>ReportTable</code> instance
     * using the given strings and coordinates
     * It uses the same align and position for each column.
     *
     * @param columns the strings to draw in the columns
     * @param left the left extension of the table
     * @param widths an array containing the width of each column
     * @param font the font to use
     * @param align the {@link ReportText#align alignment} of each column
     * @param textType the {@link ReportText#type break type} of the text
     * @param frameType the {@link ReportFrame#type type} of the frame
     * @param position the {@link ReportComponent#position position}
     *                 of each column
     */
    public ReportTable(String[] columns,double left,
                       double[] widths,Font font,
                       int align,int textType,
                       int frameType,int position)
    {
        int[] aligns=new int[columns.length];
        for (int i=0;i<aligns.length;i++) aligns[i]=align;

        int[] positions=new int[columns.length];
        for (int i=0;i<positions.length;i++) positions[i]=position;

        init(columns,left,widths,font,aligns,textType,frameType,positions);
    }

    /**
     * Constructs a new <code>ReportTable</code> instance
     * using the given strings and coordinates.
     * It uses a {@link ReportText#LEFT LEFT} alignment,
     * {@link ReportText#WORD WORD} break,
     * a {@link ReportFrame#SINGLE SINGLE} frame type and
     * a {@link ReportComponent#CENTER CENTER} position
     * for each column.
     *
     * @param columns the strings to draw in the columns
     * @param left the left extension of the table
     * @param widths an array containing the width of each column
     * @param font the font to use
     */
    public ReportTable(String[] columns,double left,
                       double[] widths,Font font)
    {
        this(columns,left,
             widths,font,
             ReportText.LEFT,ReportText.WORD,
             ReportFrame.SINGLE,ReportComponent.CENTER);
    }

    /**
     * Initializes this <code>ReportTable</code> instance
     *
     * @param columns the strings to draw in the columns
     * @param left the left extension of the table
     * @param widths an array containing the width of each column
     * @param positions an array containing the positions of each column
     * @param font the font to use
     * @param aligns the {@link ReportText#align alignment} of each column
     * @param textType the {@link ReportText#type break type} of the text
     * @param frameType the {@link ReportFrame#type type} of the frame
     * @param positions the {@link ReportComponent#position positions}
     *                  of each column
     */
    protected void init(String[] columns,double left,
                        double[] widths,Font font,
                        int[] aligns,int textType,
                        int frameType,int[] positions)
    {
        ReportComponent f=getContent();
        for (int i=0;i<columns.length && i<widths.length && i<aligns.length;
             i++)
            {
                double right=left+widths[i];

                ReportFrame n=new ReportFrame(left,right,frameType);
                n.add(f);
                f=n;

                getContent().add(new ReportText(columns[i],left,right,
                                                font,aligns[i],textType,true),
                                 positions[i]);

                left=right;
            }
        add(f);
    }

    /**
     * The component containing the content (i.e. the text components)
     * of this table
     */
    protected ReportComponent content=new ReportComponent();

    /**
     * Returns the component that contains the {@link #content content}
     * of this table
     */
    public ReportComponent getContent()
    {
        return content;
    }
}
