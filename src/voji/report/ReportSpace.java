/*****************************************************************************\
*                                                                             *
*                                                                             *
*                         V       V         JJ  IIIII                         *
*                          V     V           J    I                           *
*                           V   V   OO       J    I                           *
*                            V V   O  O  J   J    I                           *
*                             V     OO    JJJ   IIIII                         *
*                                                                             *
*                         ---------------------------                         *
*                        |Volker's  Java Improvements|                        *
*                         ---------------------------                         *
*                                                                             *
*                                                                             *
*******************************************************************************
*                   *                                     *                   *
*                   *   FILE:          ReportSpace.java   *                   *
*                   *                                     *                   *
*                    *************************************                    *
*                                                                             *
*                                                                             *
*  This library is free software; you can redistribute it and/or              *
*  modify it under the terms of the GNU Lesser General Public                 *
*  License as published by the Free Software Foundation; either               *
*  version 2.1 of the License, or (at your option) any later version.         *
*                                                                             *
*  This library is distributed in the hope that it will be useful,            *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU          *
*  Lesser General Public License for more details.                            *
*                                                                             *
*  You should have received a copy of the GNU Lesser General Public           *
*  License along with this library; if not, write to the Free Software        *
*  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA  *
*                                                                             *
*                                                                             *
\*****************************************************************************/
package voji.report;
import java.lang.*;
import voji.utils.*;

/**
 * This report component just forces some space.
 * Its sub components are drawn below this space.
 */
public class ReportSpace extends ReportComponent
{
    /**
     * Constructs a new <code>ReportSpace</code> instance
     *
     * @param height the height of this component,
     *               that means the size of the space
     */
    public ReportSpace(double height)
    {
        this.height=height;
    }

    /**
     * Constructs a new <code>ReportSpace</code> instance
     * containing the given sub component
     *
     * @param height the height of this component,
     *               that means the size of the space
     * @param component a sub component to add
     */
    public ReportSpace(double height,ReportComponent component)
    {
        this(height);
        add(component);
    }

    /**
     * The height of this component, that means the size of the space
     */
    public double height;

    /**
     * Returns the total height of this component
     *
     * @param g the graphical context where the height has to refer to
     */
    public int getHeight(PageGraphics g)
    {
        return super.getHeight(g)+g.toPixels(height);
    }

    /**
     * Draws this component
     *
     * @param g the graphical context where to draw to
     */
    public void draw(PageGraphics g)
    {
        int h=g.toPixels(height);
        g.translate(0,+h);
        super.draw(g);
        g.translate(0,-h);
    }
}
