/*****************************************************************************\
*                                                                             *
*                                                                             *
*                         V       V         JJ  IIIII                         *
*                          V     V           J    I                           *
*                           V   V   OO       J    I                           *
*                            V V   O  O  J   J    I                           *
*                             V     OO    JJJ   IIIII                         *
*                                                                             *
*                         ---------------------------                         *
*                        |Volker's  Java Improvements|                        *
*                         ---------------------------                         *
*                                                                             *
*                                                                             *
*******************************************************************************
*                   *                                     *                   *
*                   *   FILE:   VPreparedStatement.java   *                   *
*                   *                                     *                   *
*                    *************************************                    *
*                                                                             *
*                                                                             *
*  This library is free software; you can redistribute it and/or              *
*  modify it under the terms of the GNU Lesser General Public                 *
*  License as published by the Free Software Foundation; either               *
*  version 2.1 of the License, or (at your option) any later version.         *
*                                                                             *
*  This library is distributed in the hope that it will be useful,            *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU          *
*  Lesser General Public License for more details.                            *
*                                                                             *
*  You should have received a copy of the GNU Lesser General Public           *
*  License along with this library; if not, write to the Free Software        *
*  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA  *
*                                                                             *
*                                                                             *
\*****************************************************************************/
package voji.db;
import java.lang.*;
import java.sql.*;

/*
 * CLASS VPreparedStatement
 */
public class VPreparedStatement implements DatabaseListener
{
    /*
     * SQL statement
     */
    private final String sql;

    /*
     * prepared statement
     */
    private PreparedStatement ps;

    /*
     * constructor
     */
    public VPreparedStatement(String initSQL)
    {
        /* initialize */
        sql=initSQL;

        /* register */
        Database.addDatabaseListener(this);
    }

    /*
     * called when the database has been changed
     */
    public void databaseChanged() throws SQLException
    {
        /* force new preparation */
        ps=null;
    }

    /*
     * get the SQL command
     */
    public String getSQL()
    {
        return sql;
    }

    /*
     * get the prepared statement
     */
    public PreparedStatement getPS() throws SQLException
    {
        /* if necessary, (re-)prepare the statement */
        return ps==null ? ps=Database.prepareStatement(sql) : ps;
    }

    /*
     * execute a query
     */
    public ResultSet executeQuery(Object[] params) throws SQLException
    {
        /* set params */
        for (int i=0;i<params.length;i++)
            if (params[i]==null)
                getPS().setString(i+1,null);
            else
                getPS().setObject(i+1,params[i]);

        /* execute query */
        return getPS().executeQuery();
    }

    /*
     * execute an update
     */
    public int executeUpdate(Object[] params) throws SQLException
    {
        /* set params */
        for (int i=0;i<params.length;i++)
            if (params[i]==null)
                getPS().setString(i+1,null);
            else
                getPS().setObject(i+1,params[i]);

        /* execute query */
        return getPS().executeUpdate();
    }
}
