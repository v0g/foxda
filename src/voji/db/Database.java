/*****************************************************************************\
*                                                                             *
*                                                                             *
*                         V       V         JJ  IIIII                         *
*                          V     V           J    I                           *
*                           V   V   OO       J    I                           *
*                            V V   O  O  J   J    I                           *
*                             V     OO    JJJ   IIIII                         *
*                                                                             *
*                         ---------------------------                         *
*                        |Volker's  Java Improvements|                        *
*                         ---------------------------                         *
*                                                                             *
*                                                                             *
*******************************************************************************
*                   *                                     *                   *
*                   *   FILE:             Database.java   *                   *
*                   *                                     *                   *
*                    *************************************                    *
*                                                                             *
*                                                                             *
*  This library is free software; you can redistribute it and/or              *
*  modify it under the terms of the GNU Lesser General Public                 *
*  License as published by the Free Software Foundation; either               *
*  version 2.1 of the License, or (at your option) any later version.         *
*                                                                             *
*  This library is distributed in the hope that it will be useful,            *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU          *
*  Lesser General Public License for more details.                            *
*                                                                             *
*  You should have received a copy of the GNU Lesser General Public           *
*  License along with this library; if not, write to the Free Software        *
*  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA  *
*                                                                             *
*                                                                             *
\*****************************************************************************/
package voji.db;
import java.lang.*;
import java.util.*;
import java.io.*;
import java.sql.*;
import voji.utils.*;

/*
 * CLASS Database
 */
public class Database
{
    /*
     * vector of all database listeners
     */
    private static Vector<DatabaseListener> listeners=new Vector<DatabaseListener>();

    /*
     * add a database listener
     */
    public static void addDatabaseListener(DatabaseListener listener)
    {
        /* add listener to vector of listeners */
        listeners.add(listener);

        /* initialize listener if necessary */
        if (getConnection()!=null)
            {
                try { listener.databaseChanged(); }
                catch (Exception ex) {}
            }
    }

    /*
     * let all listeners know that the database has been changed
     */
    protected static void fireDatabaseChanged()
    {
        /* call databaseChanged() of all listeners */
        for (DatabaseListener databaseListener : listeners)
            {
                try { databaseListener.databaseChanged(); }
                catch (Exception ex) {}
            }
    }

    /*
     * current connection
     */
    private static Connection connection;

    /*
     * current statement
     */
    private static Statement statement;

    /*
     * connect to another database
     */
    public static void connect(Connection newConnection) throws SQLException
    {
        /* set connection */
        connection=newConnection;

        /* create statement */
        statement=createStatement();

        /* let all listeners know that the database has been changed */
        fireDatabaseChanged();
    }

    /*
     * connect to another database
     */
    public static void connect(String driver,String url,String user,
                               String password)
        throws ClassNotFoundException,SQLException
    {
        /* load driver */
        if (driver!=null) Class.forName(driver);

        /* set connection */
        connect(DriverManager.getConnection(url,user,password));
    }

    /*
     * connect to another database
     */
    public static void connect(String url,String user,String password)
        throws SQLException
    {
        /* set connection */
        connect(DriverManager.getConnection(url,user,password));
    }

    /*
     * connect to another database
     */
    public static void connect(Properties info)
        throws ClassNotFoundException,SQLException
    {
        /* set connection */
        connect(info.getProperty("voji.db.driver"),
                info.getProperty("voji.db.url"),
                info.getProperty("voji.db.user"),
                info.getProperty("voji.db.password"));
    }

    /*
     * get current connection
     */
    public static Connection getConnection()
    {
        /* return current connection */
        return connection;
    }

    /*
     * create statement
     */
    public static Statement createStatement() throws SQLException
    {
        /* create statement from current connection */
        return getConnection().createStatement();
    }

    /*
     * prepare statement
     */
    public static PreparedStatement prepareStatement(String sql)
        throws SQLException
    {
        /* prepare statement from current connection */
        return getConnection().prepareStatement(sql);
    }

    /*
     * prepare call
     */
    public static CallableStatement prepareCall(String sql) throws SQLException
    {
        /* prepare call from current connection */
        return getConnection().prepareCall(sql);
    }

    /*
     * execute query
     */
    public static ResultSet executeQuery(String sql) throws SQLException
    {
        /* execute query at created statement */
        return statement.executeQuery(sql);
    }

    /*
     * execute update
     */
    public static int executeUpdate(String sql) throws SQLException
    {
        /* execute update at created statement */
        return statement.executeUpdate(sql);
    }
}
