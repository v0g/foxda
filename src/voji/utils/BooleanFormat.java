/*****************************************************************************\
*                                                                             *
*                                                                             *
*                         V       V         JJ  IIIII                         *
*                          V     V           J    I                           *
*                           V   V   OO       J    I                           *
*                            V V   O  O  J   J    I                           *
*                             V     OO    JJJ   IIIII                         *
*                                                                             *
*                         ---------------------------                         *
*                        |Volker's  Java Improvements|                        *
*                         ---------------------------                         *
*                                                                             *
*                                                                             *
*******************************************************************************
*                   *                                     *                   *
*                   *   FILE:        BooleanFormat.java   *                   *
*                   *                                     *                   *
*                    *************************************                    *
*                                                                             *
*                                                                             *
*  This library is free software; you can redistribute it and/or              *
*  modify it under the terms of the GNU Lesser General Public                 *
*  License as published by the Free Software Foundation; either               *
*  version 2.1 of the License, or (at your option) any later version.         *
*                                                                             *
*  This library is distributed in the hope that it will be useful,            *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU          *
*  Lesser General Public License for more details.                            *
*                                                                             *
*  You should have received a copy of the GNU Lesser General Public           *
*  License along with this library; if not, write to the Free Software        *
*  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA  *
*                                                                             *
*                                                                             *
\*****************************************************************************/
package voji.utils;
import java.util.*;
import java.text.*;

/**
 * This is a formatter for the Boolean type.
 * It's useful for the SQL-Type "BIT"
 * and when you don't like "TRUE" and "FALSE" in a user interface :-)
 */
public class BooleanFormat extends Format
{
    /**
     * The String for TRUE, default <code>"X"</code>
     */
    private String sTRUE = "X";

    /**
     * The character for TRUE, default <code>""</code>
     */
    private String sFALSE = "";


    /**
     * Creates a new BooleanFormat instance with default settings
     */
    public BooleanFormat()
    {
    }

    /**
     * Creates a new BooleanFormat instance
     *
     * @param stringTRUE the String that is used for "TRUE"
     * @param stringFALSE the String that is used for "FALSE"
     */
    public BooleanFormat(String stringTRUE, String stringFALSE)
    {
        this();

        sTRUE=stringTRUE;
        sFALSE=stringFALSE;
    }

    /**
     * Formats an Object
     */
    public StringBuffer format(Object obj,StringBuffer toAppendTo,
                               FieldPosition fieldPosition)
    {
        if (Boolean.class.isAssignableFrom(obj.getClass()))
            return format(((Boolean)obj).booleanValue(),toAppendTo,fieldPosition);
        else
            return format(((Number)obj).intValue()!=0,toAppendTo,fieldPosition);
    }

    /**
     * Parses a String
     */
    public Object parseObject(String text,ParsePosition pos)
    {
        return parse(text,pos);
    }

    /**
     * Formats a boolean
     */
    public StringBuffer format(boolean bool,StringBuffer toAppendTo,
                               FieldPosition fieldPosition)
    {
        return toAppendTo.append(bool ? sTRUE : sFALSE);
    }

    /**
     * Parses a String
     */
    public Boolean parse(String text,ParsePosition pos)
    {
        pos.setIndex(text.length());

        if (sTRUE.equals(text)) return Boolean.TRUE;
        if (sFALSE.equals(text)) return Boolean.FALSE;

        pos.setErrorIndex(0);
        return null;
    }
}
