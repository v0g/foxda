/*****************************************************************************\
*                                                                             *
*                                                                             *
*                         V       V         JJ  IIIII                         *
*                          V     V           J    I                           *
*                           V   V   OO       J    I                           *
*                            V V   O  O  J   J    I                           *
*                             V     OO    JJJ   IIIII                         *
*                                                                             *
*                         ---------------------------                         *
*                        |Volker's  Java Improvements|                        *
*                         ---------------------------                         *
*                                                                             *
*                                                                             *
*******************************************************************************
*                   *                                     *                   *
*                   *   FILE:              VChoice.java   *                   *
*                   *                                     *                   *
*                    *************************************                    *
*                                                                             *
*                                                                             *
*  This library is free software; you can redistribute it and/or              *
*  modify it under the terms of the GNU Lesser General Public                 *
*  License as published by the Free Software Foundation; either               *
*  version 2.1 of the License, or (at your option) any later version.         *
*                                                                             *
*  This library is distributed in the hope that it will be useful,            *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU          *
*  Lesser General Public License for more details.                            *
*                                                                             *
*  You should have received a copy of the GNU Lesser General Public           *
*  License along with this library; if not, write to the Free Software        *
*  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA  *
*                                                                             *
*                                                                             *
\*****************************************************************************/
package voji.utils;
import java.util.*;
import java.sql.*;

/**
 * This class facilates implementing a choice for the user.
 * There are methods for mapping between names, values and indices.
 */
public class VChoice
{
    /**
     * A Vector of (name,value) pairs containing the data
     */
    protected Vector<Object> data;

    /**
     * The default index.
     * Usually, it's -1, but it can be overwritten
     * by using a <code>null</code> value.
     */
    protected int defIndex;

    /**
     * Creates a new VChoice instance with initial data
     *
     * @param data an array of name-value-name-value-...
     */
    public VChoice(Object[] data)
    {
        setData(Arrays.asList(data).iterator());
    }

    /**
     * Creates a new VChoice instance without data
     */
    public VChoice()
    {
        clear();
    }

    /**
     * Removes all entries
     */
    public void clear()
    {
        data=new Vector<Object>();
        defIndex=-1;
    }

    /**
     * Adds an entry
     *
     * @param name key of the entry (for elements())
     * @param value value of the entry (for i2v and v2i)
     */
    public void add(Object name,Object value)
    {
        data.add(new Object[]{name,value});
        if (value==null) defIndex=data.size()-1;
    }

    /**
     * Sets the data of this instance
     *
     * @param data an Iterator of name-value-name-value-...
     */
    public void setData(Iterator<Object> data)
    {
        clear();

        if (data!=null) for (;;)
            {
                if (!data.hasNext()) break;
                Object name=data.next();
                if (!data.hasNext()) break;
                Object value=data.next();

                add(name,value);
            }
    }

    /**
     * Sets the data of this instance
     *
     * @param data an ResultSet of (name;value)
     * @throws SQLException if there is an SQLException
     *                      while working with the ResultSet
     */
    public void setData(ResultSet data)
        throws SQLException
    {
        Vector<Object> v=new Vector<Object>();
        setData(v.iterator());
    }

    /**
     * Returns the index of a value
     *
     * @param value the value that has to be looked for
     * @return the corresponding index or the default index
     *         if the value wasn't found
     */
    public int v2i(Object value)
    {
        if (value==null)
            return defIndex;
        for (int i=0;i<data.size();i++)
            if (value.equals(i2v(i)))
                return i;
        return defIndex;
    }

    /**
     * Returns the value of an index
     *
     * @param index the index where to look at
     * @return the corresponding value
     */
    public Object i2v(int index)
    {
        return ((Object[])data.get(index))[1];
    }

    /**
     * Returns an Array for Lists and ComboBoxes
     *
     * @return an Array containing all names
     */
    public Object[] elements()
    {
        Object[] r=new Object[data.size()];
        for (int i=0;i<r.length;i++)
            r[i]=((Object[])data.get(i))[0];
        return r;
    }
}
