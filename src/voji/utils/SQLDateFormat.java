/*****************************************************************************\
*                                                                             *
*                                                                             *
*                         V       V         JJ  IIIII                         *
*                          V     V           J    I                           *
*                           V   V   OO       J    I                           *
*                            V V   O  O  J   J    I                           *
*                             V     OO    JJJ   IIIII                         *
*                                                                             *
*                         ---------------------------                         *
*                        |Volker's  Java Improvements|                        *
*                         ---------------------------                         *
*                                                                             *
*                                                                             *
*******************************************************************************
*                   *                                     *                   *
*                   *   FILE:        SQLDateFormat.java   *                   *
*                   *                                     *                   *
*                    *************************************                    *
*                                                                             *
*                                                                             *
*  This library is free software; you can redistribute it and/or              *
*  modify it under the terms of the GNU Lesser General Public                 *
*  License as published by the Free Software Foundation; either               *
*  version 2.1 of the License, or (at your option) any later version.         *
*                                                                             *
*  This library is distributed in the hope that it will be useful,            *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU          *
*  Lesser General Public License for more details.                            *
*                                                                             *
*  You should have received a copy of the GNU Lesser General Public           *
*  License along with this library; if not, write to the Free Software        *
*  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA  *
*                                                                             *
*                                                                             *
\*****************************************************************************/
package voji.utils;
import java.util.*;
import java.text.*;

/*
 * CLASS SQLDateFormat
 */
public class SQLDateFormat extends DateFormat
{
    /*
     * mode
     */
    private int mode;

    /*
     * date format
     */
    private DateFormat dateFormat;

    /*
     * constructor
     */
    public SQLDateFormat(int initMode,DateFormat initFormat)
    {
        mode=initMode;
        dateFormat=initFormat;
    }

    /*
     * format
     */
    public StringBuffer format(Date date,StringBuffer toAppendTo,
                               FieldPosition fieldPosition)
    {
        return dateFormat.format(date,toAppendTo,fieldPosition);
    }

    /*
     * parse
     */
    public Date parse(String text,ParsePosition pos)
    {
        return convert(dateFormat.parse(text,pos).getTime());
    }

    /*
     * modes
     */
    public static final int NONE=-1;
    public static final int DATE=0;
    public static final int TIME=1;
    public static final int TIMESTAMP=2;

    /*
     * convert
     */
    private Date convert(long date)
    {
        switch (mode)
            {
            case DATE:      return new java.sql.Date(date);
            case TIME:      return new java.sql.Time(date);
            case TIMESTAMP: return new java.sql.Timestamp(date);
            default:        return new Date(date);
            }
    }
}
