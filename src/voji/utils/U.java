/*****************************************************************************\
*                                                                             *
*                                                                             *
*                         V       V         JJ  IIIII                         *
*                          V     V           J    I                           *
*                           V   V   OO       J    I                           *
*                            V V   O  O  J   J    I                           *
*                             V     OO    JJJ   IIIII                         *
*                                                                             *
*                         ---------------------------                         *
*                        |Volker's  Java Improvements|                        *
*                         ---------------------------                         *
*                                                                             *
*                                                                             *
*******************************************************************************
*                   *                                     *                   *
*                   *   FILE:                    U.java   *                   *
*                   *                                     *                   *
*                    *************************************                    *
*                                                                             *
*                                                                             *
*  This library is free software; you can redistribute it and/or              *
*  modify it under the terms of the GNU Lesser General Public                 *
*  License as published by the Free Software Foundation; either               *
*  version 2.1 of the License, or (at your option) any later version.         *
*                                                                             *
*  This library is distributed in the hope that it will be useful,            *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU          *
*  Lesser General Public License for more details.                            *
*                                                                             *
*  You should have received a copy of the GNU Lesser General Public           *
*  License along with this library; if not, write to the Free Software        *
*  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA  *
*                                                                             *
*                                                                             *
\*****************************************************************************/
package voji.utils;
import java.lang.reflect.*;
import java.util.*;
import java.text.*;
import java.sql.*;
import javax.swing.*;

/**
 * This class contains many methods which are useful for
 * java.lang.String,
 * java.util.Vector,
 * java.sql.ResultSet and
 * java.sql.PreparedStatement.
 * These methods should be a part of the API of those classes, but they aren't.
 * So this is a work-around, but no solution.
 * <P>
 * <I>Message to Sun:</I>
 * Please complete your API!
 * The functions here are simple,
 * but are often indispensible and would be great additions.
 * </P>
 */
public final class U
{
    /**
     * This Constructor is not needed.
     * It has been disabled to make sure that it won't used.
     */
    private U(){}

    /**
     * Removes all occurences of an Object from a Vector.
     *
     * @param vector the Vector where to remove from
     * @param object the String whose equal occourences have to be removed
     * @return the given Vector
     */
    public static Vector<String> removeAllStrings(Vector<String> vector,String s)
    {
        /* remove all occourences of given Object */
        while (vector.remove(s));

        /* return given Vector */
        return vector;
    }

    /**
     * Converts an Object Array to a Vector.
     *
     * @param objects the Object Array where to take the elements from
     * @return a newly created Vector containing all elements
     *         of the given Array
     */
    public static Vector<Object> toVector(Object[] objects)
    {
        /* add Object Array to newly created Vector */
        return new Vector<Object>(Arrays.asList(objects));
    }

    /**
     * Converts an Array of Object Arrays to a Vector of Vectors
     *
     * @param objects the Array of Object Arrays
     *                where to take the elements from
     * @return a newly created Vector of Vectors containing
     *         all elements of the given Array of Object Arrays
     */
    public static Vector<Vector<Object>> toVectorOfVectors(Object[][] objects)
    {
        /* create Vector */
        Vector<Vector<Object>> r=new Vector<Vector<Object>>();

        /* copy */
        for (Object[] objectArray : objects) r.add(toVector(objectArray));

        /* return Vector */
        return r;
    }

    /**
     * Converts a Vector of Vectors to an Array of Object Arrays
     *
     * @param vector the Vector of Vectors where to take the elements from
     * @return a newly created Array of Object Arrays containing
     *         all elements of the given Vector of Vectors
     */
    public static Object[][] toArrayOfArrays(Vector<Vector<Object>> vector)
    {
        /* create Array */
        Object[][] r=new Object[vector.size()][];

        /* copy */
        for (int i=0;i<r.length;i++) r[i]=vector.get(i).toArray();

        /* return Array */
        return r;
    }

    /**
     * Puts a ResultSetMetaData's column names into a Vector
     *
     * @param metaData the ResultSetMetaData
     *                 where to take the column names from
     * @return a newly created Vector containing the column names of metaData
     * @throws SQLException if there is an SQLException
     *                      while working with the ResultSetMetaData
     */
    public static Vector<String> getColumnNames(ResultSetMetaData metaData)
        throws SQLException
    {
        /* create Vector */
        Vector<String> r=new Vector<String>();

        /* copy */
        int count=metaData.getColumnCount();
        for (int i=1;i<=count;i++) r.add(metaData.getColumnName(i));

        /* return Vector */
        return r;
    }

    /**
     * Puts a ResultSet's column names into a Vector
     *
     * @param resultSet the ResultSet where to take the column names from
     * @return a newly created Vector containing the column names of resultSet
     * @throws SQLException if there is an SQLException
     *                      while working with the ResultSet
     */
    public static Vector<String> getColumnNames(ResultSet resultSet)
        throws SQLException
    {
        /* return the column names of resultSet's meta data */
        return getColumnNames(resultSet.getMetaData());
    }

    /**
     * Puts the current row's data of a ResultSet into a Vector
     *
     * @param resultSet the ResultSet where to get the data from
     * @return a newly created Vector containing the data of the resultSet's
     *         current row
     * @throws SQLException if there is an SQLException
     *                      while working with the ResultSet
     */
    public static Vector<Object> toVector(ResultSet resultSet) throws SQLException
    {
        /* create Vector */
        Vector<Object> r=new Vector<Object>();

        /* copy */
        int count=resultSet.getMetaData().getColumnCount();
        for (int i=1;i<=count;i++) r.add(resultSet.getObject(i));

        /* return Vector */
        return r;
    }

    /**
     * Converts a ResultSet into a Vector of Vectors.
     * It goes through the whole ResultSet and copies all data
     * into a newly created Vector of Vectors.
     *
     * @param resultSet the ResultSet where to take the data from
     * @return a newly created Vector containing one Vector for each
     *         result row
     * @throws SQLException if there is an SQLException
     *                      while working with the ResultSet
     */
    public static Vector<Vector<Object>> toVectorOfVectors(ResultSet resultSet)
        throws SQLException
    {
        return toVectorOfVectors(resultSet,-1);
    }

    /**
     * Converts a ResultSet into a Vector of Vectors.
     * It goes through the whole ResultSet and copies all data
     * into a newly created Vector of Vectors.
     *
     * @param resultSet the ResultSet where to take the data from
     * @param maxRows the maximum number of rows
     *                or <code>-1</code> for no maximum
     * @return a newly created Vector containing one Vector for each
     *         result row
     * @throws SQLException if there is an SQLException
     *                      while working with the ResultSet
     */
    public static Vector<Vector<Object>> toVectorOfVectors(ResultSet resultSet,int maxRows)
        throws SQLException
    {
        /* create Vector */
        Vector<Vector<Object>> r=new Vector<Vector<Object>>();

        /* copy */
        while (resultSet.next() && (maxRows--!=0)) r.add(toVector(resultSet));

        /* return Vector */
        return r;
    }

    /**
     * Converts a Vector into an Array.
     *
     * @param vector the Vector where to take the Objects from
     * @return an Object Array containing the the elements of the Vector
     */
    public static Object[] toArray(Vector vector)
    {
        /* create Array */
        Object[] r=new Object[vector.size()];

        /* copy */
        for (int i=0;i<r.length;i++) r[i]=vector.get(i);

        /* return Array */
        return r;
    }

    /**
     * Converts a Vector into a String Array.
     * It uses the toString() method of each elements of the Vector.
     *
     * @param vector the Vector where to take the Strings from
     * @return a String Array containing the string representations
     *         of the elements of the Vector
     */
    public static String[] toStringArray(Vector<String> vector)
    {
        /* create Array */
        String[] r=new String[vector.size()];

        /* copy */
        for (int i=0;i<r.length;i++) r[i]=vector.get(i).toString();

        /* return Array */
        return r;
    }

    /**
     * Splits a String
     *
     * @param text the String to split
     * @param start the begin of the String which has to be ignored
     * @param between the separator between all the parts of <code>text</code>
     * @param end the end of the String which has to be ignored
     * @return a Vector containing the parts of <code>text</code>
     */
    public static Vector<String> splitString(String text,
                                             String start,String between,String end)
    {
        /* create Vector */
        Vector<String> r=new Vector<String>();

        /* remove start and end */
        text=text.substring(text.startsWith(start)?start.length():0,
                            text.length()-(text.endsWith(end)?end.length():0));

        /* split message at all separators */
        for (int i;(i=text.indexOf(between))>=0;text=text.substring(i+1))
            r.add(text.substring(0,i));
        r.add(text);

        /* return Vector */
        return r;
    }

    /**
     * Splits a String into non-empty parts
     *
     * @param text the String to split
     * @param start the begin of the String which has to be ignored
     * @param between the separator between all the parts of <code>text</code>
     * @param end the end of the String which has to be ignored
     * @return a Vector containing the parts of <code>text</code>
     *         without the empty ones.
     */
    public static Vector<String> splitStringNE(String text,
                                               String start,String between,String end)
    {
        /* call other splitString and remove all empty strings */
        return removeAllStrings(splitString(text,start,between,end),"");
    }

    /**
     * Concatenates some Strings
     *
     * @param parts a Vector containing the parts
     * @param start the begin of the full String
     * @param between the String which is situated between the parts
     * @param end the end of the full String
     * @return the resulting String of the concatenation
     */
    public static String toString(Vector parts,
                                  String start,String between,String end)
    {
        /* create StringBuffer */
        StringBuffer r=new StringBuffer(start);

        /* concatenate */
        for (int i=0;i<parts.size();i++)
            {
                if (i>0) r.append(between);
                r.append(parts.get(i));
            }
        r.append(end);

        /* return String representation of StringBuffer */
        return r.toString();
    }

    /**
     * Replaces all occurences of a sub String in a StringBuffer.
     * It manipulates the given StringBuffer directly!
     *
     * @param text the input StringBuffer where to replace
     * @param from the sub String which has to replaced by <code>to</code>
     * @param to the sub String
     *           which has to be used instead of <code>from</code>
     * @return the given StringBuffer
     */
    public static StringBuffer replace(StringBuffer text,String from,String to)
    {
        /* save given StringBuffer in a temporary String */
        String t=text.toString();

        /* clear given StringBuffer */
        text.setLength(0);

        /* copy and replace */
        for (int i;(i=t.indexOf(from))>=0;t=t.substring(i+1))
            text.append(t.substring(0,i)).append(to);
        text.append(t);

        /* return given StringBuffer */
        return text;
    }

    /**
     * Replaces all occurences of a sub String in a String
     *
     * @param text the input String where to replace
     * @param from the sub String which has to replaced by <code>to</code>
     * @param to the sub String
     *           which has to be used instead of <code>from</code>
     * @return a new String which is the result of the replacements
     */
    public static String replace(String text,String from,String to)
    {
        /* create new StringBuffer, then replace and return it as String */
        return replace(new StringBuffer(text),from,to).toString();
    }

    /**
     * Parses a String using a Format without throwing an exception
     *
     * @param format the Format object that has to parse
     * @param string the String that has to be parsed
     * @return the result of the parsing, <code>string</code> when
     *         format is <code>null</code> or <code>null</code> when
     *         there was an exception during the parsing
     */
    public static Object parseObject(Format format,String string)
    {
        /* try to parse */
        try { return format==null ? string : format.parseObject(string); }

        /* when there were problems ... */
        catch (Exception e) { return null; }
    }

    /**
     * Formats an Object using a Format without throwing an exception
     *
     * @param format the Format object that has to format
     * @param object the Object that has to be formatted
     * @param def the default String to return on problems
     * @return the result of the formatting or <code>object.toString()<code>
     *         when format is null or <code>def</code> when
     *         there was an exception during the formatting
     * @see #format(Format,Object)
     */
    public static String format(Format format,Object object,String def)
    {
        /* try to parse */
        try { return (format==null)
                  ? object.toString() : format.format(object); }

        /* when there were problems ... */
        catch (Exception e) { return def; }
    }

    /**
     * Formats an Object using a Format without throwing an exception
     *
     * @param format the Format object that has to format
     * @param object the Object that has to be formatted
     * @return the result of the formatting or <code>object.toString()<code>
     *         when format is null or <code>""</code> when
     *         there was an exception during the formatting
     * @see #format(Format,Object,String)
     */
    public static String format(Format format,Object object)
    {
        return format(format,object,"");
    }

    /**
     * Returns whether all class types are assignable from the given
     * parameters.
     *
     * @param types the types of e.g. a method's parameter declaration
     * @param params the parameters which have to be tested to match
     *               the types (i.e. to be assignable to them)
     * @return <code>true</code> if a method declared with <code>types</code>
     *         as parameters could take the <code>params</code>
     */
    public static boolean areAssignableFrom(Class<?>[] types,Object[] params)
    {
        if (types.length!=params.length) return false;
        for (int i=0;i<types.length;i++)
            {
                if (params[i]!=null && !types[i].isAssignableFrom(params[i].getClass()))
                    return false;
            }
        return true;
    }

    /**
     * This method is the same as <code>Class.getConstructor(Object[])</code>
     * except that the parameters' types don't need to match exactly
     * with the constructor's parameters' types. They only need to
     * be assignable.
     *
     * @param cl the <code>Class</code> whose constructor is wanted
     * @param params the parameters which should be able to be passed
     *               to the constructor
     * @throws NoSuchMethodException if no such constructor was found
     */
    public static Constructor getConstructor(Class cl,Object[] params)
        throws NoSuchMethodException
    {
        Constructor[] c=cl.getConstructors();
        for (int i=0;i<c.length;i++)
            {
                if (areAssignableFrom(c[i].getParameterTypes(),params))
                    return c[i];
            }
        throw new NoSuchMethodException();
    }

    /**
     * Current file chooser (needed by <code>synchronizeDirectory</code>)
     *
     * @see #synchronizeDirectory(JFileChooser)
     */
    private static JFileChooser currentFC=new JFileChooser(".");

    /**
     * Synchronizes the current directory between file choosers.
     * This is useful if you really need more than one file chooser for
     * your application.
     *
     * @param fileChooser the <code>FileChooser</code> whose current directory
     *                    needs to be set.
     */
    public static void synchronizeDirectory(JFileChooser fileChooser)
    {
        fileChooser.setCurrentDirectory(currentFC.getCurrentDirectory());
        currentFC=fileChooser;
    }
}
