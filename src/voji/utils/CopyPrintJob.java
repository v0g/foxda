/*****************************************************************************\
*                                                                             *
*                                                                             *
*                         V       V         JJ  IIIII                         *
*                          V     V           J    I                           *
*                           V   V   OO       J    I                           *
*                            V V   O  O  J   J    I                           *
*                             V     OO    JJJ   IIIII                         *
*                                                                             *
*                         ---------------------------                         *
*                        |Volker's  Java Improvements|                        *
*                         ---------------------------                         *
*                                                                             *
*                                                                             *
*******************************************************************************
*                   *                                     *                   *
*                   *   FILE:         CopyPrintJob.java   *                   *
*                   *                                     *                   *
*                    *************************************                    *
*                                                                             *
*                                                                             *
*  This library is free software; you can redistribute it and/or              *
*  modify it under the terms of the GNU Lesser General Public                 *
*  License as published by the Free Software Foundation; either               *
*  version 2.1 of the License, or (at your option) any later version.         *
*                                                                             *
*  This library is distributed in the hope that it will be useful,            *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU          *
*  Lesser General Public License for more details.                            *
*                                                                             *
*  You should have received a copy of the GNU Lesser General Public           *
*  License along with this library; if not, write to the Free Software        *
*  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA  *
*                                                                             *
*                                                                             *
\*****************************************************************************/
package voji.utils;
import java.util.*;
import java.text.*;
import java.awt.*;

/**
 * This class redirects all functions to another <code>PrintJob</code>.
 */
public class CopyPrintJob extends PrintJob
{
    /**
     * Creates a new empty instance of <code>CopyPrintJob</code>
     */
    public CopyPrintJob()
    {
    }

    /**
     * Creates a new empty instance of <code>CopyPrintJob</code>
     * handling the given <code>PrintJob</code>.
     *
     * @param printJob the <code>PrintJob</code> where to redirect
     *                 all actions to
     */
    public CopyPrintJob(PrintJob printJob)
    {
        this();
        setPrintJob(printJob);
    }

    /**
     * The <code>PrintJob</code> to cover
     */
    private PrintJob printJob=null;

    /**
     * The <code>PageGraphics</code> to use for
     * {@link #getPageGraphics getPageGraphics}
     */
    private PageGraphics pageGraphics=null;

    /**
     * Sets the current print job
     *
     * @param printJob the new <code>PrintJob</code> where to redirect
     *                 all actions to
     */
    public void setPrintJob(PrintJob printJob)
    {
        this.printJob=printJob;
        this.pageGraphics=new PageGraphics(this);
    }

    /**
     * The current <i>x</i>-translation in inches
     */
    private double x;

    /**
     * The current <i>y</i>-translation in inches
     */
    private double y;

    /**
     * Sets the initial translation for each page in inches
     *
     * @param x the <i>x</i> coordinate (in inches) where to
     *          {@link #setTranslation translate} all pages to
     * @param y the <i>y</i> coordinate (in inches) where to
     *          {@link #setTranslation translate} all pages to
     */
    public void setTranslation(double x,double y)
    {
        this.x=x;
        this.y=y;
    }

    /**
     * Sets the initial translation for each page in inches
     *
     * @param info the <code>Properties</code> where to take the parameters
     *             from. The keys are
     *               <code>voji.print.x</code> and
     *               <code>voji.print.y</code>.
     */
    public void setTranslation(Properties info)
    {
        setTranslation(Double.parseDouble(info.getProperty
                                          ("voji.print.offset.x","0.0")),
                       Double.parseDouble(info.getProperty
                                          ("voji.print.offset.y","0.0")));
    }

    /**
     * Gets a Graphics object that will draw to the next page.
     *
     * This graphics object will also implement the PrintGraphics
     * interface and will be {@link #setTranslation translated}.
     *
     * @return a <code>Graphics</code> object which draws to the next page.
     */
    public Graphics getGraphics()
    {
        return getPageGraphics();
    }

    /**
     * Gets a Graphics object that will draw to the next page.
     *
     * This graphics object will be {@link #setTranslation translated}.
     *
     * @return a <code>PageGraphics</code> object which draws to the next page.
     */
    public PageGraphics getPageGraphics()
    {
        pageGraphics.setGraphics(printJob.getGraphics());
        pageGraphics.translate(pageGraphics.toPixels(x),
                               pageGraphics.toPixels(y));
        return pageGraphics;
    }

    /**
     * Returns the dimensions of the page in pixels
     */
    public Dimension getPageDimension()
    {
        return printJob.getPageDimension();
    }

    /**
     * Returns the resolution of the page in pixels per inch
     */
    public int getPageResolution()
    {
        return printJob.getPageResolution();
    }

    /**
     * Returns true if the last page will be printed first
     */
    public boolean lastPageFirst()
    {
        return printJob.lastPageFirst();
    }

    /**
     * Ends the print job and does any necessary cleanup
     */
    public void end()
    {
        printJob.end();
    }
}
