/*****************************************************************************\
*                                                                             *
*                                                                             *
*                         V       V         JJ  IIIII                         *
*                          V     V           J    I                           *
*                           V   V   OO       J    I                           *
*                            V V   O  O  J   J    I                           *
*                             V     OO    JJJ   IIIII                         *
*                                                                             *
*                         ---------------------------                         *
*                        |Volker's  Java Improvements|                        *
*                         ---------------------------                         *
*                                                                             *
*                                                                             *
*******************************************************************************
*                   *                                     *                   *
*                   *   FILE:             Instance.java   *                   *
*                   *                                     *                   *
*                    *************************************                    *
*                                                                             *
*                                                                             *
*  This library is free software; you can redistribute it and/or              *
*  modify it under the terms of the GNU Lesser General Public                 *
*  License as published by the Free Software Foundation; either               *
*  version 2.1 of the License, or (at your option) any later version.         *
*                                                                             *
*  This library is distributed in the hope that it will be useful,            *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU          *
*  Lesser General Public License for more details.                            *
*                                                                             *
*  You should have received a copy of the GNU Lesser General Public           *
*  License along with this library; if not, write to the Free Software        *
*  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA  *
*                                                                             *
*                                                                             *
\*****************************************************************************/
package voji.utils;
import java.lang.reflect.*;

/**
 * This class holds parameters for the constructor of a class.
 * However, an instance will only be created if needed.
 */
public class Instance
{
    /**
     * The constructor which is to be used
     */
    protected Constructor constructor=null;

    /**
     * The parameters which have to be passed to the constructor
     */
    protected Object[] params=null;

    /**
     * The instance or
     * <code>null</code> if the instance hasn't been created yet
     */
    protected Object instance=null;

    /**
     * Constructs a new empty <code>Instance</code> instance
     */
    public Instance()
    {
    }

    /**
     * Constructs a new <code>Instance</code> instance
     * holding an instance of the given <code>Class</code>.
     *
     * @param cl The <code>Class</code> which has to be instantiated
     * @param params An <code>Array</code> containing the parameters
     *               of the constructor which has to be used
     */
    public Instance(Class cl,Object[] params)
    {
        try
            {
                this.constructor=U.getConstructor(cl,params);
            }
        catch (NoSuchMethodException e) {}
        this.params=params;
    }

    /**
     * Creates the instance.
     * This method can be overwritten in case
     * there's any more initialisation necessary.
     */
    protected Object create()
    {
        try
            {
                return constructor.newInstance(params);
            }
        catch (InvocationTargetException e) {}
        catch (InstantiationException e) {}
        catch (IllegalAccessException e) {}
        catch (NullPointerException e) {}

        return null;
    }

    /**
     * Returns the instance and creates it, if necessary
     */
    public Object get()
    {
        if (instance==null)
            instance=create();
        return instance;
    }
}
