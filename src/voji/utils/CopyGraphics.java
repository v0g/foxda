/*****************************************************************************\
*                                                                             *
*                                                                             *
*                         V       V         JJ  IIIII                         *
*                          V     V           J    I                           *
*                           V   V   OO       J    I                           *
*                            V V   O  O  J   J    I                           *
*                             V     OO    JJJ   IIIII                         *
*                                                                             *
*                         ---------------------------                         *
*                        |Volker's  Java Improvements|                        *
*                         ---------------------------                         *
*                                                                             *
*                                                                             *
*******************************************************************************
*                   *                                     *                   *
*                   *   FILE:         CopyGraphics.java   *                   *
*                   *                                     *                   *
*                    *************************************                    *
*                                                                             *
*                                                                             *
*  This library is free software; you can redistribute it and/or              *
*  modify it under the terms of the GNU Lesser General Public                 *
*  License as published by the Free Software Foundation; either               *
*  version 2.1 of the License, or (at your option) any later version.         *
*                                                                             *
*  This library is distributed in the hope that it will be useful,            *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU          *
*  Lesser General Public License for more details.                            *
*                                                                             *
*  You should have received a copy of the GNU Lesser General Public           *
*  License along with this library; if not, write to the Free Software        *
*  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA  *
*                                                                             *
*                                                                             *
\*****************************************************************************/
package voji.utils;
import java.util.*;
import java.text.*;
import java.awt.*;
import java.awt.image.*;

/**
 * This class redirects all functions to one or more specified
 * <code>Graphics</code> objects.
 */
public class CopyGraphics extends Graphics
{
    /**
     * Creates a new empty instance of <code>CopyGraphics</code>
     */
    public CopyGraphics()
    {
    }

    /**
     * Creates a new empty instance of <code>CopyGraphics</code>
     * containing the given sub <code>Graphics</code> object
     *
     * @param g the new sub <code>Graphics</code> object
     *          or <code>null</code> if there shouldn't be
     *          any sub objects
     */
    public CopyGraphics(Graphics g)
    {
        this();
        setGraphics(g);
    }

    /**
     * Creates a new instance of <code>CopyGraphics</code>
     * containing the given sub <code>Graphics</code> objects
     *
     * @param g an Array containing the new sub <code>Graphics</code> objects
     */
    public CopyGraphics(Graphics[] g)
    {
        this();
        addGraphics(g);
    }

    /**
     * Contains all sub <code>Graphics</code> objects
     */
    private Vector<Graphics> graphics=new Vector<Graphics>();

    /**
     * Adds a sub <code>Graphics</code> object
     *
     * @param g the new sub <code>Graphics</code> object
     */
    public void addGraphics(Graphics g)
    {
        addGraphics(new Graphics[]{g});
    }

    /**
     * Adds some sub <code>Graphics</code> objects
     *
     * @param g an Array containing the new sub <code>Graphics</code> objects
     */
    public void addGraphics(Graphics[] g)
    {
        graphics.addAll(Arrays.asList(g));
    }

    /**
     * Removes a sub <code>Graphics</code> object
     *
     * @param g the sub <code>Graphics</code> object to remove
     */
    public void removeGraphics(Graphics g)
    {
        graphics.remove(g);
    }

    /**
     * Sets the current sub <code>Graphics</code> object
     *
     * @param g the new sub <code>Graphics</code> object
     *          or <code>null</code> if there shouldn't be
     *          any sub objects
     */
    public void setGraphics(Graphics g)
    {
        graphics.clear();
        if (g!=null) addGraphics(g);
    }

    /**
     * Creates a new <code>CopyGraphics</code> instance
     * containing copies of all sub <code>Graphics</code> objects
     */
    public Graphics create()
    {
        CopyGraphics r=new CopyGraphics();
        for (Graphics g : graphics)
            r.addGraphics(g.create());
        return r;
    }

    /**
     * Redirects to <code>dispose</code> of the registered
     * <code>Graphics</code> objects
     */
    public void dispose()
    {
        for (Graphics g : graphics)
            g.dispose();
    }

    /**
     * Returns a <code>String</code> object representing this object's value
     *
     * @return a string representation of this graphics context
     */
    public String toString()
    {
        return "voji.utils.CopyGraphics[graphics="+graphics+"]";
    }

    /**
     * Redirects to <code>clearRect</code> of the registered
     * <code>Graphics</code> objects
     */
    public void clearRect(int x,int y,int width,int height)
    {
        for (Graphics g : graphics)
            g.clearRect(x,y,width,height);
    }

    /**
     * Redirects to <code>clipRect</code> of the registered
     * <code>Graphics</code> objects
     */
    public void clipRect(int x,int y,int width,int height)
    {
        for (Graphics g : graphics)
            g.clipRect(x,y,width,height);
    }

    /**
     * Redirects to <code>copyArea</code> of the registered
     * <code>Graphics</code> objects
     */
    public void copyArea(int x,int y,int width,int height,int dx,int dy)
    {
        for (Graphics g : graphics)
            g.copyArea(x,y,width,height,dx,dy);
    }

    /**
     * Redirects to <code>drawArc</code> of the registered
     * <code>Graphics</code> objects
     */
    public void drawArc(int x,int y,int width,int height,int startAngle,int arcAngle)
    {
        for (Graphics g : graphics)
            g.drawArc(x,y,width,height,startAngle,arcAngle);
    }

    /**
     * Redirects to <code>drawImage</code> of the registered
     * <code>Graphics</code> objects
     */
    public boolean drawImage(Image img,int x,int y,
                             Color bgcolor,ImageObserver observer)
    {
        boolean r=true;
        for (Graphics g : graphics)
            r&=g.drawImage(img,x,y,bgcolor,observer);
        return r;
    }

    /**
     * Redirects to <code>drawImage</code> of the registered
     * <code>Graphics</code> objects
     */
    public boolean drawImage(Image img,int x,int y,ImageObserver observer)
    {
        boolean r=true;
        for (Graphics g : graphics)
            r&=g.drawImage(img,x,y,observer);
        return r;
    }

    /**
     * Redirects to <code>drawImage</code> of the registered
     * <code>Graphics</code> objects
     */
    public boolean drawImage(Image img,int x,int y,int width,int height,Color bgcolor,ImageObserver observer)
    {
        boolean r=true;
        for (Graphics g : graphics)
            r&=g.drawImage(img,x,y,width,height,bgcolor,observer);
        return r;
    }

    /**
     * Redirects to <code>drawImage</code> of the registered
     * <code>Graphics</code> objects
     */
    public boolean drawImage(Image img,int x,int y,int width,int height,ImageObserver observer)
    {
        boolean r=true;
        for (Graphics g : graphics)
            r&=g.drawImage(img,x,y,width,height,observer);
        return r;
    }

    /**
     * Redirects to <code>drawImage</code> of the registered
     * <code>Graphics</code> objects
     */
    public boolean drawImage(Image img,int dx1,int dy1,int dx2,int dy2,
                             int sx1,int sy1,int sx2,int sy2,
                             Color bgcolor,ImageObserver observer)
    {
        boolean r=true;
        for (Graphics g : graphics)
            r&=g.drawImage(img,dx1,dy1,dx2,dy2,
                           sx1,sy1,sx2,sy2,
                           bgcolor,observer);
        return r;
    }

    /**
     * Redirects to <code>drawImage</code> of the registered
     * <code>Graphics</code> objects
     */
    public boolean drawImage(Image img,int dx1,int dy1,int dx2,int dy2,
                             int sx1,int sy1,int sx2,int sy2,
                             ImageObserver observer)
    {
        boolean r=true;
        for (Graphics g : graphics)
            r&=g.drawImage(img,dx1,dy1,dx2,dy2,
                           sx1,sy1,sx2,sy2,observer);
        return r;
    }

    /**
     * Redirects to <code>drawLine</code> of the registered
     * <code>Graphics</code> objects
     */
    public void drawLine(int x1,int y1,int x2,int y2)
    {
        for (Graphics g : graphics)
            g.drawLine(x1,y1,x2,y2);
    }

    /**
     * Redirects to <code>drawOval</code> of the registered
     * <code>Graphics</code> objects
     */
    public void drawOval(int x,int y,int width,int height)
    {
        for (Graphics g : graphics)
            g.drawOval(x,y,width,height);
    }

    /**
     * Redirects to <code>drawPolygon</code> of the registered
     * <code>Graphics</code> objects
     */
    public void drawPolygon(int[] xPoints,int[] yPoints,int nPoints)
    {
        for (Graphics g : graphics)
            g.drawPolygon(xPoints,yPoints,nPoints);
    }

    /**
     * Redirects to <code>drawPolyline</code> of the registered
     * <code>Graphics</code> objects
     */
    public void drawPolyline(int[] xPoints,int[] yPoints,int nPoints)
    {
        for (Graphics g : graphics)
            g.drawPolyline(xPoints,yPoints,nPoints);
    }

    /**
     * Redirects to <code>drawRoundRect</code> of the registered
     * <code>Graphics</code> objects
     */
    public void drawRoundRect(int x,int y,int width,int height,int arcWidth,int arcHeight)
    {
        for (Graphics g : graphics)
            g.drawRoundRect(x,y,width,height,arcWidth,arcHeight);
    }

    /**
     * Redirects to <code>drawString</code> of the registered
     * <code>Graphics</code> objects
     */
    public void drawString(AttributedCharacterIterator iterator,int x,int y)
    {
        for (Graphics g : graphics)
            g.drawString(iterator,x,y);
    }

    /**
     * Redirects to <code>drawString</code> of the registered
     * <code>Graphics</code> objects
     */
    public void drawString(String str,int x,int y)
    {
        for (Graphics g : graphics)
            g.drawString(str,x,y);
    }

    /**
     * Redirects to <code>fillArc</code> of the registered
     * <code>Graphics</code> objects
     */
    public void fillArc(int x,int y,int width,int height,
                        int startAngle,int arcAngle)
    {
        for (Graphics g : graphics)
            g.fillArc(x,y,width,height,
                                         startAngle,arcAngle);
    }

    /**
     * Redirects to <code>fillOval</code> of the registered
     * <code>Graphics</code> objects
     */
    public void fillOval(int x,int y,int width,int height)
    {
        for (Graphics g : graphics)
            g.fillOval(x,y,width,height);
    }

    /**
     * Redirects to <code>fillPolygon</code> of the registered
     * <code>Graphics</code> objects
     */
    public void fillPolygon(int[] xPoints,int[] yPoints,int nPoints)
    {
        for (Graphics g : graphics)
            g.fillPolygon(xPoints,yPoints,nPoints);
    }

    /**
     * Redirects to <code>fillRect</code> of the registered
     * <code>Graphics</code> objects
     */
    public void fillRect(int x,int y,int width,int height)
    {
        for (Graphics g : graphics)
            g.fillRect(x,y,width,height);
    }

    /**
     * Redirects to <code>fillRoundRect</code> of the registered
     * <code>Graphics</code> objects
     */
    public void fillRoundRect(int x,int y,int width,int height,
                              int arcWidth,int arcHeight)
    {
        for (Graphics g : graphics)
            g.fillRoundRect(x,y,width,height,
                                               arcWidth,arcHeight);
    }

    /**
     * Redirects to <code>getClip</code> of the registered
     * <code>Graphics</code> objects
     */
    public Shape getClip()
    {
        return graphics.isEmpty()?null:
            graphics.firstElement().getClip();
    }

    /**
     * Redirects to <code>getClipBounds</code> of the registered
     * <code>Graphics</code> objects
     */
    public Rectangle getClipBounds()
    {
        return graphics.isEmpty()?null:
            graphics.firstElement().getClipBounds();
    }

    /**
     * Redirects to <code>getColor</code> of the registered
     * <code>Graphics</code> objects
     */
    public Color getColor()
    {
        return graphics.isEmpty()?null:
            graphics.firstElement().getColor();
    }

    /**
     * Redirects to <code>getFont</code> of the registered
     * <code>Graphics</code> objects
     */
    public Font getFont()
    {
        return graphics.isEmpty()?null:
            graphics.firstElement().getFont();
    }

    /**
     * Redirects to <code>getFontMetrics</code> of the registered
     * <code>Graphics</code> objects
     */
    public FontMetrics getFontMetrics(Font f)
    {
        return graphics.isEmpty()?null:
            graphics.firstElement().getFontMetrics(f);
    }

    /**
     * Redirects to <code>setClip</code> of the registered
     * <code>Graphics</code> objects
     */
    public void setClip(int x,int y,int width,int height)
    {
        for (Graphics g : graphics)
            g.setClip(x,y,width,height);
    }

    /**
     * Redirects to <code>setClip</code> of the registered
     * <code>Graphics</code> objects
     */
    public void setClip(Shape clip)
    {
        for (Graphics g : graphics)
            g.setClip(clip);
    }

    /**
     * Redirects to <code>setColor</code> of the registered
     * <code>Graphics</code> objects
     */
    public void setColor(Color c)
    {
        for (Graphics g : graphics)
            g.setColor(c);
    }

    /**
     * Redirects to <code>setFont</code> of the registered
     * <code>Graphics</code> objects
     */
    public void setFont(Font font)
    {
        for (Graphics g : graphics)
            g.setFont(font);
    }

    /**
     * Redirects to <code>setPaintMode</code> of the registered
     * <code>Graphics</code> objects
     */
    public void setPaintMode()
    {
        for (Graphics g : graphics)
            g.setPaintMode();
    }

    /**
     * Redirects to <code>setXORMode</code> of the registered
     * <code>Graphics</code> objects
     */
    public void setXORMode(Color c1)
    {
        for (Graphics g : graphics)
            g.setXORMode(c1);
    }

    /**
     * Redirects to <code>translate</code> of the registered
     * <code>Graphics</code> objects
     */
    public void translate(int x,int y)
    {
        for (Graphics g : graphics)
            g.translate(x,y);
    }
}
