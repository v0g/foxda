/*****************************************************************************\
*                                                                             *
*                                                                             *
*                         V       V         JJ  IIIII                         *
*                          V     V           J    I                           *
*                           V   V   OO       J    I                           *
*                            V V   O  O  J   J    I                           *
*                             V     OO    JJJ   IIIII                         *
*                                                                             *
*                         ---------------------------                         *
*                        |Volker's  Java Improvements|                        *
*                         ---------------------------                         *
*                                                                             *
*                                                                             *
*******************************************************************************
*                   *                                     *                   *
*                   *   FILE:          ValueHolder.java   *                   *
*                   *                                     *                   *
*                    *************************************                    *
*                                                                             *
*                                                                             *
*  This library is free software; you can redistribute it and/or              *
*  modify it under the terms of the GNU Lesser General Public                 *
*  License as published by the Free Software Foundation; either               *
*  version 2.1 of the License, or (at your option) any later version.         *
*                                                                             *
*  This library is distributed in the hope that it will be useful,            *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU          *
*  Lesser General Public License for more details.                            *
*                                                                             *
*  You should have received a copy of the GNU Lesser General Public           *
*  License along with this library; if not, write to the Free Software        *
*  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA  *
*                                                                             *
*                                                                             *
\*****************************************************************************/
package voji.utils;

/**
 * This class holds a value under a certain name.
 * It seems to be the name Object,
 * but it also contains the value Object.
 * It's useful for Lists, ComboBoxes and Tables
 * when you want the user to see "Eleven" but want
 * to work with the result 11.
 * <br>
 * This could be also a replacement for voji.utils.choice,
 * if it would work :-(
 */
public class ValueHolder
{
    /**
     * The name Object
     */
    public Object name;

    /**
     * The value Object
     */
    public Object value;

    /**
     * Creates a new ValueHolder instance
     *
     * @param initName the initial name
     * @param initValue the initial value
     */
    public ValueHolder(Object initName, Object initValue)
    {
        name=initName;
        value=initValue;
    }

    /**
     * Creates a "dummy" ValueHolder instance with a <code>null</code> name
     * for comparations
     *
     * @param initValue the initial value
     */
    public ValueHolder(Object initValue)
    {
        this(null,initValue);
    }

    /**
     * Creates a new ValueHolder instance with <code>null</code> fields
     */
    public ValueHolder()
    {
        this(null,null);
    }

    /**
     * Formats this Object as a String. It just returns <code>name</code> :-)
     *
     * @return the String representation of <code>name</code>
     */
    public String toString()
    {
        return name==null ? "" : name.toString();
    }

    /**
     * Indicates whether some other object is "equal to" this one
     *
     * @return whether the values of the ValueHolders are equal
     */
    public boolean equals(Object obj)
    {
        return obj!=null && ValueHolder.class.isAssignableFrom(obj.getClass())
            && value.equals(get(obj));
    }

    /**
     * A shorter way to access the value of a ValueHolder
     *
     * @param valueHolder the ValueHolder whose value is wanted
     * @return <code>((ValueHolder)valueHolder).value</code>
     */
    public static Object get(Object valueHolder)
    {
        return ((ValueHolder)valueHolder).value;
    }
}
