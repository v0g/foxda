/*****************************************************************************\
*                                                                             *
*                                                                             *
*                         V       V         JJ  IIIII                         *
*                          V     V           J    I                           *
*                           V   V   OO       J    I                           *
*                            V V   O  O  J   J    I                           *
*                             V     OO    JJJ   IIIII                         *
*                                                                             *
*                         ---------------------------                         *
*                        |Volker's  Java Improvements|                        *
*                         ---------------------------                         *
*                                                                             *
*                                                                             *
*******************************************************************************
*                   *                                     *                   *
*                   *   FILE:           DataFormat.java   *                   *
*                   *                                     *                   *
*                    *************************************                    *
*                                                                             *
*                                                                             *
*  This library is free software; you can redistribute it and/or              *
*  modify it under the terms of the GNU Lesser General Public                 *
*  License as published by the Free Software Foundation; either               *
*  version 2.1 of the License, or (at your option) any later version.         *
*                                                                             *
*  This library is distributed in the hope that it will be useful,            *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU          *
*  Lesser General Public License for more details.                            *
*                                                                             *
*  You should have received a copy of the GNU Lesser General Public           *
*  License along with this library; if not, write to the Free Software        *
*  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA  *
*                                                                             *
*                                                                             *
\*****************************************************************************/
package voji.utils;
import java.lang.*;
import java.util.*;
import java.text.*;
import java.sql.*;

/**
 * This class defines an way to format the result of an SQL query
 * to a string. This is especially useful for printing.
 */
public class DataFormat
{
    /**
     * The currently used format
     */
    private Format format;

    /**
     * Creates a new DataFormat instance.
     * It formats using the given format.
     *
     * @param format the Format to use
     */
    public DataFormat(Format format)
    {
        this.format=format;
    }

    /**
     * Creates a new DataFormat instance
     */
    public DataFormat()
    {
        this(null);
    }

    /**
     * Formats a ResultSet
     *
     * @param resultSet the ResultSet that needs to be formatted
     * @param index the column index (starting with 1) where to start from
     * @return the String representation of <code>resultSet</code>
     * @throws SQLException if there is an SQLException
     *                      while working with the ResultSet
     */
    public String format(ResultSet resultSet, int index)
        throws SQLException
    {
        return format(U.toVector(resultSet),index-1);
    }

    /**
     * Formats an Array
     *
     * @param objects the Array of Objects that needs to be formatted
     * @param index the index where to start from
     * @return the String representation of <code>objects</code>
     */
    public String format(Object[] objects, int index)
    {
        return format(Arrays.asList(objects),index);
    }

    /**
     * Formats a List
     *
     * @param objects the List of Objects that needs to be formatted
     * @param index the index where to start from
     * @return the String representation of <code>objects</code>
     */
    public String format(Iterable<Object> objects, int index)
    {
        return format(objects.iterator(),index);
    }

    /**
     * Formats an Iterator.
     *
     * @param objects the Iterator of Objects that needs to be formatted
     * @param index the index where to start from
     * @return the String representation of <code>objects</code>
     */
    public String format(Iterator<Object> objects, int index)
    {
        while (index-->0) objects.next();
        return format(objects);
    }

    /**
     * Formats an Iterator.
     * This just returns the string representation of the
     * first element of that Iterator,
     * so it must be overwritten by child classes.
     *
     * @param objects the Iterator of Objects that needs to be formatted
     * @return the String representation of <code>objects</code>
     */
    public String format(Iterator<Object> objects)
    {
        return U.format(format,objects.next());
    }

    /**
     * Formats a ResultSet using more formats.
     * The formats are use one after another.
     *
     * @param resultSet the ResultSet that needs to be formatted
     * @param formats a List of DataFormats containing the formats to use
     * @return a Vector containing the String representations
     *         of <code>objects</code>
     * @throws SQLException if there is an SQLException
     *                      while working with the ResultSet
     */
    public static Vector<Object> format(ResultSet resultSet,Iterable<DataFormat> formats)
        throws SQLException
    {
        return format(U.toVector(resultSet),formats);
    }

    /**
     * Formats an Array using more formats.
     * The formats are use one after another.
     *
     * @param objects the Array of Objects that needs to be formatted
     * @param formats a List of DataFormats containing the formats to use
     * @return a Vector containing the String representations
     *         of <code>objects</code>
     */
    public static Vector<Object> format(Object[] objects,Iterable<DataFormat> formats)
    {
        return format(Arrays.asList(objects),formats);
    }

    /**
     * Formats a List using more formats.
     * The formats are use one after another.
     *
     * @param objects the List of Objects that needs to be formatted
     * @param formats a List of DataFormats containing the formats to use
     * @return a Vector containing the String representations
     *         of <code>objects</code>
     */
    public static Vector<Object> format(Iterable<Object> objects,Iterable<DataFormat> formats)
    {
        return format(objects.iterator(),formats);
    }

    /**
     * Formats an Iterator using more formats.
     * The formats are use one after another.
     * <br>
     * <i>Note:</i> While a format <code>DataFormat(null)</code> means
     *              "convert to string",
     *              the format <code>null</code> means
     *              "don't convert".
     *
     * @param objects the Iterator of Objects that needs to be formatted
     * @param formats a List of DataFormats containing the formats to use
     * @return a Vector containing the String representations
     *         of <code>objects</code> and some Objects unaltered
     *         (where the format was <code>null</code>).
     */
    public static Vector<Object> format(Iterator<Object> objects,Iterable<DataFormat> formats)
    {
        Vector<Object> v=new Vector<Object>();
        for (DataFormat format : formats)
            {
                v.add(format==null?objects.next():format.format(objects));
            }
        return v;
    }

    /**
     * Formats a ResultSet using more formats.
     * The formats are use one after another for each line.
     *
     * @param objects the ResultSet that needs to be formatted
     * @param formats a List of DataFormats containing the formats to use
     * @return a Vector of Vector containing the String representations
     *         of <code>objects</code>
     * @throws SQLException if there is an SQLException
     *                      while working with the ResultSet
     */
    public static Vector<Vector<Object>> formatAll(ResultSet resultSet,Iterable<DataFormat> formats)
        throws SQLException
    {
        return formatAll(U.toVectorOfVectors(resultSet),formats);
    }

    /**
     * Formats a List of List using more formats.
     * The formats are use one after another for each line.
     *
     * @param objects the List of List of Objects that needs to be formatted
     * @param formats a List of DataFormats containing the formats to use
     * @return a Vector of Vector containing the String representations
     *         of <code>objects</code>
     */
    public static Vector<Vector<Object>> formatAll(Iterable<Vector<Object>> objects,Iterable<DataFormat> formats)
    {
        Vector<Vector<Object>> v=new Vector<Vector<Object>>();
        for (Iterable<Object> list : objects)
            v.add(format(list,formats));
        return v;
    }
}
