/*****************************************************************************\
*                                                                             *
*                                                                             *
*                         V       V         JJ  IIIII                         *
*                          V     V           J    I                           *
*                           V   V   OO       J    I                           *
*                            V V   O  O  J   J    I                           *
*                             V     OO    JJJ   IIIII                         *
*                                                                             *
*                         ---------------------------                         *
*                        |Volker's  Java Improvements|                        *
*                         ---------------------------                         *
*                                                                             *
*                                                                             *
*******************************************************************************
*                   *                                     *                   *
*                   *   FILE:         PageGraphics.java   *                   *
*                   *                                     *                   *
*                    *************************************                    *
*                                                                             *
*                                                                             *
*  This library is free software; you can redistribute it and/or              *
*  modify it under the terms of the GNU Lesser General Public                 *
*  License as published by the Free Software Foundation; either               *
*  version 2.1 of the License, or (at your option) any later version.         *
*                                                                             *
*  This library is distributed in the hope that it will be useful,            *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU          *
*  Lesser General Public License for more details.                            *
*                                                                             *
*  You should have received a copy of the GNU Lesser General Public           *
*  License along with this library; if not, write to the Free Software        *
*  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA  *
*                                                                             *
*                                                                             *
\*****************************************************************************/
package voji.utils;
import java.util.*;
import java.awt.*;

/**
 * This class coveres another <code>Graphics</code> object
 * giving printing attributes and a page number to it.
 * The page number is counted automatically.
 */
public class PageGraphics extends CopyGraphics implements PrintGraphics
{
    /**
     * Creates a new <code>PageGraphics</code> instance
     * covering the given print job
     *
     * @param printJob the <code>PrintJob</code> to cover
     */
    public PageGraphics(PrintJob printJob)
    {
        this.printJob=printJob;
    }

    /**
     * Creates a new <code>PageGraphics</code> instance as a copy
     * of the current object
     */
    public Graphics create()
    {
        PageGraphics pageGraphics=new PageGraphics(getPrintJob());
        pageGraphics.setPageNumber(getPageNumber());
        pageGraphics.setGraphics(super.create());
        return pageGraphics;
    }

    /**
     * Returns a <code>String</code> object representing this object's value
     *
     * @return a string representation of this graphics context
     */
    public String toString()
    {
        return "voji.utils.PageGraphics["+printJob+","+super.toString()+"]";
    }

    /**
     * The covered print job
     */
    protected PrintJob printJob;

    /**
     * Returns the PrintJob object from which
     * this PrintGraphics object originated
     */
    public PrintJob getPrintJob()
    {
        return printJob;
    }

    /**
     * Returns the number of pixels of the given length
     *
     * @param length the length using the current page resolutions
     * @return the number of pixels that are used by <code>length</code>
     */
    public int toPixels(double length)
    {
        return (int)Math.ceil(getPrintJob().getPageResolution()*length);
    }

    /**
     * The current page number
     */
    protected int pageNumber=0;

    /**
     * Returns the current page number
     */
    public int getPageNumber()
    {
        return pageNumber;
    }

    /**
     * Sets the current page number.
     * You normally don't need to call this function directly.
     *
     * @param number the new page number
     *
     * @see #setGraphics(Graphics)
     */
    public void setPageNumber(int number)
    {
        pageNumber=number;
    }

    /**
     * Sets the current sub <code>Graphics</code> object.
     * Each direct or indirect call to this function
     * increases the current page number.
     *
     * @param graphics the new sub <code>Graphics</code> object
     */
    public void setGraphics(Graphics graphics)
    {
        super.setGraphics(graphics);
        setPageNumber(getPageNumber()+1);
    }
}
