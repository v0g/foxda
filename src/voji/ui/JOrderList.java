/*****************************************************************************\
*                                                                             *
*                                                                             *
*                         V       V         JJ  IIIII                         *
*                          V     V           J    I                           *
*                           V   V   OO       J    I                           *
*                            V V   O  O  J   J    I                           *
*                             V     OO    JJJ   IIIII                         *
*                                                                             *
*                         ---------------------------                         *
*                        |Volker's  Java Improvements|                        *
*                         ---------------------------                         *
*                                                                             *
*                                                                             *
*******************************************************************************
*                   *                                     *                   *
*                   *   FILE:           JOrderList.java   *                   *
*                   *                                     *                   *
*                    *************************************                    *
*                                                                             *
*                                                                             *
*  This library is free software; you can redistribute it and/or              *
*  modify it under the terms of the GNU Lesser General Public                 *
*  License as published by the Free Software Foundation; either               *
*  version 2.1 of the License, or (at your option) any later version.         *
*                                                                             *
*  This library is distributed in the hope that it will be useful,            *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU          *
*  Lesser General Public License for more details.                            *
*                                                                             *
*  You should have received a copy of the GNU Lesser General Public           *
*  License along with this library; if not, write to the Free Software        *
*  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA  *
*                                                                             *
*                                                                             *
\*****************************************************************************/
package voji.ui;
import java.util.*;
import javax.swing.*;

/**
 * This is a list that allows the user to change the order of its items.
 * Each item must have an integer ID.
 * The new order of these IDs can be retrieved from this list.
 * There are simple methods for changing the order of the items.
 * These methods should be called by the Dialog/Frame that contains this list.
 */
public class JOrderList extends JList<Object>
{
    /**
     * Creates a new, empty JOrderList instance
     */
    public JOrderList()
    {
        /* set own list model */
        setModel(new OrderListModel());

        /* set a better selection mode */
        setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    }

    /**
     * Creates a new JOrderList instance with the given items
     */
    public JOrderList(Vector items)
    {
        /* other constructor */
        this();

        /* set items */
        setItems(items);
    }

    /**
     * Sets the items of this list.
     * The argument has to be a Vector of Vectors with 2 elements:
     * <UL>
     * <LI>an Integer containing and unique id
     * <LI>the Object which will be shown in the list
     * </UL>
     *
     * @param items the Vector of Vector of [Integer,Object]
     *              where to take the items from.
     */
    public void setItems(Vector items)
    {
        /* redirect to the model */
        ((OrderListModel)getModel()).setItems(items);
    }

    /**
     * Returns the IDs in their current order
     *
     * @return an Array of the IDs in the same order
     *         as their Strings in the list.
     */
    public int[] getOrder()
    {
        /* redirect to the model */
        return ((OrderListModel)getModel()).getOrder();
    }

    /**
     * Moves the current item one position up
     */
    public void moveUp()
    {
        /* move this item */
        ((OrderListModel)getModel()).moveItem(getSelectedIndex(),
                                              getSelectedIndex()-1);
    }

    /**
     * Moves the current item one position down
     */
    public void moveDown()
    {
        /* move this item */
        ((OrderListModel)getModel()).moveItem(getSelectedIndex(),
                                              getSelectedIndex()+1);
    }

    /**
     * Moves the current item to the top of this list
     */
    public void moveTop()
    {
        /* move this item */
        ((OrderListModel)getModel()).moveItem(getSelectedIndex(),0);
    }

    /**
     * Moves the current item to the bottom of this list
     */
    public void moveBottom()
    {
        /* move this item */
        ((OrderListModel)getModel()).moveItem(getSelectedIndex(),
                                              getModel().getSize()-1);
    }

    /**
     * This is the list model for OrderLists
     */
    protected class OrderListModel extends AbstractListModel<Object>
    {
        /**
         * The IDs
         */
        protected int[] ids=null;

        /**
         * The shown items
         */
        protected Object[] values=null;

        /**
         * Constructs an OrderListModel with empty data
         */
        public OrderListModel()
        {
        }

        /**
         * @return the length of the list
         */
        public int getSize()
        {
            return ids==null ? 0 : ids.length;
        }

        /**
         * @param index the index whose value has to be retrieved
         * @return the value at the specified index
         */
        public Object getElementAt(int index)
        {
            return values==null ? null : values[index];
        }

        /**
         * Sets the items of this model.
         * The argument has to be a Vector of Vectors with 2 elements:
         * <UL>
         * <LI>an Integer containing and unique id
         * <LI>the Object which will be shown in the list
         * </UL>
         *
         * @param items the Vector of Vector of [Integer,Object]
         *              where to take the items from.
         */
        public void setItems(Vector items)
        {
            /* (re-)create the arrays */
            ids=new int[items.size()];
            values=new Object[items.size()];

            /* fill the arrays */
            for (int i=0;i<items.size();i++)
                {
                    Vector v=(Vector)items.get(i);
                    ids[i]=((Number)v.get(0)).intValue();
                    values[i]=v.get(1);
                }

            /* notify the list */
            fireContentsChanged(this,0,getSize()-1);
        }

        /**
         * Returns the IDs in their current order
         *
         * @return an Array of the IDs in the same order
         *         as their Strings in the list.
         */
        public int[] getOrder()
        {
            /* return a copy of "ids" */
            return (int[])ids.clone();
        }

        /**
         * Switches the positions of two items
         *
         * @param index1
         * @param index2 the two indices of these items
         */
        protected void switchItems(int index1,int index2)
        {
            /* verify the indices */
            if (index1>=0 && index1<getSize() &&
                index2>=0 && index2<getSize() &&
                index1!=index2)
                {
                    /* switch the IDs */
                    int tmpID=ids[index1];
                    ids[index1]=ids[index2];
                    ids[index2]=tmpID;

                    /* switch the values */
                    Object tmpValue=values[index1];
                    values[index1]=values[index2];
                    values[index2]=tmpValue;

                    /* switch their selections */
                    ListSelectionModel m=getSelectionModel();
                    boolean tmp1=m.isSelectedIndex(index1);
                    boolean tmp2=m.isSelectedIndex(index2);

                    if (!tmp1) m.removeIndexInterval(index2,index2);
                    if (!tmp2) m.removeIndexInterval(index1,index1);
                    if (tmp1) m.addSelectionInterval(index2,index2);
                    if (tmp2) m.addSelectionInterval(index1,index1);
                }
        }

        /**
         * Moves an item
         *
         * @param from the index of the item to move
         * @param to the index where to move the item
         */
        public void moveItem(int from,int to)
        {
            /* verify the indices */
            if (from>=0 && from<getSize() &&
                to>=0 && to<getSize() &&
                from!=to)
                {
                    /* move it by switching some positions */
                    for (int i=(to-from)/Math.abs(to-from);from!=to;)
                        switchItems(from,from+=i);

                    /* notify the list */
                    fireContentsChanged(this,from,to);
                }
        }
    }
}
