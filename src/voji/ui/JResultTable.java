/*****************************************************************************\
*                                                                             *
*                                                                             *
*                         V       V         JJ  IIIII                         *
*                          V     V           J    I                           *
*                           V   V   OO       J    I                           *
*                            V V   O  O  J   J    I                           *
*                             V     OO    JJJ   IIIII                         *
*                                                                             *
*                         ---------------------------                         *
*                        |Volker's  Java Improvements|                        *
*                         ---------------------------                         *
*                                                                             *
*                                                                             *
*******************************************************************************
*                   *                                     *                   *
*                   *   FILE:         JResultTable.java   *                   *
*                   *                                     *                   *
*                    *************************************                    *
*                                                                             *
*                                                                             *
*  This library is free software; you can redistribute it and/or              *
*  modify it under the terms of the GNU Lesser General Public                 *
*  License as published by the Free Software Foundation; either               *
*  version 2.1 of the License, or (at your option) any later version.         *
*                                                                             *
*  This library is distributed in the hope that it will be useful,            *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU          *
*  Lesser General Public License for more details.                            *
*                                                                             *
*  You should have received a copy of the GNU Lesser General Public           *
*  License along with this library; if not, write to the Free Software        *
*  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA  *
*                                                                             *
*                                                                             *
\*****************************************************************************/
package voji.ui;
import java.util.*;
import java.sql.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.*;
import voji.utils.*;

/**
 * This is a table desinged for holding results from SQL queries
 * (given via ResultSets).
 * The first field of the ResultSet is interpreted as an ID and must be
 * an integer. The other fields are shown in the table.
 * There are methods for selecting the row of a special ID and for retrieving
 * the ID of the currently selected row.
 */
public class JResultTable extends JTable
{
    /**
     * The currently selected ID
     */
    protected int currentID=-1;

    /**
     * Creates a new JResultTable instance
     */
    public JResultTable()
    {
        /* set own table model */
        setModel(new ResultTableModel());

        /* set a better selection mode */
        setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        /* react on changed selection */
        getSelectionModel().addListSelectionListener(new ListSelectionListener(){
                public void valueChanged(ListSelectionEvent e)
                {
                    int id=((ResultTableModel)getModel()).getID(getSelectedRow());
                    if (currentID!=id)
                        {
                            currentID=id;
                            fireIDChanged();
                        }
                }
            });
    }

    /**
     * Creates a new JResultTable instance
     *
     * @param maxRows the maximum number of rows shown in this table
     *                or <code>-1</code> for no maximum
     */
    public JResultTable(int maxRows)
    {
        this();

        /* set maximum number of rows */
        setMaxRows(maxRows);
    }

    /**
     * Sets the maximum number of rows shown in this table
     *
     * @param maxRows the maximum number of rows
     *                or <code>-1</code> for no maximum
     */
    public void setMaxRows(int maxRows)
    {
        ((ResultTableModel)getModel()).setMaxRows(maxRows);
    }

    /**
     * Adds a new column to this table
     *
     * @param column the TableColumn to add
     * @param format the DataFormat to use for that column
     */
    public void addColumn(TableColumn column,DataFormat format)
    {
        /* add column */
        addColumn(column);

        /* add format */
        ((ResultTableModel)getModel()).addFormat(format);
    }

    /**
     * Adds a new column to this table
     *
     * @param header the topic of that column
     * @param renderer the renderer that has to be used for that column
     * @param width the width of that column
     */
    public void addColumn(Object header,TableCellRenderer renderer,int width,
                          DataFormat format)
    {
        /* create the column */
        TableColumn c=new TableColumn(getColumnCount(),width);
        c.setHeaderValue(header);
        c.setCellRenderer(renderer);

        /* add it */
        addColumn(c,format);
    }

    /**
     * Adds a new column to this table
     *
     * @param header the topic of that column
     * @param alignment the horizontal alignment of that column
     * @param width the width of that column
     */
    public void addColumn(Object header,int alignment,int width,
                          DataFormat format)
    {
        /* create the renderer */
        DefaultTableCellRenderer r=new DefaultTableCellRenderer();
        r.setHorizontalAlignment(alignment);

        /* add column */
        addColumn(header,r,width,format);
    }

    /**
     * Selects the row with the specified ID
     *
     * @param id the ID given by the last <code>setData()</code>
     */
    public void setSelectedID(int id)
    {
        /* get the index */
        int i=((ResultTableModel)getModel()).getRow(id);

        /* select it, if valid */
        try { setRowSelectionInterval(i,i); }
        catch (IllegalArgumentException e) {}
    }

    /**
     * Returns the ID of the currently selected row
     *
     * @return the ID of the selected row or -1 if there is no row selected
     *         or the row got an invalid ID with the last
     *         <code>setData()</code>.
     */
    public int getSelectedID()
    {
        return currentID;
    }

    /**
     * Sets the data of this table.
     * It's retrieved from the given ResultSet where
     * the first field is interpreted as the ID and not shown.
     * The other fields are shown in the same order as in the given ResultSet.
     *
     * @param resultSet the ResultSet where to take the data from
     * @throws SQLException if there is an SQLException
     *                      while working with <code>resultSet</code>
     */
    public void setData(ResultSet resultSet) throws SQLException
    {
        /* save ID */
        int id=getSelectedID();

        /* set data of the model */
        ((ResultTableModel)getModel()).setData(resultSet);

        /* restore ID */
        setSelectedID(id);
    }

    /**
     * This is the table model for ResultTables
     */
    protected class ResultTableModel extends AbstractTableModel
    {
        /**
         * The maximum number of rows
         */
        protected int maxRows=-1;

        /**
         * The formats of the columns
         */
        protected Vector<DataFormat> formats=new Vector<DataFormat>();

        /**
         * The internal data
         */
        protected Object[][] data=null;

        /**
         * Constructs a ResultTableModel with empty data
         */
        public ResultTableModel()
        {
            /* don't convert the ID field */
            formats.add(null);
        }

        /**
         * @return the number of rows
         */
        public int getRowCount()
        {
            return data==null ? 0 : data.length;
        }

        /**
         * @return the number of columns
         */
        public int getColumnCount()
        {
            return formats.size()-1;
        }

        /**
         * @param row the row whose value is to be queried
         * @param column the column whose value is to be queried
         * @return the Object at the specified cell
         */
        public Object getValueAt(int row, int column)
        {
            return data[row][column+1];
        }

        /**
         * Returns the ID of the specified row
         *
         * @param row the index of the row
         * @return the ID of the given row or -1 if the row index is invalid
         *         or the row got an invalid ID with the last
         *         <code>setData()</code>.
         */
        public int getID(int row)
        {
            try
                {
                    return ((Number)data[row][0]).intValue();
                }
            catch (Exception ex) { return -1; }
        }

        /**
         * Looks for the row with the specified ID
         *
         * @param id the ID of the row
         * @return the index of the given row or -1 if the row index is invalid
         *         or the row got an invalid ID with the last
         *         <code>setData()</code>.
         */
        public int getRow(int id)
        {
            /* search */
            for (int i=0;i<getRowCount();i++) if (getID(i)==id) return i;
            return -1;
        }

        /**
         * Sets the maximum number of rows accepted by <code>setData()</code>.
         *
         * @param maxRows the maximum number of rows
         *                or <code>-1</code> for no maximum
         */
        public void setMaxRows(int maxRows)
        {
            this.maxRows=maxRows;
        }

        /**
         * Adds a new format
         *
         * @param format the DataFormat to use for the next column
         */
        public void addFormat(DataFormat format)
        {
            formats.add(format);
        }

        /**
         * Sets the data of this model.
         * It's retrieved from the given ResultSet where
         * the first field is interpreted as the ID and not shown.
         * The other fields are shown in the same order as
         * in the given ResultSet.
         *
         * @param resultSet the ResultSet where to take the data from
         * @throws SQLException if there is an SQLException
         *                      while working with <code>resultSet</code>
         */
        public void setData(ResultSet resultSet) throws SQLException
        {
            /* get data from resultSet */
            data=U.toArrayOfArrays(DataFormat.formatAll(
                        U.toVectorOfVectors(resultSet,maxRows),formats));

            /* notify the table */
            fireTableDataChanged();
        }
    }

    /**
     * Vector of all ID listeners
     */
    private Vector<IDListener> idListeners=new Vector<IDListener>();

    /**
     * Adds an ID listener to this component
     * that's notified each time the selected ID changes
     *
     * @param listener the <code>IDListener</code> to add
     */
    public void addIDListener(IDListener listener)
    {
        /* add listener to vector of listeners */
        idListeners.add(listener);
    }

    /**
     * Let all ID listeners know that the ID has been changed
     */
    protected void fireIDChanged()
    {
        /* call idChanged() of all listeners */
        for (IDListener idListener : idListeners)
            {
                idListener.idChanged(getSelectedID());
            }
    }
}
