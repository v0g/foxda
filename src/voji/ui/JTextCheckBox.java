/*****************************************************************************\
*                                                                             *
*                                                                             *
*                         V       V         JJ  IIIII                         *
*                          V     V           J    I                           *
*                           V   V   OO       J    I                           *
*                            V V   O  O  J   J    I                           *
*                             V     OO    JJJ   IIIII                         *
*                                                                             *
*                         ---------------------------                         *
*                        |Volker's  Java Improvements|                        *
*                         ---------------------------                         *
*                                                                             *
*                                                                             *
*******************************************************************************
*                   *                                     *                   *
*                   *   FILE:        JTextCheckBox.java   *                   *
*                   *                                     *                   *
*                    *************************************                    *
*                                                                             *
*                                                                             *
*  This library is free software; you can redistribute it and/or              *
*  modify it under the terms of the GNU Lesser General Public                 *
*  License as published by the Free Software Foundation; either               *
*  version 2.1 of the License, or (at your option) any later version.         *
*                                                                             *
*  This library is distributed in the hope that it will be useful,            *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU          *
*  Lesser General Public License for more details.                            *
*                                                                             *
*  You should have received a copy of the GNU Lesser General Public           *
*  License along with this library; if not, write to the Free Software        *
*  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA  *
*                                                                             *
*                                                                             *
\*****************************************************************************/
package voji.ui;
import java.lang.*;
import java.util.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.text.*;
import voji.utils.*;

/*
 * CLASS JTextCheckBox
 */
public class JTextCheckBox extends JCheckBox
{
    /*
     * text component
     */
    private JTextComponent textComponent;

    /*
     * splitted parts of text component
     */
    private Vector<String> parts;

    /*
     * separator
     */
    public static String separator=",";

    /*
     * constructor
     */
    public JTextCheckBox(String title,JTextComponent initTextComponent)
    {
        /* call previous constructor and set title */
        super(title);

        /* initialize text component */
        textComponent=initTextComponent;

        /* update check box */
        update();

        /* react on changed text */
        textComponent.getDocument().addDocumentListener(new DocumentListener(){
                public void changedUpdate(DocumentEvent e)
                {
                    /* update check box */
                    update();
                }

                public void insertUpdate(DocumentEvent e)
                {
                    /* update check box */
                    update();
                }

                public void removeUpdate(DocumentEvent e)
                {
                    /* update check box */
                    update();
                }
            });

        /* react on click */
        addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e)
                {
                    /* get own text */
                    String s=getText();

                    /* insert/remove it */
                    if (isSelected()) parts.add(s); else parts.remove(s);

                    /* put new text into text component */
                    textComponent.setText(U.toString(parts,"",separator,""));
                }
            });
    }

    /*
     * update check box
     */
    public void update()
    {
        /* split text and save it for later use */
        parts=U.splitStringNE(textComponent.getText(),"",separator,"");

        /* select if text component contains own text */
        setSelected(parts.contains(getText()));
    }
}
