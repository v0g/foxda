/*****************************************************************************\
*                                                                             *
*                                                                             *
*                         V       V         JJ  IIIII                         *
*                          V     V           J    I                           *
*                           V   V   OO       J    I                           *
*                            V V   O  O  J   J    I                           *
*                             V     OO    JJJ   IIIII                         *
*                                                                             *
*                         ---------------------------                         *
*                        |Volker's  Java Improvements|                        *
*                         ---------------------------                         *
*                                                                             *
*                                                                             *
*******************************************************************************
*                   *                                     *                   *
*                   *   FILE:          VTableModel.java   *                   *
*                   *                                     *                   *
*                    *************************************                    *
*                                                                             *
*                                                                             *
*  This library is free software; you can redistribute it and/or              *
*  modify it under the terms of the GNU Lesser General Public                 *
*  License as published by the Free Software Foundation; either               *
*  version 2.1 of the License, or (at your option) any later version.         *
*                                                                             *
*  This library is distributed in the hope that it will be useful,            *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU          *
*  Lesser General Public License for more details.                            *
*                                                                             *
*  You should have received a copy of the GNU Lesser General Public           *
*  License along with this library; if not, write to the Free Software        *
*  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA  *
*                                                                             *
*                                                                             *
\*****************************************************************************/
package voji.ui;
import java.lang.*;
import java.util.*;
import java.sql.*;
import javax.swing.table.*;
import voji.utils.*;

/*
 * CLASS VTableModel
 */
public class VTableModel extends DefaultTableModel
{
    /*
     * status
     */
    protected boolean readOnly;

    /*
     * constructor
     */
    public VTableModel(boolean initReadOnly)
    {
        /* set whether the table model is read only */
        setReadOnly(initReadOnly);
    }

    /*
     * constructor
     */
    public VTableModel(boolean initReadOnly,Object[] columns)
    {
        /* call previous constructor and set columns and no rows */
        super(columns,0);

        /* set whether the table model is read only */
        setReadOnly(initReadOnly);
    }

    /*
     * check whether the table model is read only
     */
    public boolean isReadOnly()
    {
        /* check whether this table model is read only */
        return readOnly;
    }

    /*
     * set whether the table model is read only
     */
    public void setReadOnly(boolean initReadOnly)
    {
        /* set whether the table model is read only */
        readOnly=initReadOnly;
    }

    /*
     * check whether cell is editable
     */
    public boolean isCellEditable(int row,int column)
    {
        /* check whether cell is read only; if not, the super class decides */
        return !readOnly && super.isCellEditable(row,column);
    }

    /*
     * set raw data vector
     */
    public void setRawDataVector(Vector data) throws SQLException
    {
        /* set data vector */
        dataVector=data;

        /* notify all listeners */
        fireTableDataChanged();
    }

    /*
     * set raw data vector from SQL result
     */
    public void setRawDataVector(ResultSet result) throws SQLException
    {
        /* set raw data vector */
        setRawDataVector(U.toVectorOfVectors(result));
    }

    /*
     * set data vector from SQL result
     */
    public void setDataVector(ResultSet result) throws SQLException
    {
        /* set data vector */
        setDataVector(U.toVectorOfVectors(result),U.getColumnNames(result));
    }

    /*
     * search a row from a vector
     */
    public int searchRow(Vector row)
    {
        /* look for the row in the data vector */
        return dataVector.indexOf(row);
    }

    /*
     * search a row from result set
     */
    public int searchRow(ResultSet result) throws SQLException
    {
        /* if there exists a result, convert it and call other searchRow */
        return result.next() ? searchRow(U.toVector(result)) : -1;
    }
}
