/*****************************************************************************\
*                                                                             *
*                                                                             *
*                         V       V         JJ  IIIII                         *
*                          V     V           J    I                           *
*                           V   V   OO       J    I                           *
*                            V V   O  O  J   J    I                           *
*                             V     OO    JJJ   IIIII                         *
*                                                                             *
*                         ---------------------------                         *
*                        |Volker's  Java Improvements|                        *
*                         ---------------------------                         *
*                                                                             *
*                                                                             *
*******************************************************************************
*                   *                                     *                   *
*                   *   FILE:     JFormatTextField.java   *                   *
*                   *                                     *                   *
*                    *************************************                    *
*                                                                             *
*                                                                             *
*  This library is free software; you can redistribute it and/or              *
*  modify it under the terms of the GNU Lesser General Public                 *
*  License as published by the Free Software Foundation; either               *
*  version 2.1 of the License, or (at your option) any later version.         *
*                                                                             *
*  This library is distributed in the hope that it will be useful,            *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU          *
*  Lesser General Public License for more details.                            *
*                                                                             *
*  You should have received a copy of the GNU Lesser General Public           *
*  License along with this library; if not, write to the Free Software        *
*  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA  *
*                                                                             *
*                                                                             *
\*****************************************************************************/
package voji.ui;
import java.lang.*;
import java.util.*;
import java.text.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.text.*;
import voji.utils.*;

/*
 * CLASS JFormatTextField
 */
public class JFormatTextField extends JTextField
{
    /*
     * format
     */
    private Format format;

    /*
     * internal object
     */
    private Object object;

    /*
     * constructor
     */
    public JFormatTextField(int maxLength)
    {
        this(maxLength,false,null);
    }

    /*
     * constructor
     */
    public JFormatTextField(boolean toUpperCase)
    {
        this(-1,toUpperCase,null);
    }

    /*
     * constructor
     */
    public JFormatTextField(Format format)
    {
        this(-1,false,format);
    }

    /*
     * constructor
     */
    public JFormatTextField(int maxLength,boolean toUpperCase)
    {
        this(maxLength,toUpperCase,null);
    }

    /*
     * constructor
     */
    public JFormatTextField(int maxLength,Format format)
    {
        this(maxLength,false,format);
    }

    /*
     * constructor
     */
    public JFormatTextField(boolean toUpperCase,Format format)
    {
        this(-1,toUpperCase,format);
    }

    /*
     * constructor
     */
    public JFormatTextField(int maxLength,boolean toUpperCase,Format format)
    {
        /* react on changed text */
        Document doc=new FormatTextFieldDocument(maxLength,toUpperCase);
        doc.addDocumentListener(new DocumentListener(){
                public void changedUpdate(DocumentEvent e)
                {
                    /* update object */
                    update();
                }
                public void insertUpdate(DocumentEvent e)
                {
                    /* update object */
                    update();
                }
                public void removeUpdate(DocumentEvent e)
                {
                    /* update object */
                    update();
                }
            });
        setDocument(doc);

        /* set columns */
        if (maxLength>=0) setColumns(maxLength);

        /* set format */
        setFormat(format);
    }

    public class FormatTextFieldDocument extends PlainDocument
    {
        public int maxLength;
        public boolean toUpperCase;

        FormatTextFieldDocument(int maxLength,boolean toUpperCase)
        {
            setMaxLength(maxLength);
            setToUpperCase(toUpperCase);
        }

        public int getMaxLength()
        {
            return maxLength;
        }

        public void setMaxLength(int maxLength)
        {
            this.maxLength=maxLength;
        }

        public boolean getToUpperCase()
        {
            return toUpperCase;
        }

        public void setToUpperCase(boolean toUpperCase)
        {
            this.toUpperCase=toUpperCase;
        }

        public void insertString(int offset,String str,AttributeSet attr)
            throws BadLocationException
        {
            if (str==null) return;
            if (maxLength>=0 && getLength()+str.length()>maxLength) return;
            if (toUpperCase) str=str.toUpperCase();
            super.insertString(offset,str,attr);
        }
    }

    /*
     * get format
     */
    public Format getFormat()
    {
        /* return format */
        return format;
    }

    /*
     * set format
     */
    public void setFormat(Format newFormat)
    {
        /* set format */
        format=newFormat;

        /* reformat */
        setObject(object);
    }

    public int getMaxLength()
    {
        return ((FormatTextFieldDocument)getDocument()).getMaxLength();
    }

    public void setMaxLength(int maxLength)
    {
        ((FormatTextFieldDocument)getDocument()).setMaxLength(maxLength);
    }

    public boolean getToUpperCase()
    {
        return ((FormatTextFieldDocument)getDocument()).getToUpperCase();
    }

    public void setToUpperCase(boolean toUpperCase)
    {
        ((FormatTextFieldDocument)getDocument()).setToUpperCase(toUpperCase);
    }

    /*
     * get object
     */
    public Object getObject()
    {
        /* return object */
        return object;
    }

    /*
     * set object
     */
    public void setObject(Object newObject)
    {
        /* set string */
        setText(U.format(format,newObject));

        /* fire change event */
        fireObjectChanged();
    }

    /*
     * update object
     */
    private String lastString;
    protected void update()
    {
        /* get text */
        String s=getText();

        /* check whether text has changed */
        if (!s.equals(lastString))
            {
                /* save last string */
                lastString=s;

                /* format */
                object=U.parseObject(format,s);

                /* fire change event */
                fireObjectChanged();
            }
    }

    /*
     * vector of all listeners
     */
    private Vector<ObjectListener> listeners=new Vector<ObjectListener>();

    /*
     * add a listener
     */
    public void addObjectListener(ObjectListener listener)
    {
        /* add listener to vector of listeners */
        listeners.add(listener);
    }

    /*
     * let all listeners know that the object has been changed
     */
    protected void fireObjectChanged()
    {
        /* call objectChanged() of all listeners */
        for (ObjectListener listener : listeners)
            {
                listener.objectChanged(object);
            }
    }
}
