/*****************************************************************************\
*                                                                             *
*                                                                             *
*                         V       V         JJ  IIIII                         *
*                          V     V           J    I                           *
*                           V   V   OO       J    I                           *
*                            V V   O  O  J   J    I                           *
*                             V     OO    JJJ   IIIII                         *
*                                                                             *
*                         ---------------------------                         *
*                        |Volker's  Java Improvements|                        *
*                         ---------------------------                         *
*                                                                             *
*                                                                             *
*******************************************************************************
*                   *                                     *                   *
*                   *   FILE:        JPrintPreview.java   *                   *
*                   *                                     *                   *
*                    *************************************                    *
*                                                                             *
*                                                                             *
*  This library is free software; you can redistribute it and/or              *
*  modify it under the terms of the GNU Lesser General Public                 *
*  License as published by the Free Software Foundation; either               *
*  version 2.1 of the License, or (at your option) any later version.         *
*                                                                             *
*  This library is distributed in the hope that it will be useful,            *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU          *
*  Lesser General Public License for more details.                            *
*                                                                             *
*  You should have received a copy of the GNU Lesser General Public           *
*  License along with this library; if not, write to the Free Software        *
*  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA  *
*                                                                             *
*                                                                             *
\*****************************************************************************/
package voji.ui;
import java.util.*;
import java.awt.*;
import java.awt.image.*;
import javax.swing.*;
import javax.swing.event.*;
import voji.utils.*;

/**
 * This is a preview for print jobs.
 *
 * You can print as usual using a <code>PrintJob<code> object
 * which is returned by one of the <code>getPrintJob()</code> methods.
 */
public class JPrintPreview extends JComponent
{
    protected JList<Integer> list;
    protected DefaultListModel<Integer> listModel;
    protected JLabel label;
    protected boolean locked=false;
    protected Vector<Image> pages=new Vector<Image>();

    /**
     * Creates a new JPrintPreview instance
     */
    public JPrintPreview()
    {
        /* initialize */
        setLayout(new GridLayout(1,1));

        /* create list */
        listModel=new DefaultListModel<Integer>();
        list=new JList<Integer>(listModel);
        list.addListSelectionListener(new ListSelectionListener(){
                public void valueChanged(ListSelectionEvent e)
                {
                    int i=list.getSelectedIndex();
                    label.setIcon(i<0?null:
                                  new ImageIcon((Image)pages.elementAt(i)));
                }
            });

        /* create label */
        label=new JLabel();

        /* add sub components */
        add(new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,
                           new JScrollPane(list),new JScrollPane(label)));
    }

    /**
     * Creates a print job which draws to this print preview.
     *
     * It internally creates an empty print job using
     * <code>java.awt.Toolkit</code> and uses it as a template
     * for the returned print job.
     *
     * @param frame the parent of the print dialog.
     * @param jobtitle the title of the print job
     * @param props a Properties object containing zero or more properties
     *
     * @return a <code>CopyPrintJob</code> object which draws
     *         to this print preview
     */
    public CopyPrintJob getPrintJob(Frame frame,String jobtitle,
                                    Properties props)
    {
        /* only one print job may print */
        if (locked) return null;

        /* use a "real" print job as template */
        return getPrintJob(getToolkit().getPrintJob(frame,jobtitle,props));
    }

    /**
     * Creates a print job which draws to this print preview
     *
     * It internally creates an empty print job using
     * <code>java.awt.Toolkit</code> and uses it as a template
     * for the returned print job.
     *
     * @param frame the parent of the print dialog
     * @param jobtitle the title of the print job
     * @param jobAttributes a set of job attributes
     * @param pageAttributes a set of page attributes
     *
     * @return a <code>CopyPrintJob</code> object which draws
     *         to this print preview
     */
    public CopyPrintJob getPrintJob(Frame frame,String jobtitle,
                                    JobAttributes jobAttributes,
                                    PageAttributes pageAttributes)
    {
        /* only one print job may print */
        if (locked) return null;

        /* use a "real" print job as template */
        return getPrintJob(getToolkit().getPrintJob(frame,jobtitle,
                                                    jobAttributes,
                                                    pageAttributes));
    }

    /**
     * Creates a print job which draws to this print preview
     * using the specified print job as a template.
     *
     * @param templatePrintJob the <code>PrintJob</code> that should
     *                         be used as a template for this print job.
     *
     * @return a <code>CopyPrintJob</code> object which draws
     *         to this print preview
     */
    public CopyPrintJob getPrintJob(PrintJob templatePrintJob)
    {
        /* only one print job may print */
        if (locked) return null;

        /* remove all pages */
        pages.removeAllElements();
        listModel.removeAllElements();

        /* create a new print job */
        return new CopyPrintJob(new PrintPreviewPrintJob(templatePrintJob));
    }

    /**
     * This is the table model for ResultTables
     */
    protected class PrintPreviewPrintJob extends PrintJob
    {
        /**
         * The template print job
         */
        protected PrintJob templatePrintJob;

        /**
         * The internal page graphics object
         */
        protected PageGraphics pageGraphics;

        /**
         * Constructs a PrintPreviewPrintJob using the given template.
         *
         * It behaves as the template print job, but it draws the
         * pages into a JPrintPreview.
         *
         * @param templatePrintJob the <code>PrintJob</code> that should
         *                         be used as a template for this print job.
         */
        public PrintPreviewPrintJob(PrintJob templatePrintJob)
        {
            /* set template */
            this.templatePrintJob=templatePrintJob;

            /* set graphics */
            pageGraphics=new PageGraphics(this);

            /* lock preview */
            locked=false;
        }

        /**
         * Gets a Graphics object that will draw to the next page.
         *
         * This graphics object will <b>not</b> implement the PrintGraphics
         * interface, so you should embed it into a
         * {@link voji.utils.CopyPrintJob CopyPrintJob}.
         *
         * @return a <code>PageGraphics</code> object
         *         which draws to the next page.
         */
        public Graphics getGraphics()
        {
            /* create image */
            Dimension dim=getPageDimension();
            BufferedImage image=new BufferedImage(dim.width,dim.height,
                                                  BufferedImage.TYPE_INT_RGB);

            /* add image to page list of the preview */
            pages.add(image);
            int size=listModel.size();
            listModel.addElement(size+1);
            if (size==0) list.setSelectedIndex(0);

            /* create, initialize and return the graphics object */
            Graphics graphics=image.createGraphics();
            graphics.setColor(Color.white);
            graphics.fillRect(0,0,dim.width,dim.height);
            graphics.setColor(Color.black);
            return graphics;
        }

        /**
         * Returns the dimensions of the page in pixels
         */
        public Dimension getPageDimension()
        {
            double fac=
                getPageResolution()/templatePrintJob.getPageResolution();

            Dimension dim=templatePrintJob.getPageDimension();
            dim.width*=fac;
            dim.height*=fac;

            // workaround for a really strange bug
            if (dim.width == 0 && dim.height == 0) {
                dim.width  = 1000;
                dim.height = 1000;
            }

            return dim;
        }

        /**
         * Returns the resolution of the page in pixels per inch.
         *
         * Since this is a preview, this will return the screen resolution.
         */
        public int getPageResolution()
        {
            return getToolkit().getScreenResolution();
        }

        /**
         * Returns true if the last page will be printed first.
         *
         * Since this is a preview, this will always return <code>false</code>.
         */
        public boolean lastPageFirst()
        {
            return false;
        }

        /**
         * Ends the print job and does any necessary cleanup.
         *
         * This method unlocks the preview.
         * Only unlocked previews can create a new print job.
         */
        public void end()
        {
            locked=false;
        }
    }

    /**
     * Prints out all pages.
     *
     * Note that this will print bitmaps of screen resolution,
     * so the output might look very ugly.
     *
     * It's a much better idea to print into a printer's print job
     * the same way you printed into the preview's print job.
     *
     * @param printJob the <code>PrintJob</code> where to print into
     */
    public void print(PrintJob printJob)
    {
        /* print out all pages */
        for (Image page : pages)
            {
                printJob.getGraphics().drawImage(page,0,0,this);
            }
    }
}
