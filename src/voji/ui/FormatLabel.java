/*****************************************************************************\
*                                                                             *
*                                                                             *
*                         V       V         JJ  IIIII                         *
*                          V     V           J    I                           *
*                           V   V   OO       J    I                           *
*                            V V   O  O  J   J    I                           *
*                             V     OO    JJJ   IIIII                         *
*                                                                             *
*                         ---------------------------                         *
*                        |Volker's  Java Improvements|                        *
*                         ---------------------------                         *
*                                                                             *
*                                                                             *
*******************************************************************************
*                   *                                     *                   *
*                   *   FILE:          FormatLabel.java   *                   *
*                   *                                     *                   *
*                    *************************************                    *
*                                                                             *
*                                                                             *
*  This library is free software; you can redistribute it and/or              *
*  modify it under the terms of the GNU Lesser General Public                 *
*  License as published by the Free Software Foundation; either               *
*  version 2.1 of the License, or (at your option) any later version.         *
*                                                                             *
*  This library is distributed in the hope that it will be useful,            *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU          *
*  Lesser General Public License for more details.                            *
*                                                                             *
*  You should have received a copy of the GNU Lesser General Public           *
*  License along with this library; if not, write to the Free Software        *
*  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA  *
*                                                                             *
*                                                                             *
\*****************************************************************************/
package voji.ui;
import java.lang.*;
import java.text.*;
import java.awt.*;
import voji.utils.*;

/*
 * CLASS FormatLabel
 */
public class FormatLabel extends Label
{
    /*
     * format
     */
    private Format format;

    /*
     * internal object
     */
    private Object object;

    /*
     * constructor
     */
    public FormatLabel(Format initFormat)
    {
        /* set format */
        setFormat(initFormat);
    }

    /*
     * get format
     */
    public Format getFormat()
    {
        /* return format */
        return format;
    }

    /*
     * set format
     */
    public void setFormat(Format newFormat)
    {
        /* set format */
        format=newFormat;

        /* reformat */
        setObject(object);
    }

    /*
     * get object
     */
    public Object getObject()
    {
        /* return object */
        return object;
    }

    /*
     * set object
     */
    public void setObject(Object newObject)
    {
        /* set string */
        setText(U.format(format,object=newObject));
    }
}
