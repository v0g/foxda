/*****************************************************************************\
*                                                                             *
*                                                                             *
*                         V       V         JJ  IIIII                         *
*                          V     V           J    I                           *
*                           V   V   OO       J    I                           *
*                            V V   O  O  J   J    I                           *
*                             V     OO    JJJ   IIIII                         *
*                                                                             *
*                         ---------------------------                         *
*                        |Volker's  Java Improvements|                        *
*                         ---------------------------                         *
*                                                                             *
*                                                                             *
*******************************************************************************
*                   *                                     *                   *
*                   *   FILE:   FormatCellRenderer.java   *                   *
*                   *                                     *                   *
*                    *************************************                    *
*                                                                             *
*                                                                             *
*  This library is free software; you can redistribute it and/or              *
*  modify it under the terms of the GNU Lesser General Public                 *
*  License as published by the Free Software Foundation; either               *
*  version 2.1 of the License, or (at your option) any later version.         *
*                                                                             *
*  This library is distributed in the hope that it will be useful,            *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU          *
*  Lesser General Public License for more details.                            *
*                                                                             *
*  You should have received a copy of the GNU Lesser General Public           *
*  License along with this library; if not, write to the Free Software        *
*  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA  *
*                                                                             *
*                                                                             *
\*****************************************************************************/
package voji.ui;
import java.lang.*;
import java.text.*;
import java.awt.*;
import javax.swing.*;
import javax.swing.table.*;
import voji.utils.*;

/*
 * CLASS FormatCellRenderer
 */
public class FormatCellRenderer extends DefaultTableCellRenderer
{
    /*
     * format
     */
    private Format format;

    /*
     * constructor
     */
    public FormatCellRenderer(Format initFormat)
    {
        /* set format */
        setFormat(initFormat);
    }

    /*
     * constructor
     */
    public FormatCellRenderer(Format initFormat,int alignment)
    {
        /* call other constructor */
        this(initFormat);

        /* set alignment */
        setHorizontalAlignment(alignment);
    }

    /*
     * get format
     */
    public Format getFormat()
    {
        /* return format */
        return format;
    }

    /*
     * set format
     */
    public void setFormat(Format newFormat)
    {
        /* set format */
        format=newFormat;
    }

    /*
     * get table cell renderer
     */
    public Component getTableCellRendererComponent(JTable table,
                                                   Object value,
                                                   boolean isSelected,
                                                   boolean hasFocus,
                                                   int row,int column)
    {
        return super.getTableCellRendererComponent(table,
                                                   U.format(format,value),
                                                   isSelected,hasFocus,
                                                   row,column);
    }
}
