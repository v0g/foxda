/*****************************************************************************\
*                                                                             *
*                                                                             *
*                         V       V         JJ  IIIII                         *
*                          V     V           J    I                           *
*                           V   V   OO       J    I                           *
*                            V V   O  O  J   J    I                           *
*                             V     OO    JJJ   IIIII                         *
*                                                                             *
*                         ---------------------------                         *
*                        |Volker's  Java Improvements|                        *
*                         ---------------------------                         *
*                                                                             *
*                                                                             *
*******************************************************************************
*                   *                                     *                   *
*                   *   FILE:          LogTextArea.java   *                   *
*                   *                                     *                   *
*                    *************************************                    *
*                                                                             *
*                                                                             *
*  This library is free software; you can redistribute it and/or              *
*  modify it under the terms of the GNU Lesser General Public                 *
*  License as published by the Free Software Foundation; either               *
*  version 2.1 of the License, or (at your option) any later version.         *
*                                                                             *
*  This library is distributed in the hope that it will be useful,            *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU          *
*  Lesser General Public License for more details.                            *
*                                                                             *
*  You should have received a copy of the GNU Lesser General Public           *
*  License along with this library; if not, write to the Free Software        *
*  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA  *
*                                                                             *
*                                                                             *
\*****************************************************************************/
package voji.log;
import java.lang.*;
import java.awt.*;

/*
 * CLASS LogTextArea
 */
public class LogTextArea extends Log
{
    /*
     * text area
     */
    private TextArea textArea;

    /*
     * constructor
     */
    public LogTextArea(TextArea initTextArea)
    {
        /* set text area */
        textArea=initTextArea;
    }

    /*
     * create log text area
     */
    public static TextArea create()
    {
        /* create text area */
        TextArea r=new TextArea();

        /* initialize text area */
        r.setEditable(false);

        /* create log */
        new LogTextArea(r);

        /* return text area */
        return r;
    }

    /*
     * write to log
     */
    protected void write(String message) throws Exception
    {
        /* append message */
        textArea.append(message);

        /* scroll to the end */
        textArea.setCaretPosition(textArea.getText().length());
    }
}
