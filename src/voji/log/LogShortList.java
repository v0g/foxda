/*****************************************************************************\
*                                                                             *
*                                                                             *
*                         V       V         JJ  IIIII                         *
*                          V     V           J    I                           *
*                           V   V   OO       J    I                           *
*                            V V   O  O  J   J    I                           *
*                             V     OO    JJJ   IIIII                         *
*                                                                             *
*                         ---------------------------                         *
*                        |Volker's  Java Improvements|                        *
*                         ---------------------------                         *
*                                                                             *
*                                                                             *
*******************************************************************************
*                   *                                     *                   *
*                   *   FILE:         LogShortList.java   *                   *
*                   *                                     *                   *
*                    *************************************                    *
*                                                                             *
*                                                                             *
*  This library is free software; you can redistribute it and/or              *
*  modify it under the terms of the GNU Lesser General Public                 *
*  License as published by the Free Software Foundation; either               *
*  version 2.1 of the License, or (at your option) any later version.         *
*                                                                             *
*  This library is distributed in the hope that it will be useful,            *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU          *
*  Lesser General Public License for more details.                            *
*                                                                             *
*  You should have received a copy of the GNU Lesser General Public           *
*  License along with this library; if not, write to the Free Software        *
*  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA  *
*                                                                             *
*                                                                             *
\*****************************************************************************/
package voji.log;
import java.lang.*;
import java.awt.*;

/*
 * CLASS LogShortList
 */
public class LogShortList extends ShortLog
{
    /*
     * list
     */
    private List list;

    /*
     * constructor
     */
    public LogShortList(List initList,int initCount)
    {
        /* call previous constructor and set count */
        super(initCount);

        /* set list */
        list=initList;
    }

    /*
     * constructor
     */
    public LogShortList(List initList)
    {
        /* call other constructor and set log list and count */
        this(initList,initList.getRows());
    }

    /*
     * create log list
     */
    public static List create()
    {
        /* create list */
        List r=new List();

        /* create log */
        new LogShortList(r);

        /* return list */
        return r;
    }

    /*
     * create log list
     */
    public static List create(int initCount)
    {
        /* create list */
        List r=new List(initCount);

        /* create log */
        new LogShortList(r,initCount);

        /* return list */
        return r;
    }

    /*
     * write parts to log
     */
    protected void writeShort(String[] messages) throws Exception
    {
        /* clear list */
        list.removeAll();

        /* add messages */
        for (int i=0;i<messages.length;i++) list.add(messages[i]);
    }
}
