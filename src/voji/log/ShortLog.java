/*****************************************************************************\
*                                                                             *
*                                                                             *
*                         V       V         JJ  IIIII                         *
*                          V     V           J    I                           *
*                           V   V   OO       J    I                           *
*                            V V   O  O  J   J    I                           *
*                             V     OO    JJJ   IIIII                         *
*                                                                             *
*                         ---------------------------                         *
*                        |Volker's  Java Improvements|                        *
*                         ---------------------------                         *
*                                                                             *
*                                                                             *
*******************************************************************************
*                   *                                     *                   *
*                   *   FILE:             ShortLog.java   *                   *
*                   *                                     *                   *
*                    *************************************                    *
*                                                                             *
*                                                                             *
*  This library is free software; you can redistribute it and/or              *
*  modify it under the terms of the GNU Lesser General Public                 *
*  License as published by the Free Software Foundation; either               *
*  version 2.1 of the License, or (at your option) any later version.         *
*                                                                             *
*  This library is distributed in the hope that it will be useful,            *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU          *
*  Lesser General Public License for more details.                            *
*                                                                             *
*  You should have received a copy of the GNU Lesser General Public           *
*  License along with this library; if not, write to the Free Software        *
*  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA  *
*                                                                             *
*                                                                             *
\*****************************************************************************/
package voji.log;
import java.lang.*;
import java.util.*;

/*
 * CLASS ShortLog
 */
public abstract class ShortLog extends SplitLog
{
    /*
     * parts
     */
    String[] parts;

    /*
     * constructor
     */
    public ShortLog(int initCount)
    {
        /* initialize count */
        setCount(initCount);
    }

    /*
     * constructor
     */
    public ShortLog(int initCount,boolean initFromHistory)
    {
        /* call previous constructor */
        super(initFromHistory);

        /* initialize count */
        setCount(initCount);
    }

    /*
     * constructor
     */
    public ShortLog(int initCount,boolean initFromHistory,boolean addToList)
    {
        /* call previous constructor */
        super(initFromHistory,addToList);

        /* initialize count */
        setCount(initCount);
    }

    /*
     * get count
     */
    protected int getCount()
    {
        /* return count */
        return parts.length;
    }

    /*
     * string to use for empty messages
     */
    protected String empty="";

    /*
     * set count
     */
    protected void setCount(int initCount)
    {
        /* create parts */
        parts=new String[initCount];

        /* initialize parts */
        Arrays.fill(parts,empty);
    }

    /*
     * write parts to log
     */
    protected void writeParts(String[] messages) throws Exception
    {
        /* create new string array for the new parts */
        String[] s=new String[getCount()];

        /* copy parts */
        int i=s.length-1;
        for (int j=messages.length-1;i>=0&&j>=0;i--,j--) s[i]=messages[j];
        for (int j=parts.length-1;i>=0&&j>=0;i--,j--) s[i]=parts[j];

        /* write shorts to log */
        writeShort(s);

        /* if no exception until now, copy to parts */
        parts=s;
    }

    /*
     * write shorts to log (must be overwritten)
     */
    protected abstract void writeShort(String[] messages) throws Exception;
}
