/*****************************************************************************\
*                                                                             *
*                                                                             *
*                         V       V         JJ  IIIII                         *
*                          V     V           J    I                           *
*                           V   V   OO       J    I                           *
*                            V V   O  O  J   J    I                           *
*                             V     OO    JJJ   IIIII                         *
*                                                                             *
*                         ---------------------------                         *
*                        |Volker's  Java Improvements|                        *
*                         ---------------------------                         *
*                                                                             *
*                                                                             *
*******************************************************************************
*                   *                                     *                   *
*                   *   FILE:             JLogList.java   *                   *
*                   *                                     *                   *
*                    *************************************                    *
*                                                                             *
*                                                                             *
*  This library is free software; you can redistribute it and/or              *
*  modify it under the terms of the GNU Lesser General Public                 *
*  License as published by the Free Software Foundation; either               *
*  version 2.1 of the License, or (at your option) any later version.         *
*                                                                             *
*  This library is distributed in the hope that it will be useful,            *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU          *
*  Lesser General Public License for more details.                            *
*                                                                             *
*  You should have received a copy of the GNU Lesser General Public           *
*  License along with this library; if not, write to the Free Software        *
*  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA  *
*                                                                             *
*                                                                             *
\*****************************************************************************/
package voji.log;
import java.lang.*;
import javax.swing.*;

/*
 * CLASS JLogList
 */
public class JLogList extends SplitLog
{
    /*
     * list model
     */
    private DefaultListModel<String> listModel=new DefaultListModel<String>();

    /*
     * constructor
     */
    public JLogList(JList<String> initList)
    {
        /* set list model */
        initList.setModel(listModel);
    }

    /*
     * create log list
     */
    public static JList<String> create()
    {
        /* create list */
        JList<String> r=new JList<String>();

        /* create log */
        new JLogList(r);

        /* return list */
        return r;
    }

    /*
     * write parts to log
     */
    protected void writeParts(String[] messages) throws Exception
    {
        /* append messages */
        for (int i=0;i<messages.length;i++) listModel.addElement(messages[i]);
    }
}
