/*****************************************************************************\
*                                                                             *
*                                                                             *
*                         V       V         JJ  IIIII                         *
*                          V     V           J    I                           *
*                           V   V   OO       J    I                           *
*                            V V   O  O  J   J    I                           *
*                             V     OO    JJJ   IIIII                         *
*                                                                             *
*                         ---------------------------                         *
*                        |Volker's  Java Improvements|                        *
*                         ---------------------------                         *
*                                                                             *
*                                                                             *
*******************************************************************************
*                   *                                     *                   *
*                   *   FILE:             SplitLog.java   *                   *
*                   *                                     *                   *
*                    *************************************                    *
*                                                                             *
*                                                                             *
*  This library is free software; you can redistribute it and/or              *
*  modify it under the terms of the GNU Lesser General Public                 *
*  License as published by the Free Software Foundation; either               *
*  version 2.1 of the License, or (at your option) any later version.         *
*                                                                             *
*  This library is distributed in the hope that it will be useful,            *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU          *
*  Lesser General Public License for more details.                            *
*                                                                             *
*  You should have received a copy of the GNU Lesser General Public           *
*  License along with this library; if not, write to the Free Software        *
*  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA  *
*                                                                             *
*                                                                             *
\*****************************************************************************/
package voji.log;
import java.lang.*;
import java.util.*;
import voji.utils.*;

/*
 * CLASS SplitLog
 */
public abstract class SplitLog extends Log
{
    /*
     * constructor
     */
    public SplitLog()
    {
    }

    /*
     * constructor
     */
    public SplitLog(boolean initFromHistory)
    {
        /* call previous constructor */
        super(initFromHistory);
    }

    /*
     * constructor
     */
    public SplitLog(boolean initFromHistory,boolean addToList)
    {
        /* call previous constructor */
        super(initFromHistory,addToList);
    }

    /*
     * write to log
     */
    protected void write(String message) throws Exception
    {
        /* write result of splitted message */
        writeParts(U.toStringArray(split(message)));
    }

    /*
     * split string (should be overwritten in special cases)
     */
    protected Vector<String> split(String message)
    {
        /* return splitted message */
        return U.splitString(message,"",msgSeparator,msgSeparator);
    }

    /*
     * write parts to log (must be overwritten)
     */
    protected abstract void writeParts(String[] messages) throws Exception;
}
