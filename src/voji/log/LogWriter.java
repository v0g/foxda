/*****************************************************************************\
*                                                                             *
*                                                                             *
*                         V       V         JJ  IIIII                         *
*                          V     V           J    I                           *
*                           V   V   OO       J    I                           *
*                            V V   O  O  J   J    I                           *
*                             V     OO    JJJ   IIIII                         *
*                                                                             *
*                         ---------------------------                         *
*                        |Volker's  Java Improvements|                        *
*                         ---------------------------                         *
*                                                                             *
*                                                                             *
*******************************************************************************
*                   *                                     *                   *
*                   *   FILE:            LogWriter.java   *                   *
*                   *                                     *                   *
*                    *************************************                    *
*                                                                             *
*                                                                             *
*  This library is free software; you can redistribute it and/or              *
*  modify it under the terms of the GNU Lesser General Public                 *
*  License as published by the Free Software Foundation; either               *
*  version 2.1 of the License, or (at your option) any later version.         *
*                                                                             *
*  This library is distributed in the hope that it will be useful,            *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU          *
*  Lesser General Public License for more details.                            *
*                                                                             *
*  You should have received a copy of the GNU Lesser General Public           *
*  License along with this library; if not, write to the Free Software        *
*  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA  *
*                                                                             *
*                                                                             *
\*****************************************************************************/
package voji.log;
import java.lang.*;
import java.io.*;

/*
 * CLASS LogWriter
 */
public class LogWriter extends Log
{
    /*
     * log writer
     */
    private Writer writer;

    /*
     * constructor
     */
    public LogWriter(Writer initWriter)
    {
        /* set writer */
        writer=initWriter;
    }

    /*
     * constructor
     */
    public LogWriter(OutputStream initOutputStream)
    {
        /* call other constructor */
        this(new OutputStreamWriter(initOutputStream));
    }

    /*
     * constructor
     */
    public LogWriter(String initFileName) throws IOException
    {
        /* call other constructor */
        this(new FileOutputStream(initFileName,true));
    }

    /*
     * constructor
     */
    public LogWriter(File initFile) throws IOException
    {
        /* call other constructor */
        this(initFile.getAbsolutePath());
    }

    /*
     * write to log
     */
    protected void write(String message) throws Exception
    {
        /* write message */
        writer.write(message);

        /* flush */
        writer.flush();
    }
}
