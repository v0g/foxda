/*****************************************************************************\
*                                                                             *
*                                                                             *
*                         V       V         JJ  IIIII                         *
*                          V     V           J    I                           *
*                           V   V   OO       J    I                           *
*                            V V   O  O  J   J    I                           *
*                             V     OO    JJJ   IIIII                         *
*                                                                             *
*                         ---------------------------                         *
*                        |Volker's  Java Improvements|                        *
*                         ---------------------------                         *
*                                                                             *
*                                                                             *
*******************************************************************************
*                   *                                     *                   *
*                   *   FILE:            LogLabels.java   *                   *
*                   *                                     *                   *
*                    *************************************                    *
*                                                                             *
*                                                                             *
*  This library is free software; you can redistribute it and/or              *
*  modify it under the terms of the GNU Lesser General Public                 *
*  License as published by the Free Software Foundation; either               *
*  version 2.1 of the License, or (at your option) any later version.         *
*                                                                             *
*  This library is distributed in the hope that it will be useful,            *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU          *
*  Lesser General Public License for more details.                            *
*                                                                             *
*  You should have received a copy of the GNU Lesser General Public           *
*  License along with this library; if not, write to the Free Software        *
*  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA  *
*                                                                             *
*                                                                             *
\*****************************************************************************/
package voji.log;
import java.lang.*;
import java.awt.*;

/*
 * CLASS LogLabels
 */
public class LogLabels extends ShortLog
{
    /*
     * labels
     */
    Label[] labels;

    /*
     * constructor
     */
    public LogLabels(Label[] initLabels)
    {
        /* call previous constructor and set count */
        super(initLabels.length);

        /* initialize labels */
        labels=initLabels;
    }

    /*
     * constructor
     */
    public LogLabels(Label initLabel)
    {
        /* call other constructor */
        this(new Label[]{initLabel});
    }

    /*
     * write short to log
     */
    protected void writeShort(String[] messages) throws Exception
    {
        /* set text of log labels */
        for (int i=0;i<labels.length;i++) labels[i].setText(messages[i]);
    }
}
