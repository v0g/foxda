/*****************************************************************************\
*                                                                             *
*                                                                             *
*                         V       V         JJ  IIIII                         *
*                          V     V           J    I                           *
*                           V   V   OO       J    I                           *
*                            V V   O  O  J   J    I                           *
*                             V     OO    JJJ   IIIII                         *
*                                                                             *
*                         ---------------------------                         *
*                        |Volker's  Java Improvements|                        *
*                         ---------------------------                         *
*                                                                             *
*                                                                             *
*******************************************************************************
*                   *                                     *                   *
*                   *   FILE:                  Log.java   *                   *
*                   *                                     *                   *
*                    *************************************                    *
*                                                                             *
*                                                                             *
*  This library is free software; you can redistribute it and/or              *
*  modify it under the terms of the GNU Lesser General Public                 *
*  License as published by the Free Software Foundation; either               *
*  version 2.1 of the License, or (at your option) any later version.         *
*                                                                             *
*  This library is distributed in the hope that it will be useful,            *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU          *
*  Lesser General Public License for more details.                            *
*                                                                             *
*  You should have received a copy of the GNU Lesser General Public           *
*  License along with this library; if not, write to the Free Software        *
*  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA  *
*                                                                             *
*                                                                             *
\*****************************************************************************/
package voji.log;
import java.lang.*;
import java.util.*;
import java.text.*;
import java.io.*;

/*
 * CLASS Log
 */
public abstract class Log
{
    /*
     * log list
     */
    private static Vector<Log> logs=new Vector<Log>();

    /*
     * log history
     */
    private static Vector<Object> history=new Vector<Object>();

    /*
     * log buffer
     */
    private StringBuffer buffer=new StringBuffer();

    /*
     * constructor
     */
    public Log(boolean initFromHistory,boolean addToList)
    {
        if (initFromHistory)
            {
                /* initialize from history */
                for (Object obj : history)
                    appendToBuffer(obj);
                logHere();
            }

        if (addToList)
            {
                /* add to log list */
                logs.add(this);
            }
    }

    /*
     * constructor
     */
    public Log(boolean initFromHistory)
    {
        /* call other constructor */
        this(initFromHistory,true);
    }

    /*
     * constructor
     */
    public Log()
    {
        /* call other constructor */
        this(true);
    }

    /*
     * write out all buffers
     */
    public static void log()
    {
        /* write out buffer of all logs */
        for (Log log : logs)
            log.logHere();
    }

    /*
     * write out buffer
     */
    public void logHere()
    {
        /* if there's data to store, ... */
        if (buffer.length()>0)
            {
                /* try logging */
                try
                    {
                        /* write to log */
                        write(buffer.toString());

                        /* clear buffer */
                        buffer.setLength(0);
                    }

                /* ignore exception */
                catch (Exception e) {}
            }
    }

    /*
     * log an object
     */
    public static void log(Object object)
    {
        /* append object to log history */
        history.add(object);

        /* log object into all logs */
        for (Log log : logs)
            log.logHere(object);
    }

    /*
     * log an object
     */
    public void logHere(Object object)
    {
        /* append to buffer */
        appendToBuffer(object);

        /* write out buffer */
        logHere();
    }

    /*
     * append object to buffer
     */
    private void appendToBuffer(Object object)
    {
        /* append formatted object to buffer */
        buffer.append(format(object));
    }

    /*
     * separator between the messages
     */
    protected String msgSeparator=System.getProperty("line.separator");
    protected String dateSeparator=": ";

    /*
     * format object (should be overwritten in special cases)
     */
    protected String format(Object object)
    {
        String msg="";
        try
            {
                /* try to print it out as a Throwable */
                Throwable t=(Throwable)object;

                StringWriter w=new StringWriter();
                PrintWriter p=new PrintWriter(w);
                t.printStackTrace(p);
                p.flush();
                w.flush();
                msg = w.toString();
            }
        catch (ClassCastException e)
            {
                /* if there are problems, only convert it to a string */
                msg = object==null ? "<null>" : object.toString();
            }
        return
            DateFormat.getDateTimeInstance(DateFormat.LONG,DateFormat.LONG).
            format(new Date())
            +dateSeparator
            +msg
            +msgSeparator;
    }

    /*
     * write to log (must be overwritten)
     */
    protected abstract void write(String message) throws Exception;
}
