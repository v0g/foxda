/*****************************************************************************\
*                                                                             *
*                                                                             *
*                         V       V         JJ  IIIII                         *
*                          V     V           J    I                           *
*                           V   V   OO       J    I                           *
*                            V V   O  O  J   J    I                           *
*                             V     OO    JJJ   IIIII                         *
*                                                                             *
*                         ---------------------------                         *
*                        |Volker's  Java Improvements|                        *
*                         ---------------------------                         *
*                                                                             *
*                                                                             *
*******************************************************************************
*                   *                                     *                   *
*                   *   FILE:              VDialog.java   *                   *
*                   *                                     *                   *
*                    *************************************                    *
*                                                                             *
*                                                                             *
*  This library is free software; you can redistribute it and/or              *
*  modify it under the terms of the GNU Lesser General Public                 *
*  License as published by the Free Software Foundation; either               *
*  version 2.1 of the License, or (at your option) any later version.         *
*                                                                             *
*  This library is distributed in the hope that it will be useful,            *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU          *
*  Lesser General Public License for more details.                            *
*                                                                             *
*  You should have received a copy of the GNU Lesser General Public           *
*  License along with this library; if not, write to the Free Software        *
*  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA  *
*                                                                             *
*                                                                             *
\*****************************************************************************/
package voji.vdialog;
import java.lang.*;
import java.util.*;
import java.awt.*;
import javax.swing.*;
import voji.log.*;

/*
 * CLASS VDialog
 */
public class VDialog
{
    /*
     * internal class
     */
    private static Class internClass;

    /*
     * set internal class
     */
    public static Class setInternClass(Class initInternClass)
    {
        /* if class is valid, set it */
        if (VDialogInterface.class.isAssignableFrom(initInternClass))
            internClass=initInternClass;

        /* return current internal class */
        return internClass;
    }

    /*
     * set internal class
     */
    public static Class setInternClass(String initInternClass)
        throws ClassNotFoundException
    {
        /* set and return current internal class */
        return setInternClass(Class.forName(initInternClass));
    }

    /*
     * get instance of internal class
     */
    protected static VDialogInterface newInternClassInstance()
    {
        /* create new instance and cast it */
        try { return (VDialogInterface)internClass.newInstance(); }

        /* return null if there are any problems */
        catch (Exception e) { return null; }
    }

    /*
     * internal dialog
     */
    private final VDialogInterface internDialog=newInternClassInstance();

    /*
     * content pane
     */
    protected final Container contentPane=internDialog.getContentPane();

    /*
     * constructor
     */
    public VDialog(String title,int sizeX,int sizeY,LayoutManager layout)
    {
        /* initialize internal dialog */
        internDialog.setTitle(title);
        internDialog.setLocation(new Point(0,50));
        internDialog.setSize(new Dimension(sizeX,sizeY));

        /* initialize content pane */
        contentPane.setLayout(layout);

        /* create progress bar */
        progressBar=new JProgressBar();
        progressBar.setString("");
        progressBar.setStringPainted(true);
    }

    /*
     * show dialog
     */
    public void show()
    {
        /* show internal dialog */
        internDialog.show();

        /* try to initialize */
        doInit();
    }

    /*
     * close dialog
     */
    public void close()
    {
        /* close internal dialog */
        internDialog.close();
    }

    /*
     * says how strongly the dialog's initialization is locked
     */
    private int initLock=0;

    /*
     * lock the init
     */
    protected void lockInit()
    {
        /* increase init lock number */
        initLock++;
    }

    /*
     * release the init
     */
    protected void releaseInit()
    {
        /* decrease init lock number */
        initLock--;
    }

    /*
     * try to initialize dialog
     */
    public void doInit()
    {
        /* initialize only if it's possible */
        if (initLock<=0) init();
    }

    /*
     * initialize dialog (should be overwritten in special cases)
     */
    protected void init()
    {
    }

    /*
     * enabling components vector
     */
    private Vector<Component> enabling=new Vector<Component>();

    /*
     * add an enabling component
     */
    protected void addEnabling(Component component)
    {
        /* insert into enabling components vector */
        enabling.add(component);
    }

    /*
     * set whether the components are enabled
     */
    private Component lastFocus=null;
    public void setEnabled(boolean enabled)
    {
        /* special actions before disabling */
        if (!enabled)
            {
                /* save that the dialog can't be initialized */
                lockInit();

                /* save focus */
                lastFocus=internDialog.getFocusOwner();
            }

        /* enable/disable all enabling components */
        for (Component component : enabling)
            component.setEnabled(enabled);

        /* special actions after enabling */
        if (enabled)
            {
                /* request last focus */
                if (lastFocus!=null) lastFocus.requestFocus();

                /* save that the dialog can be initialized */
                releaseInit();
            }
    }

    /*
     * progress bar
     */
    protected final JProgressBar progressBar;

    /*
     * error message
     */
    public static final String ERROR="ERROR";

    /*
     * SUB-CLASS Job
     */
    protected abstract class Job extends Thread
    {
        /*
         * maximum of progress bar
         */
        private int maximum;

        /*
         * constructor
         */
        public Job(int initMaximum)
        {
            /* save maximum of progress bar */
            maximum=initMaximum;
        }

        /*
         * run method
         */
        public void run()
        {
            /* disable all components */
            setEnabled(false);

            /* set maximum of progress bar */
            progressBar.setMaximum(maximum);

            /* clear progress bar */
            progressBar.setValue(0);

            /* try to do the job */
            try
                {
                    /* do the job itself */
                    doJob();

                    /* clear progress bar if successful */
                    progressBar.setString("");
                    progressBar.setValue(0);
                }

            /* on exception ... */
            catch (Exception e)
                {
                    /* log it */
                    Log.log(e);

                    /* display it */
                    progressBar.setString(ERROR);
                }

            /* enable all components */
            setEnabled(true);

            /* do after job work */
            afterJob();
        }

        /*
         * set progress bar to the next step
         */
        public void nextStep()
        {
            /* increment value of progress bar */
            progressBar.setValue(progressBar.getValue()+1);
        }

        /*
         * set progress bar to the next step and change it's description
         */
        public void nextStep(String description)
        {
            /* change description */
            progressBar.setString(description);

            /* set progress bar to the next step */
            nextStep();
        }

        /*
         * do after job work (should be overwritten in special cases)
         */
        public void afterJob()
        {
        }

        /*
         * do the job itself (must be overwritten)
         */
        public abstract void doJob() throws Exception;
    }
}
