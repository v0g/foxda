/*****************************************************************************\
*                                                                             *
*                                                                             *
*                         V       V         JJ  IIIII                         *
*                          V     V           J    I                           *
*                           V   V   OO       J    I                           *
*                            V V   O  O  J   J    I                           *
*                             V     OO    JJJ   IIIII                         *
*                                                                             *
*                         ---------------------------                         *
*                        |Volker's  Java Improvements|                        *
*                         ---------------------------                         *
*                                                                             *
*                                                                             *
*******************************************************************************
*                   *                                     *                   *
*                   *   FILE:                VMain.java   *                   *
*                   *                                     *                   *
*                    *************************************                    *
*                                                                             *
*                                                                             *
*  This library is free software; you can redistribute it and/or              *
*  modify it under the terms of the GNU Lesser General Public                 *
*  License as published by the Free Software Foundation; either               *
*  version 2.1 of the License, or (at your option) any later version.         *
*                                                                             *
*  This library is distributed in the hope that it will be useful,            *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU          *
*  Lesser General Public License for more details.                            *
*                                                                             *
*  You should have received a copy of the GNU Lesser General Public           *
*  License along with this library; if not, write to the Free Software        *
*  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA  *
*                                                                             *
*                                                                             *
\*****************************************************************************/
package voji.vdialog;
import java.lang.*;
import java.util.*;
import java.awt.*;
import javax.swing.*;
import voji.log.*;

/*
 * CLASS VMain
 */
public class VMain
{
    /*
     * internal dialog
     */
    private static JFrame internDialog;

    /*
     * important components
     */
    protected static JMenuBar menuBar;
    protected static JToolBar toolBar;
    private static Container desktop;

    /*
     * standard size
     */
    private static Dimension size;

    /*
     * constructor
     */
    public VMain(String title,Dimension initSize)
    {
        /* initialize standard size */
        size=initSize;

        /* initialize internal dialog */
        internDialog=new JFrame(title);
        internDialog.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        internDialog.getContentPane().setLayout(new BorderLayout());

        /* initialize important components */
        internDialog.setJMenuBar(menuBar=new JMenuBar());
        internDialog.getContentPane().add(toolBar=new JToolBar(),
                                          BorderLayout.NORTH);
        internDialog.getContentPane().add(desktop=new JDesktopPane());
    }

    /*
     * constructor
     */
    public VMain(String title,int sizeX,int sizeY)
    {
        /* call other constructor */
        this(title,new Dimension(sizeX,sizeY));
    }

    /*
     * constructor
     */
    public VMain(String title)
    {
        /* call other constructor and set default size to screen size */
        this(title,Toolkit.getDefaultToolkit().getScreenSize());
    }

    /*
     * get parent frame
     */
    public static Frame getOwner()
    {
        /* return parent */
        return internDialog;
    }

    /*
     * add component
     */
    public static void add(Component component)
    {
        /* add component */
        desktop.add(component);
    }

    /*
     * types
     */
    public static final int HIDDEN=0;
    public static final int PACKED=1;
    public static final int NORMAL=2;
    public static final int DESKTOP=3;

    /*
     * current type
     */
    private static int type=-1;

    /*
     * set type
     */
    public static int setType(int initType)
    {
        /* if this type isn't already set ... */
        if (type!=initType) switch (type=initType)
            {
            case HIDDEN:
                internDialog.setVisible(false);
                break;
            case PACKED:
                internDialog.setResizable(true);
                internDialog.setVisible(true);
                toolBar.setFloatable(false);
                internDialog.setLocation(0,0);
                internDialog.pack();
                internDialog.setResizable(false);
                internDialog.repaint();
                break;
            case NORMAL:
                internDialog.setResizable(true);
                internDialog.setVisible(true);
                toolBar.setFloatable(true);
                internDialog.setLocation(0,0);
                internDialog.setSize(size);
                internDialog.setVisible(true);
                break;
            case DESKTOP:
                internDialog.setResizable(true);
                internDialog.setVisible(true);
                toolBar.setFloatable(true);
                internDialog.setLocation(0,0);
                internDialog.setSize(size);
                internDialog.setResizable(false);
                internDialog.setVisible(true);
                break;
            default:
                type=-1;
            }

        /* return current type */
        return type;
    }
}
