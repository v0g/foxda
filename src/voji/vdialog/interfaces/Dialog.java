/*****************************************************************************\
*                                                                             *
*                                                                             *
*                         V       V         JJ  IIIII                         *
*                          V     V           J    I                           *
*                           V   V   OO       J    I                           *
*                            V V   O  O  J   J    I                           *
*                             V     OO    JJJ   IIIII                         *
*                                                                             *
*                         ---------------------------                         *
*                        |Volker's  Java Improvements|                        *
*                         ---------------------------                         *
*                                                                             *
*                                                                             *
*******************************************************************************
*                   *                                     *                   *
*                   *   FILE:               Dialog.java   *                   *
*                   *                                     *                   *
*                    *************************************                    *
*                                                                             *
*                                                                             *
*  This library is free software; you can redistribute it and/or              *
*  modify it under the terms of the GNU Lesser General Public                 *
*  License as published by the Free Software Foundation; either               *
*  version 2.1 of the License, or (at your option) any later version.         *
*                                                                             *
*  This library is distributed in the hope that it will be useful,            *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU          *
*  Lesser General Public License for more details.                            *
*                                                                             *
*  You should have received a copy of the GNU Lesser General Public           *
*  License along with this library; if not, write to the Free Software        *
*  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA  *
*                                                                             *
*                                                                             *
\*****************************************************************************/
package voji.vdialog.interfaces;
import java.lang.*;
import java.awt.*;
import java.awt.event.*;
import voji.vdialog.*;

/*
 * CLASS Dialog
 */
public class Dialog extends java.awt.Dialog implements VDialogInterface
{
    /*
     * constructor
     */
    public Dialog()
    {
        /* initialize */
        super(VMain.getOwner());
        addWindowListener(new WindowAdapter(){
                public void windowClosing(WindowEvent e)
                {
                    setVisible(false);
                }
            });
    }

    /*
     * close
     */
    public void close()
    {
        /* hide */
        setVisible(false);
    }

    /*
     * get content pane
     */
    public Container getContentPane()
    {
        /* return this as content pane */
        return this;
    }
}
