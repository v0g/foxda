/*****************************************************************************\
*                                                                             *
*  FoxdaFoxda      Foxda      Foxda   Foxda  FoxdaFoxda          Foxda        *
*  Foxda        Foxda Foxda    Foxda Foxda   Foxda  Foxda     Foxda Foxda     *
*  FoxdaFoxda  Foxda   Foxda      Foxda      Foxda   Foxda   Foxda   Foxda    *
*  Foxda        Foxda Foxda    Foxda Foxda   Foxda  Foxda   FoxdaFoxdaFoxda   *
*  Foxda           Foxda      Foxda   Foxda  FoxdaFoxda    Foxda       Foxda  *
*                                                                             *
*******************************************************************************
*                   *                                     *                   *
*                   *   FILE:          OrderDialog.java   *                   *
*                   *                                     *                   *
*                    *************************************                    *
*                                                                             *
*                                                                             *
*      This program is free software; you can redistribute it and/or          *
*      modify it under the terms of the GNU General Public License as         *
*      published by the Free Software Foundation; either version 2 of         *
*      the License, or (at your option) any later version.                    *
*                                                                             *
*      This program is distributed in the hope that it will be useful,        *
*      but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the           *
*      GNU General Public License for more details.                           *
*                                                                             *
*      You should have received a copy of the GNU General Public License      *
*      along with this program; if not, write to the Free Software            *
*      Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.              *
*                                                                             *
*                                                                             *
\*****************************************************************************/
package foxda;
import java.lang.*;
import java.util.*;
import java.sql.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import voji.utils.*;
import voji.log.*;
import voji.db.*;
import voji.ui.*;

/**
 *
 */
public class OrderDialog extends JDialog
{
    public OrderDialog()
    {
    }

    public OrderDialog(String orderSQL)
    {
        this();
        setOrderSQL(orderSQL);
    }

    public OrderDialog(String orderSQL,Vector items)
    {
        this(orderSQL);
        setItems(items);
    }

    public OrderDialog(String orderSQL,String itemsSQL) throws SQLException
    {
        this(orderSQL);
        setItems(itemsSQL);
    }

    protected JOrderList orderList;
    protected VPreparedStatement orderStmt;

    public Dimension getPreferredSize()
    {
        return new Dimension(300,450);
    }

    protected void dialogInit()
    {
        JButton b;

        super.dialogInit();
        setModal(true);
        setTitle("Sortieren");
        getContentPane().setLayout(new BorderLayout());

        orderList=new JOrderList();
        getContentPane().add(new JScrollPane(orderList),BorderLayout.CENTER);

        JPanel aroundMovePanel=new JPanel();
        aroundMovePanel.setLayout(new BorderLayout());
        getContentPane().add(aroundMovePanel,BorderLayout.EAST);

        JPanel movePanel=new JPanel();
        movePanel.setLayout(new GridLayout(0,1));
        movePanel.setBorder(BorderFactory.createRaisedBevelBorder());
        aroundMovePanel.add(movePanel,BorderLayout.NORTH);

        b=new JButton(Icons.NORTH_STOP);
        b.setMnemonic(KeyEvent.VK_HOME);
        b.setMargin(new Insets(0,0,0,0));
        b.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e)
                {
                    orderList.moveTop();
                    orderList.requestFocus();
                }
            });
        movePanel.add(b);

        b=new JButton(Icons.NORTH);
        b.setMnemonic(KeyEvent.VK_UP);
        b.setMargin(new Insets(0,0,0,0));
        b.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e)
                {
                    orderList.moveUp();
                    orderList.requestFocus();
                }
            });
        movePanel.add(b);

        b=new JButton(Icons.SOUTH);
        b.setMnemonic(KeyEvent.VK_DOWN);
        b.setMargin(new Insets(0,0,0,0));
        b.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e)
                {
                    orderList.moveDown();
                    orderList.requestFocus();
                }
            });
        movePanel.add(b);

        b=new JButton(Icons.SOUTH_STOP);
        b.setMnemonic(KeyEvent.VK_END);
        b.setMargin(new Insets(0,0,0,0));
        b.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e)
                {
                    orderList.moveBottom();
                    orderList.requestFocus();
                }
            });
        movePanel.add(b);

        JPanel buttonPanel=new JPanel();
        buttonPanel.setLayout(new GridLayout(1,0));
        buttonPanel.setBorder(BorderFactory.createRaisedBevelBorder());
        getContentPane().add(buttonPanel,BorderLayout.SOUTH);

        b=new JButton("OK");
        b.setMnemonic('O');
        b.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e)
                {
                    setOrder();
                    setVisible(false);
                }
            });
        buttonPanel.add(b);
        getRootPane().setDefaultButton(b);

        b=new JButton("Abbrechen");
        b.setMnemonic('A');
        b.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e)
                {
                    setVisible(false);
                }
            });
        buttonPanel.add(b);

        pack();
    }

    public void setOrderSQL(String orderSQL)
    {
        orderStmt=new VPreparedStatement(orderSQL);
    }

    public void setItems(Vector items)
    {
        orderList.setItems(items);
    }

    public void setItems(String itemsSQL) throws SQLException
    {
        setItems(U.toVectorOfVectors(Database.executeQuery(itemsSQL)));
    }

    protected void setOrder()
    {
        try
            {
                int[] r=orderList.getOrder();
                for (int i=0;i<r.length;i++)
                    orderStmt.executeUpdate(new Object[]{
                        i,
                        r[i]
                    });
            }
        catch (Exception e) { Log.log(e); }
    }
}
