/*****************************************************************************\
*                                                                             *
*  FoxdaFoxda      Foxda      Foxda   Foxda  FoxdaFoxda          Foxda        *
*  Foxda        Foxda Foxda    Foxda Foxda   Foxda  Foxda     Foxda Foxda     *
*  FoxdaFoxda  Foxda   Foxda      Foxda      Foxda   Foxda   Foxda   Foxda    *
*  Foxda        Foxda Foxda    Foxda Foxda   Foxda  Foxda   FoxdaFoxdaFoxda   *
*  Foxda           Foxda      Foxda   Foxda  FoxdaFoxda    Foxda       Foxda  *
*                                                                             *
*******************************************************************************
*                   *                                     *                   *
*                   *   FILE:           EditDialog.java   *                   *
*                   *                                     *                   *
*                    *************************************                    *
*                                                                             *
*                                                                             *
*      This program is free software; you can redistribute it and/or          *
*      modify it under the terms of the GNU General Public License as         *
*      published by the Free Software Foundation; either version 2 of         *
*      the License, or (at your option) any later version.                    *
*                                                                             *
*      This program is distributed in the hope that it will be useful,        *
*      but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the           *
*      GNU General Public License for more details.                           *
*                                                                             *
*      You should have received a copy of the GNU General Public License      *
*      along with this program; if not, write to the Free Software            *
*      Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.              *
*                                                                             *
*                                                                             *
\*****************************************************************************/
package foxda;
import java.lang.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/**
 *
 */
public abstract class EditDialog extends JDialog
{
    protected EditDialog(Dialog parent)
    {
        super(parent);
    }

    protected EditDialog(Frame parent)
    {
        super(parent);
    }

    private JPanel contentPanel;
    private int currentID;

    protected void dialogInit()
    {
        JButton b;

        super.dialogInit();
        setModal(true);
        getContentPane().setLayout(new BorderLayout());

        contentPanel=new JPanel();
        getContentPane().add(contentPanel,BorderLayout.CENTER);

        JPanel closePanel=new JPanel();
        closePanel.setLayout(new GridLayout(1,0));
        closePanel.setBorder(BorderFactory.createRaisedBevelBorder());
        getContentPane().add(closePanel,BorderLayout.SOUTH);

        b=new JButton("OK");
        getRootPane().setDefaultButton(b);
        b.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e)
                {
                    if (currentID<0)
                        currentID=insertData();
                    else
                        updateData(currentID);
                    setVisible(false);
                }
            });
        closePanel.add(b);

        b=new JButton("Abbrechen");
        b.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e)
                {
                    setVisible(false);
                }
            });
        closePanel.add(b);
    }

    public int showInsert()
    {
        currentID=-1;
        newData();
        initComponents();
        super.setVisible(true);
        return currentID;
    }

    public void showUpdate(int id)
    {
        selectData(currentID=id);
        initComponents();
        super.setVisible(true);
    }

    public void showDelete(int id)
    {
        if (Main.msgConfirm("Soll dieser Datensatz wirklich gelöscht werden?"))
            {
                deleteData(id);
            }
    }

    protected int getID()
    {
        return currentID;
    }

    protected JPanel getContentPanel()
    {
        return contentPanel;
    }

    protected void initComponents() {};
    protected void newData() {};
    protected abstract void selectData(int id);
    protected abstract int insertData();
    protected abstract void updateData(int id);
    protected abstract void deleteData(int id);
}
