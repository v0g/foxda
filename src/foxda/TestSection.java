/*****************************************************************************\
*                                                                             *
*  FoxdaFoxda      Foxda      Foxda   Foxda  FoxdaFoxda          Foxda        *
*  Foxda        Foxda Foxda    Foxda Foxda   Foxda  Foxda     Foxda Foxda     *
*  FoxdaFoxda  Foxda   Foxda      Foxda      Foxda   Foxda   Foxda   Foxda    *
*  Foxda        Foxda Foxda    Foxda Foxda   Foxda  Foxda   FoxdaFoxdaFoxda   *
*  Foxda           Foxda      Foxda   Foxda  FoxdaFoxda    Foxda       Foxda  *
*                                                                             *
*******************************************************************************
*                   *                                     *                   *
*                   *   FILE:          TestSection.java   *                   *
*                   *                                     *                   *
*                    *************************************                    *
*                                                                             *
*                                                                             *
*      This program is free software; you can redistribute it and/or          *
*      modify it under the terms of the GNU General Public License as         *
*      published by the Free Software Foundation; either version 2 of         *
*      the License, or (at your option) any later version.                    *
*                                                                             *
*      This program is distributed in the hope that it will be useful,        *
*      but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the           *
*      GNU General Public License for more details.                           *
*                                                                             *
*      You should have received a copy of the GNU General Public License      *
*      along with this program; if not, write to the Free Software            *
*      Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.              *
*                                                                             *
*                                                                             *
\*****************************************************************************/
package foxda;
import java.lang.*;
import java.sql.*;
import java.io.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import voji.log.*;
import voji.db.*;

/**
 * This is the test section of foxda.
 * Here the user can perform various tests for the database.
 */
public class TestSection extends JComponent
{
    public TestSection()
    {
        JButton b;

        setLayout(new BorderLayout());

        JPanel generalPanel=new JPanel();
        generalPanel.setLayout(new GridLayout(1,0));
        generalPanel.setBorder(BorderFactory.createTitledBorder(
                               BorderFactory.createEtchedBorder(),
                               "Allgemein",
                               TitledBorder.CENTER,TitledBorder.TOP));
        add(generalPanel,BorderLayout.NORTH);

        b=new JButton("SELECT-Test");
        b.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e)
                {
                    new Thread(){
                        public void run()
                        {
                            testSelect();
                        }
                    }.start();
                }
            });
        generalPanel.add(b);

        b=new JButton("Link-Test");
        b.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e)
                {
                    new Thread(){
                        public void run()
                        {
                            testLink();
                        }
                    }.start();
                }
            });
        generalPanel.add(b);
    }

    protected void testSelect()
    {
        String b="";
        int len=Main.tableInfo.length;
        ProgressMonitor pm=new ProgressMonitor(this,"Teste ...","",0,len);
        pm.setMillisToDecideToPopup(0);
        pm.setMillisToPopup(0);
        for (int i=0;i<len;i++)
            {
                if (pm.isCanceled())
                    {
                        Main.msgInfo("Test abgebrochen");
                        pm.close();
                        return;
                    }

                Main.TableInfo t=Main.tableInfo[i];
                pm.setNote(t.name);

                try
                    {
                        ResultSet r=t.select.executeQuery(new Object[]{});
                        int l=r.getMetaData().getColumnCount();
                        while (r.next())
                            for (int j=1;j<=l;j++)
                                r.getString(j);
                    }
                catch (SQLException e) { Log.log(e); b+=" "+t.name; }

                pm.setProgress(i+1);
            }

        if (b.length()>0)
            Main.msgError("Nicht alle Tabellen sind korrekt!\n"+
                          "Betroffene Tabellen:"+b);
        else
            Main.msgInfo("Alle Tabellen haben den Test bestanden");

        pm.close();
    }

    protected void testLink()
    {
        String b="";
        int len=Main.linkInfo.length;
        ProgressMonitor pm=new ProgressMonitor(this,"Teste ...","",0,len);
        pm.setMillisToDecideToPopup(0);
        pm.setMillisToPopup(0);
        try
            {
                for (int i=0;i<len;i++)
                    {
                        if (pm.isCanceled())
                            {
                                Main.msgInfo("Test abgebrochen");
                                pm.close();
                                return;
                            }

                        String[] l=Main.linkInfo[i];
                        ResultSet r;
                        Object n;

                        pm.setNote(l[0]+"."+l[1]+" => "+l[2]+"."+l[3]);

                        r=Database.executeQuery(
                            "SELECT COUNT(*) "+
                            "FROM "+l[2]);
                        if (!r.next()) throw new SQLException();
                        n=r.getObject(1);

                        r=new VPreparedStatement(
                            "SELECT t.id "+
                            "FROM "+l[0]+" t,"+l[2]+" l "+
                            "WHERE t."+l[1]+"!=l."+l[3]+" "+
                            "GROUP BY t.id "+
                            "HAVING COUNT(*)!=?-1").
                            executeQuery(new Object[]{n});
                        if (r.next())
                            {
                                b+="\n"+l[0]+"."+l[1]+" => "+l[2]+"."+l[3];
                                b+=" at "+l[0]+".id ("+r.getString(1);
                                while (r.next())
                                    b+=","+r.getString(1);
                                b+=")";
                            }

                        pm.setProgress(i+1);
                    }

                if (b.length()>0)
                    Main.msgError("Folgende Links sind fehlerhaft:\n"+b);
                else
                    Main.msgInfo("Alle Links sind korrekt");
            }
        catch (Exception e)
            {
                Log.log(e);
                Main.msgError("Link-Test konnte nicht vollständig "+
                              "durchgeführt werden!\nZwischenergebnis:\n"+b);
            }
        pm.close();
    }
}
