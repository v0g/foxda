/*****************************************************************************\
*                                                                             *
*  FoxdaFoxda      Foxda      Foxda   Foxda  FoxdaFoxda          Foxda        *
*  Foxda        Foxda Foxda    Foxda Foxda   Foxda  Foxda     Foxda Foxda     *
*  FoxdaFoxda  Foxda   Foxda      Foxda      Foxda   Foxda   Foxda   Foxda    *
*  Foxda        Foxda Foxda    Foxda Foxda   Foxda  Foxda   FoxdaFoxdaFoxda   *
*  Foxda           Foxda      Foxda   Foxda  FoxdaFoxda    Foxda       Foxda  *
*                                                                             *
*******************************************************************************
*                   *                                     *                   *
*                   *   FILE:           ListDialog.java   *                   *
*                   *                                     *                   *
*                    *************************************                    *
*                                                                             *
*                                                                             *
*      This program is free software; you can redistribute it and/or          *
*      modify it under the terms of the GNU General Public License as         *
*      published by the Free Software Foundation; either version 2 of         *
*      the License, or (at your option) any later version.                    *
*                                                                             *
*      This program is distributed in the hope that it will be useful,        *
*      but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the           *
*      GNU General Public License for more details.                           *
*                                                                             *
*      You should have received a copy of the GNU General Public License      *
*      along with this program; if not, write to the Free Software            *
*      Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.              *
*                                                                             *
*                                                                             *
\*****************************************************************************/
package foxda;
import java.lang.*;
import java.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import voji.utils.*;
import voji.ui.*;

/**
 *
 */
public abstract class ListDialog extends JDialog
{
    protected ListDialog(Dialog parent)
    {
        super(parent);
    }

    protected ListDialog(Frame parent)
    {
        super(parent);
    }

    private EditDialog editDialog;
    private JResultTable resultTable;
    private JButton orderButton;
    private int returnID;
    private JLabel
        infoLabel;
    private JPanel
        closePanel,
        searchPanel,
        specialPanel;

    protected JButton specialButton=null;

    protected void dialogInit()
    {
        JButton b;

        super.dialogInit();
        setModal(true);

        getContentPane().setLayout(new BorderLayout());

        resultTable=new JResultTable();
        resultTable.getInputMap().
            put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER,0),"ok");
        resultTable.getActionMap().put("ok",new AbstractAction(){
                public void actionPerformed(ActionEvent e)
                {
                    returnID=getSelectedID();
                    setVisible(false);
                }
            });
        getContentPane().add(new JScrollPane(resultTable),BorderLayout.CENTER);

        JPanel southPanel=new JPanel();
        southPanel.setLayout(new BorderLayout());
        getContentPane().add(southPanel,BorderLayout.SOUTH);

        JPanel editPanel=new JPanel();
        editPanel.setLayout(new BorderLayout());
        southPanel.add(editPanel,BorderLayout.NORTH);

        searchPanel=new JPanel();
        searchPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
        editPanel.add(searchPanel,BorderLayout.WEST);

        infoLabel=new JLabel("",JLabel.RIGHT);
        editPanel.add(infoLabel,BorderLayout.CENTER);

        JPanel eastPanel=new JPanel();
        eastPanel.setLayout(new BorderLayout());
        editPanel.add(eastPanel,BorderLayout.EAST);

        JPanel actionPanel=new JPanel();
        actionPanel.setLayout(new GridLayout(1,0));
        actionPanel.setBorder(BorderFactory.createRaisedBevelBorder());
        eastPanel.add(actionPanel,BorderLayout.NORTH);

        if (specialButton!=null)
            {
                specialButton.setMargin(new Insets(0,0,0,0));
                specialButton.addActionListener(new ActionListener(){
                        public void actionPerformed(ActionEvent e)
                        {
                            special();
                        }
                    });
                actionPanel.add(specialButton);
                actionPanel.add(new JLabel());
            }

        b=new JButton(Icons.NEW);
        b.setToolTipText("Neuer Datensatz");
        b.setMnemonic(KeyEvent.VK_INSERT);
        b.setMargin(new Insets(0,0,0,0));
        b.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e)
                {
                    insert();
                }
            });
        actionPanel.add(b);

        b=new JButton(Icons.OPEN);
        b.setToolTipText("Datensatz bearbeiten");
        b.setMnemonic(KeyEvent.VK_ENTER);
        b.setMargin(new Insets(0,0,0,0));
        b.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e)
                {
                    update();
                }
            });
        actionPanel.add(b);

        b=new JButton(Icons.TRASH);
        b.setToolTipText("Datensatz löschen");
        b.setMnemonic(KeyEvent.VK_DELETE);
        b.setMargin(new Insets(0,0,0,0));
        b.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e)
                {
                    delete();
                }
            });
        actionPanel.add(b);

        JPanel buttonPanel=new JPanel();
        buttonPanel.setLayout(new BorderLayout());
        southPanel.add(buttonPanel,BorderLayout.SOUTH);

        specialPanel=new JPanel();
        specialPanel.setBorder(BorderFactory.createCompoundBorder(
            BorderFactory.createEmptyBorder(8,8,8,8),
            BorderFactory.createLoweredBevelBorder()));
        buttonPanel.add(specialPanel,BorderLayout.CENTER);

        closePanel=new JPanel();
        closePanel.setLayout(new CardLayout());
        closePanel.setBorder(BorderFactory.createRaisedBevelBorder());
        buttonPanel.add(closePanel,BorderLayout.SOUTH);

        b=new JButton("Schließen");
        b.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e)
                {
                    setVisible(false);
                }
            });
        closePanel.add(b,"CLOSE");

        JPanel okcancelPanel=new JPanel();
        okcancelPanel.setLayout(new GridLayout(1,0));
        closePanel.add(okcancelPanel,"OKCANCEL");

        b=new JButton("OK");
        b.setMnemonic('O');
        b.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e)
                {
                    returnID=getSelectedID();
                    setVisible(false);
                }
            });
        okcancelPanel.add(b);

        b=new JButton("Abbrechen");
        b.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e)
                {
                    setVisible(false);
                }
            });
        okcancelPanel.add(b);
    }

    public void setVisible(boolean visible)
    {
        if (visible) {
            initComponents();

            ((CardLayout)closePanel.getLayout()).show(closePanel,"CLOSE");
        }
        super.setVisible(visible);
    }

    public int show(int id)
    {
        initComponents();
        getResultTable().setSelectedID(returnID=id);

        ((CardLayout)closePanel.getLayout()).show(closePanel,"OKCANCEL");
        super.setVisible(true);

        return returnID;
    }

    protected void insert()
    {
        int id;
        do
            {
                id=getEditDialog().showInsert();
                updateComponents();
                getResultTable().setSelectedID(id);
            }
        while (id>=0);
    }

    protected void update()
    {
        getEditDialog().showUpdate(getSelectedID());
        updateComponents();
    }

    protected void delete()
    {
        getEditDialog().showDelete(getSelectedID());
        updateComponents();
    }

    protected void special()
    {
        doSpecial();
        updateComponents();
    }

    private boolean lockDataFields=false;
    protected void updateDataFields()
    {
        if (!lockDataFields && isVisible())
            {
                lockDataFields=true;
                doUpdateDataFields();
                lockDataFields=false;
            }
    }

    private boolean lockComponents=false;
    protected void updateComponents()
    {
        if (!lockComponents && isVisible())
            {
                lockComponents=true;
                getResultTable().requestFocus();
                doUpdateComponents();
                getResultTable().requestFocus();
                lockComponents=false;
            }
    }

    protected void initComponents()
    {
        doUpdateComponents();
    }

    protected void doUpdateComponents()
    {
        doUpdateDataFields();
    }

    protected JResultTable getResultTable()
    {
        return resultTable;
    }

    protected int getSelectedID() throws IndexOutOfBoundsException
    {
        int id=getResultTable().getSelectedID();
        if (id<0)
            {
                Main.msgError("Es wurde nichts ausgewählt");
                throw new IndexOutOfBoundsException();
            }
        return id;
    }

    protected void setInfo(String info)
    {
        infoLabel.setText(" "+info+" ");
    }

    protected void addSearchLabel(String title,char mnemonic,Component comp)
    {
        JLabel l=new JLabel(" "+title+":");
        l.setDisplayedMnemonic(mnemonic);
        l.setLabelFor(comp);
        searchPanel.add(l);
    }

    protected JPanel getSearchPanel()
    {
        return searchPanel;
    }

    protected JPanel getSpecialPanel()
    {
        return specialPanel;
    }

    protected EditDialog getEditDialog()
    {
        return editDialog==null ? editDialog=createEditDialog() : editDialog;
    }

    protected void doSpecial() {}
    protected abstract EditDialog createEditDialog();
    protected abstract void doUpdateDataFields();
}
