/*****************************************************************************\
*                                                                             *
*  FoxdaFoxda      Foxda      Foxda   Foxda  FoxdaFoxda          Foxda        *
*  Foxda        Foxda Foxda    Foxda Foxda   Foxda  Foxda     Foxda Foxda     *
*  FoxdaFoxda  Foxda   Foxda      Foxda      Foxda   Foxda   Foxda   Foxda    *
*  Foxda        Foxda Foxda    Foxda Foxda   Foxda  Foxda   FoxdaFoxdaFoxda   *
*  Foxda           Foxda      Foxda   Foxda  FoxdaFoxda    Foxda       Foxda  *
*                                                                             *
*******************************************************************************
*                   *                                     *                   *
*                   *   FILE:          FarbenListD.java   *                   *
*                   *                                     *                   *
*                    *************************************                    *
*                                                                             *
*                                                                             *
*      This program is free software; you can redistribute it and/or          *
*      modify it under the terms of the GNU General Public License as         *
*      published by the Free Software Foundation; either version 2 of         *
*      the License, or (at your option) any later version.                    *
*                                                                             *
*      This program is distributed in the hope that it will be useful,        *
*      but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the           *
*      GNU General Public License for more details.                           *
*                                                                             *
*      You should have received a copy of the GNU General Public License      *
*      along with this program; if not, write to the Free Software            *
*      Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.              *
*                                                                             *
*                                                                             *
\*****************************************************************************/
package foxda;
import java.lang.*;
import java.util.*;
import java.sql.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import voji.log.*;
import voji.utils.*;
import voji.db.*;
import voji.report.*;

/**
 *
 */
public class FarbenListD extends ListDialog
{
    public static final String MODULE="rassen";

    public FarbenListD(Dialog parent)
    {
        super(parent);
    }

    public FarbenListD(Frame parent)
    {
        super(parent);
    }

    private OrderDialog orderDialog;
    private JTextField
        searchKuerzel;

    protected void dialogInit()
    {
        JButton b;

        Main.checkModule(MODULE);

        specialButton=new JButton(Icons.NEXT);
        specialButton.setToolTipText("Datensätze sortieren");
        specialButton.setMnemonic(KeyEvent.VK_S);

        super.dialogInit();
        setTitle("Rassen und Farben");

        orderDialog=new OrderDialog("UPDATE farben SET sid=? WHERE id=?");

        getResultTable().addColumn("Rasse",SwingConstants.LEFT,75,
                                   new DataFormat(null));
        getResultTable().addColumn("Farbe",SwingConstants.LEFT,75,
                                   new DataFormat(null));

        getSpecialPanel().setLayout(new GridLayout(1,0));

        b=new JButton("Drucken");
        b.setMnemonic('D');
        b.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e)
                {
                    print();
                }
            });
        getSpecialPanel().add(b);

        searchKuerzel=new JTextField(5);
        searchKuerzel.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e)
                {
                    updateComponents();
                }
            });
        addSearchLabel("Kürzel",'K',searchKuerzel);
        getSearchPanel().add(searchKuerzel);

        pack();
    }

    private void print()
    {
        Report rep=new Report(PageAttributes.OrientationRequestedType.LANDSCAPE);
        ReportComponent c;
        Font f=Font.decode("sans-8");

        rep.addHeader(new ReportSpace(0.5));

        rep.addBottom(new ReportSpace(0.5));
        Report.printAndPreviewPdf(Main.mainFrame, rep, "foxda - Rassen- und Farben-Liste");
    }

    protected EditDialog createEditDialog()
    {
        return new FarbenEditD(this);
    }

    protected void initComponents()
    {
        searchKuerzel.requestFocus();
        super.initComponents();
    }

    protected void doUpdateComponents()
    {
        super.doUpdateComponents();
        searchKuerzel.selectAll();
    }

    private static final VPreparedStatement tableStmt=new VPreparedStatement(
        "SELECT f.id,r.name,f.farbe "+
        "FROM farben f,rassen r "+
        "WHERE f.rasse=r.id "+
        "AND r.kuerzel LIKE ? "+
        "ORDER BY f.sid");
    protected void doUpdateDataFields()
    {
        try
            {
                getResultTable().setData(tableStmt.executeQuery(new Object[]{
                    searchKuerzel.getText()+'%'}));
            }
        catch (SQLException e) { Log.log(e); }
    }

    private static final VPreparedStatement orderStmt=new VPreparedStatement(
        "SELECT f.id,r.kuerzel,f.farbe "+
        "FROM farben f,rassen r "+
        "WHERE f.rasse=r.id "+
        "ORDER BY f.sid");
    protected void doSpecial()
    {
        try
            {
                orderDialog.setItems(DataFormat.formatAll(
                        orderStmt.executeQuery(new Object[]{}),
                        Arrays.asList(new DataFormat[]{null,
                                                       Main.rassenFormat})));
                orderDialog.setVisible(true);
            }
        catch (Exception e) { Log.log(e); }
    }
}
