/*****************************************************************************\
*                                                                             *
*  FoxdaFoxda      Foxda      Foxda   Foxda  FoxdaFoxda          Foxda        *
*  Foxda        Foxda Foxda    Foxda Foxda   Foxda  Foxda     Foxda Foxda     *
*  FoxdaFoxda  Foxda   Foxda      Foxda      Foxda   Foxda   Foxda   Foxda    *
*  Foxda        Foxda Foxda    Foxda Foxda   Foxda  Foxda   FoxdaFoxdaFoxda   *
*  Foxda           Foxda      Foxda   Foxda  FoxdaFoxda    Foxda       Foxda  *
*                                                                             *
*******************************************************************************
*                   *                                     *                   *
*                   *   FILE:          AboutDialog.java   *                   *
*                   *                                     *                   *
*                    *************************************                    *
*                                                                             *
*                                                                             *
*      This program is free software; you can redistribute it and/or          *
*      modify it under the terms of the GNU General Public License as         *
*      published by the Free Software Foundation; either version 2 of         *
*      the License, or (at your option) any later version.                    *
*                                                                             *
*      This program is distributed in the hope that it will be useful,        *
*      but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the           *
*      GNU General Public License for more details.                           *
*                                                                             *
*      You should have received a copy of the GNU General Public License      *
*      along with this program; if not, write to the Free Software            *
*      Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.              *
*                                                                             *
*                                                                             *
\*****************************************************************************/
package foxda;
import java.lang.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/**
 *
 */
public class AboutDialog extends JDialog
{
    public AboutDialog(Dialog parent)
    {
        super(parent);
    }

    public AboutDialog(Frame parent)
    {
        super(parent);
    }

    public Dimension getPreferredSize()
    {
        return new Dimension(500,250);
    }

    protected void dialogInit()
    {
        JButton b;
        JPanel p;
        JTextArea a;

        super.dialogInit();
        setTitle("Info");
        setModal(true);

        getContentPane().setLayout(new BorderLayout());

        p=new JPanel();
        p.setLayout(new GridLayout(0,1));
        p.setBorder(BorderFactory.createRaisedBevelBorder());
        getContentPane().add(p,BorderLayout.NORTH);

        p.add(new JLabel());
        p.add(new JLabel("Foxda",JLabel.CENTER));
        p.add(new JLabel("\"Die universelle Datenbank\"",JLabel.CENTER));
        p.add(new JLabel("Version "+Version.VERSION,JLabel.CENTER));
        p.add(new JLabel("(C) "+Version.YEARS+"  "+Version.AUTHORS,
                         JLabel.CENTER));
        p.add(new JLabel());

        p=new JPanel();
        p.setLayout(new GridLayout(0,1));
        p.setBorder(BorderFactory.createRaisedBevelBorder());
        getContentPane().add(p,BorderLayout.CENTER);

        a=new JTextArea(
          /*
          "This program is free software; you can redistribute it and/or "+
          "modify it under the terms of the GNU General Public License as "+
          "published by the Free Software Foundation; either version 2 of "+
          "the License, or (at your option) any later version.\n"+
          "\n"+
          "This program is distributed in the hope that it will be useful, "+
          "but WITHOUT ANY WARRANTY; without even the implied warranty of "+
          "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the "+
          "GNU General Public License for more details.\n"+
          "\n"+
          "You should have received a copy of the GNU General Public License "+
          "along with this program; if not, write to the Free Software "+
          "Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.");
          */
          "Dieses Programm ist freie Software. Sie können es unter "+
          "den Bedingungen der GNU General Public License, wie von der "+
          "Free Software Foundation herausgegeben, weitergeben und/oder "+
          "modifizieren, entweder unter Version 2 der Lizenz oder (wenn "+
          "Sie es wünschen) jeder späteren Version.\n"+
          "\n"+
          "Die Veröffentlichung dieses Programms erfolgt in der "+
          "Hoffnung, daß es Ihnen von Nutzen sein wird, aber OHNE JEDE "+
          "GEWÄHRLEISTUNG - sogar ohne die implizite Gewährleistung "+
          "der MARKTREIFE oder der EIGNUNG FÜR EINEN BESTIMMTEN ZWECK. "+
          "Details finden Sie in der GNU General Public License.\n"+
          "\n"+
          "Sie sollten eine Kopie der GNU General Public License zusammen "+
          "mit diesem Programm erhalten haben. Falls nicht, schreiben Sie "+
          "an die Free Software Foundation, Inc., 675 Mass Ave, Cambridge, "+
          "MA 02139, USA.");

        a.setEditable(false);
        a.setWrapStyleWord(true);
        a.setLineWrap(true);
        p.add(new JScrollPane(a),BorderLayout.CENTER);

        p=new JPanel();
        p.setLayout(new BorderLayout());
        p.setBorder(BorderFactory.createRaisedBevelBorder());
        getContentPane().add(p,BorderLayout.SOUTH);

        b=new JButton("OK");
        getRootPane().setDefaultButton(b);
        b.setMnemonic('O');
        b.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e)
                {
                    setVisible(false);
                }
            });
        p.add(b);

        pack();
    }
}
