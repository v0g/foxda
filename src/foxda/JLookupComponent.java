/*****************************************************************************\
*                                                                             *
*  FoxdaFoxda      Foxda      Foxda   Foxda  FoxdaFoxda          Foxda        *
*  Foxda        Foxda Foxda    Foxda Foxda   Foxda  Foxda     Foxda Foxda     *
*  FoxdaFoxda  Foxda   Foxda      Foxda      Foxda   Foxda   Foxda   Foxda    *
*  Foxda        Foxda Foxda    Foxda Foxda   Foxda  Foxda   FoxdaFoxdaFoxda   *
*  Foxda           Foxda      Foxda   Foxda  FoxdaFoxda    Foxda       Foxda  *
*                                                                             *
*******************************************************************************
*                   *                                     *                   *
*                   *   FILE:     JLookupComponent.java   *                   *
*                   *                                     *                   *
*                    *************************************                    *
*                                                                             *
*                                                                             *
*      This program is free software; you can redistribute it and/or          *
*      modify it under the terms of the GNU General Public License as         *
*      published by the Free Software Foundation; either version 2 of         *
*      the License, or (at your option) any later version.                    *
*                                                                             *
*      This program is distributed in the hope that it will be useful,        *
*      but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the           *
*      GNU General Public License for more details.                           *
*                                                                             *
*      You should have received a copy of the GNU General Public License      *
*      along with this program; if not, write to the Free Software            *
*      Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.              *
*                                                                             *
*                                                                             *
\*****************************************************************************/
package foxda;
import java.lang.*;
import java.util.*;
import java.sql.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import voji.log.*;
import voji.utils.*;
import voji.db.*;
import voji.ui.*;

/**
 *
 */
public class JLookupComponent extends JComponent
{
    private Instance listDialog;
    private DataFormat dataFormat;
    private VPreparedStatement stmt;
    private JTextField textField;
    private JButton button;
    private int selectedID;

    public JLookupComponent(Instance dialog,DataFormat format,String sql)
    {
        listDialog=dialog;
        dataFormat=format==null?new DataFormat():format;
        stmt=sql==null?null:new VPreparedStatement(sql);

        setLayout(new BorderLayout());

        textField=new JTextField();
        textField.setEditable(false);
        textField.setFocusable(false);
        add(textField,BorderLayout.CENTER);

        button=new JButton(Icons.FIND);
        button.setMargin(new Insets(0,0,0,0));
        button.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e)
                {
                    setSelectedID
                        (((ListDialog)listDialog.get()).show(getSelectedID()));
                }
            });
        add(button,BorderLayout.EAST);

        setSelectedID(0);
    }

    public JLookupComponent(Instance dialog,DataFormat format,String sql,
                            char mnemonic)
    {
        this(dialog,format,sql);
        setMnemonic(mnemonic);
    }

    public void setSelectedID(int id)
    {
        selectedID=id;

        String s=getData(selectedID);
        textField.setText(s);
        textField.setToolTipText(s.length()>0?s:null);
        
        fireIDChanged(id);
    }

    public int getSelectedID()
    {
        return selectedID;
    }

    public void setMnemonic(char mnemonic)
    {
        button.setMnemonic(mnemonic);
    }

    public void setButtonFocusable(boolean focusable)
    {
        button.setFocusable(focusable);
    }

    protected String getData(int id)
    {
        try
            {
                ResultSet r=stmt.executeQuery(new Object[]{id});
                return r.next()?dataFormat.format(r,1):"";
            }
        catch (SQLException e)
            {
                Log.log(e);
                return "";
            }
    }

    private Vector<IDListener> idListeners=new Vector<IDListener>();

    public void addIDListener(IDListener l)
    {
        idListeners.add(l);
    }

    protected void fireIDChanged(int id)
    {
        for (IDListener idListener : idListeners)
            idListener.idChanged(id);
    }
}
