/*****************************************************************************\
*                                                                             *
*  FoxdaFoxda      Foxda      Foxda   Foxda  FoxdaFoxda          Foxda        *
*  Foxda        Foxda Foxda    Foxda Foxda   Foxda  Foxda     Foxda Foxda     *
*  FoxdaFoxda  Foxda   Foxda      Foxda      Foxda   Foxda   Foxda   Foxda    *
*  Foxda        Foxda Foxda    Foxda Foxda   Foxda  Foxda   FoxdaFoxdaFoxda   *
*  Foxda           Foxda      Foxda   Foxda  FoxdaFoxda    Foxda       Foxda  *
*                                                                             *
*******************************************************************************
*                   *                                     *                   *
*                   *   FILE:          BelegeEditD.java   *                   *
*                   *                                     *                   *
*                    *************************************                    *
*                                                                             *
*                                                                             *
*      This program is free software; you can redistribute it and/or          *
*      modify it under the terms of the GNU General Public License as         *
*      published by the Free Software Foundation; either version 2 of         *
*      the License, or (at your option) any later version.                    *
*                                                                             *
*      This program is distributed in the hope that it will be useful,        *
*      but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the           *
*      GNU General Public License for more details.                           *
*                                                                             *
*      You should have received a copy of the GNU General Public License      *
*      along with this program; if not, write to the Free Software            *
*      Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.              *
*                                                                             *
*                                                                             *
\*****************************************************************************/
package foxda;
import java.lang.*;
import java.util.*;
import java.sql.*;
import java.awt.*;
import java.awt.event.*;
import java.beans.*;
import javax.swing.*;
import voji.log.*;
import voji.utils.*;
import voji.db.*;
import voji.ui.*;

/**
 *
 */
public class BelegeEditD extends EditDialog
{
    public static final String MODULE="kassen";

    protected Object kasse=null;

    public BelegeEditD(Dialog parent)
    {
        super(parent);
    }

    public BelegeEditD(Frame parent)
    {
        super(parent);
    }

    private JTextField
        dataZweck;
    private JFormatTextField
        dataNummer,
        dataKonto,
        dataBetrag,
        dataDatum;
    private JLookupComponent
        dataKontoLookup;

    public Dimension getPreferredSize()
    {
        return new Dimension(400,super.getPreferredSize().height);
    }

    protected void dialogInit()
    {
        JLabel l;
        JPanel p;
        JButton b;

        Main.checkModule(MODULE);

        super.dialogInit();
        setTitle("Belegdaten");

        getParent().addPropertyChangeListener("kasse",
                                              new PropertyChangeListener(){
                public void propertyChange(PropertyChangeEvent e)
                {
                    firePropertyChange(e.getPropertyName(),
                                       e.getOldValue(),e.getNewValue());
                    kasse=e.getNewValue();
                }
            });

        getContentPanel().setLayout(new BorderLayout());

        JPanel labelPanel=new JPanel();
        labelPanel.setLayout(new GridLayout(0,1));
        getContentPanel().add(labelPanel,BorderLayout.WEST);

        JPanel inputPanel=new JPanel();
        inputPanel.setLayout(new GridLayout(0,1));
        getContentPanel().add(inputPanel,BorderLayout.CENTER);

        dataNummer=new JFormatTextField(Main.numberFormat);
        dataNummer.setObject(1);
        inputPanel.add(dataNummer);
        l=new JLabel(" Nummer: ",JLabel.RIGHT);
        l.setDisplayedMnemonic('N');
        l.setLabelFor(dataNummer);
        labelPanel.add(l);

        dataBetrag=new JFormatTextField(Main.numberFormat);
        inputPanel.add(dataBetrag);
        l=new JLabel(" Betrag: ",JLabel.RIGHT);
        l.setDisplayedMnemonic('B');
        l.setLabelFor(dataBetrag);
        labelPanel.add(l);

        p=new JPanel();
        p.setLayout(new BorderLayout());
        inputPanel.add(p);

        dataKonto=new JFormatTextField(null);
        dataKonto.setColumns(5);
        dataKonto.addObjectListener(new ObjectListener(){
                public void objectChanged(Object obj)
                {
                    kontoChanged();
                }
            });
        p.add(dataKonto,BorderLayout.WEST);
        l=new JLabel(" Konto: ",JLabel.RIGHT);
        l.setDisplayedMnemonic('K');
        l.setLabelFor(dataKonto);
        labelPanel.add(l);

        dataKontoLookup=new JLookupComponent
            (new Instance(){
                    public Object create()
                    {
                        Object o=new KontenListD(BelegeEditD.this);
                        firePropertyChange("kasse",null,kasse);
                        return o;
                    }},
             null,
             "SELECT name "+
             "FROM konten "+
             "WHERE id=?");
        dataKontoLookup.addIDListener(new IDListener(){
                public void idChanged(int id)
                {
                    kontoLookupChanged(id);
                }
            });
        dataKontoLookup.setButtonFocusable(false);
        p.add(dataKontoLookup,BorderLayout.CENTER);

        p=new JPanel();
        p.setLayout(new BorderLayout());
        inputPanel.add(p);

        dataDatum=new JFormatTextField(Main.dateFormat);
        p.add(dataDatum);
        l=new JLabel(" Datum: ",JLabel.RIGHT);
        l.setDisplayedMnemonic('D');
        l.setLabelFor(dataDatum);
        labelPanel.add(l);


        b=new JButton(Icons.DATE);
        b.setFocusable(false);
        b.setToolTipText("Aktuelles Datum");
        b.setMnemonic('A');
        b.setMargin(new Insets(0,0,0,0));
        b.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e)
                {
                    calcDatum();
                }
            });
        p.add(b,BorderLayout.EAST);

        dataZweck=new JTextField();
        inputPanel.add(dataZweck);
        l=new JLabel(" Zweck: ",JLabel.RIGHT);
        l.setDisplayedMnemonic('Z');
        l.setLabelFor(dataZweck);
        labelPanel.add(l);

        pack();
    }

    protected void initComponents()
    {
        dataNummer.selectAll();
        dataBetrag.selectAll();
        dataKonto.selectAll();
        dataDatum.selectAll();
        dataZweck.selectAll();
        dataBetrag.requestFocus();
    }

    protected boolean noChange=false;

    private static final VPreparedStatement kontoStmt=new VPreparedStatement(
        "SELECT id "+
        "FROM konten "+
        "WHERE kasse=? AND kuerzel=?");
    protected void kontoChanged()
    {
        if (noChange) return;
        noChange=true;
        try
            {
                int i=1;
                ResultSet r=kontoStmt.executeQuery(new Object[]{
                    kasse,
                    dataKonto.getObject()});
                if (!r.next()) throw new SQLException();

                dataKontoLookup.setSelectedID(r.getInt(i++));
            }
        catch (Exception e)
            {
                dataKontoLookup.setSelectedID(0);
            }
        noChange=false;
    }

    private static final VPreparedStatement kontoLookupStmt=new VPreparedStatement(
        "SELECT kuerzel "+
        "FROM konten "+
        "WHERE kasse=? AND id=?");
    protected void kontoLookupChanged(int id)
    {
        if (noChange) return;
        noChange=true;
        try
            {
                int i=1;
                ResultSet r=kontoLookupStmt.executeQuery(new Object[]{
                    kasse,
                    id});
                if (!r.next()) throw new SQLException();

                dataKonto.setObject(r.getObject(i++));
            }
        catch (Exception e)
            {
                dataKonto.setObject(null);
            }
        noChange=false;
    }

    protected void calcDatum()
    {
        dataDatum.setObject(new java.util.Date());
    }

    private static final VPreparedStatement newStmt=new VPreparedStatement(
        "SELECT MAX(nummer)+1 "+
        "FROM belege "+
        "WHERE kasse=?");
    protected void newData()
    {
        int nr;
        try
            {
                int i=1;
                ResultSet r=newStmt.executeQuery(new Object[]{
                    kasse});
                if (!r.next()) throw new SQLException();

                nr=r.getInt(i++);
            }
        catch (Exception e) { nr=1; }

        dataNummer.setObject(nr);
        dataNummer.setVisible(true);
        dataBetrag.setText("");
        dataKonto.setObject(null);
        dataDatum.setObject(new java.util.Date());
        dataZweck.setText("");
    }

    private static final VPreparedStatement selectStmt=new VPreparedStatement(
        "SELECT nummer,betrag,konto,datum,zweck "+
        "FROM belege "+
        "WHERE id=?");
    protected void selectData(int id)
    {
        try
            {
                int i=1;
                ResultSet r=selectStmt.executeQuery(new Object[]{
                    id});
                if (!r.next()) throw new SQLException();
                dataNummer.setObject(r.getObject(i++));
                dataBetrag.setObject(r.getObject(i++));
                dataKontoLookup.setSelectedID(r.getInt(i++));
                dataDatum.setObject(r.getObject(i++));
                dataZweck.setText(r.getString(i++));
            }
        catch (SQLException e) { Log.log(e); }
    }

    private static final VPreparedStatement idStmt=new VPreparedStatement(
        "SELECT MAX(id)+1 "+
        "FROM belege");
    private static final VPreparedStatement insertStmt=new VPreparedStatement(
        "INSERT INTO belege "+
        "(nummer,betrag,konto,datum,zweck,kasse,id) "+
        "VALUES (?,?,?,?::DATE,?,?,?)");
    protected int insertData()
    {
        try
            {
                int id;
                ResultSet r=idStmt.executeQuery(new Object[]{});
                if (!r.next()) throw new SQLException();
                id=r.getInt(1);
                insertStmt.executeUpdate(new Object[]{
                    dataNummer.getObject(),
                    dataBetrag.getObject(),
                    dataKontoLookup.getSelectedID(),
                    dataDatum.getObject(),
                    dataZweck.getText(),
                    kasse,
                    id});
                return id;
            }
        catch (SQLException e) { Log.log(e); }
        return -1;
    }

    private static final VPreparedStatement updateStmt=new VPreparedStatement(
        "UPDATE belege SET "+
        "nummer=?,"+
        "betrag=?,"+
        "konto=?,"+
        "datum=?::DATE,"+
        "zweck=? "+
        "WHERE id=?");
    protected void updateData(int id)
    {
        try
            {
                updateStmt.executeUpdate(new Object[]{
                    dataNummer.getObject(),
                    dataBetrag.getObject(),
                    dataKontoLookup.getSelectedID(),
                    dataDatum.getObject(),
                    dataZweck.getText(),
                    id});
            }
        catch (SQLException e) { Log.log(e); }
    }

    private static final VPreparedStatement deleteStmt=new VPreparedStatement(
        "DELETE FROM belege "+
        "WHERE id=?");
    protected void deleteData(int id)
    {
        try
            {
                deleteStmt.executeUpdate(new Object[]{
                    id});
            }
        catch (SQLException e) { Log.log(e); }
    }
}
