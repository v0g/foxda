/*****************************************************************************\
*                                                                             *
*  FoxdaFoxda      Foxda      Foxda   Foxda  FoxdaFoxda          Foxda        *
*  Foxda        Foxda Foxda    Foxda Foxda   Foxda  Foxda     Foxda Foxda     *
*  FoxdaFoxda  Foxda   Foxda      Foxda      Foxda   Foxda   Foxda   Foxda    *
*  Foxda        Foxda Foxda    Foxda Foxda   Foxda  Foxda   FoxdaFoxdaFoxda   *
*  Foxda           Foxda      Foxda   Foxda  FoxdaFoxda    Foxda       Foxda  *
*                                                                             *
*******************************************************************************
*                   *                                     *                   *
*                   *   FILE:     TimeChooseDialog.java   *                   *
*                   *                                     *                   *
*                    *************************************                    *
*                                                                             *
*                                                                             *
*      This program is free software; you can redistribute it and/or          *
*      modify it under the terms of the GNU General Public License as         *
*      published by the Free Software Foundation; either version 2 of         *
*      the License, or (at your option) any later version.                    *
*                                                                             *
*      This program is distributed in the hope that it will be useful,        *
*      but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the           *
*      GNU General Public License for more details.                           *
*                                                                             *
*      You should have received a copy of the GNU General Public License      *
*      along with this program; if not, write to the Free Software            *
*      Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.              *
*                                                                             *
*                                                                             *
\*****************************************************************************/
package foxda;
import java.lang.*;
import java.util.*;
import java.sql.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import voji.log.*;
import voji.utils.*;
import voji.db.*;
import voji.report.*;
import voji.ui.*;

/**
 *
 */
class TimeChooseDialog extends JDialog
{
    private final VChoice chMonth=new VChoice(new Object[]{
        "Januar",    1,
        "Februar",   2,
        "März",      3,
        "April",     4,
        "Mai",       5,
        "Juni",      6,
        "Juli",      7,
        "August",    8,
        "September", 9,
        "Oktober",   10,
        "November",  11,
        "Dezember",  12
    });
    private final VChoice chQuarter=new VChoice(new Object[]{
        "1. Quartal (Januar-März)",      1,
        "2. Quartal (April-Juni)",       4,
        "3. Quartal (Juli-September)",   7,
        "4. Quartal (Oktober-Dezember)", 10
    });
    protected JComboBox<Object> monthField;
    protected JComboBox<Object> quarterField;
    protected SpinnerNumberModel yearFieldModel;
    protected JSpinner yearField;

    public static final int MONTH=0;
    public static final int QUARTER=1;
    public static final int YEAR=2;
    protected int type;

    public TimeChooseDialog(JDialog parent,int type,int year,int month)
    {
        super(parent,true);
        this.type=type;
        switch (type)
            {
            case MONTH:   setTitle("Bitte Monat wählen"); break;
            case QUARTER: setTitle("Bitte Quartal wählen"); break;
            case YEAR:    setTitle("Bitte Jahr wählen"); break;
            default: throw new IllegalArgumentException();
            }

        getContentPane().setLayout(new BorderLayout());

        JPanel p=new JPanel();
        p.setLayout(new BorderLayout());
        p.setBorder(BorderFactory.createEmptyBorder(8,8,8,8));
        getContentPane().add(p,BorderLayout.CENTER);

        if (type==MONTH)
            {
                monthField=new JComboBox<Object>(chMonth.elements());
                monthField.setSelectedIndex(month-Calendar.JANUARY);
                p.add(monthField,BorderLayout.CENTER);
            }

        if (type==QUARTER)
            {
                quarterField=new JComboBox<Object>(chQuarter.elements());
                quarterField.setSelectedIndex((month-Calendar.JANUARY)/3);
                p.add(quarterField,BorderLayout.CENTER);
            }

        yearFieldModel=new SpinnerNumberModel(year, 1970, null, 1);
        yearField=new JSpinner(yearFieldModel);
        yearField.setEditor(new JSpinner.NumberEditor(yearField,"####"));
        p.add(yearField,BorderLayout.EAST);

        JButton b=new JButton("OK");
        getRootPane().setDefaultButton(b);
        b.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e)
                {
                    setVisible(false);
                }
            });
        getContentPane().add(b,BorderLayout.SOUTH);

        pack();
    }

    public TimeChooseDialog(JDialog parent,int type)
    {
        this(parent,type,Calendar.getInstance().get(Calendar.YEAR),Calendar.getInstance().get(Calendar.MONTH));
    }

    public Object[] getParams()
    {
        this.setVisible(true);
        int year=yearFieldModel.getNumber().intValue();
        int i;
        switch (type)
            {
            case MONTH:
                i=((Number)(chMonth.i2v(monthField.
                                        getSelectedIndex()))).intValue();
                return new Object[]
                    {i,i,year,
                     monthField.getSelectedItem()+"  "+year};

            case QUARTER:
                i=((Number)(chQuarter.i2v(quarterField.
                                          getSelectedIndex()))).intValue();
                return new Object[]
                    {i,i+2,year,
                     quarterField.getSelectedItem()+"  "+year};

            case YEAR:
                return new Object[]
                    // {1,3,year,
                    // {4,12,year,
                    {1,12,year,
                     ""+year};

            default:
                return null;
            }
    }

    public Object[] getRange()
    {
        this.setVisible(true);
        int year=yearFieldModel.getNumber().intValue();
        int i;
        switch (type)
            {
            case MONTH:
                return null;

            case QUARTER:
                return null;

            case YEAR:
                Calendar calEnd=Calendar.getInstance();
                calEnd.set(year,Calendar.DECEMBER,31);
//                calEnd.set(year+1,Calendar.FEBRUARY,28);
                Calendar calStart=Calendar.getInstance();
                calStart.set(year,Calendar.JANUARY,1);
//                calStart.set(year,Calendar.FEBRUARY,28);
//                calStart.add(Calendar.DAY_OF_MONTH,+1);
                return new Object[]
                    {new java.sql.Date(calStart.getTime().getTime()),
                     new java.sql.Date(calEnd.getTime().getTime())};

            default:
                return null;
            }
    }
}
