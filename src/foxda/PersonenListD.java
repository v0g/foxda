/*****************************************************************************\
*                                                                             *
*  FoxdaFoxda      Foxda      Foxda   Foxda  FoxdaFoxda          Foxda        *
*  Foxda        Foxda Foxda    Foxda Foxda   Foxda  Foxda     Foxda Foxda     *
*  FoxdaFoxda  Foxda   Foxda      Foxda      Foxda   Foxda   Foxda   Foxda    *
*  Foxda        Foxda Foxda    Foxda Foxda   Foxda  Foxda   FoxdaFoxdaFoxda   *
*  Foxda           Foxda      Foxda   Foxda  FoxdaFoxda    Foxda       Foxda  *
*                                                                             *
*******************************************************************************
*                   *                                     *                   *
*                   *   FILE:        PersonenListD.java   *                   *
*                   *                                     *                   *
*                    *************************************                    *
*                                                                             *
*                                                                             *
*      This program is free software; you can redistribute it and/or          *
*      modify it under the terms of the GNU General Public License as         *
*      published by the Free Software Foundation; either version 2 of         *
*      the License, or (at your option) any later version.                    *
*                                                                             *
*      This program is distributed in the hope that it will be useful,        *
*      but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the           *
*      GNU General Public License for more details.                           *
*                                                                             *
*      You should have received a copy of the GNU General Public License      *
*      along with this program; if not, write to the Free Software            *
*      Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.              *
*                                                                             *
*                                                                             *
\*****************************************************************************/
package foxda;
import java.lang.*;
import java.sql.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import voji.log.*;
import voji.utils.*;
import voji.db.*;
import voji.report.*;
import voji.ui.*;

/**
 *
 */
public class PersonenListD extends ListDialog
{
    public static final String MODULE="personen";

    public PersonenListD(Dialog parent)
    {
        super(parent);
    }

    public PersonenListD(Frame parent)
    {
        super(parent);
    }

    private Instance
        vereineD;
    private JTextField
        searchNachname,
        searchVorname;
    private JComboBox<String>
        searchFunktion;

    protected void dialogInit()
    {
        JButton b;

        Main.checkModule(MODULE);

        super.dialogInit();
        setTitle("Personen");

        vereineD=new Instance(VereineListD.class,new Object[]{this});

        getResultTable().addColumn("Nachname",SwingConstants.LEFT,75,
                                   new DataFormat(null));
        getResultTable().addColumn("Vorname",SwingConstants.LEFT,75,
                                   new DataFormat(null));
        getResultTable().addColumn("Ort",SwingConstants.LEFT,75,
                                   new DataFormat(null));

        getSpecialPanel().setLayout(new GridLayout(1,0));

        b=new JButton("Gesamtliste",Icons.PRINT);
        b.setMnemonic('G');
        b.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e)
                {
                    printGesamt();
                }
            });
        getSpecialPanel().add(b);

        b=new JButton("Rassen",Icons.PRINT);
        b.setMnemonic('R');
        b.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e)
                {
                    printRassen();
                }
            });
        getSpecialPanel().add(b);

        b=new JButton("Tierliste",Icons.PRINT);
        b.setMnemonic('T');
        b.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e)
                {
                    printTiere();
                }
            });
        getSpecialPanel().add(b);

        b=new JButton("Vereine",Icons.BOOK);
        b.setMnemonic('V');
        b.setEnabled(Main.isEnabledModule(VereineListD.MODULE));
        b.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e)
                {
                    ((JDialog)vereineD.get()).setVisible(true);
                }
            });
        getSpecialPanel().add(b);

        searchNachname=new JTextField(5);
        searchNachname.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e)
                {
                    updateComponents();
                }
            });
        addSearchLabel("Nachname",'N',searchNachname);
        getSearchPanel().add(searchNachname);

        searchVorname=new JTextField(5);
        searchVorname.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e)
                {
                    updateComponents();
                }
            });
        addSearchLabel("Vorname",'V',searchVorname);
        getSearchPanel().add(searchVorname);

        searchFunktion=new JComboBox<String>(new String[]{
            "",
            "Vorsitzende",
            "Ansprechpartner",
            "Kassierer",
            "Zuchtwart",
            "Jugendwart",
            "Tätowiermeister"
        });
        searchFunktion.setEditable(true);
        searchFunktion.addItemListener(new ItemListener(){
                public void itemStateChanged(ItemEvent e)
                {
                    updateDataFields();
                }
            });
        addSearchLabel("Funkt.",'F',searchFunktion);
        getSearchPanel().add(searchFunktion);

        pack();
    }

    public void setSearchFunktion(String funktion)
    {
        searchFunktion.setSelectedItem(funktion);
    }

    private static final VPreparedStatement printGesamtStmt=new VPreparedStatement(
        "SELECT p.nachname,p.vorname,p.strassenr,p.plz,p.ort,"+
        "       v.typ,v.kuerzel,p.telefon,p.geburt,p.geschlecht "+
        "FROM personen p,vereine v "+
        "WHERE p.id!=0 AND p.verein=v.id "+
        "ORDER BY p.nachname,p.vorname,p.id");
    private void printGesamt()
    {
        Report rep=new Report(PageAttributes.OrientationRequestedType.LANDSCAPE);
        ReportComponent c;
        Font f=Font.decode("sans-12");

        rep.addHeader(new ReportSpace(0.5));
        c=new ReportComponent();
        c.add(new ReportText
              ("Personen-Liste",
               0.5,10.5,
               Font.decode("times-bold-24"),
               ReportText.CENTER));
        c.add(new ReportPageNumber
              ("Seite ","",
               0,11,
               Font.decode("sans-10")));
        rep.addHeader(c);
        rep.addHeader(new ReportSpace(0.2));
        rep.addHeader(new ReportTable
                      (new String[]{
                          "Name","Adresse","Verein","Telefon","Geb.-Datum"},
                       0.5,new double[]{2.0,3.0,2.5,1.5,1.5},
                       Font.decode("sans-bold-12"),
                       ReportText.CENTER,ReportText.NEWLINE,ReportFrame.SINGLE,
                       ReportComponent.CENTER));

        try {
        ResultSet r=printGesamtStmt.executeQuery(new Object[]{});
        while (r.next())
            {
                int i=1;
                c=new ReportComponent();
                c.add(new ReportTable
                      (new String[]{
                          r.getString(i++)+", "+r.getString(i++),
                          r.getString(i++)+"\n"+
                              r.getString(i++)+" "+r.getString(i++),
                          r.getString(i++)+" "+r.getString(i++),
                          r.getString(i++),
                          U.format(Main.dateFormat,r.getDate(i++))},
                       0.5,new double[]{2.0,3.0,2.5,1.5,1.5},
                       f,
                       new int[]{
                           ReportText.LEFT,
                           ReportText.LEFT,
                           ReportText.LEFT,
                           ReportText.LEFT,
                           ReportText.RIGHT},
                       ReportText.WORD,
                       ReportFrame.SINGLE,
                       ReportComponent.CENTER));
                c.add(new ReportText
                      (" ("+r.getString(i++)+")",
                       11.0,11.5,
                       f,ReportText.LEFT,ReportText.WORD,true));
                rep.add(c);
            }
        }
        catch (SQLException e) { Log.log(e); }

        rep.addBottom(new ReportSpace(0.5));
        Report.printAndPreviewPdf(Main.mainFrame, rep, "foxda - Personen-Liste");
    }

    private static final VPreparedStatement printRassenStmt=new VPreparedStatement(
        "SELECT f.id,r.name,f.farbe,p.nachname,p.vorname,p.strassenr,"+
        "       p.plz,p.ort,p.telefon,v.kuerzel "+
        "FROM personen p,vereine v,rassen r,farben f "+
        "WHERE p.id!=0 AND f.id!=0 AND p.verein=v.id AND f.rasse=r.id AND "+
        "     ((f.farbe='' AND "+
        "       (p.rassen LIKE r.kuerzel OR "+
        "        p.rassen LIKE '%, '||r.kuerzel OR "+
        "        p.rassen LIKE r.kuerzel||',%' OR "+
        "        p.rassen LIKE '%, '||r.kuerzel||',%')) "+
        "   OR (f.farbe!='' AND "+
        "       (p.rassen LIKE r.kuerzel||' ('||f.farbe||')' OR "+
        "        p.rassen LIKE '%, '||r.kuerzel||' ('||f.farbe||')' OR "+
        "        p.rassen LIKE r.kuerzel||' ('||f.farbe||')'||',%' OR "+
        "        p.rassen LIKE '%, '||r.kuerzel||' ('||f.farbe||')'||',%'))) "+
        "ORDER BY f.sid,p.nachname,p.vorname,p.id");
    private void printRassen()
    {
        Report rep=new Report(PageAttributes.OrientationRequestedType.LANDSCAPE);
        ReportComponent c;
        Font f=Font.decode("sans-8");

        rep.addHeader(new ReportSpace(0.5));
        c=new ReportComponent();
        c.add(new ReportText
              ("Gezüchtete Rassen",
               1,10.5,
               Font.decode("times-bold-24"),
               ReportText.CENTER));
        c.add(new ReportPageNumber
              ("Seite ","",
               0,11,
               Font.decode("sans-10")));
        rep.addHeader(c);
        rep.addHeader(new ReportSpace(0.2));
        rep.addHeader(new ReportTable
                      (new String[]{
                          "Rasse","Name","Straße/Nr.","PLZ/Ort","Telefon",
                          "Verein"},
                       0.5,new double[]{2.5,2.0,2.0,2.0,1.0,1.0},
                       Font.decode("sans-bold-10"),
                       ReportText.CENTER,ReportText.WORD,ReportFrame.SINGLE,
                       ReportComponent.CENTER));

        try {
        ResultSet r=printRassenStmt.executeQuery(new Object[]{});
        int ofid=-1;
        while (r.next())
            {
                int i=1;
                c=new ReportComponent();

                int fid=r.getInt(i++);
                if (fid!=ofid)
                    c.add(new ReportText
                          (Main.rassenFormat.format(r,i),
                           0.5,2.9,
                           Font.decode("sans-italic-8"),
                           ReportText.RIGHT,ReportText.WORD,true));
                i+=2; // rasse,farbe
                ofid=fid;

                c.add(new ReportTable
                      (new String[]{
                          r.getString(i++)+", "+r.getString(i++),
                          r.getString(i++),
                          r.getString(i++)+" "+r.getString(i++),
                          r.getString(i++),
                          r.getString(i++)},
                       3.0,new double[]{2.0,2.0,2.0,1.0,1.0},
                       f,
                       new int[]{
                           ReportText.LEFT,
                           ReportText.LEFT,
                           ReportText.LEFT,
                           ReportText.LEFT,
                           ReportText.LEFT},
                       ReportText.WORD,ReportFrame.SINGLE,
                       ReportComponent.CENTER));

                rep.add(c);
            }
        }
        catch (SQLException e) { Log.log(e); }

        rep.addBottom(new ReportSpace(0.5));
        Report.printAndPreviewPdf(Main.mainFrame, rep, "foxda - Rassen-Mitglieder-Liste");
    }


    private static final VPreparedStatement printTiereStmt=new VPreparedStatement(
        "SELECT r.id,r.kuerzel,r.name,"+
        "       k.tr,k.tl_monat,k.tl_jahr,k.tl_nummer,f.farbe,"+
        "       k.geschlecht,k.geburt,"+
        "       v.tr,v.tl_monat,v.tl_jahr,v.tl_nummer,fv.farbe,"+
        "       m.tr,m.tl_monat,m.tl_jahr,m.tl_nummer,fm.farbe,"+
        "       z.nachname,z.vorname,z2.nachname,z2.vorname,"+
        "       t.nachname,t.vorname "+
        "FROM kaninchen k,rassen r,farben f,"+
        "     kaninchen v,farben fv,"+
        "     kaninchen m,farben fm,"+
        "     personen z,personen z2,personen t "+
        "WHERE k.id!=0 AND "+
        "      (k.zuechter=? OR k.zuechter2=?) AND "+
        "      (extract(month FROM k.geburt) BETWEEN ? AND ?) AND "+
        "      extract(year FROM k.geburt)=? AND "+
        "      k.farbe=f.id AND f.rasse=r.id AND "+
        "      k.vater=v.id AND v.farbe=fv.id AND "+
        "      k.mutter=m.id AND m.farbe=fm.id AND "+
        "      k.zuechter=z.id AND k.zuechter2=z2.id AND k.taeto_m=t.id "+
        "ORDER BY r.kuerzel,k.geburt,k.tl_nummer");
    private void printTiere()
    {
        Object person=getSelectedID();

        Object[] params=new TimeChooseDialog
            (this,TimeChooseDialog.YEAR).getParams();

        Report rep=new Report(PageAttributes.OrientationRequestedType.PORTRAIT);
        ReportComponent c;
        Font f=Font.decode("sans-11");
        double s=0.3;
        double[] w=new double[]{1.4,1.4,1.0,2.2,1.4};

        rep.addHeader(new ReportSpace(0.5));
        c=new ReportComponent();
        c.add(new ReportText
              ("Tier-Liste  "+params[3],
               s,s+w[0]+w[1]+w[2]+w[3]+w[4],
               Font.decode("times-bold-24"),
               ReportText.CENTER));
        c.add(new ReportPageNumber
              ("Seite ","",
               s,s+w[0]+w[1]+w[2]+w[3]+w[4],
               Font.decode("sans-10")));
        rep.addHeader(c);
        rep.addHeader(new ReportSpace(0.2));
        rep.addHeader(new ReportTable
                      (new String[]{
                          "Vater","Mutter","Wurfdatum","Züchter","Tier"},
                       s,w,
                       Font.decode("sans-bold-11"),
                       ReportText.CENTER,ReportText.NEWLINE,
                       ReportFrame.BOLD,ReportComponent.CENTER));

        try {
        ResultSet r=printTiereStmt.executeQuery(new Object[]
            {person,person,params[0],params[1],params[2]});
        int orid=-1;
        while (r.next())
            {
                int i=1;
                int rid=r.getInt(i++);
                String r_kuerzel=r.getString(i++);
                String r_name=r.getString(i++);
                String taeto=Main.taetoFormat.format(r,i++);
                i++; // tl_monat
                i++; // tl_jahr
                i++; // tl_nummer
                String farbe=r.getString(i++);
                String gesch=r.getString(i++);
                String geburt=U.format(Main.dateFormat,r.getObject(i++));
                String taeto_vater=Main.taetoFormat.format(r,i++);
                i++; // tl_monat
                i++; // tl_jahr
                i++; // tl_nummer
                String farbe_vater=r.getString(i++);
                String taeto_mutter=Main.taetoFormat.format(r,i++);
                i++; // tl_monat
                i++; // tl_jahr
                i++; // tl_nummer
                String farbe_mutter=r.getString(i++);
                String zuechter=Main.zuechterFormat.format(r,i++);
                i++; // z.vorname
                i++; // z2.nachname
                i++; // z2.vorname
                String taeto_m=Main.personenFormat.format(r,i++);
                i++; // vorname

                if (rid!=orid)
                    {
                        rep.add(new ReportSpace(0.1));
                        rep.add(new ReportTable
                                (new String[]{r_name+"  ("+r_kuerzel+")"},
                                 s,new double[]{w[0]+w[1]+w[2]+w[3]+w[4]},
                                 Font.decode("sans-12"),
                                 ReportText.CENTER,ReportText.WORD,
                                 ReportFrame.SINGLE,ReportComponent.CENTER));
                        rep.add(new ReportSpace(0.1));
                    }

                ReportTable t=new ReportTable
                    (new String[]{
                        "",
                        "",
                        geburt,
                        zuechter,
                        ""},
                     s,w,
                     f,
                     new int[]{
                         ReportText.CENTER,
                         ReportText.CENTER,
                         ReportText.CENTER,
                         ReportText.CENTER,
                         ReportText.CENTER},
                     ReportText.WORD,
                     ReportFrame.SINGLE,
                     ReportComponent.CENTER);

                c=new ReportList();
                c.add(new ReportText
                      (taeto_vater,
                       s,s+w[0],
                       f,ReportText.CENTER,ReportText.WORD,true));
                if (farbe_vater.length()>0)
                c.add(new ReportText
                      (farbe_vater,
                       s,s+w[0],
                       Font.decode("sans-8"),
                       ReportText.LEFT,ReportText.WORD,true));
                t.getContent().add(c);

                c=new ReportList();
                c.add(new ReportText
                      (taeto_mutter,
                       s+w[0],s+w[0]+w[1],
                       f,ReportText.CENTER,ReportText.WORD,true));
                if (farbe_mutter.length()>0)
                c.add(new ReportText
                      (farbe_mutter,
                       s+w[0],s+w[0]+w[1],
                       Font.decode("sans-8"),
                       ReportText.LEFT,ReportText.WORD,true));
                t.getContent().add(c);

                c=new ReportList();
                c.add(new ReportText
                      (taeto,
                       s+w[0]+w[1]+w[2]+w[3],s+w[0]+w[1]+w[2]+w[3]+w[4],
                       f,ReportText.CENTER,ReportText.WORD,true));
                if (farbe.length()>0)
                c.add(new ReportText
                      (farbe,
                       s+w[0]+w[1]+w[2]+w[3],s+w[0]+w[1]+w[2]+w[3]+w[4],
                       Font.decode("sans-8"),
                       ReportText.LEFT,ReportText.WORD,true));
                t.getContent().add(c);

                t.getContent().add(new ReportText
                                   ("("+gesch+")",
                                    s+w[0]+w[1]+w[2]+w[3]+w[4],
                                    s+w[0]+w[1]+w[2]+w[3]+w[4]+0.5,
                                    Font.decode("sans-8"),
                                    ReportText.LEFT,ReportText.WORD,true),
                                   ReportComponent.CENTER);

                rep.add(t);

                orid=rid;
            }
        }
        catch (SQLException e) { Log.log(e); }

        rep.addBottom(new ReportSpace(0.5));
        Report.printAndPreviewPdf(Main.mainFrame, rep, "foxda - Tier-Liste");
    }

    protected EditDialog createEditDialog()
    {
        return new PersonenEditD(this);
    }

    protected void initComponents()
    {
        searchNachname.requestFocus();
        super.initComponents();
    }

    protected void doUpdateComponents()
    {
        super.doUpdateComponents();
        searchNachname.selectAll();
        searchVorname.selectAll();
    }

    private static final VPreparedStatement tableStmt=new VPreparedStatement(
        "SELECT id,nachname,vorname,ort "+
        "FROM personen "+
        "WHERE nachname LIKE ? AND vorname LIKE ? AND funktionen LIKE ? "+
        "ORDER BY nachname,vorname,id");
    protected void doUpdateDataFields()
    {
        try
            {
                getResultTable().setData(tableStmt.executeQuery(new Object[]{
                    searchNachname.getText()+'%',
                    searchVorname.getText()+'%',
                    "%"+searchFunktion.getSelectedItem()+"%"}));
            }
        catch (SQLException e) { Log.log(e); }
    }
}
