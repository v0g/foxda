/*****************************************************************************\
*                                                                             *
*  FoxdaFoxda      Foxda      Foxda   Foxda  FoxdaFoxda          Foxda        *
*  Foxda        Foxda Foxda    Foxda Foxda   Foxda  Foxda     Foxda Foxda     *
*  FoxdaFoxda  Foxda   Foxda      Foxda      Foxda   Foxda   Foxda   Foxda    *
*  Foxda        Foxda Foxda    Foxda Foxda   Foxda  Foxda   FoxdaFoxdaFoxda   *
*  Foxda           Foxda      Foxda   Foxda  FoxdaFoxda    Foxda       Foxda  *
*                                                                             *
*******************************************************************************
*                   *                                     *                   *
*                   *   FILE:          KassenListD.java   *                   *
*                   *                                     *                   *
*                    *************************************                    *
*                                                                             *
*                                                                             *
*      This program is free software; you can redistribute it and/or          *
*      modify it under the terms of the GNU General Public License as         *
*      published by the Free Software Foundation; either version 2 of         *
*      the License, or (at your option) any later version.                    *
*                                                                             *
*      This program is distributed in the hope that it will be useful,        *
*      but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the           *
*      GNU General Public License for more details.                           *
*                                                                             *
*      You should have received a copy of the GNU General Public License      *
*      along with this program; if not, write to the Free Software            *
*      Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.              *
*                                                                             *
*                                                                             *
\*****************************************************************************/
package foxda;
import java.lang.*;
import java.sql.*;
import java.awt.*;
import java.awt.event.*;
import java.beans.*;
import javax.swing.*;
import voji.log.*;
import voji.utils.*;
import voji.db.*;

/**
 *
 */
public class KassenListD extends ListDialog
{
    public static final String MODULE="kassen";

    public KassenListD(Dialog parent)
    {
        super(parent);
    }

    public KassenListD(Frame parent)
    {
        super(parent);
    }

    private Instance
        belegeD;

    protected void dialogInit()
    {
        JButton b;

        Main.checkModule(MODULE);

        super.dialogInit();
        setTitle("Kassen");

        belegeD=new Instance(BelegeListD.class,new Object[]{this});

        getResultTable().addColumn("Name",SwingConstants.LEFT,100,
                                   new DataFormat(null));

        getSpecialPanel().setLayout(new GridLayout(1,0));

        b=new JButton("Belege",Icons.BOOK);
        b.setMnemonic('B');
        b.setEnabled(Main.isEnabledModule(BelegeListD.MODULE));
        b.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e)
                {
                    JDialog d=(JDialog)belegeD.get();
                    firePropertyChange("kasse",null,
                                       getSelectedID());
                    d.setVisible(true);
                }
            });
        getSpecialPanel().add(b);

        pack();
    }

    protected EditDialog createEditDialog()
    {
        return new KassenEditD(this);
    }

    private static final VPreparedStatement tableStmt=new VPreparedStatement(
        "SELECT id,name "+
        "FROM kassen "+
        "WHERE id!=0 "+
        "ORDER BY name,id");
    protected void doUpdateDataFields()
    {
        try
            {
                getResultTable().
                    setData(tableStmt.executeQuery(new Object[]{}));
            }
        catch (SQLException e) { Log.log(e); }
    }
}
