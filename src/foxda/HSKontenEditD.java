/*****************************************************************************\
*                                                                             *
*  FoxdaFoxda      Foxda      Foxda   Foxda  FoxdaFoxda          Foxda        *
*  Foxda        Foxda Foxda    Foxda Foxda   Foxda  Foxda     Foxda Foxda     *
*  FoxdaFoxda  Foxda   Foxda      Foxda      Foxda   Foxda   Foxda   Foxda    *
*  Foxda        Foxda Foxda    Foxda Foxda   Foxda  Foxda   FoxdaFoxdaFoxda   *
*  Foxda           Foxda      Foxda   Foxda  FoxdaFoxda    Foxda       Foxda  *
*                                                                             *
*******************************************************************************
*                   *                                     *                   *
*                   *   FILE:        HSKontenEditD.java   *                   *
*                   *                                     *                   *
*                    *************************************                    *
*                                                                             *
*                                                                             *
*      This program is free software; you can redistribute it and/or          *
*      modify it under the terms of the GNU General Public License as         *
*      published by the Free Software Foundation; either version 2 of         *
*      the License, or (at your option) any later version.                    *
*                                                                             *
*      This program is distributed in the hope that it will be useful,        *
*      but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the           *
*      GNU General Public License for more details.                           *
*                                                                             *
*      You should have received a copy of the GNU General Public License      *
*      along with this program; if not, write to the Free Software            *
*      Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.              *
*                                                                             *
*                                                                             *
\*****************************************************************************/
package foxda;
import java.lang.*;
import java.util.*;
import java.sql.*;
import java.awt.*;
import javax.swing.*;
import voji.log.*;
import voji.utils.*;
import voji.db.*;
import voji.ui.*;

/**
 *
 */
public class HSKontenEditD extends EditDialog
{
    public static final String MODULE="hskasse";

    public HSKontenEditD(Dialog parent)
    {
        super(parent);
    }

    public HSKontenEditD(Frame parent)
    {
        super(parent);
    }

    private JFormatTextField
        dataNummer,
        dataGlied;
    private JTextField
        dataBeschreibung;
    private JComboBox<Object>
        dataSteuer,
        dataEinnahme;

    private static final VChoice chSteuer=new VChoice(new Object[]{
        "0%",                0.00,
        "7%",                0.07,
        "16%",               0.16,
        "19%",               0.19,
        "nur Steuern",       -1.00,
    });

    private static final VChoice ch=new VChoice(new Object[]{
        "Einnahme",        "E",
        "Ausgabe",         "A",
        "",                ""
    });

    protected void dialogInit()
    {
        JLabel l;

        Main.checkModule(MODULE);

        super.dialogInit();
        setTitle("HS-Kontodaten");

        getContentPanel().setLayout(new BorderLayout());

        JPanel labelPanel=new JPanel();
        labelPanel.setLayout(new GridLayout(0,1));
        getContentPanel().add(labelPanel,BorderLayout.WEST);

        JPanel inputPanel=new JPanel();
        inputPanel.setLayout(new GridLayout(0,1));
        getContentPanel().add(inputPanel,BorderLayout.CENTER);

        dataNummer=new JFormatTextField(Main.simpleNumberFormat);
        inputPanel.add(dataNummer);
        l=new JLabel(" Nummer: ",JLabel.RIGHT);
        l.setDisplayedMnemonic('N');
        l.setLabelFor(dataNummer);
        labelPanel.add(l);

        dataSteuer=new JComboBox<Object>(chSteuer.elements());
        inputPanel.add(dataSteuer);
        l=new JLabel(" Steuer: ",JLabel.RIGHT);
        l.setDisplayedMnemonic('S');
        l.setLabelFor(dataSteuer);
        labelPanel.add(l);

        dataEinnahme=new JComboBox<Object>(ch.elements());
        inputPanel.add(dataEinnahme);
        l=new JLabel(" E/A: ",JLabel.RIGHT);
        l.setDisplayedMnemonic('E');
        l.setLabelFor(dataEinnahme);
        labelPanel.add(l);

        dataGlied=new JFormatTextField(Main.numberFormat);
        inputPanel.add(dataGlied);
        l=new JLabel(" Glied: ",JLabel.RIGHT);
        l.setDisplayedMnemonic('G');
        l.setLabelFor(dataGlied);
        labelPanel.add(l);

        dataBeschreibung=new JTextField();
        inputPanel.add(dataBeschreibung);
        l=new JLabel(" Beschreibung: ",JLabel.RIGHT);
        l.setDisplayedMnemonic('B');
        l.setLabelFor(dataBeschreibung);
        labelPanel.add(l);

        pack();
    }

    protected void initComponents()
    {
        dataNummer.selectAll();
        dataGlied.selectAll();
        dataBeschreibung.selectAll();
        dataNummer.requestFocus();
    }

    private static final VPreparedStatement selectStmt=new VPreparedStatement(
        "SELECT nummer,steuersatz,einnahme,glied,beschr "+
        "FROM hskonten "+
        "WHERE id=?");
    protected void selectData(int id)
    {
        try
            {
                int i=1;
                ResultSet r=selectStmt.executeQuery(new Object[]{
                    id});
                if (!r.next()) throw new SQLException();
                dataNummer.setObject(r.getObject(i++));
                dataSteuer.setSelectedIndex(chSteuer.v2i(r.getObject(i++)));
                dataEinnahme.setSelectedIndex(ch.v2i(r.getObject(i++)));
                dataGlied.setObject(r.getObject(i++));
                dataBeschreibung.setText(r.getString(i++));
            }
        catch (SQLException e) { Log.log(e); }
    }

    private static final VPreparedStatement idStmt=new VPreparedStatement(
        "SELECT MAX(id)+1 "+
        "FROM hskonten");
    private static final VPreparedStatement insertStmt=new VPreparedStatement(
        "INSERT INTO hskonten "+
        "(nummer,steuersatz,einnahme,glied,beschr,id) "+
        "VALUES (?,?,?,?,?,?)");
    protected int insertData()
    {
        try
            {
                int id;
                ResultSet r=idStmt.executeQuery(new Object[]{});
                if (!r.next()) throw new SQLException();
                id=r.getInt(1);
                insertStmt.executeUpdate(new Object[]{
                    dataNummer.getObject(),
                    chSteuer.i2v(dataSteuer.getSelectedIndex()),
                    ch.i2v(dataEinnahme.getSelectedIndex()),
                    dataGlied.getObject(),
                    dataBeschreibung.getText(),
                    id});
                return id;
            }
        catch (SQLException e) { Log.log(e); }
        return -1;
    }

    private static final VPreparedStatement updateStmt=new VPreparedStatement(
        "UPDATE hskonten SET "+
        "nummer=?,"+
        "steuersatz=?,"+
        "einnahme=?,"+
        "glied=?,"+
        "beschr=? "+
        "WHERE id=?");
    protected void updateData(int id)
    {
        try
            {
                updateStmt.executeUpdate(new Object[]{
                    dataNummer.getObject(),
                    chSteuer.i2v(dataSteuer.getSelectedIndex()),
                    ch.i2v(dataEinnahme.getSelectedIndex()),
                    dataGlied.getObject(),
                    dataBeschreibung.getText(),
                    id});
            }
        catch (SQLException e) { Log.log(e); }
    }

    private static final VPreparedStatement deleteStmt=new VPreparedStatement(
        "DELETE FROM hskonten "+
        "WHERE id=?");
    protected void deleteData(int id)
    {
        try
            {
                deleteStmt.executeUpdate(new Object[]{
                    id});
            }
        catch (SQLException e) { Log.log(e); }
    }
}
