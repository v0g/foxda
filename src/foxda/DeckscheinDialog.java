/*****************************************************************************\
*                                                                             *
*  FoxdaFoxda      Foxda      Foxda   Foxda  FoxdaFoxda          Foxda        *
*  Foxda        Foxda Foxda    Foxda Foxda   Foxda  Foxda     Foxda Foxda     *
*  FoxdaFoxda  Foxda   Foxda      Foxda      Foxda   Foxda   Foxda   Foxda    *
*  Foxda        Foxda Foxda    Foxda Foxda   Foxda  Foxda   FoxdaFoxdaFoxda   *
*  Foxda           Foxda      Foxda   Foxda  FoxdaFoxda    Foxda       Foxda  *
*                                                                             *
*******************************************************************************
*                   *                                     *                   *
*                   *   FILE:     DeckscheinDialog.java   *                   *
*                   *                                     *                   *
*                    *************************************                    *
*                                                                             *
*                                                                             *
*      This program is free software; you can redistribute it and/or          *
*      modify it under the terms of the GNU General Public License as         *
*      published by the Free Software Foundation; either version 2 of         *
*      the License, or (at your option) any later version.                    *
*                                                                             *
*      This program is distributed in the hope that it will be useful,        *
*      but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the           *
*      GNU General Public License for more details.                           *
*                                                                             *
*      You should have received a copy of the GNU General Public License      *
*      along with this program; if not, write to the Free Software            *
*      Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.              *
*                                                                             *
*                                                                             *
\*****************************************************************************/
package foxda;
import java.lang.*;
import java.util.*;
import java.sql.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import voji.log.*;
import voji.utils.*;
import voji.db.*;
import voji.ui.*;

/**
 *
 */
public class DeckscheinDialog extends JDialog
{
    public static final String MODULE="kaninchen";

    public DeckscheinDialog(Dialog parent)
    {
        super(parent);
    }

    public DeckscheinDialog(Frame parent)
    {
        super(parent);
    }

    private int currentID;

    private JFormatTextField
        dataGeburt,
        dataAnzahl;
    private JLookupComponent
        dataZuechter,
        dataZuechter2,
        dataTaetoM,
        dataVater,
        dataMutter;
    private JPanel
        dataPanel;

    protected class TierComponents
    {
        public JPanel            panel      = null;
        public JComboBox<Object> geschlecht = null;
        public JLookupComponent  farbe      = null;
        public JTextField        tr         = null;
        public JFormatTextField  tl_monat   = null;
        public JFormatTextField  tl_jahr    = null;
        public JFormatTextField  tl_nummer  = null;
    };
    private TierComponents[] dataTiere;
    private int              anzahlTiere=0;

    private static final VChoice ch=new VChoice(new Object[]{
        "1,0", "1,0",
        "0,1", "0,1",
        "",    ""
    });

    protected class set01ActionListener implements ActionListener
    {
        private int n;

        public set01ActionListener(int nr)
        {
            n=nr;
        }

        public void actionPerformed(ActionEvent e)
        {
            setGeschlecht(n,"0,1");
        }
    }

    protected void dialogInit()
    {
        JButton b;
        JLabel l;
        JPanel p;
        ActionListener a;
        Set<AWTKeyStroke> s;

        Main.checkModule(MODULE);

        super.dialogInit();
        setModal(true);
        setTitle("Deckschein eintragen");
        getContentPane().setLayout(new BorderLayout());

        JPanel generalPanel=new JPanel();
        generalPanel.setLayout(new GridLayout(0,2));
        getContentPane().add(generalPanel,BorderLayout.NORTH);

        JLabel vaterLabel=new JLabel("Vater:",JLabel.CENTER);
        vaterLabel.setDisplayedMnemonic('V');
        generalPanel.add(vaterLabel);

        JLabel mutterLabel=new JLabel("Mutter:",JLabel.CENTER);
        mutterLabel.setDisplayedMnemonic('M');
        generalPanel.add(mutterLabel);

        dataVater=new JLookupComponent
            (new Instance(){
                    public Object create()
                    {
                        KaninchenListD d=
                            new KaninchenListD(DeckscheinDialog.this);
                        d.setSearchGeschlecht("1,0");
                        return d;
                    }},
             Main.taetoFormat,
             "SELECT tr,tl_monat,tl_jahr,tl_nummer "+
             "FROM kaninchen "+
             "WHERE id=?",
             'V');
        generalPanel.add(dataVater);

        dataMutter=new JLookupComponent
            (new Instance(){
                    public Object create()
                    {
                        KaninchenListD d=
                            new KaninchenListD(DeckscheinDialog.this);
                        d.setSearchGeschlecht("0,1");
                        return d;
                    }},
             Main.taetoFormat,
             "SELECT tr,tl_monat,tl_jahr,tl_nummer "+
             "FROM kaninchen "+
             "WHERE id=?",
             'M');
        dataMutter.addIDListener(new IDListener(){
                public void idChanged(int id)
                {
                    mutterChanged();
                }
            });
        generalPanel.add(dataMutter);

        dataZuechter=new JLookupComponent
            (new Instance(PersonenListD.class,new Object[]{this}),
             Main.personenFormat,
             "SELECT nachname,vorname "+
             "FROM personen "+
             "WHERE id=?",
             'Z');
        dataZuechter.addIDListener(new IDListener(){
                public void idChanged(int id)
                {
                    zuechterChanged();
                }
            });
        l=new JLabel(" Züchter: ",JLabel.RIGHT);
        l.setDisplayedMnemonic('Z');
        generalPanel.add(l);
        generalPanel.add(dataZuechter);

        dataZuechter2=new JLookupComponent
            (new Instance(PersonenListD.class,new Object[]{this}),
             Main.personenFormat,
             "SELECT nachname,vorname "+
             "FROM personen "+
             "WHERE id=?",
             'H');
        l=new JLabel(" 2. Züchter: ",JLabel.RIGHT);
        l.setDisplayedMnemonic('H');
        generalPanel.add(l);
        generalPanel.add(dataZuechter2);

        dataGeburt=new JFormatTextField(Main.dateFormat);
        dataGeburt.addObjectListener(new ObjectListener(){
                public void objectChanged(Object o)
                {
                    geburtChanged();
                }
            });
        l=new JLabel(" Geburtsdatum: ",JLabel.RIGHT);
        l.setDisplayedMnemonic('D');
        l.setLabelFor(dataGeburt);
        generalPanel.add(l);
        generalPanel.add(dataGeburt);

        dataAnzahl=new JFormatTextField(Main.numberFormat);
        dataAnzahl.addObjectListener(new ObjectListener(){
                public void objectChanged(Object obj)
                {
                    try
                        {
                            setAnzahlTiere(((Number)dataAnzahl.
                                            getObject()).intValue());
                        }
                    catch (Exception e) {}
                }
            });
        l=new JLabel(" Anzahl: ",JLabel.RIGHT);
        l.setDisplayedMnemonic('N');
        l.setLabelFor(dataAnzahl);
        generalPanel.add(l);
        generalPanel.add(dataAnzahl);

        dataTaetoM=new JLookupComponent
            (new Instance(){
                    public Object create()
                    {
                        PersonenListD d=
                            new PersonenListD(DeckscheinDialog.this);
                        d.setSearchFunktion("Tätowiermeister");
                        return d;
                    }},
             Main.personenFormat,
             "SELECT nachname,vorname "+
             "FROM personen "+
             "WHERE id=?",
             'T');
        l=new JLabel(" Täto-Meister: ",JLabel.RIGHT);
        l.setDisplayedMnemonic('T');
        generalPanel.add(l);
        generalPanel.add(dataTaetoM);

        dataPanel=new JPanel();
        dataPanel.setLayout(new GridLayout(0,1));
        getContentPane().add(dataPanel,BorderLayout.CENTER);

        dataTiere=new TierComponents[12];
        for (int i=0;i<dataTiere.length;i++)
            {
                TierComponents t=dataTiere[i]=new TierComponents();

                t.panel=new JPanel();
                t.panel.setLayout(new GridLayout(1,0));
                t.panel.setVisible(false);
                dataPanel.add(t.panel);

                p=new JPanel();
                p.setLayout(new FlowLayout(FlowLayout.RIGHT,0,0));
                t.panel.add(p);

                p.add(new JLabel((i+1)+". "));

                b=new JButton(Icons.SOUTH);
                b.setToolTipText("Ab hier alles Häsinnen");
                b.setMnemonic(i<10 ? '0'+((i+1)%10) : 'A'+i-10);
                b.setMargin(new Insets(0,0,0,0));
                b.addActionListener(new set01ActionListener(i));
                p.add(b);

                p.add(new JLabel(" "));

                t.geschlecht=new JComboBox<Object>(ch.elements());
                p.add(t.geschlecht);

                t.farbe=new JLookupComponent
                    (new Instance(FarbenListD.class,new Object[]{this}),
                     Main.rassenFormat,
                     "SELECT r.kuerzel,f.farbe "+
                     "FROM farben f,rassen r "+
                     "WHERE f.rasse=r.id AND "+
                     "f.id=?");
                t.panel.add(t.farbe);

                p=new JPanel();
                p.setLayout(new FlowLayout(FlowLayout.LEFT,0,0));
                t.panel.add(p);

                l=new JLabel(" TR: ");
                if (i==0) l.setDisplayedMnemonic('R');
                p.add(l);

                t.tr=new JTextField(5);
                if (i==0) t.tr.getDocument().addDocumentListener(new DocumentListener(){
                        public void changedUpdate(DocumentEvent e)
                        {
                            setTR(1,dataTiere[0].tr.getText());
                        }
                        public void insertUpdate(DocumentEvent e)
                        {
                            setTR(1,dataTiere[0].tr.getText());
                        }
                        public void removeUpdate(DocumentEvent e)
                        {
                            setTR(1,dataTiere[0].tr.getText());
                        }
                    });
                l.setLabelFor(t.tr);
                p.add(t.tr);

                p=new JPanel();
                p.setLayout(new FlowLayout(FlowLayout.LEFT,0,0));
                t.panel.add(p);

                l=new JLabel(" TL: ");
                if (i==0) l.setDisplayedMnemonic('L');
                p.add(l);

                t.tl_monat=new JFormatTextField(Main.numberFormat);
                t.tl_monat.setColumns(2);
                s=new HashSet<AWTKeyStroke>(t.tl_monat.getFocusTraversalKeys
                                            (KeyboardFocusManager.FORWARD_TRAVERSAL_KEYS));
                s.add(KeyStroke.getKeyStroke(KeyEvent.VK_PERIOD,0));
                t.tl_monat.setFocusTraversalKeys
                    (KeyboardFocusManager.FORWARD_TRAVERSAL_KEYS,s);
                if (i==0) t.tl_monat.addObjectListener(new ObjectListener(){
                        public void objectChanged(Object o)
                        {
                            setTL_monat(1,o);
                        }
                    });
                p.add(t.tl_monat);

                p.add(new JLabel(" . "));

                t.tl_jahr=new JFormatTextField(Main.numberFormat);
                t.tl_jahr.setColumns(1);
                s=new HashSet<AWTKeyStroke>(t.tl_jahr.getFocusTraversalKeys
                                            (KeyboardFocusManager.FORWARD_TRAVERSAL_KEYS));
                s.add(KeyStroke.getKeyStroke(KeyEvent.VK_PERIOD,0));
                t.tl_jahr.setFocusTraversalKeys
                    (KeyboardFocusManager.FORWARD_TRAVERSAL_KEYS,s);
                if (i==0) t.tl_jahr.addObjectListener(new ObjectListener(){
                        public void objectChanged(Object o)
                        {
                            setTL_jahr(1,o);
                        }
                    });
                p.add(t.tl_jahr);

                p.add(new JLabel(" . "));

                t.tl_nummer=new JFormatTextField(Main.numberFormat);
                t.tl_nummer.setColumns(3);
                if (i==0) t.tl_nummer.addObjectListener(new ObjectListener(){
                        public void objectChanged(Object o)
                        {
                            setTL_nummer(1,o);
                        }
                    });
                l.setLabelFor(t.tl_nummer);
                p.add(t.tl_nummer);
            }

        JPanel closePanel=new JPanel();
        closePanel.setLayout(new GridLayout(1,0));
        closePanel.setBorder(BorderFactory.createRaisedBevelBorder());
        getContentPane().add(closePanel,BorderLayout.SOUTH);

        b=new JButton("OK");
        getRootPane().setDefaultButton(b);
        b.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e)
                {
                    currentID=insertData();
                    setVisible(false);
                }
            });
        closePanel.add(b);

        b=new JButton("Abbrechen");
        b.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e)
                {
                    setVisible(false);
                }
            });
        closePanel.add(b);

        pack();
    }

    public void setVisible(boolean visible)
    {
        if (visible) {
            dataGeburt.setObject(null);
            dataAnzahl.setObject(null);
            anzahlTiere=0;
            for (int i=0;i<dataTiere.length;i++)
                {
                    dataTiere[i].panel.setVisible(false);
                    dataTiere[i].geschlecht.setSelectedIndex(ch.v2i("1,0"));
                    dataTiere[i].tl_monat.setObject(null);
                    dataTiere[i].tl_nummer.setObject(null);
                }
        }
        super.setVisible(visible);
    }

    public int showInsert()
    {
        currentID=-1;
        setVisible(true);
        return currentID;
    }

    protected void setAnzahlTiere(int n)
    {
        if (n>0 && n!=anzahlTiere)
            {
                anzahlTiere=Math.min(n,dataTiere.length);
                for (int i=0;i<dataTiere.length;i++)
                    dataTiere[i].panel.setVisible(i<anzahlTiere);
            }
    }

    protected void setGeschlecht(int n,String g)
    {
        if (n>=0)
            {
                for (int i=n;i<dataTiere.length;i++)
                    dataTiere[i].geschlecht.setSelectedIndex(ch.v2i(g));
            }
    }

    protected void setTR(int n,String t)
    {
        if (n>=0)
            {
                for (int i=n;i<dataTiere.length;i++)
                    dataTiere[i].tr.setText(t);
            }
    }

    protected void setTL_monat(int n,Object t)
    {
        if (n>=0)
            {
                for (int i=n;i<dataTiere.length;i++)
                    dataTiere[i].tl_monat.setObject(t);
            }
    }

    protected void setTL_jahr(int n,Object t)
    {
        if (n>=0)
            {
                for (int i=n;i<dataTiere.length;i++)
                    dataTiere[i].tl_jahr.setObject(t);
            }
    }

    protected void setTL_nummer(int n,Object o)
    {
        int t;
        try { t=((Number)o).intValue(); }
        catch (Exception e) { return; }

        if (n>=0)
            {
                for (int i=n;i<dataTiere.length;i++)
                    dataTiere[i].tl_nummer.setObject(t+i-n+1);
            }
    }

    private static final VPreparedStatement idStmt=new VPreparedStatement(
        "SELECT MAX(id)+1 "+
        "FROM kaninchen");
    private static final VPreparedStatement insertStmt=new VPreparedStatement(
        "INSERT INTO kaninchen "+
        "(tr,tl_monat,tl_jahr,tl_nummer,farbe,geschlecht,"+
        "geburt,vater,mutter,zuechter,zuechter2,taeto_m,id) "+
        "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)");
    protected int insertData()
    {
        try
            {
                int id;
                ResultSet r=idStmt.executeQuery(new Object[]{});
                if (!r.next()) throw new SQLException();
                id=r.getInt(1);

                for (int i=0;i<anzahlTiere;i++)
                    {
                        insertStmt.executeUpdate(new Object[]{
                            dataTiere[i].tr.getText(),
                            dataTiere[i].tl_monat.getObject(),
                            dataTiere[i].tl_jahr.getObject(),
                            dataTiere[i].tl_nummer.getObject(),
                            dataTiere[i].farbe.getSelectedID(),
                            ch.i2v(dataTiere[i].geschlecht.
                                   getSelectedIndex()),
                            dataGeburt.getObject(),
                            dataVater.getSelectedID(),
                            dataMutter.getSelectedID(),
                            dataZuechter.getSelectedID(),
                            dataZuechter2.getSelectedID(),
                            dataTaetoM.getSelectedID(),
                            id+i});
                    }
                return id+anzahlTiere-1;
            }
        catch (SQLException e) { Log.log(e); }
        return -1;
    }

    private static final VPreparedStatement mutterStmt=new VPreparedStatement(
        "SELECT farbe,zuechter,zuechter2 FROM kaninchen "+
        "WHERE id=?");
    protected void mutterChanged()
    {
        int id=dataMutter.getSelectedID();
        if (id==0) return;
        try
            {
                int i=1;
                ResultSet r=mutterStmt.executeQuery(new Object[]{
                    id});
                if (!r.next()) throw new SQLException();
                int f=r.getInt(i++);
                for (int j=0;j<dataTiere.length;j++)
                    dataTiere[j].farbe.setSelectedID(f);
                dataZuechter.setSelectedID(r.getInt(i++));
                dataZuechter2.setSelectedID(r.getInt(i++));
            }
        catch (SQLException e) { Log.log(e); }
    }

    private static final VPreparedStatement zuechterStmt=new VPreparedStatement(
        "SELECT v.kuerzel,p.kennung FROM vereine v,personen p "+
        "WHERE p.id=? AND v.id=p.verein");
    protected void zuechterChanged()
    {
        int id=dataZuechter.getSelectedID();
        if (id==0) return;
        try
            {
                int i=1;
                ResultSet r=zuechterStmt.executeQuery(new Object[]{
                    id});
                if (!r.next()) throw new SQLException();
                for (int j=0;j<dataTiere.length;j++)
                    dataTiere[j].tr.setText(Main.trFormat.format(r,1));
            }
        catch (SQLException e) { Log.log(e); }
    }

    protected void geburtChanged()
    {
        java.sql.Date d=(java.sql.Date)dataGeburt.getObject();
        if (d==null) return;

        Calendar c=Calendar.getInstance();
        c.setTime(d);
        Integer monat=c.get(Calendar.MONTH)+1;
        Integer jahr=c.get(Calendar.YEAR)%10;

        for (int j=0;j<dataTiere.length;j++)
            {
                dataTiere[j].tl_monat.setObject(monat);
                dataTiere[j].tl_jahr.setObject(jahr);
            }
    }
}
