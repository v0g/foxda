/*****************************************************************************\
*                                                                             *
*  FoxdaFoxda      Foxda      Foxda   Foxda  FoxdaFoxda          Foxda        *
*  Foxda        Foxda Foxda    Foxda Foxda   Foxda  Foxda     Foxda Foxda     *
*  FoxdaFoxda  Foxda   Foxda      Foxda      Foxda   Foxda   Foxda   Foxda    *
*  Foxda        Foxda Foxda    Foxda Foxda   Foxda  Foxda   FoxdaFoxdaFoxda   *
*  Foxda           Foxda      Foxda   Foxda  FoxdaFoxda    Foxda       Foxda  *
*                                                                             *
*******************************************************************************
*                   *                                     *                   *
*                   *   FILE:        HSKassenEditD.java   *                   *
*                   *                                     *                   *
*                    *************************************                    *
*                                                                             *
*                                                                             *
*      This program is free software; you can redistribute it and/or          *
*      modify it under the terms of the GNU General Public License as         *
*      published by the Free Software Foundation; either version 2 of         *
*      the License, or (at your option) any later version.                    *
*                                                                             *
*      This program is distributed in the hope that it will be useful,        *
*      but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the           *
*      GNU General Public License for more details.                           *
*                                                                             *
*      You should have received a copy of the GNU General Public License      *
*      along with this program; if not, write to the Free Software            *
*      Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.              *
*                                                                             *
*                                                                             *
\*****************************************************************************/
package foxda;
import java.lang.*;
import java.util.*;
import java.sql.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import voji.log.*;
import voji.utils.*;
import voji.db.*;
import voji.ui.*;

/**
 *
 */
public class HSKassenEditD extends EditDialog
{
    public static final String MODULE="hskasse";

    public HSKassenEditD(Dialog parent)
    {
        super(parent);
    }

    public HSKassenEditD(Frame parent)
    {
        super(parent);
    }

    private HSAnlagenEditD anlagenD;

    private JFormatTextField
        dataNummer,
        dataBetrag,
        dataSteuer,
        dataKonto,
        dataDatum;
    private JCheckBox
        dataKasse,
        dataBuch;
    private JComboBox<Object>
        dataChSteuer,
        dataZweck,
        dataStatus;
    private JLookupComponent
        dataKontoLookup;
    private JButton
        buttonAnlagen,
        buttonDatum;

    private static final VChoice chSteuer=new VChoice(new Object[]{
        "0%",          0.00,
        "7%",          0.07,
        "16%",         0.16,
        "19%",         0.19,
        "nur Steuern", -1.00,
        "",            null,
    });

    private static final VChoice ch=new VChoice(new Object[]{
        "Erledigt",               "",
        "Offen (X)",              "X",
        "Teilweise erledigt (*)", "*",
    });

    public Dimension getPreferredSize()
    {
        return new Dimension(400,super.getPreferredSize().height);
    }

    protected void dialogInit()
    {
        JLabel l;
        JPanel p;

        Main.checkModule(MODULE);

        super.dialogInit();
        setTitle("HS-Belegdaten");

        anlagenD=new HSAnlagenEditD(this);

        getContentPanel().setLayout(new BorderLayout());

        JPanel labelPanel=new JPanel();
        labelPanel.setLayout(new GridLayout(0,1));
        getContentPanel().add(labelPanel,BorderLayout.WEST);

        JPanel inputPanel=new JPanel();
        inputPanel.setLayout(new GridLayout(0,1));
        getContentPanel().add(inputPanel,BorderLayout.CENTER);

        dataNummer=new JFormatTextField(Main.numberFormat);
        inputPanel.add(dataNummer);
        l=new JLabel(" Nummer: ",JLabel.RIGHT);
        l.setDisplayedMnemonic('N');
        l.setLabelFor(dataNummer);
        labelPanel.add(l);

        p=new JPanel();
        p.setLayout(new BorderLayout());
        inputPanel.add(p);

        dataBetrag=new JFormatTextField(Main.numberFormat);
        dataBetrag.addObjectListener(new ObjectListener(){
                public void objectChanged(Object obj)
                {
                    calcSteuer();
                }
            });
        p.add(dataBetrag,BorderLayout.CENTER);
        l=new JLabel(" Betrag: ",JLabel.RIGHT);
        l.setDisplayedMnemonic('B');
        l.setLabelFor(dataBetrag);
        labelPanel.add(l);

        buttonAnlagen=new JButton("Anlagen");
        buttonAnlagen.setMnemonic('L');
        buttonAnlagen.setFocusable(false);
        buttonAnlagen.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e)
                {
                    anlagenD.show(getID());
                }
            });
        p.add(buttonAnlagen,BorderLayout.EAST);

        p=new JPanel();
        p.setLayout(new BorderLayout());
        inputPanel.add(p);

        dataKonto=new JFormatTextField(Main.simpleNumberFormat);
        dataKonto.setColumns(5);
        dataKonto.addObjectListener(new ObjectListener(){
                public void objectChanged(Object obj)
                {
                    kontoChanged(obj==null ? 0 : ((Number)obj).intValue());
                }
            });
        p.add(dataKonto,BorderLayout.WEST);
        l=new JLabel(" Konto: ",JLabel.RIGHT);
        l.setDisplayedMnemonic('K');
        l.setLabelFor(dataKonto);
        labelPanel.add(l);

        dataKontoLookup=new JLookupComponent
            (new Instance(HSKontenListD.class,new Object[]{this}),
             null,
             "SELECT beschr "+
             "FROM hskonten "+
             "WHERE id=?");
        dataKontoLookup.addIDListener(new IDListener(){
                public void idChanged(int id)
                {
                    kontoLookupChanged(id);
                }
            });
        dataKontoLookup.setButtonFocusable(false);
        p.add(dataKontoLookup,BorderLayout.CENTER);

        p=new JPanel();
        p.setLayout(new BorderLayout());
        inputPanel.add(p);

        dataChSteuer=new JComboBox<Object>(chSteuer.elements());
        dataChSteuer.addItemListener(new ItemListener(){
                public void itemStateChanged(ItemEvent e)
                {
                    calcSteuer();
                }
            });
        p.add(dataChSteuer,BorderLayout.WEST);

        dataSteuer=new JFormatTextField(Main.numberFormat);
        dataSteuer.addObjectListener(new ObjectListener(){
                public void objectChanged(Object obj)
                {
                    calcChSteuer();
                }
            });
        p.add(dataSteuer,BorderLayout.CENTER);

        l=new JLabel(" Steuer: ",JLabel.RIGHT);
        l.setDisplayedMnemonic('S');
        l.setLabelFor(dataChSteuer);
        labelPanel.add(l);

        p=new JPanel();
        p.setLayout(new BorderLayout());
        inputPanel.add(p);

        dataDatum=new JFormatTextField(Main.dateFormat);
        dataDatum.setEditable(false);
        p.add(dataDatum,BorderLayout.CENTER);
        l=new JLabel(" Datum: ",JLabel.RIGHT);
        l.setDisplayedMnemonic('D');
        l.setLabelFor(dataDatum);
        labelPanel.add(l);

        buttonDatum=new JButton(Icons.DATE);
        buttonDatum.setEnabled(false);
        buttonDatum.setFocusable(false);
        buttonDatum.setToolTipText("Aktuelles Datum");
        buttonDatum.setMnemonic('A');
        buttonDatum.setMargin(new Insets(0,0,0,0));
        buttonDatum.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e)
                {
                    calcDatum();
                }
            });
        p.add(buttonDatum,BorderLayout.EAST);

        dataZweck=new JComboBox<Object>(new Object[]{
            "Rechnung ",
            "Amazon.de",
            "Benzin",
            "Containerdienst",
            "DKV",
            "Diesel",
            "Einlage",
            "Fries",
            "Kontoführungsgeühren",
            "Leymann",
            "MwSt",
            "Porto",
            "Printus",
            "Privatentnahme",
            "Raiffeisen-AGRAVIS",
            "Rauthmann",
            "Rente",
            "Rohwedder",
            "Schlegel",
            "Schüler",
            "Steuer",
            "Telekom",
            "toom",
            "Versicherung",
            "Vodafone"
        });
        dataZweck.setEditable(true);
        inputPanel.add(dataZweck);
        l=new JLabel(" Zweck: ",JLabel.RIGHT);
        l.setDisplayedMnemonic('Z');
        l.setLabelFor(dataZweck);
        labelPanel.add(l);

        p=new JPanel();
        p.setLayout(new GridLayout(1,0));
        inputPanel.add(p);
        labelPanel.add(new JLabel(" Eintrag: ",JLabel.RIGHT));

        dataKasse=new JCheckBox("Handkasse");
        dataKasse.setMnemonic('H');
        p.add(dataKasse);

        dataBuch=new JCheckBox("Buchung");
        dataBuch.setMnemonic('U');
        p.add(dataBuch);

        dataStatus=new JComboBox<Object>(ch.elements());
        dataStatus.addItemListener(new ItemListener(){
                public void itemStateChanged(ItemEvent e)
                {
                    boolean lock=
                        "".equals(ch.i2v(dataStatus.getSelectedIndex()));
                    dataDatum.setEditable(!lock);
                    buttonDatum.setEnabled(!lock);
                }
            });
        inputPanel.add(dataStatus);
        l=new JLabel(" Status: ",JLabel.RIGHT);
        l.setDisplayedMnemonic('T');
        l.setLabelFor(dataStatus);
        labelPanel.add(l);

        pack();
    }

    protected void initComponents()
    {
        dataNummer.selectAll();
        dataBetrag.selectAll();
        dataSteuer.selectAll();
        dataKonto.selectAll();
        dataDatum.selectAll();
        dataZweck.getEditor().selectAll();
        dataBetrag.requestFocus();
    }

    protected boolean noLookupChange=false;

    private static final VPreparedStatement kontoStmt=new VPreparedStatement(
        "SELECT id,steuersatz,einnahme "+
        "FROM hskonten "+
        "WHERE nummer=?");
    protected void kontoChanged(int konto)
    {
        noLookupChange=true;

        boolean k=konto>0;
        dataNummer.setVisible(k);
        dataKasse.setSelected(true);
        dataBuch.setSelected(k);

        try
            {
                int i=1;
                ResultSet r=kontoStmt.executeQuery(new Object[]{
                    konto});
                if (!r.next()) throw new SQLException();

                dataKontoLookup.setSelectedID(r.getInt(i++));

                dataChSteuer.setSelectedIndex(chSteuer.v2i(r.getObject(i++)));

                String einnahme=r.getString(i++);
                if (!"".equals(einnahme)) dataBetrag.setObject(
                    ("E".equals(einnahme) ? +1 : -1)*
                    Math.abs(((Number)dataBetrag.getObject()).doubleValue()));
            }
        catch (SQLException e)
            {
                dataKontoLookup.setSelectedID(-1);
            }
        catch (NullPointerException e) {}

        noLookupChange=false;
    }

    private static final VPreparedStatement kontoLookupStmt=new VPreparedStatement(
        "SELECT nummer "+
        "FROM hskonten "+
        "WHERE id=?");
    protected void kontoLookupChanged(int id)
    {
        if (noLookupChange) return;

        try
            {
                int i=1;
                ResultSet r=kontoLookupStmt.executeQuery(new Object[]{
                    id});
                if (!r.next()) throw new SQLException();

                dataKonto.setObject(r.getObject(i++));
            }
        catch (SQLException e) {}
    }

    protected boolean noCalc=false;

    protected void calcChSteuer()
    {
        if (noCalc) return;

        Number full=(Number)dataBetrag.getObject();
        Number steuer=(Number)dataSteuer.getObject();
        if (full==null || steuer==null) return;

        noCalc=true;
        dataChSteuer.setSelectedIndex(chSteuer.v2i(
            Main.getSteuerPercent(full.doubleValue(),steuer.doubleValue())));
        noCalc=false;
    }

    protected void calcSteuer()
    {
        if (noCalc) return;

        Number full=(Number)dataBetrag.getObject();
        Number steuer=(Number)chSteuer.i2v(dataChSteuer.getSelectedIndex());
        if (full==null || steuer==null) return;

        if (steuer.doubleValue()<0)
            {
                dataBuch.setSelected(true);
            }

        noCalc=true;
        dataSteuer.setObject(
            Main.getSteuer(full.doubleValue(),steuer.doubleValue()));
        noCalc=false;
    }

    protected void calcDatum()
    {
        dataDatum.setObject(new java.util.Date());
    }

    private static final VPreparedStatement newStmt=new VPreparedStatement(
        "SELECT MAX(nummer)+1 "+
        "FROM hskasse");
    protected void newData()
    {
        int nr;
        try
            {
                int i=1;
                ResultSet r=newStmt.executeQuery(new Object[]{});
                if (!r.next()) throw new SQLException();

                nr=r.getInt(i++);
            }
        catch (SQLException e) { nr=1; }

        dataKonto.setObject(null);
        dataNummer.setObject(nr);
        dataNummer.setVisible(true);
        dataBetrag.setText("");
        dataChSteuer.setSelectedIndex(chSteuer.v2i(0));
        dataSteuer.setText("");
        dataDatum.setObject(new java.util.Date());
        dataZweck.setSelectedItem("");
        dataKasse.setSelected(false);
        dataBuch.setSelected(false);
        dataStatus.setSelectedIndex(ch.v2i("X"));
        buttonAnlagen.setEnabled(false);
    }

    private static final VPreparedStatement selectStmt=new VPreparedStatement(
        "SELECT konto,nummer,betrag,steuer,steuersatz,status,datum,zweck,"+
        "       kasse!=0,buch!=0 "+
        "FROM hskasse "+
        "WHERE id=?");
    protected void selectData(int id)
    {
        try
            {
                int i=1;
                ResultSet r=selectStmt.executeQuery(new Object[]{
                    id});
                if (!r.next()) throw new SQLException();
                noCalc=true;
                noLookupChange=true;
                dataKonto.setObject(r.getObject(i++));
                dataNummer.setObject(r.getObject(i++));
                dataBetrag.setObject(r.getObject(i++));
                dataSteuer.setObject(r.getObject(i++));
                dataChSteuer.setSelectedIndex(chSteuer.v2i(r.getObject(i++)));
                String status=r.getString(i++);
                dataDatum.setObject("".equals(status)
                                    ? r.getObject(i)
                                    : new java.util.Date());
                i++;
                dataZweck.setSelectedItem(r.getObject(i++));
                dataKasse.setSelected(r.getBoolean(i++));
                dataBuch.setSelected(r.getBoolean(i++));
                dataStatus.setSelectedIndex(ch.v2i(status));
                buttonAnlagen.setEnabled(true);
                noCalc=false;
                noLookupChange=false;
            }
        catch (SQLException e) { Log.log(e); }
    }

    private static final VPreparedStatement idStmt=new VPreparedStatement(
        "SELECT MAX(id)+1 "+
        "FROM hskasse");
    private static final VPreparedStatement insertStmt=new VPreparedStatement(
        "INSERT INTO hskasse "+
        "(nummer,betrag,steuer,steuersatz,konto,datum,zweck,kasse,buch,"+
        " status,anlage,anlagetyp,id) "+
        "VALUES (?::INTEGER,?,?,?,?,?,?,?,?,?,?,?,?)");
    protected int insertData()
    {
        try
            {
                int id;
                ResultSet r=idStmt.executeQuery(new Object[]{});
                if (!r.next()) throw new SQLException();
                id=r.getInt(1);
                insertStmt.executeUpdate(new Object[]{
                    dataNummer.isVisible()
                    ? dataNummer.getObject() : 0,
                    dataBetrag.getObject(),
                    dataSteuer.getObject(),
                    chSteuer.i2v(dataChSteuer.getSelectedIndex()),
                    dataKonto.getObject(),
                    dataDatum.getObject(),
                    dataZweck.getSelectedItem(),
                    dataKasse.isSelected() ? 1 : 0,
                    dataBuch.isSelected() ? 1 : 0,
                    ch.i2v(dataStatus.getSelectedIndex()),
                    0,
                    "",
                    id});
                return id;
            }
        catch (SQLException e) { Log.log(e); }
        return -1;
    }

    private static final VPreparedStatement updateStmt=new VPreparedStatement(
        "UPDATE hskasse SET "+
        "nummer=?::INTEGER,"+
        "betrag=?,"+
        "steuer=?,"+
        "steuersatz=?,"+
        "konto=?,"+
        "datum=?,"+
        "zweck=?,"+
        "kasse=?,"+
        "buch=?,"+
        "status=? "+
        "WHERE id=?");
    protected void updateData(int id)
    {
        try
            {
                updateStmt.executeUpdate(new Object[]{
                    dataNummer.isVisible()
                    ? dataNummer.getObject() : 0,
                    dataBetrag.getObject(),
                    dataSteuer.getObject(),
                    chSteuer.i2v(dataChSteuer.getSelectedIndex()),
                    dataKonto.getObject(),
                    dataDatum.getObject(),
                    dataZweck.getSelectedItem(),
                    dataKasse.isSelected() ? 1 : 0,
                    dataBuch.isSelected() ? 1 : 0,
                    ch.i2v(dataStatus.getSelectedIndex()),
                    id});
            }
        catch (SQLException e) { Log.log(e); }
    }

    private static final VPreparedStatement deleteStmt=new VPreparedStatement(
        "DELETE FROM hskasse "+
        "WHERE id=?");
    protected void deleteData(int id)
    {
        try
            {
                deleteStmt.executeUpdate(new Object[]{
                    id});
            }
        catch (SQLException e) { Log.log(e); }
    }
}
