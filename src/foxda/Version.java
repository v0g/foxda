package foxda;
public interface Version{
 public static final String VERSION="0.4.9";
 public static final String AUTHORS="Volker Diels-Grabsch <v@njh.eu>";
 public static final String YEARS="2001-2019";
}
