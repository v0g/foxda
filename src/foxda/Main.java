/*****************************************************************************\
*                                                                             *
*  FoxdaFoxda      Foxda      Foxda   Foxda  FoxdaFoxda          Foxda        *
*  Foxda        Foxda Foxda    Foxda Foxda   Foxda  Foxda     Foxda Foxda     *
*  FoxdaFoxda  Foxda   Foxda      Foxda      Foxda   Foxda   Foxda   Foxda    *
*  Foxda        Foxda Foxda    Foxda Foxda   Foxda  Foxda   FoxdaFoxdaFoxda   *
*  Foxda           Foxda      Foxda   Foxda  FoxdaFoxda    Foxda       Foxda  *
*                                                                             *
*******************************************************************************
*                   *                                     *                   *
*                   *   FILE:                 Main.java   *                   *
*                   *                                     *                   *
*                    *************************************                    *
*                                                                             *
*                                                                             *
*      This program is free software; you can redistribute it and/or          *
*      modify it under the terms of the GNU General Public License as         *
*      published by the Free Software Foundation; either version 2 of         *
*      the License, or (at your option) any later version.                    *
*                                                                             *
*      This program is distributed in the hope that it will be useful,        *
*      but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the           *
*      GNU General Public License for more details.                           *
*                                                                             *
*      You should have received a copy of the GNU General Public License      *
*      along with this program; if not, write to the Free Software            *
*      Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.              *
*                                                                             *
*                                                                             *
\*****************************************************************************/
package foxda;
import java.lang.*;
import java.util.*;
import java.text.*;
import java.sql.*;
import java.io.*;
import javax.swing.*;
import voji.utils.*;
import voji.log.*;
import voji.db.*;
import voji.ui.*;

/**
 * This is the main class.
 * Here are the startup code, locale definition, format objects,
 * cell renderers, log destinations and database connection.
 */
public class Main
{
    // properties
    public static Properties properties=new Properties();

    // ini file
    public static String iniFileName="foxda.ini";

    // l10n
    private static final Locale l=Locale.GERMANY;

    // formats
    public static final BooleanFormat booleanFormat=
        new BooleanFormat();
    public static final NumberFormat simpleNumberFormat=
        new DecimalFormat("#");
    public static final NumberFormat numberFormat=
        NumberFormat.getNumberInstance(l);
    public static final NumberFormat percentFormat=
        NumberFormat.getPercentInstance(l);
    public static final NumberFormat currencyFormat=
        NumberFormat.getCurrencyInstance(l);
    public static final SQLDateFormat dateFormat=
        new SQLDateFormat(SQLDateFormat.DATE,
                          new SimpleDateFormat("dd.MM.yyyy",l));
    public static final SQLDateFormat timeFormat=
        new SQLDateFormat(SQLDateFormat.TIME,
                          DateFormat.getTimeInstance(DateFormat.SHORT,l));
    public static final SQLDateFormat timestampFormat=
        new SQLDateFormat(SQLDateFormat.TIMESTAMP,
                          DateFormat.getDateTimeInstance(DateFormat.SHORT,
                                                         DateFormat.SHORT,l));

    // data formats
    public static DataFormat ausgabenFormat=new DataFormat(){
            public String format(Iterator<Object> objects)
            {
                Number betrag=(Number)objects.next();
                double b=betrag==null?0:betrag.doubleValue();

                return b<0?U.format(currencyFormat,-b):"";
            }
        };

    public static DataFormat einnahmenFormat=new DataFormat(){
            public String format(Iterator<Object> objects)
            {
                Number betrag=(Number)objects.next();
                double b=betrag==null?0:betrag.doubleValue();

                return b>0?U.format(currencyFormat,b):"";
            }
        };

    public static DataFormat rassenFormat=new DataFormat(){
            public String format(Iterator<Object> objects)
            {
                String rasse=(String)objects.next();
                String farbe=(String)objects.next();

                if ("".equals(farbe))
                    return rasse;
                return rasse+" ("+farbe+")";
            }
        };

    public static DataFormat personenFormat=new DataFormat(){
            public String format(Iterator<Object> objects)
            {
                String nname=(String)objects.next();
                String vname=(String)objects.next();

                if ("".equals(nname))
                    return "";
                return vname+" "+nname;
            }
        };

    public static DataFormat zuechterFormat=new DataFormat(){
            public String format(Iterator<Object> objects)
            {
                String nname1=(String)objects.next();
                String vname1=(String)objects.next();
                String nname2=(String)objects.next();
                String vname2=(String)objects.next();

                if ("".equals(nname1))
                    return "";
                if ("".equals(nname2))
                    return vname1+" "+nname1;
                if (nname1.equals(nname2))
                    return vname1+" + "+vname2+" "+nname2;
                return vname1+" "+nname1+" + "+vname2+" "+nname2;
            }
        };

    public static DataFormat tlFormat=new DataFormat(){
            public String format(Iterator<Object> objects)
            {
                Integer tl_monat=(Integer)objects.next();
                Integer tl_jahr=(Integer)objects.next();
                Integer tl_nummer=(Integer)objects.next();

                return tl_monat+"."+tl_jahr+"."+tl_nummer;
            }
        };

    public static DataFormat taetoFormat=new DataFormat(){
            public String format(Iterator<Object> objects)
            {
                String tr=(String)objects.next();
                String tl=tlFormat.format(objects);

                if ("".equals(tr))
                    return "unbekannt";
                return tr+" / "+tl;
            }
        };

    public static DataFormat trFormat=new DataFormat(){
            public String format(Iterator<Object> objects)
            {
                String tr=(String)objects.next();
                String jugend=(String)objects.next();

                if (jugend.indexOf("Jugend")>=0)
                    return tr.charAt(0)+"J"+tr.substring(1);
                return tr;
            }
        };

    public static DataFormat zweckFormat=new DataFormat(){
            public String format(Iterator<Object> objects)
            {
                return objects.next()+" | "+objects.next();
            }
        };

    // calculating with money
    public static double roundMoney(double m)
    {
        return (double)Math.round(m*100)/100;
    }

    public static double roundPercent(double p)
    {
        return (double)Math.round(p*100)/100;
    }

    public static double getSteuer(double full,double steuer)
    {
        return steuer < 0
            ? full
            : roundMoney(full*steuer/(1+steuer));
    }

    public static double getSteuerPercent(double full,double steuer)
    {
        return full == steuer
            ? -1
            : roundPercent(steuer/(full-steuer));
    }

    // formatting routines
    public static String enECSV(String s,int t)
    {
        if (s==null)
            {
                return "\\0"+t;
            }
        else
            {
                StringBuffer b=new StringBuffer();
                while (s.length()>0)
                    {
                        if (s.startsWith("\\"))
                            { b.append("\\\\"); s=s.substring(1); }

                        else if (s.startsWith(","))
                            { b.append("\\c"); s=s.substring(1); }

                        else if (s.startsWith("\n"))
                            { b.append("\\n"); s=s.substring(1); }

                        else
                            { b.append(s.charAt(0)); s=s.substring(1); }
                    }
                return b.toString();
            }
    }

    public static void deECSV(String s,PreparedStatement p,int i)
        throws SQLException
    {
        if (s.startsWith("\\0"))
            {
                p.setNull(i,Integer.parseInt(s.substring(2)));
            }
        else
            {
                StringBuffer b=new StringBuffer();
                while (s.length()>0)
                    {
                        if (s.startsWith("\\\\"))
                            { b.append("\\"); s=s.substring(2); }

                        else if (s.startsWith("\\c"))
                            { b.append(","); s=s.substring(2); }

                        else if (s.startsWith("\\n"))
                            { b.append("\n"); s=s.substring(2); }

                        else
                            { b.append(s.charAt(0)); s=s.substring(1); }
                    }
                p.setString(i,b.toString());
            }
    }

    public static String enECSV(ResultSet r)
        throws SQLException
    {
        StringBuffer b=new StringBuffer();
        ResultSetMetaData m=r.getMetaData();
        int c=m.getColumnCount();
        b.append(enECSV(r.getString(1),m.getColumnType(1)));
        for (int i=2;i<=c;i++)
            b.append(","+enECSV(r.getString(i),m.getColumnType(i)));
        return b.toString();
    }

    public static PreparedStatement deECSV(String s,PreparedStatement p)
        throws SQLException
    {
        int i=1;
        for (int j;(j=s.indexOf(','))>=0;i++)
            {
                deECSV(s.substring(0,j),p,i);
                s=s.substring(j+1);
            }
        deECSV(s,p,i);
        return p;
    }

    // table information
    public static final class TableInfo
    {
        public final String name;
        public final String delete;
        public final String insert;
        public final VPreparedStatement select;
        public final VPreparedStatement[] recreate;

        public TableInfo(String n,String t,String[][] f)
        {
            String s;

            String l="id";
            for (int i=0;i<f.length;i++) l+=","+f[i][0];

            name=n;

            delete="DELETE FROM "+t;

            s="INSERT INTO "+t+" ("+l+") VALUES (?";
            for (int i=0;i<f.length;i++) s+=",?";
            insert=s+")";

            select=
                new VPreparedStatement("SELECT "+l+" FROM "+t+" ORDER BY id");

            recreate=new VPreparedStatement[3];
            recreate[0]=new VPreparedStatement("DROP TABLE "+t);

            s="CREATE TABLE "+t+"(id INTEGER NOT NULL";
            for (int i=0;i<f.length;i++) s+=","+f[i][0]+" "+f[i][1];
            recreate[1]=new VPreparedStatement(s+",PRIMARY KEY (id))");

            s="INSERT INTO "+t+" ("+l+") VALUES (0";
            for (int i=0;i<f.length;i++) s+=","+f[i][2];
            recreate[2]=new VPreparedStatement(s+")");
        }

        public String toString()
        {
            return name;
        }
    }

    public static final TableInfo[] tableInfo=new TableInfo[]{
        new TableInfo("Personen",
                      "personen",
                      new String[][]{
                          {"nachname",                "VARCHAR(128)",        "''"},
                          {"vorname",                "VARCHAR(128)",        "''"},
                          {"strassenr",                "VARCHAR(128)",        "''"},
                          {"plz",                "VARCHAR(16)",        "''"},
                          {"ort",                "VARCHAR(128)",        "''"},
                          {"telefon",                "VARCHAR(128)",        "''"},
                          {"geburt",                "DATE",                "NULL"},
                          {"geschlecht",        "VARCHAR(1)",        "''"},
                          {"verein",                "INTEGER",        "0"},
                          {"eintritt",                "DATE",                "NULL"},
                          {"kennung",                "VARCHAR(16)",        "''"},
                          {"funktionen",        "VARCHAR(128)",        "''"},
                          {"a_silber",                "VARCHAR(4)",        "''"},
                          {"a_gold",                "VARCHAR(4)",        "''"},
                          {"a_meister",                "VARCHAR(4)",        "''"},
                          {"a_emitglied",        "VARCHAR(4)",        "''"},
                          {"rassen",                "VARCHAR(128)",        "''"},
                      }),

        new TableInfo("Vereine",
                      "vereine",
                      new String[][]{
                          {"typ",        "VARCHAR(4)",        "''"},
                          {"kuerzel",        "VARCHAR(16)",        "''"},
                          {"name",        "VARCHAR(128)",        "''"},
                          {"verband",        "INTEGER",        "0"},
                      }),

        new TableInfo("Rassen",
                      "rassen",
                      new String[][]{
                          {"kuerzel",        "VARCHAR(16)",        "''"},
                          {"name",        "VARCHAR(128)",        "''"},
                      }),

        new TableInfo("Farben",
                      "farben",
                      new String[][]{
                          {"sid",        "INTEGER",        "0"},
                          {"rasse",        "INTEGER",        "0"},
                          {"farbe",        "VARCHAR(32)",        "''"},
                      }),

        new TableInfo("Kaninchen",
                      "kaninchen",
                      new String[][]{
                          {"tr",        "VARCHAR(16)",        "''"},
                          {"tl_monat",        "INTEGER",        "0"},
                          {"tl_jahr",        "INTEGER",        "0"},
                          {"tl_nummer",        "INTEGER",        "0"},
                          {"farbe",        "INTEGER",        "0"},
                          {"geschlecht","VARCHAR(3)",        "''"},
                          {"geburt",        "DATE",                "NULL"},
                          {"vater",        "INTEGER",        "0"},
                          {"mutter",        "INTEGER",        "0"},
                          {"zuechter",        "INTEGER",        "0"},
                          {"zuechter2",        "INTEGER",        "0"},
                          {"taeto_m",        "INTEGER",        "0"},
                      }),

        new TableInfo("Kassen",
                      "kassen",
                      new String[][]{
                          {"name",        "VARCHAR(128)",        "''"},
                      }),

        new TableInfo("Konten",
                      "konten",
                      new String[][]{
                          {"kasse",        "INTEGER",        "0"},
                          {"kuerzel",        "VARCHAR(16)",        "''"},
                          {"name",        "VARCHAR(128)",        "''"},
                      }),

        new TableInfo("Belege",
                      "belege",
                      new String[][]{
                          {"kasse",        "INTEGER",        "0"},
                          {"nummer",        "INTEGER",        "0"},
                          {"betrag",        "FLOAT",        "0"},
                          {"konto",        "INTEGER",        "0"},
                          {"datum",        "DATE",                "NULL"},
                          {"zweck",        "VARCHAR(128)",        "''"},
                      }),

        new TableInfo("HS-Konten",
                      "hskonten",
                      new String[][]{
                          {"nummer",        "INTEGER",        "0"},
                          {"steuersatz","FLOAT",        "0"},
                          {"einnahme",        "VARCHAR(1)",        "'A'"},
                          {"glied",        "INTEGER",        "-1"},
                          {"beschr",        "VARCHAR(128)",        "''"},
                      }),

        new TableInfo("HS-Kasse",
                      "hskasse",
                      new String[][]{
                          {"nummer",        "INTEGER",        "0"},
                          {"betrag",        "FLOAT",        "0"},
                          {"steuer",        "FLOAT",        "0"},
                          {"steuersatz","FLOAT",        "0"},
                          {"konto",        "INTEGER",        "0"},
                          {"kasse",        "INTEGER",        "0"},
                          {"buch",        "INTEGER",        "0"},
                          {"status",        "VARCHAR(1)",        "''"},
                          {"datum",        "DATE",                "NULL"},
                          {"zweck",        "VARCHAR(128)",        "''"},
                          {"anlage",        "INTEGER",        "0"},
                          {"anlagetyp",        "VARCHAR(32)",        "''"},
                      }),

        new TableInfo("HS-Anlagen",
                      "hsanlagen",
                      new String[][]{
                          {"beleg",        "INTEGER",        "0"},
                          {"jahr",        "INTEGER",        "0"},
                          {"betrag",        "FLOAT",        "0"},
                      }),

        new TableInfo("Ueberweisungen",
                      "ueberweisungen",
                      new String[][]{
                          {"an_name",        "VARCHAR(128)",        "''"},
                          {"an_knr",        "VARCHAR(16)",        "''"},
                          {"an_blz",        "VARCHAR(16)",        "''"},
                          {"an_bank",        "VARCHAR(32)",        "''"},
                          {"von_name",        "VARCHAR(128)",        "''"},
                          {"von_knr",        "VARCHAR(16)",        "''"},
                          {"von_blz",        "VARCHAR(16)",        "''"},
                          {"von_bank",        "VARCHAR(32)",        "''"},
                          {"betrag",        "FLOAT",        "0"},
                          {"zweck1",        "VARCHAR(32)",        "''"},
                          {"zweck2",        "VARCHAR(32)",        "''"},
                          {"datum",        "DATE",                "NULL"},
                      })
            };

    public static final String[][] linkInfo=new String[][]{
        new String[]{"personen","verein",        /* => */ "vereine","id"},
        new String[]{"vereine","verband",        /* => */ "vereine","id"},
        new String[]{"farben","rasse",                /* => */ "rassen","id"},
        new String[]{"kaninchen","farbe",        /* => */ "farben","id"},
        new String[]{"kaninchen","vater",        /* => */ "kaninchen","id"},
        new String[]{"kaninchen","mutter",        /* => */ "kaninchen","id"},
        new String[]{"kaninchen","zuechter",        /* => */ "personen","id"},
        new String[]{"kaninchen","zuechter2",        /* => */ "personen","id"},
        new String[]{"kaninchen","taeto_m",        /* => */ "personen","id"},
        new String[]{"konten","kasse",                /* => */ "kassen","id"},
        new String[]{"belege","kasse",                /* => */ "kassen","id"},
        new String[]{"belege","konto",                /* => */ "konten","id"},
        new String[]{"hskasse","konto",                /* => */ "hskonten","nummer"},
        new String[]{"hsanlagen","beleg",        /* => */ "hskasse","id"},
    };

    // standard message dialogs
    public static void msgInfo(String message)
    {
        JOptionPane p=new JOptionPane(U.replace(message,",",", "),
                                      JOptionPane.INFORMATION_MESSAGE,0,null,
                                      new Object[]{"Alles klar!"},
                                      "Alles klar!"){
                public int getMaxCharactersPerLineCount()
                {
                    return 70;
                }
            };
        JDialog d=p.createDialog(null,"Information");
        p.selectInitialValue();
        d.setVisible(true);
    }

    public static void msgError(String message)
    {
        JOptionPane p=new JOptionPane(U.replace(message,",",", "),
                                      JOptionPane.ERROR_MESSAGE,0,null,
                                      new Object[]{"Mist!"},"Mist!"){
                public int getMaxCharactersPerLineCount()
                {
                    return 70;
                }
            };
        JDialog d=p.createDialog(null,"Fehler");
        p.selectInitialValue();
        d.setVisible(true);
    }

    public static boolean msgConfirm(String message)
    {
        JOptionPane p=new JOptionPane(U.replace(message,",",", "),
                                      JOptionPane.QUESTION_MESSAGE,
                                      JOptionPane.YES_NO_OPTION,null,
                                      new Object[]{"Ja","Nein"},"Nein"){
                public int getMaxCharactersPerLineCount()
                {
                    return 70;
                }
            };
        JDialog d=p.createDialog(null,"Bestätigung");
        p.selectInitialValue();
        d.setVisible(true);
        return "Ja".equals(p.getValue());
    }

    // dialogs
    public static MainFrame mainFrame=null;

    // progress monitoring
    private static ProgressMonitor progressMonitor=null;
    private static int progress=0;
    public static void setProgress(String note)
    {
        if (progressMonitor.isCanceled())
            {
                Log.log("Foxda's startup was canceled");
                System.exit(1);
            }
        progressMonitor.setNote(note);
        progressMonitor.setProgress(++progress);
        Log.log("Startup-"+progress+": "+note);
    }

    // get a property
    public static String getProperty(String key, String defaultValue)
    {
        return properties.getProperty(key,defaultValue);
    }

    // returns whether a module is enabled
    public static boolean isEnabledModule(String module)
    {
        return
            getProperty("foxda.modules."+module,"on").
            compareToIgnoreCase("on")==0;
    }

    // check a module
    public static void checkModule(String module)
    {
        if (!isEnabledModule(module))
            {
                Log.log("Attempt to use the disabled module '"+module+"'");
                msgError("Dieses Programm-Modul ist deaktiviert");
                throw new UnsupportedOperationException();
            }
    }

    // gui startup code
    public static void runGUI()
    {
        // progress monitor
        progressMonitor=new ProgressMonitor(null,"Starte Foxda ...","",0,5);
        progressMonitor.setMillisToDecideToPopup(0);
        progressMonitor.setMillisToPopup(0);

        // load ini file
        setProgress("Lade Einstellungen");
        try { properties.load(new FileReader(iniFileName)); }
        catch (IOException e) { Log.log(e); }

        // log to file
        new LogFile(getProperty("foxda.log.filename","foxda.log"));

        // connect to database
        setProgress("Verbinde zur Datenbank");
        try { Database.connect(properties); }
        catch (SQLException e) { Log.log(e); }
        catch (ClassNotFoundException e) { Log.log(e); }

        // main frame
        setProgress("Lade Hauptfenster");
        mainFrame=new MainFrame();

        // show main frame
        setProgress("Zeige Hauptfenster");
        mainFrame.setVisible(true);

        // close progress monitor
        setProgress("--- Fertig ---");
    }

    // silent startup code
    public static void runSilent()
    {
        Log.log("Lade Einstellungen");
        try { properties.load(new FileInputStream(iniFileName)); }
        catch (IOException e) { Log.log(e); }

        new LogFile(getProperty("foxda.log.filename","foxda.log"));

        Log.log("Verbinde zur Datenbank");
        try { Database.connect(properties); }
        catch (SQLException e) { Log.log(e); }
        catch (ClassNotFoundException e) { Log.log(e); }
    }

    // usage notice
    public static void usage()
    {
            System.err.println("usage:");
            System.err.println("  foxda");
            System.err.println("  foxda --import /path/*.csv");
            System.err.println("  foxda --export /path/");
            System.exit(1);
    }

    // startup code
    public static void main(String[] args)
    {
        // set locale
        Locale.setDefault(l);

        // log to stderr
        new LogWriter(System.err);

        // arguments
        if (args.length==0)
        {
                runGUI();
        }
        else if (args.length>=2)
        {
                String action=args[0];
                if ("--import".equals(action))
                {
                        runSilent();
                        for (int i=1;i<args.length;i++)
                        {
                                DBSection.doSilentImport(new File(args[i]));
                        }
                }
                else if ("--export".equals(action))
                {
                        if (args.length==2)
                        {
                                runSilent();
                                DBSection.doSilentExportAll(new File(args[1]));
                        }
                        else usage();
                }
                else usage();
        }
        else usage();

        // run with GUI
    }
}
