/*****************************************************************************\
*                                                                             *
*  FoxdaFoxda      Foxda      Foxda   Foxda  FoxdaFoxda          Foxda        *
*  Foxda        Foxda Foxda    Foxda Foxda   Foxda  Foxda     Foxda Foxda     *
*  FoxdaFoxda  Foxda   Foxda      Foxda      Foxda   Foxda   Foxda   Foxda    *
*  Foxda        Foxda Foxda    Foxda Foxda   Foxda  Foxda   FoxdaFoxdaFoxda   *
*  Foxda           Foxda      Foxda   Foxda  FoxdaFoxda    Foxda       Foxda  *
*                                                                             *
*******************************************************************************
*                   *                                     *                   *
*                   *   FILE:        HSKassenListD.java   *                   *
*                   *                                     *                   *
*                    *************************************                    *
*                                                                             *
*                                                                             *
*      This program is free software; you can redistribute it and/or          *
*      modify it under the terms of the GNU General Public License as         *
*      published by the Free Software Foundation; either version 2 of         *
*      the License, or (at your option) any later version.                    *
*                                                                             *
*      This program is distributed in the hope that it will be useful,        *
*      but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the           *
*      GNU General Public License for more details.                           *
*                                                                             *
*      You should have received a copy of the GNU General Public License      *
*      along with this program; if not, write to the Free Software            *
*      Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.              *
*                                                                             *
*                                                                             *
\*****************************************************************************/
package foxda;
import java.lang.*;
import java.util.*;
import java.text.*;
import java.io.*;
import java.sql.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import voji.log.*;
import voji.utils.*;
import voji.db.*;
import voji.report.*;
import voji.ui.*;

/**
 *
 */
public class HSKassenListD extends ListDialog
{
    public static final String MODULE="hskasse";

    public HSKassenListD(Dialog parent)
    {
        super(parent);
    }

    public HSKassenListD(Frame parent)
    {
        super(parent);
    }

    private SpinnerNumberModel
        searchYearModel;
    private JSpinner
        searchYear;

    private int search=0;

    public Dimension getPreferredSize()
    {
        return new Dimension(800,super.getPreferredSize().height);
    }

    private String HS_NAME;

    protected void dialogInit()
    {
        JButton b;

        Main.checkModule(MODULE);

        HS_NAME=Main.getProperty("foxda.hskasse.hs_name","(HS_NAME)");

        super.dialogInit();
        setTitle("HS-Kasse");

        getResultTable().addColumn("Nummer",SwingConstants.RIGHT,20,
                                   new DataFormat(null));
        getResultTable().addColumn("Ausgaben",SwingConstants.RIGHT,50,
                                   Main.ausgabenFormat);
        getResultTable().addColumn("Einnahmen",SwingConstants.RIGHT,50,
                                   Main.einnahmenFormat);
        getResultTable().addColumn("Konto",SwingConstants.RIGHT,20,
                                   new DataFormat(null));
        getResultTable().addColumn("Anlage",SwingConstants.CENTER,10,
                                   new DataFormat(Main.booleanFormat));
        getResultTable().addColumn("Kasse",SwingConstants.CENTER,5,
                                   new DataFormat(Main.booleanFormat));
        getResultTable().addColumn("Buch",SwingConstants.CENTER,5,
                                   new DataFormat(Main.booleanFormat));
        getResultTable().addColumn("Status",SwingConstants.CENTER,5,
                                   new DataFormat(null));
        getResultTable().addColumn("Datum",SwingConstants.CENTER,50,
                                   new DataFormat(Main.dateFormat));
        getResultTable().addColumn("Zweck",SwingConstants.LEFT,150,
                                   new DataFormat(null));

        getSpecialPanel().setLayout(new GridLayout(0,3));

        b=new JButton("Jahres-Abrechnung",Icons.PRINT);
        b.setMnemonic('J');
        b.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e)
                {
                    int year=searchYearModel.getNumber().intValue();
                    printYear(year);
                }
            });
        getSpecialPanel().add(b);

        b=new JButton("Quartals-Umsatzsteuer",Icons.PRINT);
        b.setMnemonic('Q');
        b.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent event)
                {
                    int type=TimeChooseDialog.QUARTER;
                    int year=searchYearModel.getNumber().intValue();
                    int month=Calendar.JANUARY;
                    Object[] params=new TimeChooseDialog(HSKassenListD.this,type,year,month).getParams();
                    try {
                        openUmsatzsteuerVoranmeldung(params);
                    }
                    catch (Exception e) { Log.log(e); }
                }
            });
        getSpecialPanel().add(b);

        b=new JButton("Monats-Umsatzsteuer",Icons.PRINT);
        b.setMnemonic('M');
        b.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent event)
                {
                    int type=TimeChooseDialog.MONTH;
                    int year=searchYearModel.getNumber().intValue();
                    int month=Calendar.JANUARY;
                    Object[] params=new TimeChooseDialog(HSKassenListD.this,type,year,month).getParams();
                    try {
                        openUmsatzsteuerVoranmeldung(params);
                    }
                    catch (Exception e) { Log.log(e); }
                }
            });
        getSpecialPanel().add(b);

        searchYearModel=new SpinnerNumberModel(Calendar.getInstance().get(Calendar.YEAR), 1998, null, 1);
        searchYear=new JSpinner(searchYearModel);
        searchYear.setEditor(new JSpinner.NumberEditor(searchYear,"####"));
        searchYear.addChangeListener(new ChangeListener(){
                public void stateChanged(ChangeEvent e)
                {
                    updateDataFields();
                }
            });
        addSearchLabel("Jahr",'J',searchYear);
        getSearchPanel().add(searchYear);

        for (char c='0';c<='9';c++)
            getResultTable().getInputMap().
                put(KeyStroke.getKeyStroke(c),"s_digit");
        getResultTable().getActionMap().put("s_digit",new AbstractAction(){
                public void actionPerformed(ActionEvent e)
                {
                    search=search*10 + Integer.parseInt(e.getActionCommand());
                    updateDataFields();
                }
            });

        getResultTable().getInputMap().
            put(KeyStroke.getKeyStroke(KeyEvent.VK_BACK_SPACE,0),"s_back");
        getResultTable().getActionMap().put("s_back",new AbstractAction(){
                public void actionPerformed(ActionEvent e)
                {
                    search/=10;
                    updateDataFields();
                }
            });

        getResultTable().getInputMap().
            put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE,0),"s_esc");
        getResultTable().getActionMap().put("s_esc",new AbstractAction(){
                public void actionPerformed(ActionEvent e)
                {
                    search=0;
                    updateDataFields();
                }
            });

        getResultTable().addFocusListener(new FocusAdapter(){
                public void focusLost(FocusEvent e)
                {
                    search=0;
                    updateDataFields();
                }
            });

        pack();
    }

    private static String yearRangeString(int year)
    {
        Calendar cal=Calendar.getInstance();
        String prefix="vom 01.01."+year+" bis ";
        if (cal.get(Calendar.YEAR) == year)
            {
                return prefix+Main.dateFormat.format(cal.getTime());
            }
        else
            {
                return prefix+"31.12."+year;
            }
    }

    private static final VPreparedStatement ausgabenStmt=new VPreparedStatement(
        "SELECT -SUM(betrag-steuer),-SUM(steuer) "+
        "FROM hskasse "+
        "WHERE buch!=0 AND status='' AND nummer>0 "+
        "      AND (extract(month FROM datum) BETWEEN ? AND ?) "+
        "      AND extract(year FROM datum)=? "+
        "      AND betrag<0");
    private static final VPreparedStatement einnahmenStmt=new VPreparedStatement(
        "SELECT SUM(betrag-steuer),SUM(steuer) "+
        "FROM hskasse "+
        "WHERE buch!=0 AND status='' AND nummer>0 "+
        "      AND (extract(month FROM datum) BETWEEN ? AND ?) "+
        "      AND extract(year FROM datum)=? "+
        "      AND betrag>=0");

    private static final VPreparedStatement listStmt=new VPreparedStatement(
        "SELECT ka.nummer,ka.datum,ka.betrag-ka.steuer,ka.steuer,ka.konto,"+
        "       ka.zweck,ko.beschr "+
        "FROM hskasse ka,hskonten ko "+
        "WHERE ka.buch!=0 AND status='' AND ka.nummer>0 "+
        "      AND ka.konto=ko.nummer "+
        "      AND (extract(month FROM ka.datum) BETWEEN ? AND ?) "+
        "      AND extract(year FROM ka.datum)=? "+
        "ORDER BY ka.datum,ka.nummer,ka.id");
    private Report printListe(Object[] params)
        throws SQLException
    {
        Report rep=new Report(PageAttributes.OrientationRequestedType.LANDSCAPE);
        ReportComponent c;
        Font f=Font.decode("sans-8");

        rep.addHeader(new ReportSpace(0.5));
        c=new ReportComponent();
        c.add(new ReportText
              (""+params[3],
               1,10.5,
               Font.decode("times-bold-24"),
               ReportText.CENTER));
        c.add(new ReportPageNumber
              ("Seite ","",
               0,11,
               Font.decode("sans-10")));
        rep.addHeader(c);
        rep.addHeader(new ReportSpace(0.2));
        rep.addHeader(new ReportTable
                      (new String[]{
                          "Nr.","Datum","Ausgaben","A.-MwSt.",
                          "Einnahmen","E.-MwSt.","Stand","Konto","Zweck"},
                       0.5,new double[]{0.5,1.0,1.0,1.0,1.0,1.0,1.0,2.5,1.5},
                       Font.decode("sans-bold-10"),
                       ReportText.CENTER,ReportText.WORD,ReportFrame.SINGLE,
                       ReportComponent.CENTER));

        ResultSet r=listStmt.executeQuery(new Object[]{params[0],params[1],params[2]});
        double stand=0;
        while (r.next())
            {
                double betrag=r.getDouble(3);
                double steuer=r.getDouble(4);
                String betragStr=Main.currencyFormat.format(Math.abs(betrag));
                String steuerStr=Main.currencyFormat.format(Math.abs(steuer));
                stand+=betrag;
                rep.add(new ReportTable
                        (new String[]{
                            U.format(Main.numberFormat,r.getObject(1)),
                            U.format(Main.dateFormat,r.getDate(2)),
                            betrag<0 ? betragStr : "",
                            betrag<0 ? steuerStr : "",
                            betrag>=0 ? betragStr : "",
                            betrag>=0 ? steuerStr : "",
                            Main.currencyFormat.format(stand),
                            r.getString(5)+" ("+r.getString(7)+")",
                            r.getString(6)},
                         0.5,new double[]{0.5,1.0,1.0,1.0,1.0,1.0,1.0,2.5,1.5},
                         f,
                         new int[]{
                             ReportText.RIGHT,
                             ReportText.RIGHT,
                             ReportText.RIGHT,
                             ReportText.RIGHT,
                             ReportText.RIGHT,
                             ReportText.RIGHT,
                             ReportText.RIGHT,
                             ReportText.LEFT,
                             ReportText.LEFT},
                         ReportText.WORD,
                         ReportFrame.SINGLE,
                         ReportComponent.TOP));
            }

        r=ausgabenStmt.executeQuery(new Object[]{params[0],params[1],params[2]});
        if (!r.next()) throw new SQLException();
        double sumA=r.getDouble(1);
        double sumAS=r.getDouble(2);

        r=einnahmenStmt.executeQuery(new Object[]{params[0],params[1],params[2]});
        if (!r.next()) throw new SQLException();
        double sumE=r.getDouble(1);
        double sumES=r.getDouble(2);

        f=Font.decode("sans-10");

        c=new ReportComponent();
        c.add(new ReportText
              ("Gesamt:",
               0,2.0,
               f,ReportText.RIGHT,ReportText.NEWLINE,true));
        c.add(new ReportTable
              (new String[]{
                  Main.currencyFormat.format(sumA),
                  Main.currencyFormat.format(sumAS),
                  Main.currencyFormat.format(sumE),
                  Main.currencyFormat.format(sumES)},
               2.0,new double[]{1.0,1.0,1.0,1.0},
               f,
               new int[]{
                   ReportText.RIGHT,
                   ReportText.RIGHT,
                   ReportText.RIGHT,
                   ReportText.RIGHT},
               ReportText.WORD,
               ReportFrame.SINGLE,
               ReportComponent.CENTER));
        rep.add(c);

        c=new ReportComponent();
        c.add(new ReportText
              ("Summe:",
               0,4.0,
               f,ReportText.RIGHT,ReportText.NEWLINE,true));
        c.add(new ReportTable
              (new String[]{
                  Main.currencyFormat.format(sumE-sumA),
                  Main.currencyFormat.format(sumES-sumAS)},
               4.0,new double[]{1.0,1.0},
               f,
               new int[]{
                   ReportText.RIGHT,
                   ReportText.RIGHT},
               ReportText.WORD,
               ReportFrame.SINGLE,
               ReportComponent.CENTER));
        rep.add(c);

        rep.addBottom(new ReportSpace(0.5));
        return rep;
    }

    private static String getZeitraumTxt(Object[] params)
        throws Exception
    {
        if (params[0].equals(1) && params[1].equals(1)) return "1";
        if (params[0].equals(2) && params[1].equals(2)) return "2";
        if (params[0].equals(3) && params[1].equals(3)) return "3";
        if (params[0].equals(4) && params[1].equals(4)) return "4";
        if (params[0].equals(5) && params[1].equals(5)) return "5";
        if (params[0].equals(6) && params[1].equals(6)) return "6";
        if (params[0].equals(7) && params[1].equals(7)) return "7";
        if (params[0].equals(8) && params[1].equals(8)) return "8";
        if (params[0].equals(9) && params[1].equals(9)) return "9";
        if (params[0].equals(10) && params[1].equals(10)) return "10";
        if (params[0].equals(11) && params[1].equals(11)) return "11";
        if (params[0].equals(12) && params[1].equals(12)) return "12";
        if (params[0].equals(1) && params[1].equals(3)) return "I. Kalendervierteljahr";
        if (params[0].equals(4) && params[1].equals(6)) return "II. Kalendervierteljahr";
        if (params[0].equals(7) && params[1].equals(9)) return "III. Kalendervierteljahr";
        if (params[0].equals(10) && params[1].equals(12)) return "IV. Kalendervierteljahr";
        throw new Exception("Unable to calculate \"zeitraum\" from params = {"+params[0]+","+params[1]+",...}");
    }

    private static String getZeitraumShort(Object[] params)
        throws Exception
    {
        if (params[0].equals(1) && params[1].equals(1)) return "1";
        if (params[0].equals(2) && params[1].equals(2)) return "2";
        if (params[0].equals(3) && params[1].equals(3)) return "3";
        if (params[0].equals(4) && params[1].equals(4)) return "4";
        if (params[0].equals(5) && params[1].equals(5)) return "5";
        if (params[0].equals(6) && params[1].equals(6)) return "6";
        if (params[0].equals(7) && params[1].equals(7)) return "7";
        if (params[0].equals(8) && params[1].equals(8)) return "8";
        if (params[0].equals(9) && params[1].equals(9)) return "9";
        if (params[0].equals(10) && params[1].equals(10)) return "10";
        if (params[0].equals(11) && params[1].equals(11)) return "11";
        if (params[0].equals(12) && params[1].equals(12)) return "12";
        if (params[0].equals(1) && params[1].equals(3)) return "Q1";
        if (params[0].equals(4) && params[1].equals(6)) return "Q2";
        if (params[0].equals(7) && params[1].equals(9)) return "Q3";
        if (params[0].equals(10) && params[1].equals(12)) return "Q4";
        throw new Exception("Unable to calculate \"zeitraum\" from params = {"+params[0]+","+params[1]+",...}");
    }

    private static void showHtmlFile(File htmlFile)
        throws Exception
    {
        ProcessBuilder pb = new ProcessBuilder(
            "firefox",
            "--new-window",
            htmlFile.getAbsolutePath()
        );
        Process p = pb.start();
        p.getOutputStream().close();
        p.getInputStream().close();
        p.getErrorStream().close();
    }

    private static final VPreparedStatement kontoStmt=new VPreparedStatement(
        "SELECT SUM(betrag-steuer) "+
        "FROM hskasse "+
        "WHERE buch!=0 AND status='' "+
        "      AND (extract(month FROM datum) BETWEEN ? AND ?) "+
        "      AND extract(year FROM datum)=? "+
        "      AND konto=?");
    private void openUmsatzsteuerVoranmeldung(Object[] params)
        throws SQLException, IOException, Exception
    {
        final String name = Main.getProperty("foxda.hskasse.ustva.name",""); // HS_NAME is too long
        final String strasse = Main.getProperty("foxda.hskasse.ustva.strasse","");
        final String plz = Main.getProperty("foxda.hskasse.ustva.plz","");
        final String ort = Main.getProperty("foxda.hskasse.ustva.ort","");
        final String telefon = Main.getProperty("foxda.hskasse.ustva.telefon","");
        final String email = Main.getProperty("foxda.hskasse.ustva.email","");
        final String land = Main.getProperty("foxda.hskasse.ustva.land","");
        final String steuernummer = Main.getProperty("foxda.hskasse.ustva.steuernummer","");

        final String year = params[2].toString();
        final String zeitraumTxt = getZeitraumTxt(params);
        final String zeitraumShort = getZeitraumShort(params);
        final File htmlFile = new File(
            System.getProperty("user.home"),
            Main.getProperty("foxda.hskasse.ustva.dir","foxda/Umsatzsteuer")
            +"/"+"UStVA-"+year+"-"+zeitraumShort+".html"
        );

        final DecimalFormat ustvaIntegerFormat = new DecimalFormat("#", DecimalFormatSymbols.getInstance(Locale.GERMANY));
        final DecimalFormat ustvaCurrencyFormat = new DecimalFormat("#0.00", DecimalFormatSymbols.getInstance(Locale.GERMANY));

        ResultSet r;

        r=ausgabenStmt.executeQuery(new Object[]{params[0],params[1],params[2]});
        if (!r.next()) throw new SQLException();
        double SA=r.getDouble(2);

        r=kontoStmt.executeQuery(new Object[]{params[0],params[1],params[2],8300});
        if (!r.next()) throw new SQLException();
        double E7=Math.ceil(r.getDouble(1));
        double S7=E7*0.07;

        r=kontoStmt.executeQuery(new Object[]{params[0],params[1],params[2],8920});
        if (!r.next()) throw new SQLException();
        double E8920=r.getDouble(1);

        r=kontoStmt.executeQuery(new Object[]{params[0],params[1],params[2],8400});
        if (!r.next()) throw new SQLException();
        double E16=Math.ceil(E8920+r.getDouble(1));
        double S16=E16*0.16;

        r=kontoStmt.executeQuery(new Object[]{params[0],params[1],params[2],8930});
        if (!r.next()) throw new SQLException();
        double E8930=r.getDouble(1);

        r=kontoStmt.executeQuery(new Object[]{params[0],params[1],params[2],8500});
        if (!r.next()) throw new SQLException();
        double E19=Math.ceil(E8930+r.getDouble(1));
        double S19=E19*0.19;

        double Ub=S19+S16+S7;
        double S=Ub-SA;

        String html =
            "<!doctype html>\n"+
            "<html>\n"+
            "<head>\n"+
            "<meta charset='utf-8'>"+
            "<body>\n"+
            "Jahr = "+year+"<br>\n"+
            "Zeitraum = "+zeitraumTxt+"<br>\n"+
            "Feld 81 = "+ustvaIntegerFormat.format(E19)+" (Steuerpflichtige Umsätze 19%)<br>\n"+
            "Feld 86 = "+ustvaIntegerFormat.format(E7)+" (Steuerpflichtige Umsätze 7%)<br>\n"+
            "Feld 35 = "+ustvaIntegerFormat.format(E16)+" (Steuerpflichtige Umsätze zu anderem Steuersatz, hier: 16%)<br>\n"+
            "Feld 36 = "+ustvaCurrencyFormat.format(S16)+" (Umsatzsteuer zu anderem Steuersatz, hier: 16%)<br>\n"+
            "Feld 66 = "+ustvaCurrencyFormat.format(SA)+" (Abziehbare Vorsteuerbeträge aus Rechnungen anderer Unternehmer)<br>\n"+
            "Feld 83 = "+ustvaCurrencyFormat.format(S)+" (zu zahlender Betrag)<br>\n"+
            "";

        try (PrintWriter writer = new PrintWriter(htmlFile)) {
            writer.print(html);
        }

        showHtmlFile(htmlFile);
    }

    private static final VPreparedStatement kontenStmt=new VPreparedStatement(
        "SELECT id,konto,betrag,steuer,anlage!=0,steuersatz "+
        "FROM hskasse "+
        "WHERE buch!=0 AND status='' "+
        "      AND (extract(month FROM datum) BETWEEN ? AND ?) "+
        "      AND extract(year FROM datum)=?");
    private static final VPreparedStatement anlagenNStmt=new VPreparedStatement(
        "SELECT -SUM(a.betrag) "+
        "FROM hsanlagen a,hskasse ka "+
        "WHERE a.beleg=ka.id AND ka.konto!=480 AND a.jahr=?");
    private static final VPreparedStatement anlagenGStmt=new VPreparedStatement(
        "SELECT -SUM(a.betrag) "+
        "FROM hsanlagen a,hskasse ka "+
        "WHERE a.beleg=ka.id AND ka.konto=480 AND a.jahr=?");
    private static final VPreparedStatement gliederStmt=new VPreparedStatement(
        "SELECT glied,nummer,beschr "+
        "FROM hskonten "+
        "ORDER BY nummer");
    private Vector<Report> printGewinnermittlung(int year)
        throws SQLException
    {
        final int firstMonth=1;
        final int lastMonth=12;
        String rangeStr=yearRangeString(year);

        Vector<Report> reports=new Vector<Report>();
        Report rep;
        ReportComponent c;
        Font f;


        /*
         * calculate
         */

        double gew=0;
        double[] glied=new double[20];
        for (int i=0;i<glied.length;i++) glied[i]=0;
        double[] konto=new double[10000];
        for (int i=0;i<konto.length;i++) konto[i]=0;
        String[] kontoDescr=new String[konto.length];
        for (int i=0;i<kontoDescr.length;i++) kontoDescr[i]="";
        Vector<Vector<Integer>> gliedKonto=new Vector<Vector<Integer>>();
        for (int i=0;i<glied.length;i++) gliedKonto.add(new Vector<Integer>());
        String[] gliedDescr=new String[]{
            /*  0 */ "Einnahmen (112)",
            /*  1 */ "Privatanteile (108)",
            /*  2 */ "Neutrale Erträge (103)",
            /*  3 */ "Umsatzsteuer (140)",
            /*  4 */ "Roh-, Hilfs- und Betriebsstoffe, bezogene Waren (100)",
            /*  5 */ "Fremdleistungen (110)",
            /*  6 */ "Personalkosten (120)",
            /*  7 */ "Umsatzsteuer-Erstattung (141)",  /* "glied 7" receives positive values of "glied 18" = "Umsatzsteuer-Zahlung (186)" */
            /*  8 */ "Steuern, Versicherung, Maut (145)",
            /*  9 */ "Reparaturen, Treibstoff (146)",
            /* 10 */ "Telekommunikation (280)",
            /* 11 */ "Fortbildung (281)",
            /* 12 */ "_unused_",
            /* 13 */ "_unused_",
            /* 14 */ "Abschreibungen auf Anlagevermögen (130)",
            /* 15 */ "Abschreibungen auf geringwertige Anlagegüter (132)",
            /* 16 */ "Verschiedene Kosten (183)",
            /* 17 */ "Vorsteuer (185)",
            /* 18 */ "Umsatzsteuer-Zahlung (186)",
            /* 19 */ "Neutrale Aufwendungen",
        };

        ResultSet r;

        /* calculate "kasse" */
        r=kontenStmt.executeQuery(new Object[]{firstMonth,lastMonth,year});
        while (r.next())
            {
                int i=1;
                int id=r.getInt(i++);
                int k=r.getInt(i++);
                double b=r.getDouble(i++);
                double s=r.getDouble(i++);
                boolean a=r.getBoolean(i++);
                double v=b-s;
                int percent=(int)(r.getDouble(i++)*100);

                switch (percent)
                    {
                    case    0: break;
                    case    7: konto[s<0 ? 1571 : 1771] += s; break;
                    case   16: konto[s<0 ? 1575 : 1775] += s; break;
                    case   19: konto[s<0 ? 1576 : 1776] += s; break;
                    case -100: v=s; break;
                    default:   Log.log("Strange percent number "+
                                       "("+percent+"%) "+
                                       "at HSKasse-ID "+id);
                    }

                if (!a) konto[k] += v;
            }

        /* calculate "anlagen" */
        r=anlagenNStmt.executeQuery(new Object[]{year});
        if (!r.next()) throw new SQLException();
        konto[4830]=r.getDouble(1);

        r=anlagenGStmt.executeQuery(new Object[]{year});
        if (!r.next()) throw new SQLException();
        konto[4855]=r.getDouble(1);

        /* calculate "konten" */
        r=gliederStmt.executeQuery(new Object[]{});
        while (r.next())
            {
                int i=1;
                int g=r.getInt(i++);
                int k=r.getInt(i++);
                String kd=r.getString(i++);

                if (konto[k]!=0)
                    {
                        if (g<0) Log.log("need 'glied' for 'konto="+k+"'");
                        if (gliedDescr[g].charAt(0)=='_') Log.log("invalid (unused) 'glied' for 'konto="+k+"'");

                        gew += konto[k];
                        glied[g] += konto[k];
                        kontoDescr[k]=kd;
                        gliedKonto.get(g).add(k);
                    }
            }


        /*
         * Transform "glied 18" = "Umsatzsteuer-Zahlung (186)"
         * to "glied 7" = "Umsatzsteuer-Erstattung (141)"
         * if it has a positive value.
         */
        if (glied[18] > 0) {
            glied[7] = glied[18];
            gliedKonto.set(7, gliedKonto.get(18));
            glied[18] = 0;
            gliedKonto.set(18, new Vector<Integer>());
        }


        /*
         * print front page
         */

        rep=new Report(PageAttributes.OrientationRequestedType.PORTRAIT);

        rep.add(new ReportSpace(3));
        rep.add(new ReportText
                ("Gewinnermittlung",
                 1,7,
                 Font.decode("times-bold-24"),
                 ReportText.CENTER));
        rep.add(new ReportText
                ("\n"+
                 "nach §4 Abs. 3 EStG\n"+
                 "\n"+
                 rangeStr,
                 1,7,
                 Font.decode("times-24"),
                 ReportText.CENTER));

        rep.add(new ReportSpace(2));
        rep.add(new ReportText
                (HS_NAME,
                 1,7,
                 Font.decode("times-bold-24"),
                 ReportText.CENTER));

        reports.add(rep);


        /*
         * print summary page(s)
         */

        rep=new Report(PageAttributes.OrientationRequestedType.PORTRAIT);
        rep.addHeader(new ReportSpace(0.5));
        rep.addHeader(new ReportText
                      ("Gewinnermittlung nach §4 Abs. 3 EStG "+
                       rangeStr,
                       0.5,7.9,
                       Font.decode("sans-bold-10"),
                       ReportText.LEFT));
        c=new ReportComponent();
        c.add(new ReportText
              (HS_NAME,
               0.5,7,
               Font.decode("sans-10"),
               ReportText.LEFT));
        c.add(new ReportPageNumber
              ("Seite ","",
               0,8,
               Font.decode("sans-10")));
        rep.addHeader(c);
        rep.addHeader(new ReportSpace(0.5));
        f=Font.decode("sans-10");

        rep.add(new ReportText("A) Betriebseinnahmen\n\n",
                               0.5,5.5,f,ReportText.LEFT));

        c=new ReportComponent();
        c.add(new ReportText("1. "+gliedDescr[0],
                             1.0,5.5,f,ReportText.LEFT));
        c.add(new ReportText(Main.currencyFormat.format(glied[0]),
                             6.5,7.5,f,ReportText.RIGHT));
        rep.add(c);

        c=new ReportComponent();
        c.add(new ReportText("2. "+gliedDescr[1],
                             1.0,5.5,f,ReportText.LEFT));
        c.add(new ReportText(Main.currencyFormat.format(glied[1]),
                             6.5,7.5,f,ReportText.RIGHT));
        rep.add(c);

        c=new ReportComponent();
        c.add(new ReportText("3. "+gliedDescr[2],
                             1.0,5.5,f,ReportText.LEFT));
        c.add(new ReportText(Main.currencyFormat.format(glied[2]),
                             6.5,7.5,f,ReportText.RIGHT));
        rep.add(c);

        c=new ReportComponent();
        c.add(new ReportText("4. "+gliedDescr[3],
                             1.0,5.5,f,ReportText.LEFT));
        c.add(new ReportText(Main.currencyFormat.format(glied[3]),
                             6.5,7.5,f,ReportText.RIGHT));
        rep.add(c);

        c=new ReportComponent();
        c.add(new ReportText("5. "+gliedDescr[7],
                             1.0,5.5,f,ReportText.LEFT));
        c.add(new ReportText(Main.currencyFormat.format(glied[7]),
                             6.5,7.5,f,ReportText.RIGHT));
        rep.add(c);

        c.add(new ReportText("\n\n",
                             1.0,5.5,f,ReportText.LEFT));

        c=new ReportComponent();
        c.add(new ReportText("Summe Betriebseinnahmen (159)",
                             1.0,5.5,f,ReportText.LEFT));
        c.add(new ReportText(Main.currencyFormat.format
                             (glied[0]+glied[1]+glied[2]+glied[3]+glied[7]),
                             6.5,7.5,f,ReportText.RIGHT));
        rep.add(c);

        rep.add(new ReportSpace(0.5));
        rep.add(new ReportText("B) Betriebsausgaben\n\n",
                               0.5,5.5,f,ReportText.LEFT));

        rep.add(new ReportText("1. Materialausgaben",
                               1.0,5.5,f,ReportText.LEFT));

        c=new ReportComponent();
        c.add(new ReportText("a) "+gliedDescr[4],
                             1.5,5.5,f,ReportText.LEFT));
        c.add(new ReportText(Main.currencyFormat.format(glied[4]),
                             5.5,6.5,f,ReportText.RIGHT));
        rep.add(c);

        c=new ReportComponent();
        c.add(new ReportText("b) "+gliedDescr[5],
                             1.5,5.5,f,ReportText.LEFT));
        c.add(new ReportText(Main.currencyFormat.format(glied[5]),
                             5.5,6.5,f,ReportText.RIGHT));
        c.add(new ReportText(Main.currencyFormat.format
                             (glied[4]+glied[5]),
                             6.5,7.5,f,ReportText.RIGHT));
        rep.add(c);

        c=new ReportComponent();
        c.add(new ReportText("2. "+gliedDescr[6],
                             1.0,5.5,f,ReportText.LEFT));
        c.add(new ReportText(Main.currencyFormat.format(glied[6]),
                             6.5,7.5,f,ReportText.RIGHT));
        rep.add(c);

        rep.add(new ReportText("3. Fahrzeugkosten",
                               1.0,5.5,f,ReportText.LEFT));

        c=new ReportComponent();
        c.add(new ReportText("a) "+gliedDescr[8],
                             1.5,5.5,f,ReportText.LEFT));
        c.add(new ReportText(Main.currencyFormat.format(glied[8]),
                             5.5,6.5,f,ReportText.RIGHT));
        rep.add(c);

        c=new ReportComponent();
        c.add(new ReportText("b) "+gliedDescr[9],
                             1.5,5.5,f,ReportText.LEFT));
        c.add(new ReportText(Main.currencyFormat.format(glied[9]),
                             5.5,6.5,f,ReportText.RIGHT));
        c.add(new ReportText(Main.currencyFormat.format
                             (glied[8]+glied[9]),
                             6.5,7.5,f,ReportText.RIGHT));
        rep.add(c);

        c=new ReportComponent();
        c.add(new ReportText("4. "+gliedDescr[10],
                             1.0,5.5,f,ReportText.LEFT));
        c.add(new ReportText(Main.currencyFormat.format(glied[10]),
                             6.5,7.5,f,ReportText.RIGHT));
        rep.add(c);

        c=new ReportComponent();
        c.add(new ReportText("5. "+gliedDescr[11],
                             1.0,5.5,f,ReportText.LEFT));
        c.add(new ReportText(Main.currencyFormat.format(glied[11]),
                             6.5,7.5,f,ReportText.RIGHT));
        rep.add(c);

        rep.add(new ReportText("6. Abschreibungen",
                               1.0,5.5,f,ReportText.LEFT));

        c=new ReportComponent();
        c.add(new ReportText("a) "+gliedDescr[14],
                             1.5,5.5,f,ReportText.LEFT));
        c.add(new ReportText(Main.currencyFormat.format(glied[14]),
                             5.5,6.5,f,ReportText.RIGHT));
        rep.add(c);

        c=new ReportComponent();
        c.add(new ReportText("b) "+gliedDescr[15],
                             1.5,5.5,f,ReportText.LEFT));
        c.add(new ReportText(Main.currencyFormat.format(glied[15]),
                             5.5,6.5,f,ReportText.RIGHT));
        c.add(new ReportText(Main.currencyFormat.format
                             (glied[14]+glied[15]),
                             6.5,7.5,f,ReportText.RIGHT));
        rep.add(c);

        c=new ReportComponent();
        c.add(new ReportText("7. "+gliedDescr[16],
                             1.0,5.5,f,ReportText.LEFT));
        c.add(new ReportText(Main.currencyFormat.format(glied[16]),
                             6.5,7.5,f,ReportText.RIGHT));
        rep.add(c);

        c=new ReportComponent();
        c.add(new ReportText("8. "+gliedDescr[17],
                             1.0,5.5,f,ReportText.LEFT));
        c.add(new ReportText(Main.currencyFormat.format(glied[17]),
                             6.5,7.5,f,ReportText.RIGHT));
        rep.add(c);

        c=new ReportComponent();
        c.add(new ReportText("9. "+gliedDescr[18],
                             1.0,5.5,f,ReportText.LEFT));
        c.add(new ReportText(Main.currencyFormat.format(glied[18]),
                             6.5,7.5,f,ReportText.RIGHT));
        rep.add(c);

        c.add(new ReportText("\n\n",
                             1.0,5.5,f,ReportText.LEFT));

        c=new ReportComponent();
        c.add(new ReportText("Summe Kosten",
                             1.0,5.5,f,ReportText.LEFT));
        c.add(new ReportText(Main.currencyFormat.format
                             (glied[4]+glied[5]+glied[6]+
                              glied[8]+glied[9]+
                              glied[10]+glied[11]+glied[14]+glied[15]+
                              glied[16]+glied[17]+glied[18]),
                             6.5,7.5,f,ReportText.RIGHT));
        rep.add(c);

        c.add(new ReportText("\n\n",
                             1.0,5.5,f,ReportText.LEFT));

        c=new ReportComponent();
        c.add(new ReportText("10. "+gliedDescr[19],
                             1.0,5.5,f,ReportText.LEFT));
        c.add(new ReportText(Main.currencyFormat.format(glied[19]),
                             6.5,7.5,f,ReportText.RIGHT));
        rep.add(c);

        c.add(new ReportText("\n\n",
                             1.0,5.5,f,ReportText.LEFT));

        c=new ReportComponent();
        c.add(new ReportText("Summe Betriebsausgaben (199)",
                             1.0,5.5,f,ReportText.LEFT));
        c.add(new ReportText(Main.currencyFormat.format
                             (glied[4]+glied[5]+glied[6]+
                              glied[8]+glied[9]+
                              glied[10]+glied[11]+glied[14]+glied[15]+
                              glied[16]+glied[17]+glied[18]+
                              glied[19]),
                             6.5,7.5,f,ReportText.RIGHT));
        rep.add(c);

        rep.add(new ReportSpace(0.5));
        c=new ReportComponent();
        c.add(new ReportText("C) Gewinn (219)",
                             0.5,5.5,f,ReportText.LEFT));
        c.add(new ReportText(Main.currencyFormat.format(gew),
                             6.5,7.5,f,ReportText.RIGHT));
        rep.add(c);

        rep.addBottom(new ReportSpace(0.5));
        reports.add(rep);


        /*
         * print "Konten" pages
         */

        rep=new Report(PageAttributes.OrientationRequestedType.PORTRAIT);
        rep.addHeader(new ReportSpace(0.5));
        rep.addHeader(new ReportText
                      ("Kontennachweis zur Gewinnermittlung nach §4 Abs. 3 EStG "+
                       rangeStr,
                       0.5,7.9,
                       Font.decode("sans-bold-10"),
                       ReportText.LEFT));
        c=new ReportComponent();
        c.add(new ReportText
              (HS_NAME,
               0.5,7,
               Font.decode("sans-10"),
               ReportText.LEFT));
        c.add(new ReportPageNumber
              ("Seite ","",
               0,8,
               Font.decode("sans-10")));
        rep.addHeader(c);
        rep.addHeader(new ReportSpace(0.5));
        f=Font.decode("sans-10");

        for (int i=0;i<glied.length;i++)
            {
                Iterator<Integer> j=gliedKonto.get(i).iterator();

                if (!j.hasNext()) continue;

                rep.add(new ReportText(gliedDescr[i],
                                       1.1,5.5,
                                       Font.decode("times-bold-10"),
                                       ReportText.LEFT));

                while (j.hasNext())
                    {
                        int k=j.next().intValue();

                        c=new ReportComponent();
                        c.add(new ReportText(""+k,
                                             0.5,0.9,f,ReportText.RIGHT));
                        c.add(new ReportText(kontoDescr[k],
                                             1.2,5.5,f,ReportText.LEFT));
                        c.add(new ReportText(Main.currencyFormat.format
                                             (konto[k]),
                                             5.5,6.5,f,ReportText.RIGHT));

                        if (!j.hasNext())
                            c.add(new ReportText(Main.currencyFormat.format
                                                 (glied[i]),
                                                 6.5,7.5,f,ReportText.RIGHT));
                        rep.add(c);
                    }

                rep.add(new ReportSpace(0.2));
            }

        c=new ReportComponent();
        c.add(new ReportText("Gewinn",
                             1.1,5.5,
                             Font.decode("times-bold-10"),
                             ReportText.LEFT));
        c.add(new ReportText(Main.currencyFormat.format(gew),
                             6.5,7.5,f,ReportText.RIGHT));
        rep.add(c);

        rep.addBottom(new ReportSpace(0.5));
        reports.add(rep);

        return reports;
    }

    private static final VPreparedStatement anlageKontenStmt=new VPreparedStatement(
        "SELECT DISTINCT ka.konto,ko.beschr "+
        "FROM hsanlagen a,hskasse ka,hskonten ko "+
        "WHERE a.beleg=ka.id AND ka.konto=ko.nummer AND a.jahr=? "+
        "ORDER BY ka.konto");
    private static final VPreparedStatement anlageStmt=new VPreparedStatement(
        "SELECT SUM(a.betrag),ka.nummer,ka.zweck,ka.datum,ka.anlagetyp "+
        "FROM hsanlagen a,hskasse ka "+
        "WHERE a.beleg=ka.id AND ka.konto=? AND a.jahr=? "+
        "GROUP BY ka.nummer,ka.zweck,ka.datum,ka.anlagetyp "+
        "ORDER BY ka.nummer");
    private static final VPreparedStatement anlagePreStmt=new VPreparedStatement(
        "SELECT SUM(a.betrag),COUNT(*) "+
        "FROM hsanlagen a,hskasse ka "+
        "WHERE a.beleg=ka.id AND ka.konto=? AND a.jahr<=? "+
        "GROUP BY ka.nummer "+
        "HAVING MAX(a.jahr)>=? "+
        "ORDER BY ka.nummer");
    private static final VPreparedStatement anlagePostStmt=new VPreparedStatement(
        "SELECT SUM(a.betrag),COUNT(*) "+
        "FROM hsanlagen a,hskasse ka "+
        "WHERE a.beleg=ka.id AND ka.konto=? AND a.jahr>=? "+
        "GROUP BY ka.nummer "+
        "HAVING MIN(a.jahr)<=? "+
        "ORDER BY ka.nummer");
    private Vector<Report> printAnlagen(int year)
        throws SQLException
    {
        String rangeStr=yearRangeString(year);

        Report rep=new Report(PageAttributes.OrientationRequestedType.PORTRAIT);
        Report repA=new Report(PageAttributes.OrientationRequestedType.PORTRAIT);
        ReportComponent c;
        Font f;

        rep.addHeader(new ReportSpace(0.5));
        c=new ReportComponent();
        c.add(new ReportText
              ("Entwicklung des Anlagevermögens "+
               rangeStr+
               " - Kontenübersicht",
               0.5,7.9,
               Font.decode("sans-bold-10"),
               ReportText.LEFT));
        c.add(new ReportPageNumber
              ("Seite ","",
               0,8,
               Font.decode("sans-10")));
        rep.addHeader(c);
        rep.addHeader(new ReportText
                      (HS_NAME,
                       0.5,7,
                       Font.decode("sans-10"),
                       ReportText.LEFT));
        rep.addHeader(new ReportSpace(0.5));
        rep.addHeader(new ReportTable
                      (new String[]{
                          "Konto",
                          "Bezeichnung",
                          "Entwicklung\nder",
                          "Stand zum 01.01."+year,
                          "Zugang\nAbgang",
                          "Umbuchung",
                          "Abschreibung\nZuschreibung",
                          "Stand zum 31.12."+year},
                       0.5,new double[]{0.4,1.9,1.0,0.8,0.8,0.8,1.0,0.8},
                       Font.decode("times-bold-8"),
                       ReportText.CENTER,ReportText.WORD,ReportFrame.SINGLE,
                       ReportComponent.TOP));

        repA.addHeader(new ReportSpace(0.5));
        c=new ReportComponent();
        c.add(new ReportText
              ("Entwicklung des Anlagevermögens "+
               rangeStr+
               " - Anlagen",
               0.5,7.9,
               Font.decode("sans-bold-10"),
               ReportText.LEFT));
        c.add(new ReportPageNumber
              ("Seite ","",
               0,8,
               Font.decode("sans-10")));
        repA.addHeader(c);
        repA.addHeader(new ReportText
                       (HS_NAME,
                        0.5,7,
                        Font.decode("sans-10"),
                        ReportText.LEFT));
        repA.addHeader(new ReportSpace(0.5));
        repA.addHeader(new ReportTable
                       (new String[]{
                           "Nr.",
                           "Bezeichnung",
                           "Datum\nAfA-Art",
                           "Entwicklung\nder",
                           "Stand zum 01.01."+year,
                           "Zugang\nAbgang",
                           "Umbuchung",
                           "Abschreibung\nZuschreibung",
                           "Stand zum 31.12."+year},
                        0.5,new double[]{0.4,1.3,0.8,0.8,0.8,0.8,0.8,1.0,0.8},
                        Font.decode("times-bold-8"),
                        ReportText.CENTER,ReportText.WORD,ReportFrame.SINGLE,
                        ReportComponent.TOP));

        f=Font.decode("sans-8");
        double sumP=0;
        double sumPre=0;
        double sumPost=0;
        double sumFirst=0;

        ResultSet r=anlageKontenStmt.executeQuery(new Object[]{year});
        while (r.next())
            {
                int i=1;
                int konto=r.getInt(i++);
                String beschr=r.getString(i++);
                double kontoP=0;
                double kontoPre=0;
                double kontoPost=0;
                double kontoFirst=0;

                repA.add(new ReportSpace(0.1));
                repA.add(new ReportTable
                         (new String[]{"Konto "+konto+"  ("+beschr+")"},
                          0.5,new double[]{7.5},
                          Font.decode("times-bold-10"),
                          ReportText.LEFT,
                          ReportText.WORD,
                          ReportFrame.NONE,
                          ReportComponent.TOP));

                ResultSet rA=anlageStmt.executeQuery(new Object[]{konto,year});
                ResultSet rAPre=anlagePreStmt.executeQuery(new Object[]{konto,year,year});
                ResultSet rAPost=anlagePostStmt.executeQuery(new Object[]{konto,year,year});
                while (rA.next() && rAPre.next() && rAPost.next())
                    {
                        int j=1;
                        double betrag=rA.getDouble(j++);
                        int nummer=rA.getInt(j++);
                        String zweck=rA.getString(j++);
                        Object datum=rA.getObject(j++);
                        String typ=rA.getString(j++);
                        j=1;
                        double betragPre=rAPre.getDouble(j++)-betrag;
                        int jahre=rAPre.getInt(j++);
                        j=1;
                        double betragPost=rAPost.getDouble(j++)-betrag;
                        int gesJahre=rAPost.getInt(j++)+jahre-1;

                        repA.add(new ReportTable
                                 (new String[]{
                                  ""+nummer,
                                  zweck,
                                  Main.dateFormat.format(datum)+"\n"+
                                  typ+"\n"+
                                  jahre+"/"+gesJahre+" Jahre",
                                  "AHK\n"+
                                  "Abschreib.\n"+
                                  "Buchwerte",
                                  betragPre==0 ? "\n\n"+Main.currencyFormat.format(0) :
                                  Main.currencyFormat.format(betragPre+betrag+betragPost)+"\n"+
                                  Main.currencyFormat.format(betragPre)+"\n"+
                                  Main.currencyFormat.format(betrag+betragPost),
                                  (betragPre==0
                                   ? Main.currencyFormat.format(betragPre+betrag+betragPost)
                                   : "")+"\n"+
                                  Main.currencyFormat.format(betrag)+"\n"+
                                  (betragPre==0
                                   ? Main.currencyFormat.format(betragPre+betrag+betragPost)
                                   : ""),
                                  "",
                                  "\n"+
                                  "\n"+
                                  Main.currencyFormat.format(betrag),
                                  Main.currencyFormat.format(betragPre+betrag+betragPost)+"\n"+
                                  Main.currencyFormat.format(betragPre+betrag)+"\n"+
                                  Main.currencyFormat.format(betragPost)},
                                  0.5,new double[]{0.4,1.3,0.8,0.8,0.8,0.8,0.8,1.0,0.8},
                                  f,
                                  new int[]{
                                      ReportText.RIGHT,
                                      ReportText.LEFT,
                                      ReportText.CENTER,
                                      ReportText.RIGHT,
                                      ReportText.RIGHT,
                                      ReportText.RIGHT,
                                      ReportText.RIGHT,
                                      ReportText.RIGHT,
                                      ReportText.RIGHT},
                                  ReportText.WORD,
                                  ReportFrame.SINGLE,
                                  ReportComponent.TOP));

                        kontoP += betrag;
                        kontoPre += betragPre;
                        kontoPost += betragPost;
                        if (betragPre==0) kontoFirst += betrag+betragPost;
                    }

                repA.add(new ReportTable
                         (new String[]{
                             "AHK\n"+
                             "Abschreib.\n"+
                             "Buchwerte",
                             Main.currencyFormat.format(kontoPre+kontoP+kontoPost-kontoFirst)+"\n"+
                             Main.currencyFormat.format(kontoPre)+"\n"+
                             Main.currencyFormat.format(kontoP+kontoPost-kontoFirst),
                             Main.currencyFormat.format(kontoFirst)+"\n"+
                             Main.currencyFormat.format(kontoP)+"\n"+
                             Main.currencyFormat.format(kontoFirst),
                             "",
                             "\n"+
                             "\n"+
                             Main.currencyFormat.format(kontoP),
                             Main.currencyFormat.format(kontoPre+kontoP+kontoPost)+"\n"+
                             Main.currencyFormat.format(kontoPre+kontoP)+"\n"+
                             Main.currencyFormat.format(kontoPost)},
                          3.0,new double[]{0.8,0.8,0.8,0.8,1.0,0.8},
                          f,
                          new int[]{
                              ReportText.RIGHT,
                              ReportText.RIGHT,
                              ReportText.RIGHT,
                              ReportText.RIGHT,
                              ReportText.RIGHT,
                              ReportText.RIGHT},
                          ReportText.WORD,
                          ReportFrame.BOLD,
                          ReportComponent.TOP));

                rep.add(new ReportTable
                        (new String[]{
                            ""+konto,
                            beschr,
                            "Ansch-/Herst-K\n"+
                            "Abschreibung\n"+
                            "Buchwerte",
                            Main.currencyFormat.format(kontoPre+kontoP+kontoPost-kontoFirst)+"\n"+
                            Main.currencyFormat.format(kontoPre)+"\n"+
                            Main.currencyFormat.format(kontoP+kontoPost-kontoFirst),
                            Main.currencyFormat.format(kontoFirst)+"\n"+
                            Main.currencyFormat.format(kontoP)+"\n"+
                            Main.currencyFormat.format(kontoFirst),
                            "",
                            "\n"+
                            "\n"+
                            Main.currencyFormat.format(kontoP),
                            Main.currencyFormat.format(kontoPre+kontoP+kontoPost)+"\n"+
                            Main.currencyFormat.format(kontoPre+kontoP)+"\n"+
                            Main.currencyFormat.format(kontoPost)},
                         0.5,new double[]{0.4,1.9,1.0,0.8,0.8,0.8,1.0,0.8},
                         f,
                         new int[]{
                             ReportText.RIGHT,
                             ReportText.LEFT,
                             ReportText.RIGHT,
                             ReportText.RIGHT,
                             ReportText.RIGHT,
                             ReportText.RIGHT,
                             ReportText.RIGHT,
                             ReportText.RIGHT},
                         ReportText.WORD,
                         ReportFrame.SINGLE,
                         ReportComponent.TOP));

                sumP += kontoP;
                sumPre += kontoPre;
                sumPost += kontoPost;
                sumFirst += kontoFirst;
            }

        rep.add(new ReportTable
                (new String[]{
                    "Ansch-/Herst-K\n"+
                    "Abschreibung\n"+
                    "Buchwerte",
                    Main.currencyFormat.format(sumPre+sumP+sumPost-sumFirst)+"\n"+
                    Main.currencyFormat.format(sumPre)+"\n"+
                    Main.currencyFormat.format(sumP+sumPost-sumFirst),
                    Main.currencyFormat.format(sumFirst)+"\n"+
                    Main.currencyFormat.format(sumP)+"\n"+
                    Main.currencyFormat.format(sumFirst),
                    "",
                    "\n"+
                    "\n"+
                    Main.currencyFormat.format(sumP),
                    Main.currencyFormat.format(sumPre+sumP+sumPost)+"\n"+
                    Main.currencyFormat.format(sumPre+sumP)+"\n"+
                    Main.currencyFormat.format(sumPost)},
                 2.8,new double[]{1.0,0.8,0.8,0.8,1.0,0.8},
                 f,
                 new int[]{
                     ReportText.RIGHT,
                     ReportText.RIGHT,
                     ReportText.RIGHT,
                     ReportText.RIGHT,
                     ReportText.RIGHT,
                     ReportText.RIGHT},
                 ReportText.WORD,
                 ReportFrame.BOLD,
                 ReportComponent.TOP));

        rep.addBottom(new ReportSpace(0.5));
        repA.addBottom(new ReportSpace(0.5));

        Vector<Report> reports=new Vector<Report>();
        reports.add(rep);
        reports.add(repA);
        return reports;
    }

    private static final VPreparedStatement sumSGezahltStmt=new VPreparedStatement(
        "SELECT -SUM(steuer) "+
        "FROM hskasse "+
        "WHERE buch!=0 AND status='' "+
        "      AND ((extract(year FROM datum)=? AND konto=1780)"+
        "       OR (extract(year FROM datum)=?+1 AND konto=1790))");
    private Report printUmsatzsteuer(int year)
        throws SQLException
    {
        final int firstMonth=1;
        final int lastMonth=12;
        ResultSet r;

        r=ausgabenStmt.executeQuery(new Object[]{firstMonth,lastMonth,year});
        if (!r.next()) throw new SQLException();
        double SA=r.getDouble(2);

        r=kontoStmt.executeQuery(new Object[]{firstMonth,lastMonth,year,8920});
        if (!r.next()) throw new SQLException();
        double E8920=Math.round(r.getDouble(1));
        double S8920=E8920*0.16;

        r=kontoStmt.executeQuery(new Object[]{firstMonth,lastMonth,year,8930});
        if (!r.next()) throw new SQLException();
        double E8930=Math.round(r.getDouble(1));
        double S8930=E8930*0.19;

        r=kontoStmt.executeQuery(new Object[]{firstMonth,lastMonth,year,8500});
        if (!r.next()) throw new SQLException();
        double E19=Math.round(r.getDouble(1));
        double S19=E19*0.19;

        r=kontoStmt.executeQuery(new Object[]{firstMonth,lastMonth,year,8400});
        if (!r.next()) throw new SQLException();
        double E16=Math.round(r.getDouble(1));
        double S16=E16*0.16;

        r=kontoStmt.executeQuery(new Object[]{firstMonth,lastMonth,year,8300});
        if (!r.next()) throw new SQLException();
        double E7=Math.round(r.getDouble(1));
        double S7=E7*0.07;

        double Ub=S8930+S8920+S19+S16+S7;
        double SGesamt=Ub-SA;

        r=sumSGezahltStmt.executeQuery(new Object[]{year,year});
        if (!r.next()) throw new SQLException();
        double SGezahlt=r.getDouble(1);

        double S=SGesamt-SGezahlt;

        String text =
            "Feld 177: Lieferungen und sonstige Leistungen zu 19%\n"+
            Main.currencyFormat.format(E19)+"  / Steuer: "+Main.currencyFormat.format(S19)+"\n\n"+
            "Feld 179: Sonstige Leistungen nach § 3 Absatz 9a UStG zu 19%\n"+
            Main.currencyFormat.format(E8930)+"  / Steuer: "+Main.currencyFormat.format(S8930)+"\n\n"+
            "Feld 275: Lieferungen und sonstige Leistungen zu 7%\n"+
            Main.currencyFormat.format(E7)+"  / Steuer: "+Main.currencyFormat.format(S7)+"\n\n"+
            "(Alt) Feld 290: Lieferungen und sonstige Leistungen zu 16%\n"+
            Main.currencyFormat.format(E16)+"  / Steuer: "+Main.currencyFormat.format(S16)+"\n\n"+
            "(Alt) Feld 176: Sonstige Leistungen nach § 3 Absatz 9a UStG zu 16%\n"+
            Main.currencyFormat.format(E8920)+"  / Steuer: "+Main.currencyFormat.format(S8920)+"\n\n"+
            "Zwischensumme Steuer\n"+
            Main.currencyFormat.format(Ub)+"\n\n"+
            "Feld 320: Vorsteuerbeträge aus Rechnungen von anderen Unternehmern\n"+
            Main.currencyFormat.format(SA)+"\n\n"+
            "Feld 816: Verbleibende Umsatzsteuer/Verbleibender Überschuss\n"+
            Main.currencyFormat.format(SGesamt)+"\n\n"+
            "Vorauszahlungssoll "+year+" (einschließlich Sondervorauszahlung)\n"+
            Main.currencyFormat.format(SGezahlt)+"\n\n"+
            "Feld 820: Noch an die Finanzkasse zu entrichten\n"+
            Main.currencyFormat.format(S);

        Report rep=new Report(PageAttributes.OrientationRequestedType.PORTRAIT);
        rep.add(new ReportSpace(2));
        rep.add(new ReportText("Umsatzsteuer "+year,1,7,Font.decode("times-bold-24"),ReportText.CENTER));
        rep.add(new ReportSpace(1));
        rep.add(new ReportText(text,1.0,9.0,Font.decode("sans-12"),ReportText.LEFT));
        return rep;
    }

    private void printYear(final int year)
    {
        final boolean useMonths = true;
        final File pdfFile = new File(
            System.getProperty("user.home"),
            Main.getProperty("foxda.hskasse.tax_return_dir","foxda/Steuererklärung")
            + "/" + year + "/Jahres-Abrechnung-" + year + ".pdf"
        );
        final String message = (
            "Soll eine neue Jahres-Abrechnung für " + year + " erzeugt werden?\n" +
            "\n" +
            pdfFile.getAbsolutePath()
        );
        if (!Main.msgConfirm(message)) {
            return;
        }
        final ProgressMonitor progressMonitor = new ProgressMonitor(this, "Berechne Jahres-Abrechnung für " + year + " ...", "", 0, 25);
        progressMonitor.setMillisToDecideToPopup(0);
        progressMonitor.setMillisToPopup(0);
        final SwingWorker worker = new SwingWorker() {
            protected Object doInBackground() throws Exception {
                try
                    {
                        int progress = 0;
                        pdfFile.getParentFile().mkdirs();
                        progressMonitor.setProgress(++progress);
                        Vector<Report> reports = new Vector<Report>();
                        reports.add(printUmsatzsteuer(year));
                        progressMonitor.setProgress(++progress);
                        reports.addAll(printGewinnermittlung(year));
                        progressMonitor.setProgress(++progress);
                        reports.addAll(printAnlagen(year));
                        progressMonitor.setProgress(++progress);
                        if (useMonths) {
                            reports.add(printListe(new Object[]{ 1,  1, year, "Januar  " + year}));
                            progressMonitor.setProgress(++progress);
                            reports.add(printListe(new Object[]{ 2,  2, year, "Februar  " + year}));
                            progressMonitor.setProgress(++progress);
                            reports.add(printListe(new Object[]{ 3,  3, year, "März  " + year}));
                            progressMonitor.setProgress(++progress);
                            reports.add(printListe(new Object[]{ 4,  4, year, "April  " + year}));
                            progressMonitor.setProgress(++progress);
                            reports.add(printListe(new Object[]{ 5,  5, year, "Mai  " + year}));
                            progressMonitor.setProgress(++progress);
                            reports.add(printListe(new Object[]{ 6,  6, year, "Juni  " + year}));
                            progressMonitor.setProgress(++progress);
                            reports.add(printListe(new Object[]{ 7,  7, year, "Juli  " + year}));
                            progressMonitor.setProgress(++progress);
                            reports.add(printListe(new Object[]{ 8,  8, year, "August  " + year}));
                            progressMonitor.setProgress(++progress);
                            reports.add(printListe(new Object[]{ 9,  9, year, "September  " + year}));
                            progressMonitor.setProgress(++progress);
                            reports.add(printListe(new Object[]{10, 10, year, "Oktober  " + year}));
                            progressMonitor.setProgress(++progress);
                            reports.add(printListe(new Object[]{11, 11, year, "November  " + year}));
                            progressMonitor.setProgress(++progress);
                            reports.add(printListe(new Object[]{12, 12, year, "Dezember  " + year}));
                            progressMonitor.setProgress(++progress);
                        } else {
                            reports.add(printListe(new Object[]{ 1,  3, year, "1. Quartal (Januar-März)  " + year}));
                            progressMonitor.setProgress(++progress);
                            reports.add(printListe(new Object[]{ 4,  6, year, "2. Quartal (April-Juni)  " + year}));
                            progressMonitor.setProgress(++progress);
                            reports.add(printListe(new Object[]{ 7,  9, year, "3. Quartal (Juli-September)  " + year}));
                            progressMonitor.setProgress(++progress);
                            reports.add(printListe(new Object[]{10, 12, year, "4. Quartal (Oktober-Dezember)  " + year}));
                            progressMonitor.setProgress(++progress);
                        }
                        Report.printPdf(Main.mainFrame, pdfFile, reports);
                        progressMonitor.setProgress(++progress);
                        progressMonitor.close();
                        Report.runPdfViewer(pdfFile);
                    }
                catch (Exception e) { Log.log(e); }
                return null;
            }
        };
        worker.execute();
    }

    protected EditDialog createEditDialog()
    {
        return new HSKassenEditD(this);
    }

    private static final VPreparedStatement tableStmt=new VPreparedStatement(
        "SELECT id,nummer,betrag,betrag,konto,anlage!=0,kasse!=0,buch!=0,"+
        "       status,datum,zweck "+
        "FROM hskasse "+
        "WHERE id!=0 "+
        "      AND (extract(year FROM datum)=? OR status!='' OR anlage!=0) "+
        "ORDER BY datum DESC,nummer DESC,id DESC");
    private static final VPreparedStatement searchStmt=new VPreparedStatement(
        "SELECT id "+
        "FROM hskasse "+
        "WHERE nummer=? "+
        "      AND extract(year FROM datum)=?");
    private static final VPreparedStatement sumKasseStmt=new VPreparedStatement(
        "SELECT SUM(betrag) "+
        "FROM hskasse "+
        "WHERE kasse!=0 "+
        "      AND extract(year FROM datum)<=?");
    private static final VPreparedStatement sumVorhStmt=new VPreparedStatement(
        "SELECT SUM(betrag) "+
        "FROM hskasse "+
        "WHERE kasse!=0 AND status='' "+
        "      AND extract(year FROM datum)<=?");
    private static final VPreparedStatement sumBuchStmt=new VPreparedStatement(
        "SELECT SUM(betrag) "+
        "FROM hskasse "+
        "WHERE buch!=0 AND status='' "+
        "      AND extract(year FROM datum)=?");
    private static final VPreparedStatement sumSteuerStmt=new VPreparedStatement(
        "SELECT SUM(steuer) "+
        "FROM hskasse "+
        "WHERE buch!=0 AND status='' "+
        "      AND ((extract(year FROM datum)=? AND konto!=1790)"+
        "       OR (extract(year FROM datum)=?+1 AND konto=1790))");
    private static final VPreparedStatement sumAllSteuerStmt=new VPreparedStatement(
        "SELECT SUM(steuer) "+
        "FROM hskasse "+
        "WHERE buch!=0 "+
        "      AND extract(year FROM datum)<=?");
    protected void doUpdateDataFields()
    {
        try
            {
                ResultSet r;
                int year=searchYearModel.getNumber().intValue();

                getResultTable().
                    setData(tableStmt.executeQuery(new Object[]{year}));

                if (search>0)
                    {
                        setInfo("Suche nach Beleg-Nummer: "+search);
                        r=searchStmt.executeQuery(new Object[]{search,year});
                        if (!r.next()) throw new SQLException();
                        getResultTable().setSelectedID(r.getInt(1));
                        return;
                    }

                r=sumKasseStmt.executeQuery(new Object[]{year});
                if (!r.next()) throw new SQLException();
                double sumKasse=r.getDouble(1);

                r=sumVorhStmt.executeQuery(new Object[]{year});
                if (!r.next()) throw new SQLException();
                double sumVorh=r.getDouble(1);

                r=sumBuchStmt.executeQuery(new Object[]{year});
                if (!r.next()) throw new SQLException();
                double sumBuch=r.getDouble(1);

                r=sumSteuerStmt.executeQuery(new Object[]{year,year});
                if (!r.next()) throw new SQLException();
                double sumSteuer=r.getDouble(1);

                r=sumAllSteuerStmt.executeQuery(new Object[]{year});
                if (!r.next()) throw new SQLException();
                double sumAllSteuer=r.getDouble(1);

                setInfo("Vorhanden: "+Main.currencyFormat.format(sumVorh)+
                        " | Gewinn: "+Main.currencyFormat.format(sumKasse-
                                                                 sumAllSteuer)+
                        " | Buch: "+Main.currencyFormat.format(sumBuch-
                                                               sumSteuer)+
                        " | Steuer: "+Main.currencyFormat.format(sumSteuer)
                        );
            }
        catch (SQLException e) { Log.log(e); }
    }
}
