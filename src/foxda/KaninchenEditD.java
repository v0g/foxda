/*****************************************************************************\
*                                                                             *
*  FoxdaFoxda      Foxda      Foxda   Foxda  FoxdaFoxda          Foxda        *
*  Foxda        Foxda Foxda    Foxda Foxda   Foxda  Foxda     Foxda Foxda     *
*  FoxdaFoxda  Foxda   Foxda      Foxda      Foxda   Foxda   Foxda   Foxda    *
*  Foxda        Foxda Foxda    Foxda Foxda   Foxda  Foxda   FoxdaFoxdaFoxda   *
*  Foxda           Foxda      Foxda   Foxda  FoxdaFoxda    Foxda       Foxda  *
*                                                                             *
*******************************************************************************
*                   *                                     *                   *
*                   *   FILE:       KaninchenEditD.java   *                   *
*                   *                                     *                   *
*                    *************************************                    *
*                                                                             *
*                                                                             *
*      This program is free software; you can redistribute it and/or          *
*      modify it under the terms of the GNU General Public License as         *
*      published by the Free Software Foundation; either version 2 of         *
*      the License, or (at your option) any later version.                    *
*                                                                             *
*      This program is distributed in the hope that it will be useful,        *
*      but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the           *
*      GNU General Public License for more details.                           *
*                                                                             *
*      You should have received a copy of the GNU General Public License      *
*      along with this program; if not, write to the Free Software            *
*      Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.              *
*                                                                             *
*                                                                             *
\*****************************************************************************/
package foxda;
import java.lang.*;
import java.util.*;
import java.sql.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import voji.log.*;
import voji.utils.*;
import voji.db.*;
import voji.ui.*;

/**
 *
 */
public class KaninchenEditD extends EditDialog
{
    public static final String MODULE="kaninchen";

    public KaninchenEditD(Dialog parent)
    {
        super(parent);
    }

    public KaninchenEditD(Frame parent)
    {
        super(parent);
    }

    private JTextField
        dataTR;
    private JFormatTextField
        dataTL_monat,
        dataTL_jahr,
        dataTL_nummer,
        dataGeburt;
    private JComboBox<Object>
        dataGeschlecht;
    private JLookupComponent
        dataZuechter,
        dataZuechter2,
        dataTaetoM,
        dataVater,
        dataMutter,
        dataFarbe;

    private static final VChoice ch=new VChoice(new Object[]{
        "1,0 (Rammler)","1,0",
        "0,1 (Häsin)",        "0,1",
        "",                ""
    });

    public Dimension getPreferredSize()
    {
        return new Dimension(300,super.getPreferredSize().height);
    }

    protected void dialogInit()
    {
        JLabel l;
        JPanel p;
        Set<AWTKeyStroke> s;

        Main.checkModule(MODULE);

        super.dialogInit();
        setTitle("Kaninchendaten");

        getContentPanel().setLayout(new BorderLayout());

        JPanel labelPanel=new JPanel();
        labelPanel.setLayout(new GridLayout(0,1));
        getContentPanel().add(labelPanel,BorderLayout.WEST);

        JPanel inputPanel=new JPanel();
        inputPanel.setLayout(new GridLayout(0,1));
        getContentPanel().add(inputPanel,BorderLayout.CENTER);

        dataVater=new JLookupComponent
            (new Instance(){
                    public Object create()
                    {
                        KaninchenListD d=
                            new KaninchenListD(KaninchenEditD.this);
                        d.setSearchGeschlecht("1,0");
                        return d;
                    }},
             Main.taetoFormat,
             "SELECT tr,tl_monat,tl_jahr,tl_nummer "+
             "FROM kaninchen "+
             "WHERE id=?");
        dataVater.setMnemonic('V');
        inputPanel.add(dataVater);
        l=new JLabel(" Vater: ",JLabel.RIGHT);
        l.setDisplayedMnemonic('V');
        labelPanel.add(l);

        dataMutter=new JLookupComponent
            (new Instance(){
                    public Object create()
                    {
                        KaninchenListD d=
                            new KaninchenListD(KaninchenEditD.this);
                        d.setSearchGeschlecht("0,1");
                        return d;
                    }},
             Main.taetoFormat,
             "SELECT tr,tl_monat,tl_jahr,tl_nummer "+
             "FROM kaninchen "+
             "WHERE id=?",
             'M');
        dataMutter.addIDListener(new IDListener(){
                public void idChanged(int id)
                {
                    mutterChanged();
                }
            });
        inputPanel.add(dataMutter);
        l=new JLabel(" Mutter: ",JLabel.RIGHT);
        l.setDisplayedMnemonic('M');
        labelPanel.add(l);

        dataFarbe=new JLookupComponent
            (new Instance(FarbenListD.class,new Object[]{this}),
             Main.rassenFormat,
             "SELECT r.kuerzel,f.farbe "+
             "FROM farben f,rassen r "+
             "WHERE f.rasse=r.id AND "+
             "f.id=?",
             'F');
        inputPanel.add(dataFarbe);
        l=new JLabel(" Rasse/Farbe: ",JLabel.RIGHT);
        l.setDisplayedMnemonic('F');
        labelPanel.add(l);

        dataZuechter=new JLookupComponent
            (new Instance(PersonenListD.class,new Object[]{this}),
             Main.personenFormat,
             "SELECT nachname,vorname "+
             "FROM personen "+
             "WHERE id=?",
             'Z');
        dataZuechter.addIDListener(new IDListener(){
                public void idChanged(int id)
                {
                    zuechterChanged();
                }
            });
        inputPanel.add(dataZuechter);
        l=new JLabel(" Züchter: ",JLabel.RIGHT);
        l.setDisplayedMnemonic('Z');
        labelPanel.add(l);

        dataZuechter2=new JLookupComponent
            (new Instance(PersonenListD.class,new Object[]{this}),
             Main.personenFormat,
             "SELECT nachname,vorname "+
             "FROM personen "+
             "WHERE id=?",
             '2');
        inputPanel.add(dataZuechter2);
        l=new JLabel(" 2. Züchter: ",JLabel.RIGHT);
        l.setDisplayedMnemonic('2');
        labelPanel.add(l);

        dataGeburt=new JFormatTextField(Main.dateFormat);
        dataGeburt.addObjectListener(new ObjectListener(){
                public void objectChanged(Object o)
                {
                    geburtChanged();
                }
            });
        inputPanel.add(dataGeburt);
        l=new JLabel(" Geburtsdatum: ",JLabel.RIGHT);
        l.setDisplayedMnemonic('D');
        l.setLabelFor(dataGeburt);
        labelPanel.add(l);

        p=new JPanel();
        p.setLayout(new FlowLayout(FlowLayout.LEFT,0,0));
        inputPanel.add(p);

        dataTR=new JTextField(5);
        p.add(dataTR);

        l=new JLabel(" Täto rechts: ",JLabel.RIGHT);
        l.setDisplayedMnemonic('R');
        l.setLabelFor(dataTR);
        labelPanel.add(l);

        p=new JPanel();
        p.setLayout(new FlowLayout(FlowLayout.LEFT,0,0));
        inputPanel.add(p);

        dataTL_monat=new JFormatTextField(Main.numberFormat);
        dataTL_monat.setColumns(2);
        s=new HashSet<AWTKeyStroke>(dataTL_monat.getFocusTraversalKeys
                                    (KeyboardFocusManager.FORWARD_TRAVERSAL_KEYS));
        s.add(KeyStroke.getKeyStroke(KeyEvent.VK_PERIOD,0));
        dataTL_monat.setFocusTraversalKeys
            (KeyboardFocusManager.FORWARD_TRAVERSAL_KEYS,s);
        p.add(dataTL_monat);

        p.add(new JLabel(" . "));

        dataTL_jahr=new JFormatTextField(Main.numberFormat);
        dataTL_jahr.setColumns(1);
        s=new HashSet<AWTKeyStroke>(dataTL_jahr.getFocusTraversalKeys
                                    (KeyboardFocusManager.FORWARD_TRAVERSAL_KEYS));
        s.add(KeyStroke.getKeyStroke(KeyEvent.VK_PERIOD,0));
        dataTL_jahr.setFocusTraversalKeys
            (KeyboardFocusManager.FORWARD_TRAVERSAL_KEYS,s);
        p.add(dataTL_jahr);

        p.add(new JLabel(" . "));

        dataTL_nummer=new JFormatTextField(Main.numberFormat);
        dataTL_nummer.setColumns(3);
        p.add(dataTL_nummer);

        l=new JLabel(" Täto links: ",JLabel.RIGHT);
        l.setDisplayedMnemonic('L');
        l.setLabelFor(dataTL_nummer);
        labelPanel.add(l);

        dataGeschlecht=new JComboBox<Object>(ch.elements());
        inputPanel.add(dataGeschlecht);
        l=new JLabel(" Geschlecht: ",JLabel.RIGHT);
        l.setDisplayedMnemonic('G');
        l.setLabelFor(dataGeschlecht);
        labelPanel.add(l);

        dataTaetoM=new JLookupComponent
            (new Instance(){
                    public Object create()
                    {
                        PersonenListD d=new PersonenListD(KaninchenEditD.this);
                        d.setSearchFunktion("Tätowiermeister");
                        return d;
                    }},
             Main.personenFormat,
             "SELECT nachname,vorname "+
             "FROM personen "+
             "WHERE id=?",
             'T');
        inputPanel.add(dataTaetoM);
        l=new JLabel(" Täto-Meister: ",JLabel.RIGHT);
        l.setDisplayedMnemonic('T');
        labelPanel.add(l);

        pack();
    }

    protected void initComponents()
    {
        dataTR.selectAll();
        dataTL_monat.selectAll();
        dataTL_jahr.selectAll();
        dataTL_nummer.selectAll();
        dataGeburt.selectAll();
        dataMutter.requestFocus();
    }

    private static final VPreparedStatement selectStmt=new VPreparedStatement(
        "SELECT vater,mutter,farbe,zuechter,zuechter2,geburt,"+
        "tr,tl_monat,tl_jahr,tl_nummer,geschlecht,taeto_m "+
        "FROM kaninchen "+
        "WHERE id=?");
    protected void selectData(int id)
    {
        try
            {
                int i=1;
                ResultSet r=selectStmt.executeQuery(new Object[]{
                    id});
                if (!r.next()) throw new SQLException();
                dataVater.setSelectedID(r.getInt(i++));
                dataMutter.setSelectedID(r.getInt(i++));
                dataFarbe.setSelectedID(r.getInt(i++));
                dataZuechter.setSelectedID(r.getInt(i++));
                dataZuechter2.setSelectedID(r.getInt(i++));
                dataGeburt.setObject(r.getObject(i++));
                dataTR.setText(r.getString(i++));
                dataTL_monat.setObject(r.getObject(i++));
                dataTL_jahr.setObject(r.getObject(i++));
                dataTL_nummer.setObject(r.getObject(i++));
                dataGeschlecht.setSelectedIndex(ch.v2i(r.getString(i++)));
                dataTaetoM.setSelectedID(r.getInt(i++));
            }
        catch (SQLException e) { Log.log(e); }
    }

    private static final VPreparedStatement idStmt=new VPreparedStatement(
        "SELECT MAX(id)+1 "+
        "FROM kaninchen");
    private static final VPreparedStatement insertStmt=new VPreparedStatement(
        "INSERT INTO kaninchen "+
        "(tr,tl_monat,tl_jahr,tl_nummer,farbe,geschlecht,"+
        "geburt,vater,mutter,zuechter,zuechter2,taeto_m,id) "+
        "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)");
    protected int insertData()
    {
        try
            {
                int id;
                ResultSet r=idStmt.executeQuery(new Object[]{});
                if (!r.next()) throw new SQLException();
                id=r.getInt(1);
                insertStmt.executeUpdate(new Object[]{
                    dataTR.getText(),
                    dataTL_monat.getObject(),
                    dataTL_jahr.getObject(),
                    dataTL_nummer.getObject(),
                    dataFarbe.getSelectedID(),
                    ch.i2v(dataGeschlecht.getSelectedIndex()),
                    dataGeburt.getObject(),
                    dataVater.getSelectedID(),
                    dataMutter.getSelectedID(),
                    dataZuechter.getSelectedID(),
                    dataZuechter2.getSelectedID(),
                    dataTaetoM.getSelectedID(),
                    id});
                return id;
            }
        catch (SQLException e) { Log.log(e); }
        return -1;
    }

    private static final VPreparedStatement updateStmt=new VPreparedStatement(
        "UPDATE kaninchen SET "+
        "tr=?,"+
        "tl_monat=?,"+
        "tl_jahr=?,"+
        "tl_nummer=?,"+
        "farbe=?,"+
        "geschlecht=?,"+
        "geburt=?,"+
        "vater=?,"+
        "mutter=?,"+
        "zuechter=?,"+
        "zuechter2=?,"+
        "taeto_m=? "+
        "WHERE id=?");
    protected void updateData(int id)
    {
        try
            {
                updateStmt.executeUpdate(new Object[]{
                    dataTR.getText(),
                    dataTL_monat.getObject(),
                    dataTL_jahr.getObject(),
                    dataTL_nummer.getObject(),
                    dataFarbe.getSelectedID(),
                    ch.i2v(dataGeschlecht.getSelectedIndex()),
                    dataGeburt.getObject(),
                    dataVater.getSelectedID(),
                    dataMutter.getSelectedID(),
                    dataZuechter.getSelectedID(),
                    dataZuechter2.getSelectedID(),
                    dataTaetoM.getSelectedID(),
                    id});
            }
        catch (SQLException e) { Log.log(e); }
    }

    private static final VPreparedStatement deleteStmt=new VPreparedStatement(
        "DELETE FROM kaninchen "+
        "WHERE id=?");
    protected void deleteData(int id)
    {
        try
            {
                deleteStmt.executeUpdate(new Object[]{
                    id});
            }
        catch (SQLException e) { Log.log(e); }
    }

    private static final VPreparedStatement mutterStmt=new VPreparedStatement(
        "SELECT farbe,zuechter,zuechter2 FROM kaninchen "+
        "WHERE id=?");
    protected void mutterChanged()
    {
        int id=dataMutter.getSelectedID();
        if (id==0) return;
        try
            {
                int i=1;
                ResultSet r=mutterStmt.executeQuery(new Object[]{
                    id});
                if (!r.next()) throw new SQLException();
                dataFarbe.setSelectedID(r.getInt(i++));
                dataZuechter.setSelectedID(r.getInt(i++));
                dataZuechter2.setSelectedID(r.getInt(i++));
            }
        catch (SQLException e) { Log.log(e); }
    }

    private static final VPreparedStatement zuechterStmt=new VPreparedStatement(
        "SELECT v.kuerzel,p.kennung FROM vereine v,personen p "+
        "WHERE p.id=? AND v.id=p.verein");
    protected void zuechterChanged()
    {
        int id=dataZuechter.getSelectedID();
        if (id==0) return;
        try
            {
                int i=1;
                ResultSet r=zuechterStmt.executeQuery(new Object[]{
                    id});
                if (!r.next()) throw new SQLException();
                dataTR.setText(Main.trFormat.format(r,1));
            }
        catch (SQLException e) { Log.log(e); }
    }

    protected void geburtChanged()
    {
        java.sql.Date d=(java.sql.Date)dataGeburt.getObject();
        if (d==null) return;
        Calendar c=Calendar.getInstance();
        c.setTime(d);
        dataTL_monat.setObject(c.get(Calendar.MONTH)+1);
        dataTL_jahr.setObject(c.get(Calendar.YEAR)%10);
    }
}
