/*****************************************************************************\
*                                                                             *
*  FoxdaFoxda      Foxda      Foxda   Foxda  FoxdaFoxda          Foxda        *
*  Foxda        Foxda Foxda    Foxda Foxda   Foxda  Foxda     Foxda Foxda     *
*  FoxdaFoxda  Foxda   Foxda      Foxda      Foxda   Foxda   Foxda   Foxda    *
*  Foxda        Foxda Foxda    Foxda Foxda   Foxda  Foxda   FoxdaFoxdaFoxda   *
*  Foxda           Foxda      Foxda   Foxda  FoxdaFoxda    Foxda       Foxda  *
*                                                                             *
*******************************************************************************
*                   *                                     *                   *
*                   *   FILE:        PersonenEditD.java   *                   *
*                   *                                     *                   *
*                    *************************************                    *
*                                                                             *
*                                                                             *
*      This program is free software; you can redistribute it and/or          *
*      modify it under the terms of the GNU General Public License as         *
*      published by the Free Software Foundation; either version 2 of         *
*      the License, or (at your option) any later version.                    *
*                                                                             *
*      This program is distributed in the hope that it will be useful,        *
*      but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the           *
*      GNU General Public License for more details.                           *
*                                                                             *
*      You should have received a copy of the GNU General Public License      *
*      along with this program; if not, write to the Free Software            *
*      Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.              *
*                                                                             *
*                                                                             *
\*****************************************************************************/
package foxda;
import java.lang.*;
import java.sql.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import voji.log.*;
import voji.utils.*;
import voji.db.*;
import voji.ui.*;

/**
 *
 */
public class UeberweisungenEditD extends EditDialog
{
    public static final String MODULE="ueberweisungen";

    public UeberweisungenEditD(Dialog parent)
    {
        super(parent);
    }

    public UeberweisungenEditD(Frame parent)
    {
        super(parent);
    }

    private JFormatTextField
        dataAnName,
        dataAnKNr,
        dataAnBLZ,
        dataAnBank,
        dataVonName,
        dataVonKNr,
        dataVonBLZ,
        dataVonBank,
        dataBetrag,
        dataZweck1,
        dataZweck2,
        dataDatum;
    private JComboBox<Object>
        searchAn,
        searchVon;

    private VChoice
        chAn=new VChoice(),
        chVon=new VChoice();

    protected void dialogInit()
    {
        JLabel l;
        JPanel p;
        JButton b;

        Main.checkModule(MODULE);

        super.dialogInit();
        setTitle("Überweisungsschein");

        getContentPanel().setLayout(new BorderLayout());

        JPanel labelPanel=new JPanel();
        labelPanel.setLayout(new GridLayout(0,1));
        getContentPanel().add(labelPanel,BorderLayout.WEST);

        JPanel inputPanel=new JPanel();
        inputPanel.setLayout(new GridLayout(0,1));
        getContentPanel().add(inputPanel,BorderLayout.CENTER);

        inputPanel.add(new JLabel());
        labelPanel.add(new JLabel());

        l=new JLabel("Von",JLabel.CENTER);
        l.setDisplayedMnemonic('V');
        inputPanel.add(l);
        labelPanel.add(new JLabel());

        searchVon=new JComboBox<Object>();
        searchVon.addItemListener(new ItemListener(){
                public void itemStateChanged(ItemEvent e)
                {
                    updateVon();
                }
            });
        inputPanel.add(searchVon);
        labelPanel.add(new JLabel());

        dataVonName=new JFormatTextField(27);
        inputPanel.add(dataVonName);
        l.setLabelFor(dataVonName);
        l=new JLabel(" Name: ",JLabel.RIGHT);
        l.setLabelFor(dataVonName);
        labelPanel.add(l);

        dataVonKNr=new JFormatTextField(10);
        inputPanel.add(dataVonKNr);
        l=new JLabel(" Konto-Nr.: ",JLabel.RIGHT);
        l.setLabelFor(dataVonKNr);
        labelPanel.add(l);

        dataVonBLZ=new JFormatTextField(8);
        inputPanel.add(dataVonBLZ);
        l=new JLabel(" BLZ: ",JLabel.RIGHT);
        l.setLabelFor(dataVonBLZ);
        labelPanel.add(l);

        dataVonBank=new JFormatTextField(27);
        inputPanel.add(dataVonBank);
        l=new JLabel(" Bank: ",JLabel.RIGHT);
        l.setLabelFor(dataVonBank);
        labelPanel.add(l);

        inputPanel.add(new JLabel());
        labelPanel.add(new JLabel());

        inputPanel.add(new JLabel());
        labelPanel.add(new JLabel());

        p=new JPanel();
        p.setLayout(new BorderLayout());
        inputPanel.add(p);

        dataDatum=new JFormatTextField(Main.dateFormat);
        p.add(dataDatum,BorderLayout.CENTER);
        l=new JLabel(" Datum: ",JLabel.RIGHT);
        l.setDisplayedMnemonic('D');
        l.setLabelFor(dataDatum);
        labelPanel.add(l);

        b=new JButton(Icons.DATE);
        b.setToolTipText("Aktuelles Datum");
        b.setMargin(new Insets(0,0,0,0));
        b.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e)
                {
                    calcDatum();
                }
            });
        p.add(b,BorderLayout.EAST);

        dataBetrag=new JFormatTextField(Main.numberFormat);
        inputPanel.add(dataBetrag);
        l=new JLabel(" Betrag: ",JLabel.RIGHT);
        l.setDisplayedMnemonic('B');
        l.setLabelFor(dataBetrag);
        labelPanel.add(l);

        dataZweck1=new JFormatTextField(27);
        inputPanel.add(dataZweck1);
        l=new JLabel(" Zweck: ",JLabel.RIGHT);
        l.setDisplayedMnemonic('Z');
        l.setLabelFor(dataZweck1);
        labelPanel.add(l);

        dataZweck2=new JFormatTextField(27);
        inputPanel.add(dataZweck2);
        labelPanel.add(new JLabel());

        inputPanel.add(new JLabel());
        labelPanel.add(new JLabel());

        l=new JLabel("An:",JLabel.CENTER);
        l.setDisplayedMnemonic('A');
        inputPanel.add(l);
        labelPanel.add(new JLabel());

        searchAn=new JComboBox<Object>();
        searchAn.addItemListener(new ItemListener(){
                public void itemStateChanged(ItemEvent e)
                {
                    updateAn();
                }
            });
        inputPanel.add(searchAn);
        labelPanel.add(new JLabel());

        dataAnName=new JFormatTextField(27);
        inputPanel.add(dataAnName);
        l.setLabelFor(dataAnName);
        l=new JLabel(" Name: ",JLabel.RIGHT);
        l.setLabelFor(dataAnName);
        labelPanel.add(l);

        dataAnKNr=new JFormatTextField(10);
        inputPanel.add(dataAnKNr);
        l=new JLabel(" Konto-Nr.: ",JLabel.RIGHT);
        l.setLabelFor(dataAnKNr);
        labelPanel.add(l);

        dataAnBLZ=new JFormatTextField(8);
        inputPanel.add(dataAnBLZ);
        l=new JLabel(" BLZ: ",JLabel.RIGHT);
        l.setLabelFor(dataAnBLZ);
        labelPanel.add(l);

        dataAnBank=new JFormatTextField(27);
        inputPanel.add(dataAnBank);
        l=new JLabel(" Bank: ",JLabel.RIGHT);
        l.setLabelFor(dataAnBank);
        labelPanel.add(l);

        inputPanel.add(new JLabel());
        labelPanel.add(new JLabel());

        pack();
    }

    private static final VPreparedStatement listAnStmt=new VPreparedStatement(
        "SELECT an_name,MAX(id) "+
        "FROM ueberweisungen "+
        "WHERE id!=0 "+
        "GROUP BY an_name "+
        "ORDER BY COUNT(*) DESC");
    private static final VPreparedStatement listVonStmt=new VPreparedStatement(
        "SELECT von_name,MAX(id) "+
        "FROM ueberweisungen "+
        "WHERE id!=0 "+
        "GROUP BY von_name "+
        "ORDER BY COUNT(*) DESC");
    protected void initComponents()
    {
        try
            {
                ResultSet r;

                r=listAnStmt.executeQuery(new Object[]{});
                chAn=new VChoice();
                chAn.add("",null);
                while (r.next())
                    chAn.add(r.getObject(1),r.getObject(2));
                searchAn.setModel(new DefaultComboBoxModel<Object>(chAn.elements()));

                r=listVonStmt.executeQuery(new Object[]{});
                chVon=new VChoice();
                chVon.add("",null);
                while (r.next())
                    chVon.add(r.getObject(1),r.getObject(2));
                searchVon.setModel(new DefaultComboBoxModel<Object>(chVon.elements()));
            }
        catch (SQLException e) { Log.log(e); }

        dataAnName.selectAll();
        dataAnKNr.selectAll();
        dataAnBLZ.selectAll();
        dataAnBank.selectAll();
        dataVonName.selectAll();
        dataVonKNr.selectAll();
        dataVonBLZ.selectAll();
        dataVonBank.selectAll();
        dataBetrag.selectAll();
        dataZweck1.selectAll();
        dataZweck2.selectAll();
        dataDatum.selectAll();
        dataBetrag.requestFocus();
    }

    protected void calcDatum()
    {
        dataDatum.setObject(new java.util.Date());
    }

    private static final VPreparedStatement anStmt=new VPreparedStatement(
        "SELECT an_name,an_knr,an_blz,an_bank "+
        "FROM ueberweisungen "+
        "WHERE id=?");
    protected void updateAn()
    {
        try
            {
                int i=1;
                ResultSet r=anStmt.executeQuery(new Object[]{
                    chAn.i2v(searchAn.getSelectedIndex())});
                if (!r.next()) return;
                dataAnName.setText(r.getString(i++));
                dataAnKNr.setText(r.getString(i++));
                dataAnBLZ.setText(r.getString(i++));
                dataAnBank.setText(r.getString(i++));
            }
        catch (SQLException e) { Log.log(e); }
    }

    private static final VPreparedStatement vonStmt=new VPreparedStatement(
        "SELECT von_name,von_knr,von_blz,von_bank "+
        "FROM ueberweisungen "+
        "WHERE id=?");
    protected void updateVon()
    {
        try
            {
                int i=1;
                ResultSet r=vonStmt.executeQuery(new Object[]{
                    chVon.i2v(searchVon.getSelectedIndex())});
                if (!r.next()) return;
                dataVonName.setText(r.getString(i++));
                dataVonKNr.setText(r.getString(i++));
                dataVonBLZ.setText(r.getString(i++));
                dataVonBank.setText(r.getString(i++));
            }
        catch (SQLException e) { Log.log(e); }
    }

    protected void newData()
    {
        dataBetrag.setText("");
        dataZweck1.setText("");
        dataZweck2.setText("");
        dataDatum.setObject(new java.util.Date());
    }

    private static final VPreparedStatement selectStmt=new VPreparedStatement(
        "SELECT an_name,an_knr,an_blz,an_bank,"+
        "       von_name,von_knr,von_blz,von_bank,"+
        "       betrag,zweck1,zweck2,datum "+
        "FROM ueberweisungen "+
        "WHERE id=?");
    protected void selectData(int id)
    {
        try
            {
                int i=1;
                ResultSet r=selectStmt.executeQuery(new Object[]{
                    id});
                if (!r.next()) throw new SQLException();
                dataAnName.setText(r.getString(i++));
                dataAnKNr.setText(r.getString(i++));
                dataAnBLZ.setText(r.getString(i++));
                dataAnBank.setText(r.getString(i++));
                dataVonName.setText(r.getString(i++));
                dataVonKNr.setText(r.getString(i++));
                dataVonBLZ.setText(r.getString(i++));
                dataVonBank.setText(r.getString(i++));
                dataBetrag.setObject(r.getObject(i++));
                dataZweck1.setText(r.getString(i++));
                dataZweck2.setText(r.getString(i++));
                dataDatum.setObject(r.getObject(i++));
            }
        catch (SQLException e) { Log.log(e); }
    }

    private static final VPreparedStatement idStmt=new VPreparedStatement(
        "SELECT MAX(id)+1 "+
        "FROM ueberweisungen");
    private static final VPreparedStatement insertStmt=new VPreparedStatement(
        "INSERT INTO ueberweisungen "+
        "(an_name,an_knr,an_blz,an_bank,"+
        " von_name,von_knr,von_blz,von_bank,"+
        " betrag,zweck1,zweck2,datum,"+
        " id) "+
        "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)");
    protected int insertData()
    {
        try
            {
                int id;
                ResultSet r=idStmt.executeQuery(new Object[]{});
                if (!r.next()) throw new SQLException();
                id=r.getInt(1);
                insertStmt.executeUpdate(new Object[]{
                    dataAnName.getText(),
                    dataAnKNr.getText(),
                    dataAnBLZ.getText(),
                    dataAnBank.getText(),
                    dataVonName.getText(),
                    dataVonKNr.getText(),
                    dataVonBLZ.getText(),
                    dataVonBank.getText(),
                    dataBetrag.getObject(),
                    dataZweck1.getText(),
                    dataZweck2.getText(),
                    dataDatum.getObject(),
                    id});
                return id;
            }
        catch (SQLException e) { Log.log(e); }
        return -1;
    }

    private static final VPreparedStatement updateStmt=new VPreparedStatement(
        "UPDATE ueberweisungen SET "+
        "an_name=?,"+
        "an_knr=?,"+
        "an_blz=?,"+
        "an_bank=?,"+
        "von_name=?,"+
        "von_knr=?,"+
        "von_blz=?,"+
        "von_bank=?,"+
        "betrag=?,"+
        "zweck1=?,"+
        "zweck2=?,"+
        "datum=? "+
        "WHERE id=?");
    protected void updateData(int id)
    {
        if (dataDatum.getObject() != null)
            Log.log(dataDatum.getObject());
        else
            Log.log("NULL");
        try
            {
                updateStmt.executeUpdate(new Object[]{
                    dataAnName.getText(),
                    dataAnKNr.getText(),
                    dataAnBLZ.getText(),
                    dataAnBank.getText(),
                    dataVonName.getText(),
                    dataVonKNr.getText(),
                    dataVonBLZ.getText(),
                    dataVonBank.getText(),
                    dataBetrag.getObject(),
                    dataZweck1.getText(),
                    dataZweck2.getText(),
                    dataDatum.getObject(),
                    id});
            }
        catch (SQLException e) { Log.log(e); }
    }

    private static final VPreparedStatement deleteStmt=new VPreparedStatement(
        "DELETE FROM ueberweisungen "+
        "WHERE id=?");
    protected void deleteData(int id)
    {
        try
            {
                deleteStmt.executeUpdate(new Object[]{
                    id});
            }
        catch (SQLException e) { Log.log(e); }
    }
}
