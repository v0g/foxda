/*****************************************************************************\
*                                                                             *
*  FoxdaFoxda      Foxda      Foxda   Foxda  FoxdaFoxda          Foxda        *
*  Foxda        Foxda Foxda    Foxda Foxda   Foxda  Foxda     Foxda Foxda     *
*  FoxdaFoxda  Foxda   Foxda      Foxda      Foxda   Foxda   Foxda   Foxda    *
*  Foxda        Foxda Foxda    Foxda Foxda   Foxda  Foxda   FoxdaFoxdaFoxda   *
*  Foxda           Foxda      Foxda   Foxda  FoxdaFoxda    Foxda       Foxda  *
*                                                                             *
*******************************************************************************
*                   *                                     *                   *
*                   *   FILE:   PrintPreviewDialog.java   *                   *
*                   *                                     *                   *
*                    *************************************                    *
*                                                                             *
*                                                                             *
*      This program is free software; you can redistribute it and/or          *
*      modify it under the terms of the GNU General Public License as         *
*      published by the Free Software Foundation; either version 2 of         *
*      the License, or (at your option) any later version.                    *
*                                                                             *
*      This program is distributed in the hope that it will be useful,        *
*      but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the           *
*      GNU General Public License for more details.                           *
*                                                                             *
*      You should have received a copy of the GNU General Public License      *
*      along with this program; if not, write to the Free Software            *
*      Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.              *
*                                                                             *
*                                                                             *
\*****************************************************************************/
package foxda;
import java.lang.*;
import java.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import voji.utils.*;
import voji.ui.*;
import voji.report.*;

/**
 *
 */
public class PrintPreviewDialog extends JDialog
{
    protected String title="";
    protected String fileName="";
    protected Frame frame=null;
    protected Vector<Report> reports=new Vector<Report>();
    protected CopyPrintJob printJob=new CopyPrintJob();

    protected JPrintPreview preview;

    public PrintPreviewDialog(Frame frame)
    {
        printJob.setTranslation(Main.properties);
        fileName=Main.getProperty("foxda.print.filename","");
        this.frame = frame;
    }

    public Dimension getPreferredSize()
    {
        return new Dimension(600,450);
    }

    protected void dialogInit()
    {
        JButton b;

        super.dialogInit();
        setModal(true);
        setTitle("Druck-Vorschau");
        getContentPane().setLayout(new BorderLayout());

        preview=new JPrintPreview();
        getContentPane().add(preview,BorderLayout.CENTER);

        JPanel buttonPanel=new JPanel();
        buttonPanel.setLayout(new GridLayout(1,0));
        buttonPanel.setBorder(BorderFactory.createRaisedBevelBorder());
        getContentPane().add(buttonPanel,BorderLayout.SOUTH);

        b=new JButton("Einstellungen");
        b.setMnemonic('E');
        b.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e)
                {
                    updatePrintJob(JobAttributes.DialogType.COMMON, "");
                }
            });
        buttonPanel.add(b);
        getRootPane().setDefaultButton(b);

        b=new JButton("Drucken");
        b.setMnemonic('D');
        b.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e)
                {
                    print();
                }
            });
        buttonPanel.add(b);
        getRootPane().setDefaultButton(b);

        b=new JButton("Abbrechen");
        b.setMnemonic('A');
        b.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e)
                {
                    setVisible(false);
                }
            });
        buttonPanel.add(b);

        pack();
    }

    public void show(Vector<Report> reports,String title)
    {
        this.reports = reports;
        setTitle(title);
        updatePrintJob(JobAttributes.DialogType.NONE, title);
        setVisible(true);
    }

    public void show(Report report,String title)
    {
        Vector<Report> reports_=new Vector<Report>();
        reports_.add(report);
        show(reports_,title);
    }

    public void updatePrintJob(JobAttributes.DialogType dialog, String title)
    {
        // Use orientation of last report, or PORTRAIT if there are no reports
        PageAttributes.OrientationRequestedType orientation = PageAttributes.OrientationRequestedType.PORTRAIT;
        for (Report report : reports) {
            orientation = report.getOrientation();
        }
        JobAttributes jobAttr = new JobAttributes();
        jobAttr.setDialog(dialog);
        if (fileName.length()>0)
            {
                jobAttr.setDestination(JobAttributes.DestinationType.FILE);
                jobAttr.setFileName(fileName);
            }
        PageAttributes pageAttr = new PageAttributes();
        pageAttr.setOrientationRequested(orientation);
        PrintJob p = getToolkit().getPrintJob(frame,title,jobAttr,pageAttr);
        if (p==null) return;

        printJob.setPrintJob(p);

        CopyPrintJob previewPrintJob=preview.getPrintJob(p);
        for (Report report : reports)
            report.print(previewPrintJob);
        previewPrintJob.end();
    }

    public void print()
    {
        for (Report report : reports)
            report.print(printJob);
        printJob.end();
        setVisible(false);
    }
}
