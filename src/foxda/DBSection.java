/*****************************************************************************\
*                                                                             *
*  FoxdaFoxda      Foxda      Foxda   Foxda  FoxdaFoxda          Foxda        *
*  Foxda        Foxda Foxda    Foxda Foxda   Foxda  Foxda     Foxda Foxda     *
*  FoxdaFoxda  Foxda   Foxda      Foxda      Foxda   Foxda   Foxda   Foxda    *
*  Foxda        Foxda Foxda    Foxda Foxda   Foxda  Foxda   FoxdaFoxdaFoxda   *
*  Foxda           Foxda      Foxda   Foxda  FoxdaFoxda    Foxda       Foxda  *
*                                                                             *
*******************************************************************************
*                   *                                     *                   *
*                   *   FILE:            DBSection.java   *                   *
*                   *                                     *                   *
*                    *************************************                    *
*                                                                             *
*                                                                             *
*      This program is free software; you can redistribute it and/or          *
*      modify it under the terms of the GNU General Public License as         *
*      published by the Free Software Foundation; either version 2 of         *
*      the License, or (at your option) any later version.                    *
*                                                                             *
*      This program is distributed in the hope that it will be useful,        *
*      but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the           *
*      GNU General Public License for more details.                           *
*                                                                             *
*      You should have received a copy of the GNU General Public License      *
*      along with this program; if not, write to the Free Software            *
*      Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.              *
*                                                                             *
*                                                                             *
\*****************************************************************************/
package foxda;
import java.lang.*;
import java.sql.*;
import java.io.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.table.*;
import voji.log.*;
import voji.utils.*;
import voji.db.*;

/**
 * This is the database section of foxda.
 * Here the user can execute SQL commands
 * and can do various changes with a single table or all tables.
 */
public class DBSection extends JComponent
{
    private JTable table;
    private JTextField sqlField;
    private JComboBox<Main.TableInfo> tables;

    public DBSection()
    {
        JButton b;

        setLayout(new BorderLayout());

        table=new JTable(){
                public boolean isCellEditable(int r,int c)
                {
                    return false;
                }
            };
        table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        add(new JScrollPane(table),BorderLayout.CENTER);

        JPanel editPanel=new JPanel();
        editPanel.setLayout(new BorderLayout());
        add(editPanel,BorderLayout.SOUTH);

        sqlField=new JTextField();
        sqlField.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e)
                {
                    executeSQL();
                }
            });
        editPanel.add(sqlField,BorderLayout.CENTER);

        JPanel buttonPanel=new JPanel();
        buttonPanel.setLayout(new GridLayout(2,0));
        editPanel.add(buttonPanel,BorderLayout.SOUTH);

        tables=new JComboBox<Main.TableInfo>(Main.tableInfo);
        buttonPanel.add(tables);

        b=new JButton("Ansehen");
        b.setMnemonic('S');
        b.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e)
                {
                    selectTable();
                }
            });
        buttonPanel.add(b);

        b=new JButton("Exportieren");
        b.setMnemonic('E');
        b.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e)
                {
                    exportTable();
                }
            });
        buttonPanel.add(b);

        b=new JButton("Erstellen");
        b.setMnemonic('R');
        b.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e)
                {
                    recreateTable();
                }
            });
        buttonPanel.add(b);

        buttonPanel.add(new JLabel());

        b=new JButton("Importieren");
        b.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e)
                {
                    importTable();
                }
            });
        buttonPanel.add(b);

        b=new JButton("Alles exportieren");
        b.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e)
                {
                    exportAll();
                }
            });
        buttonPanel.add(b);

        b=new JButton("Alles erstellen");
        b.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e)
                {
                    recreateAll();
                }
            });
        buttonPanel.add(b);
    }

    protected Main.TableInfo getTableInfo()
    {
        return (Main.TableInfo)tables.getSelectedItem();
    }

    protected void setData(ResultSet r) throws SQLException
    {
        table.setModel(new DefaultTableModel(U.toVectorOfVectors(r),
                                             U.getColumnNames(r)));
    }

    protected void setData(String s)
    {
        table.setModel(new DefaultTableModel(new Object[][]{new Object[]{s}},
                                             new Object[]{"Ergebnis"}));
    }

    public static void doExport(Main.TableInfo i,File f)
    {
        try
            {
                PrintWriter w=new PrintWriter(new FileWriter(f));

                ResultSet r=i.select.executeQuery(new Object[]{});
                w.println(i.delete);
                w.println(i.insert);
                while (r.next())
                    w.println(Main.enECSV(r));

                w.close();
            }
        catch (Exception e)
            { Log.log(e); Main.msgError("Fehler beim Exportieren"); }
    }

    public static void doSilentExportAll(File path)
    {
        for (int j=0;j<Main.tableInfo.length;j++)
        {
        Main.TableInfo i=Main.tableInfo[j];
        File f=new File(path,i.name+".csv");
        try
            {
                Log.log("Exportiere "+i.name+" ...");
                PrintWriter w=new PrintWriter(new FileWriter(f));

                ResultSet r=i.select.executeQuery(new Object[]{});
                w.println(i.delete);
                w.println(i.insert);
                while (r.next())
                    w.println(Main.enECSV(r));

                w.close();
                Log.log("... fertig!  ["+i.name+"]");
            }
        catch (Exception e)
            { Log.log(e); }
        }
    }

    public static void doImport(File f)
    {
        try
            {
                BufferedReader r=new BufferedReader(new FileReader(f));

                String delete=r.readLine(),insert=r.readLine();
                if (Main.msgConfirm(
                    "Diese Aktion kann Datenverlust verursachen!\n"+
                    "\n"+
                    "SQL (delete): "+delete+"\n"+
                    "SQL (insert): "+insert+"\n"+
                    "\n"+
                    "Soll wirklich mit diesen Befehlen importiert werden?"))
                    {
                        Database.executeUpdate(delete);
                        PreparedStatement p=Database.prepareStatement(insert);
                        for (String s;(s=r.readLine())!=null;)
                            Main.deECSV(s,p).executeUpdate();
                    }

                r.close();
            }
        catch (Exception e)
            { Log.log(e); Main.msgError("Fehler beim Importieren"); }
    }

    public static void doSilentImport(File f)
    {
        try
            {
                Log.log("Importiere "+f+" ...");
                BufferedReader r=new BufferedReader(new FileReader(f));

                String delete=r.readLine(),insert=r.readLine();
                Database.executeUpdate(delete);
                PreparedStatement p=Database.prepareStatement(insert);
                for (String s;(s=r.readLine())!=null;)
                    Main.deECSV(s,p).executeUpdate();

                r.close();
                Log.log("... fertig!  ["+f+"]");
            }
        catch (Exception e)
            { Log.log(e); }
    }

    protected void executeSQL()
    {
        try
            {
                Statement s=Database.createStatement();
                if (s.execute(sqlField.getText()))
                    setData(s.getResultSet());
                else
                    setData(s.getUpdateCount()+" Zeilen betroffen");
            }
        catch (SQLException e) { Log.log(e); setData("Fehler!"); }
    }

    protected void selectTable()
    {
        try
            {
                setData(getTableInfo().select.executeQuery(new Object[]{}));
            }
        catch (SQLException e) { Log.log(e); }
    }

    protected void exportTable()
    {
        JFileChooser f=new JFileChooser();
        U.synchronizeDirectory(f);
        f.setSelectedFile(new File(f.getCurrentDirectory(),
                                   getTableInfo().name+".csv"));
        if (f.showDialog(this,"Export")==JFileChooser.APPROVE_OPTION)
            doExport(getTableInfo(),f.getSelectedFile());
    }

    protected void recreateTable()
    {
        if (Main.msgConfirm("Diese Aktion kann Datenverlust verursachen!\n"+
                            "Soll diese Tabelle wirklich "+
                            "neu erstellt werden?"))
            {
                VPreparedStatement[] s=getTableInfo().recreate;
                for (int i=0;i<s.length;i++)
                    {
                        Log.log("SQL: "+s[i].getSQL());
                        try
                            {
                                s[i].executeUpdate(new Object[]{});
                            }
                        catch (SQLException e) { Log.log(e); }
                    }
            }
    }

    protected void importTable()
    {
        JFileChooser f=new JFileChooser();
        f.setMultiSelectionEnabled(true);
        U.synchronizeDirectory(f);
        if (f.showDialog(this,"Import")==JFileChooser.APPROVE_OPTION)
            {
                File[] files=f.getSelectedFiles();
                for (int i=0;i<files.length;i++)
                    doImport(files[i]);
            }
    }

    protected void exportAll()
    {
        JFileChooser f=new JFileChooser();
        f.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        U.synchronizeDirectory(f);
        if (f.showDialog(this,"Export")==JFileChooser.APPROVE_OPTION)
            for (int i=0;i<Main.tableInfo.length;i++)
                doExport(Main.tableInfo[i],
                         new File(f.getSelectedFile(),
                                  Main.tableInfo[i].name+".csv"));
    }

    protected void recreateAll()
    {
        if (Main.msgConfirm("Diese Aktion kann Datenverlust verursachen!\n"+
                            "Sollen wirklich alle Tabellen "+
                            "neu erstellt werden?"))
            {
                doRecreateAll();
            }
    }

    public static void doRecreateAll()
    {
        for (int i=0;i<Main.tableInfo.length;i++)
            {
                VPreparedStatement[] s=Main.tableInfo[i].recreate;
                for (int j=0;j<s.length;j++)
                    {
                        Log.log("SQL: "+s[j].getSQL());
                        try
                            {
                                s[j].executeUpdate(new Object[]{});
                            }
                        catch (SQLException e) { Log.log(e); }
                    }
            }
    }
}
