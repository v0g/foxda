/*****************************************************************************\
*                                                                             *
*  FoxdaFoxda      Foxda      Foxda   Foxda  FoxdaFoxda          Foxda        *
*  Foxda        Foxda Foxda    Foxda Foxda   Foxda  Foxda     Foxda Foxda     *
*  FoxdaFoxda  Foxda   Foxda      Foxda      Foxda   Foxda   Foxda   Foxda    *
*  Foxda        Foxda Foxda    Foxda Foxda   Foxda  Foxda   FoxdaFoxdaFoxda   *
*  Foxda           Foxda      Foxda   Foxda  FoxdaFoxda    Foxda       Foxda  *
*                                                                             *
*******************************************************************************
*                   *                                     *                   *
*                   *   FILE:          FarbenEditD.java   *                   *
*                   *                                     *                   *
*                    *************************************                    *
*                                                                             *
*                                                                             *
*      This program is free software; you can redistribute it and/or          *
*      modify it under the terms of the GNU General Public License as         *
*      published by the Free Software Foundation; either version 2 of         *
*      the License, or (at your option) any later version.                    *
*                                                                             *
*      This program is distributed in the hope that it will be useful,        *
*      but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the           *
*      GNU General Public License for more details.                           *
*                                                                             *
*      You should have received a copy of the GNU General Public License      *
*      along with this program; if not, write to the Free Software            *
*      Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.              *
*                                                                             *
*                                                                             *
\*****************************************************************************/
package foxda;
import java.lang.*;
import java.sql.*;
import java.awt.*;
import javax.swing.*;
import voji.log.*;
import voji.utils.*;
import voji.db.*;
import voji.ui.*;

/**
 *
 */
public class FarbenEditD extends EditDialog
{
    public static final String MODULE="rassen";

    public FarbenEditD(Dialog parent)
    {
        super(parent);
    }

    public FarbenEditD(Frame parent)
    {
        super(parent);
    }

    private JTextField
        dataFarbe;
    private JLookupComponent
        dataRasse;

    protected void dialogInit()
    {
        JLabel l;

        Main.checkModule(MODULE);

        super.dialogInit();
        setTitle("Farbendaten");

        getContentPanel().setLayout(new BorderLayout());

        JPanel labelPanel=new JPanel();
        labelPanel.setLayout(new GridLayout(0,1));
        getContentPanel().add(labelPanel,BorderLayout.WEST);

        JPanel inputPanel=new JPanel();
        inputPanel.setLayout(new GridLayout(0,1));
        getContentPanel().add(inputPanel,BorderLayout.CENTER);

        dataRasse=new JLookupComponent
            (new Instance(RassenListD.class,new Object[]{this}),
             null,
             "SELECT kuerzel "+
             "FROM rassen "+
             "WHERE id=?",
             'R');
        inputPanel.add(dataRasse);
        l=new JLabel(" Rasse: ",JLabel.RIGHT);
        l.setDisplayedMnemonic('R');
        labelPanel.add(l);

        dataFarbe=new JTextField();
        inputPanel.add(dataFarbe);
        l=new JLabel(" Farbe: ",JLabel.RIGHT);
        l.setDisplayedMnemonic('F');
        l.setLabelFor(dataFarbe);
        labelPanel.add(l);

        pack();
    }

    protected void initComponents()
    {
        dataFarbe.selectAll();
        dataRasse.requestFocus();
    }

    private static final VPreparedStatement selectStmt=new VPreparedStatement(
        "SELECT farbe,rasse "+
        "FROM farben "+
        "WHERE id=?");
    protected void selectData(int id)
    {
        try
            {
                int i=1;
                ResultSet r=selectStmt.executeQuery(new Object[]{
                    id});
                if (!r.next()) throw new SQLException();
                dataFarbe.setText(r.getString(i++));
                dataRasse.setSelectedID(r.getInt(i++));
            }
        catch (SQLException e) { Log.log(e); }
    }

    private static final VPreparedStatement idStmt=new VPreparedStatement(
        "SELECT MAX(id)+1 "+
        "FROM farben");
    private static final VPreparedStatement insertStmt=new VPreparedStatement(
        "INSERT INTO farben "+
        "(farbe,rasse,sid,id) "+
        "VALUES (?,?,?,?)");
    protected int insertData()
    {
        try
            {
                int id;
                ResultSet r=idStmt.executeQuery(new Object[]{});
                if (!r.next()) throw new SQLException();
                id=r.getInt(1);
                insertStmt.executeUpdate(new Object[]{
                    dataFarbe.getText(),
                    dataRasse.getSelectedID(),
                    id,
                    id});
                return id;
            }
        catch (SQLException e) { Log.log(e); }
        return -1;
    }

    private static final VPreparedStatement updateStmt=new VPreparedStatement(
        "UPDATE farben SET "+
        "farbe=?,"+
        "rasse=? "+
        "WHERE id=?");
    protected void updateData(int id)
    {
        try
            {
                updateStmt.executeUpdate(new Object[]{
                    dataFarbe.getText(),
                    dataRasse.getSelectedID(),
                    id});
            }
        catch (SQLException e) { Log.log(e); }
    }

    private static final VPreparedStatement deleteStmt=new VPreparedStatement(
        "DELETE FROM farben "+
        "WHERE id=?");
    protected void deleteData(int id)
    {
        try
            {
                deleteStmt.executeUpdate(new Object[]{
                    id});
            }
        catch (SQLException e) { Log.log(e); }
    }
}
