/*****************************************************************************\
*                                                                             *
*  FoxdaFoxda      Foxda      Foxda   Foxda  FoxdaFoxda          Foxda        *
*  Foxda        Foxda Foxda    Foxda Foxda   Foxda  Foxda     Foxda Foxda     *
*  FoxdaFoxda  Foxda   Foxda      Foxda      Foxda   Foxda   Foxda   Foxda    *
*  Foxda        Foxda Foxda    Foxda Foxda   Foxda  Foxda   FoxdaFoxdaFoxda   *
*  Foxda           Foxda      Foxda   Foxda  FoxdaFoxda    Foxda       Foxda  *
*                                                                             *
*******************************************************************************
*                   *                                     *                   *
*                   *   FILE:        PersonenEditD.java   *                   *
*                   *                                     *                   *
*                    *************************************                    *
*                                                                             *
*                                                                             *
*      This program is free software; you can redistribute it and/or          *
*      modify it under the terms of the GNU General Public License as         *
*      published by the Free Software Foundation; either version 2 of         *
*      the License, or (at your option) any later version.                    *
*                                                                             *
*      This program is distributed in the hope that it will be useful,        *
*      but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the           *
*      GNU General Public License for more details.                           *
*                                                                             *
*      You should have received a copy of the GNU General Public License      *
*      along with this program; if not, write to the Free Software            *
*      Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.              *
*                                                                             *
*                                                                             *
\*****************************************************************************/
package foxda;
import java.lang.*;
import java.sql.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import voji.log.*;
import voji.utils.*;
import voji.db.*;
import voji.ui.*;

/**
 *
 */
public class PersonenEditD extends EditDialog
{
    public static final String MODULE="personen";

    public PersonenEditD(Dialog parent)
    {
        super(parent);
    }

    public PersonenEditD(Frame parent)
    {
        super(parent);
    }

    private JTextField
        dataNachname,
        dataVorname,
        dataStrasseNr,
        dataPLZ,
        dataOrt,
        dataTelefon,
        dataFunktionen,
        dataASilber,
        dataAGold,
        dataAMeister,
        dataAEMitglied,
        dataRassen;
    private JFormatTextField
        dataGeburt,
        dataEintritt;
    private JComboBox<Object>
        dataGeschlecht;
    private JComboBox<String>
        dataKennung;
    private JLookupComponent
        dataVerein;
    private Instance
        farbenD;

    private static final VChoice ch=new VChoice(new Object[]{
        "männlich",        "m",
        "weiblich",        "w",
        "",                ""
    });

    protected void dialogInit()
    {
        JLabel l;
        JPanel p;
        JButton b;

        Main.checkModule(MODULE);

        super.dialogInit();
        setTitle("Personendaten");

        getContentPanel().setLayout(new BorderLayout());

        JPanel labelPanel=new JPanel();
        labelPanel.setLayout(new GridLayout(0,1));
        getContentPanel().add(labelPanel,BorderLayout.WEST);

        JPanel inputPanel=new JPanel();
        inputPanel.setLayout(new GridLayout(0,1));
        getContentPanel().add(inputPanel,BorderLayout.CENTER);

        dataNachname=new JTextField();
        inputPanel.add(dataNachname);
        l=new JLabel(" Nachname: ",JLabel.RIGHT);
        l.setDisplayedMnemonic('N');
        l.setLabelFor(dataNachname);
        labelPanel.add(l);

        dataVorname=new JTextField();
        inputPanel.add(dataVorname);
        l=new JLabel(" Vorname: ",JLabel.RIGHT);
        l.setDisplayedMnemonic('O');
        l.setLabelFor(dataVorname);
        labelPanel.add(l);

        dataGeschlecht=new JComboBox<Object>(ch.elements());
        inputPanel.add(dataGeschlecht);
        l=new JLabel(" Geschlecht: ",JLabel.RIGHT);
        l.setDisplayedMnemonic('G');
        l.setLabelFor(dataGeschlecht);
        labelPanel.add(l);

        dataStrasseNr=new JTextField();
        inputPanel.add(dataStrasseNr);
        l=new JLabel(" Straße/Nr.: ",JLabel.RIGHT);
        l.setDisplayedMnemonic('S');
        l.setLabelFor(dataStrasseNr);
        labelPanel.add(l);

        p=new JPanel();
        p.setLayout(new BorderLayout());
        inputPanel.add(p);

        dataPLZ=new JTextField(5);
        p.add(dataPLZ,BorderLayout.WEST);

        dataOrt=new JTextField(15);
        p.add(dataOrt,BorderLayout.CENTER);

        l=new JLabel(" PLZ/Ort: ",JLabel.RIGHT);
        l.setDisplayedMnemonic('P');
        l.setLabelFor(dataPLZ);
        labelPanel.add(l);

        dataTelefon=new JTextField();
        inputPanel.add(dataTelefon);
        l=new JLabel(" Telefon: ",JLabel.RIGHT);
        l.setDisplayedMnemonic('T');
        l.setLabelFor(dataTelefon);
        labelPanel.add(l);

        dataGeburt=new JFormatTextField(Main.dateFormat);
        inputPanel.add(dataGeburt);
        l=new JLabel(" Geburtsdatum: ",JLabel.RIGHT);
        l.setDisplayedMnemonic('D');
        l.setLabelFor(dataGeburt);
        labelPanel.add(l);

        dataVerein=new JLookupComponent
            (new Instance(VereineListD.class,new Object[]{this}),
             null,
             "SELECT kuerzel "+
             "FROM vereine "+
             "WHERE id=?",
             'V');
        inputPanel.add(dataVerein);
        l=new JLabel(" Verein: ",JLabel.RIGHT);
        l.setDisplayedMnemonic('V');
        labelPanel.add(l);

        dataEintritt=new JFormatTextField(Main.dateFormat);
        inputPanel.add(dataEintritt);
        l=new JLabel(" Eintritt: ",JLabel.RIGHT);
        l.setDisplayedMnemonic('E');
        l.setLabelFor(dataEintritt);
        labelPanel.add(l);

        dataKennung=new JComboBox<String>(new String[]{
            "Allg. Klasse",
            "Jugend",
            "Ehrenmitglied",
            ""
        });
        inputPanel.add(dataKennung);
        l=new JLabel(" Kennung: ",JLabel.RIGHT);
        l.setDisplayedMnemonic('K');
        l.setLabelFor(dataKennung);
        labelPanel.add(l);

        dataFunktionen=new JTextField();
        inputPanel.add(dataFunktionen);
        l=new JLabel(" Funktionen: ",JLabel.RIGHT);
        l.setDisplayedMnemonic('F');
        l.setLabelFor(dataFunktionen);
        labelPanel.add(l);

        p=new JPanel();
        p.setLayout(new GridLayout(1,0));
        inputPanel.add(p);
        labelPanel.add(new JLabel());

        p.add(new JTextCheckBox("1. Vorsitzender",dataFunktionen));
        p.add(new JTextCheckBox("2. Vorsitzender",dataFunktionen));
        p.add(new JTextCheckBox("Ansprechpartner",dataFunktionen));

        p=new JPanel();
        p.setLayout(new GridLayout(1,0));
        inputPanel.add(p);
        labelPanel.add(new JLabel());

        p.add(new JTextCheckBox("Kassierer",dataFunktionen));
        p.add(new JTextCheckBox("Zuchtwart",dataFunktionen));
        p.add(new JTextCheckBox("Jugendwart",dataFunktionen));
        p.add(new JTextCheckBox("Tätowiermeister",dataFunktionen));

        p=new JPanel();
        p.setLayout(new GridLayout(1,0));
        inputPanel.add(p);
        labelPanel.add(new JLabel());

        p.add(new JLabel("Silber",JLabel.CENTER));
        p.add(new JLabel("Gold",JLabel.CENTER));
        p.add(new JLabel("Meister d. RKZ",JLabel.CENTER));
        p.add(new JLabel("Ehrenmitglied",JLabel.CENTER));

        p=new JPanel();
        p.setLayout(new GridLayout(1,0));
        inputPanel.add(p);

        dataASilber=new JTextField();
        p.add(dataASilber);

        dataAGold=new JTextField();
        p.add(dataAGold);

        dataAMeister=new JTextField();
        p.add(dataAMeister);

        dataAEMitglied=new JTextField();
        p.add(dataAEMitglied);

        l=new JLabel(" Auszeichnungen: ",JLabel.RIGHT);
        l.setDisplayedMnemonic('A');
        l.setLabelFor(dataASilber);
        labelPanel.add(l);

        p=new JPanel();
        p.setLayout(new BorderLayout());
        inputPanel.add(p);

        dataRassen=new JTextField();
        p.add(dataRassen,BorderLayout.CENTER);

        farbenD=new Instance(FarbenListD.class,new Object[]{this});

        b=new JButton(Icons.FIND);
        b.setMargin(new Insets(0,0,0,0));
        b.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e)
                {
                    addRasse();
                }
            });
        p.add(b,BorderLayout.EAST);

        l=new JLabel(" Rassen: ",JLabel.RIGHT);
        l.setDisplayedMnemonic('R');
        l.setLabelFor(dataRassen);
        labelPanel.add(l);

        pack();
    }

    protected void initComponents()
    {
        dataNachname.selectAll();
        dataVorname.selectAll();
        dataStrasseNr.selectAll();
        dataPLZ.selectAll();
        dataOrt.selectAll();
        dataTelefon.selectAll();
        dataGeburt.selectAll();
        dataEintritt.selectAll();
        dataFunktionen.selectAll();
        dataASilber.selectAll();
        dataAGold.selectAll();
        dataAMeister.selectAll();
        dataAEMitglied.selectAll();
        dataRassen.selectAll();
        dataNachname.requestFocus();
    }

    private static final VPreparedStatement rassenStmt=new VPreparedStatement(
        "SELECT r.kuerzel,f.farbe "+
        "FROM farben f,rassen r "+
        "WHERE f.rasse=r.id AND f.id=?");
    protected void addRasse()
    {
        try
            {
                ResultSet r=rassenStmt.executeQuery(
                    new Object[]{((FarbenListD)farbenD.get()).
                                 show(-1)});
                if (!r.next()) throw new SQLException();
                String f=Main.rassenFormat.format(r,1);
                String t=dataRassen.getText();
                dataRassen.setText("".equals(t) ? f : t+", "+f);
            }
        catch (Exception e) {}
    }

    private static final VPreparedStatement selectStmt=new VPreparedStatement(
        "SELECT nachname,vorname,strassenr,plz,ort,telefon,geburt,geschlecht,"+
        "       verein,eintritt,kennung,funktionen,"+
        "       a_silber,a_gold,a_meister,a_emitglied,rassen "+
        "FROM personen "+
        "WHERE id=?");
    protected void selectData(int id)
    {
        try
            {
                int i=1;
                ResultSet r=selectStmt.executeQuery(new Object[]{
                    id});
                if (!r.next()) throw new SQLException();
                dataNachname.setText(r.getString(i++));
                dataVorname.setText(r.getString(i++));
                dataStrasseNr.setText(r.getString(i++));
                dataPLZ.setText(r.getString(i++));
                dataOrt.setText(r.getString(i++));
                dataTelefon.setText(r.getString(i++));
                dataGeburt.setObject(r.getObject(i++));
                dataGeschlecht.setSelectedIndex(ch.v2i(r.getString(i++)));
                dataVerein.setSelectedID(r.getInt(i++));
                dataEintritt.setObject(r.getObject(i++));
                dataKennung.setSelectedItem(r.getString(i++));
                dataFunktionen.setText(r.getString(i++));
                dataASilber.setText(r.getString(i++));
                dataAGold.setText(r.getString(i++));
                dataAMeister.setText(r.getString(i++));
                dataAEMitglied.setText(r.getString(i++));
                dataRassen.setText(r.getString(i++));
            }
        catch (SQLException e) { Log.log(e); }
    }

    private static final VPreparedStatement idStmt=new VPreparedStatement(
        "SELECT MAX(id)+1 "+
        "FROM personen");
    private static final VPreparedStatement insertStmt=new VPreparedStatement(
        "INSERT INTO personen "+
        "(nachname,vorname,strassenr,plz,ort,telefon,geburt,geschlecht,"+
        " verein,eintritt,kennung,funktionen,"+
        " a_silber,a_gold,a_meister,a_emitglied,rassen,id) "+
        "VALUES (?,?,?,?,?,?,?::DATE,?,?,?::DATE,?,?,?,?,?,?,?,?)");
    protected int insertData()
    {
        try
            {
                int id;
                ResultSet r=idStmt.executeQuery(new Object[]{});
                if (!r.next()) throw new SQLException();
                id=r.getInt(1);
                insertStmt.executeUpdate(new Object[]{
                    dataNachname.getText(),
                    dataVorname.getText(),
                    dataStrasseNr.getText(),
                    dataPLZ.getText(),
                    dataOrt.getText(),
                    dataTelefon.getText(),
                    dataGeburt.getObject(),
                    ch.i2v(dataGeschlecht.getSelectedIndex()),
                    dataVerein.getSelectedID(),
                    dataEintritt.getObject(),
                    dataKennung.getSelectedItem(),
                    dataFunktionen.getText(),
                    dataASilber.getText(),
                    dataAGold.getText(),
                    dataAMeister.getText(),
                    dataAEMitglied.getText(),
                    dataRassen.getText(),
                    id});
                return id;
            }
        catch (SQLException e) { Log.log(e); }
        return -1;
    }

    private static final VPreparedStatement updateStmt=new VPreparedStatement(
        "UPDATE personen SET "+
        "nachname=?,"+
        "vorname=?,"+
        "strassenr=?,"+
        "plz=?,"+
        "ort=?,"+
        "telefon=?,"+
        "geburt=?::DATE,"+
        "geschlecht=?,"+
        "verein=?,"+
        "eintritt=?::DATE,"+
        "kennung=?,"+
        "funktionen=?,"+
        "a_silber=?,"+
        "a_gold=?,"+
        "a_meister=?,"+
        "a_emitglied=?,"+
        "rassen=? "+
        "WHERE id=?");
    protected void updateData(int id)
    {
        try
            {
                updateStmt.executeUpdate(new Object[]{
                    dataNachname.getText(),
                    dataVorname.getText(),
                    dataStrasseNr.getText(),
                    dataPLZ.getText(),
                    dataOrt.getText(),
                    dataTelefon.getText(),
                    dataGeburt.getObject(),
                    ch.i2v(dataGeschlecht.getSelectedIndex()),
                    dataVerein.getSelectedID(),
                    dataEintritt.getObject(),
                    dataKennung.getSelectedItem(),
                    dataFunktionen.getText(),
                    dataASilber.getText(),
                    dataAGold.getText(),
                    dataAMeister.getText(),
                    dataAEMitglied.getText(),
                    dataRassen.getText(),
                    id});
            }
        catch (SQLException e) { Log.log(e); }
    }

    private static final VPreparedStatement deleteStmt=new VPreparedStatement(
        "DELETE FROM personen "+
        "WHERE id=?");
    protected void deleteData(int id)
    {
        try
            {
                deleteStmt.executeUpdate(new Object[]{
                    id});
            }
        catch (SQLException e) { Log.log(e); }
    }
}
