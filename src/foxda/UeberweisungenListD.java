/*****************************************************************************\
*                                                                             *
*  FoxdaFoxda      Foxda      Foxda   Foxda  FoxdaFoxda          Foxda        *
*  Foxda        Foxda Foxda    Foxda Foxda   Foxda  Foxda     Foxda Foxda     *
*  FoxdaFoxda  Foxda   Foxda      Foxda      Foxda   Foxda   Foxda   Foxda    *
*  Foxda        Foxda Foxda    Foxda Foxda   Foxda  Foxda   FoxdaFoxdaFoxda   *
*  Foxda           Foxda      Foxda   Foxda  FoxdaFoxda    Foxda       Foxda  *
*                                                                             *
*******************************************************************************
*                   *                                     *                   *
*                   *   FILE:  UeberweisungenListD.java   *                   *
*                   *                                     *                   *
*                    *************************************                    *
*                                                                             *
*                                                                             *
*      This program is free software; you can redistribute it and/or          *
*      modify it under the terms of the GNU General Public License as         *
*      published by the Free Software Foundation; either version 2 of         *
*      the License, or (at your option) any later version.                    *
*                                                                             *
*      This program is distributed in the hope that it will be useful,        *
*      but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the           *
*      GNU General Public License for more details.                           *
*                                                                             *
*      You should have received a copy of the GNU General Public License      *
*      along with this program; if not, write to the Free Software            *
*      Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.              *
*                                                                             *
*                                                                             *
\*****************************************************************************/
package foxda;
import java.lang.*;
import java.util.*;
import java.sql.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import voji.log.*;
import voji.utils.*;
import voji.db.*;
import voji.report.*;
import voji.ui.*;

/**
 *
 */
public class UeberweisungenListD extends ListDialog
{
    public static final String MODULE="ueberweisungen";

    public UeberweisungenListD(Dialog parent)
    {
        super(parent);
    }

    public UeberweisungenListD(Frame parent)
    {
        super(parent);
    }

    private JSpinner
        searchJahr;

    public Dimension getPreferredSize()
    {
        return new Dimension(710,super.getPreferredSize().height);
    }

    protected void dialogInit()
    {
        JButton b;

        Main.checkModule(MODULE);

        super.dialogInit();
        setTitle("Überweisungen");

        getResultTable().addColumn("Von",SwingConstants.LEFT,100,
                                   new DataFormat(null));
        getResultTable().addColumn("An",SwingConstants.LEFT,100,
                                   new DataFormat(null));
        getResultTable().addColumn("Betrag",SwingConstants.RIGHT,50,
                                   new DataFormat(Main.currencyFormat));
        getResultTable().addColumn("Datum",SwingConstants.CENTER,50,
                                   new DataFormat(Main.dateFormat));
        getResultTable().addColumn("Zweck",SwingConstants.LEFT,220,
                                   Main.zweckFormat);

        getSpecialPanel().setLayout(new GridLayout(1,0));

        b=new JButton("Überweisung drucken",Icons.PRINT);
        b.setMnemonic('W');
        b.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e)
                {
                    print();
                }
            });
        getSpecialPanel().add(b);

        searchJahr=new JSpinner();
        searchJahr.setValue(Calendar.getInstance().
                            get(Calendar.YEAR));
        searchJahr.setEditor(new JSpinner.NumberEditor(searchJahr,"####"));
        searchJahr.addChangeListener(new ChangeListener(){
                public void stateChanged(ChangeEvent e)
                {
                    updateDataFields();
                }
            });
        addSearchLabel("Jahr",'J',searchJahr);
        getSearchPanel().add(searchJahr);

        pack();
    }

    private static final VPreparedStatement kontoStmt=new VPreparedStatement(
        "SELECT von_bank,von_blz,an_name,an_knr,an_blz,an_bank,"+
        "       betrag,zweck1,zweck2,von_name,von_knr,datum "+
        "FROM ueberweisungen "+
        "WHERE id=?");
    private void print()
    {
        Report rep=new Report(PageAttributes.OrientationRequestedType.PORTRAIT);
        ReportComponent c;
        Font f=Font.decode("sans-12");
        ResultSet r;

        try {
        r=kontoStmt.executeQuery(new Object[]{getSelectedID()});
        r.next();

        c=new ReportComponent();
        boolean orig=true;
        for (double k=0;k<5;k+=4.2)
            {
                int i=1;
                c.add(new ReportSpace(3.80+k,new ReportText
                                      (r.getString(i++),
                                       0.25,2.65,f)));
                c.add(new ReportSpace(3.80+k,new ReportText
                                      (r.getString(i++),
                                       2.90,4.30,f)));
                c.add(new ReportSpace(4.30+k,new ReportText
                                      (r.getString(i++),
                                       0.40,5.55,f)));
                c.add(new ReportSpace(4.65+k,new ReportText
                                      (r.getString(i++),
                                       0.40,2.20,f)));
                c.add(new ReportSpace(4.65+k,new ReportText
                                      (r.getString(i++),
                                       4.10,5.55,f)));
                c.add(new ReportSpace(4.95+k,new ReportText
                                      (r.getString(i++),
                                       0.40,5.55,f)));
                c.add(new ReportSpace(5.30+k,new ReportText
                                      (U.format(Main.currencyFormat,r.getObject(i++)),
                                       3.35,5.50,f)));
                c.add(new ReportSpace(5.65+k,new ReportText
                                      (r.getString(i++),
                                       0.40,5.55,f)));
                c.add(new ReportSpace(5.95+k,new ReportText
                                      (r.getString(i++),
                                       0.40,5.55,f)));
                c.add(new ReportSpace(6.30+k,new ReportText
                                      (r.getString(i++),
                                       0.40,5.55,f)));
                c.add(new ReportSpace(6.65+k,new ReportText
                                      (r.getString(i++),
                                       0.40,2.15,f)));
                if (orig)
                c.add(new ReportSpace(7.30+k,new ReportText
                                      (U.format(Main.dateFormat,r.getObject(i++)),
                                       2.50,5.55,f)));
                orig=false;
            }
        rep.add(c);
        }
        catch (SQLException e) { Log.log(e); }

        Report.printAndPreviewPdf(Main.mainFrame, rep, "foxda - HS-Überweisung");
    }

    protected EditDialog createEditDialog()
    {
        return new UeberweisungenEditD(this);
    }

    private static final VPreparedStatement tableStmt=new VPreparedStatement(
        "SELECT id,von_name,an_name,betrag,datum,zweck1,zweck2 "+
        "FROM ueberweisungen "+
        "WHERE id!=0 AND ((datum IS NULL) OR (extract(year FROM datum)=?)) "+
        "ORDER BY datum DESC,id DESC");
    protected void doUpdateDataFields()
    {
        try
            {
                ResultSet r;
                Object jahr=searchJahr.getValue();

                getResultTable().
                    setData(tableStmt.executeQuery(new Object[]{jahr}));
            }
        catch (SQLException e) { Log.log(e); }
    }
}
