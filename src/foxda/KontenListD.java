/*****************************************************************************\
*                                                                             *
*  FoxdaFoxda      Foxda      Foxda   Foxda  FoxdaFoxda          Foxda        *
*  Foxda        Foxda Foxda    Foxda Foxda   Foxda  Foxda     Foxda Foxda     *
*  FoxdaFoxda  Foxda   Foxda      Foxda      Foxda   Foxda   Foxda   Foxda    *
*  Foxda        Foxda Foxda    Foxda Foxda   Foxda  Foxda   FoxdaFoxdaFoxda   *
*  Foxda           Foxda      Foxda   Foxda  FoxdaFoxda    Foxda       Foxda  *
*                                                                             *
*******************************************************************************
*                   *                                     *                   *
*                   *   FILE:          KontenListD.java   *                   *
*                   *                                     *                   *
*                    *************************************                    *
*                                                                             *
*                                                                             *
*      This program is free software; you can redistribute it and/or          *
*      modify it under the terms of the GNU General Public License as         *
*      published by the Free Software Foundation; either version 2 of         *
*      the License, or (at your option) any later version.                    *
*                                                                             *
*      This program is distributed in the hope that it will be useful,        *
*      but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the           *
*      GNU General Public License for more details.                           *
*                                                                             *
*      You should have received a copy of the GNU General Public License      *
*      along with this program; if not, write to the Free Software            *
*      Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.              *
*                                                                             *
*                                                                             *
\*****************************************************************************/
package foxda;
import java.lang.*;
import java.util.*;
import java.sql.*;
import java.awt.*;
import java.awt.event.*;
import java.beans.*;
import javax.swing.*;
import javax.swing.event.*;
import voji.log.*;
import voji.utils.*;
import voji.db.*;
import voji.report.*;

/**
 *
 */
public class KontenListD extends ListDialog
{
    public static final String MODULE="kassen";

    protected Object kasse=null;

    public KontenListD(Dialog parent)
    {
        super(parent);
    }

    public KontenListD(Frame parent)
    {
        super(parent);
    }

    private JSpinner
        searchJahr;

    protected void dialogInit()
    {
        JLabel l;
        JButton b;

        Main.checkModule(MODULE);

        super.dialogInit();
        setTitle("Konten");

        getParent().addPropertyChangeListener("kasse",
                                              new PropertyChangeListener(){
                public void propertyChange(PropertyChangeEvent e)
                {
                    firePropertyChange(e.getPropertyName(),
                                       e.getOldValue(),e.getNewValue());
                    kasse=e.getNewValue();
                }
            });

        getResultTable().addColumn("Kürzel",SwingConstants.LEFT,10,
                                   new DataFormat(null));
        getResultTable().addColumn("Name",SwingConstants.LEFT,300,
                                   new DataFormat(null));
        getResultTable().addColumn("Stand",SwingConstants.RIGHT,50,
                                   new DataFormat(Main.currencyFormat));

        getSpecialPanel().setLayout(new GridLayout(1,0));

        b=new JButton("Konto",Icons.PRINT);
        b.setMnemonic('K');
        b.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e)
                {
                    printKonto();
                }
            });
        getSpecialPanel().add(b);

        b=new JButton("Alle Konten",Icons.PRINT);
        b.setMnemonic('A');
        b.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e)
                {
                    printAllKonto();
                }
            });
        getSpecialPanel().add(b);

        searchJahr=new JSpinner();
        searchJahr.setValue(Calendar.getInstance().get(Calendar.YEAR));
        searchJahr.setEditor(new JSpinner.NumberEditor(searchJahr,"####"));
        searchJahr.addChangeListener(new ChangeListener(){
                public void stateChanged(ChangeEvent e)
                {
                    updateDataFields();
                }
            });
        addSearchLabel("Jahr",'J',searchJahr);
        getSearchPanel().add(searchJahr);

        pack();
    }

    private static final VPreparedStatement listStmt=new VPreparedStatement(
        "SELECT nummer,datum,betrag,zweck "+
        "FROM belege "+
        "WHERE kasse=? AND konto=? AND nummer>0 "+
        "      AND (datum BETWEEN ? AND ?) "+
        "ORDER BY nummer,datum,id");
    private static final VPreparedStatement kasseStmt=new VPreparedStatement(
        "SELECT name "+
        "FROM kassen "+
        "WHERE id=?");
    private static final VPreparedStatement kontenStmt=new VPreparedStatement(
        "SELECT id "+
        "FROM konten "+
        "WHERE kasse=? "+
        "ORDER BY kuerzel,id");
    private static final VPreparedStatement kontoStmt=new VPreparedStatement(
        "SELECT kuerzel,name "+
        "FROM konten "+
        "WHERE kasse=? AND id=?");
    private static final VPreparedStatement ausgabenStmt=new VPreparedStatement(
        "SELECT -SUM(betrag) "+
        "FROM belege "+
        "WHERE kasse=? AND konto=? AND nummer>0 "+
        "      AND (datum BETWEEN ? AND ?) "+
        "      AND betrag<0");
    private static final VPreparedStatement einnahmenStmt=new VPreparedStatement(
        "SELECT SUM(betrag) "+
        "FROM belege "+
        "WHERE kasse=? AND konto=? AND nummer>0 "+
        "      AND (datum BETWEEN ? AND ?) "+
        "      AND betrag>=0");
    private Report reportKonto(Object konto,String kasseStr,Object[] range)
        throws SQLException
    {
        Report rep=new Report(PageAttributes.OrientationRequestedType.LANDSCAPE);
        ReportComponent c;
        Font f=Font.decode("sans-8");

        ResultSet r;

        r=kontoStmt.executeQuery(new Object[]{kasse,konto});
        if (!r.next()) throw new SQLException();
        rep.addHeader(new ReportSpace(0.5));
        c=new ReportComponent();
        c.add(new ReportText
              (kasseStr+"\n"+
               "Konto "+r.getString(1)+"  ("+r.getString(2)+")",
               1,10.5,
               Font.decode("times-bold-24"),
               ReportText.CENTER));
        c.add(new ReportPageNumber
              ("Seite ","",
               0,11,
               Font.decode("sans-10")));
        rep.addHeader(c);
        rep.addHeader(new ReportSpace(0.1));
        rep.addHeader(new ReportText
                      (Main.dateFormat.format(range[0])+
                       " - "+
                       Main.dateFormat.format(range[1]),
                       1,10.5,
                       Font.decode("times-14"),
                       ReportText.CENTER));
        rep.addHeader(new ReportSpace(0.2));
        rep.addHeader(new ReportTable
                      (new String[]{
                          "Nr.","Datum","Ausgaben","Einnahmen","Stand",
                          "Zweck"},
                       0.5,new double[]{0.5,1.0,1.0,1.0,1.0,6.0},
                       Font.decode("sans-bold-10"),
                       ReportText.CENTER,ReportText.WORD,ReportFrame.SINGLE,
                       ReportComponent.CENTER));

        double stand=0;

        r=listStmt.executeQuery(new Object[]
            {kasse,konto,range[0],range[1]});
        while (r.next())
            {
                double betrag=r.getDouble(3);
                String betragStr=U.format(Main.currencyFormat,
                                          Math.abs(betrag));
                stand+=betrag;
                rep.add(new ReportTable
                        (new String[]{
                            U.format(Main.numberFormat,r.getObject(1)),
                            U.format(Main.dateFormat,r.getDate(2)),
                            betrag<0 ? betragStr : "",
                            betrag>=0 ? betragStr : "",
                            U.format(Main.currencyFormat,stand),
                            r.getString(4)},
                         0.5,new double[]{0.5,1.0,1.0,1.0,1.0,6.0},
                         f,
                         new int[]{
                             ReportText.RIGHT,
                             ReportText.RIGHT,
                             ReportText.RIGHT,
                             ReportText.RIGHT,
                             ReportText.RIGHT,
                             ReportText.LEFT},
                         ReportText.WORD,
                         ReportFrame.SINGLE,
                         ReportComponent.TOP));
            }

        r=ausgabenStmt.executeQuery(new Object[]
            {kasse,konto,range[0],range[1]});
        if (!r.next()) throw new SQLException();
        double sumA=r.getDouble(1);

        r=einnahmenStmt.executeQuery(new Object[]
            {kasse,konto,range[0],range[1]});
        if (!r.next()) throw new SQLException();
        double sumE=r.getDouble(1);

        f=Font.decode("sans-10");

        c=new ReportComponent();
        c.add(new ReportText
              ("Gesamt:",
               0,2.0,
               f,ReportText.RIGHT,ReportText.NEWLINE,true));
        c.add(new ReportTable
              (new String[]{
                  U.format(Main.currencyFormat,sumA),
                  U.format(Main.currencyFormat,sumE)},
               2.0,new double[]{1.0,1.0},
               f,
               new int[]{
                   ReportText.RIGHT,
                   ReportText.RIGHT},
               ReportText.WORD,
               ReportFrame.SINGLE,
               ReportComponent.CENTER));
        rep.add(c);

        c=new ReportComponent();
        c.add(new ReportTable
              (new String[]{
                  U.format(Main.currencyFormat,sumE-sumA)},
               2.0,new double[]{2.0},
               f,
               new int[]{
                   ReportText.CENTER},
               ReportText.WORD,
               ReportFrame.SINGLE,
               ReportComponent.CENTER));
        rep.add(c);

        rep.addBottom(new ReportSpace(0.5));

        return rep;
    }

    private void printKonto()
    {
        Object konto=getSelectedID();

        Object[] range=new TimeChooseDialog
            (this,TimeChooseDialog.YEAR).getRange();

        try
            {
                ResultSet r=kasseStmt.executeQuery(new Object[]{kasse});
                if (!r.next()) throw new SQLException();
                String kasseStr=r.getString(1);
                final Report rep = reportKonto(konto,kasseStr,range);
                Report.printAndPreviewPdf(Main.mainFrame, rep, "foxda - Kassenliste eines Kontos");
            }
        catch (SQLException e) { Log.log(e); }
    }

    private void printAllKonto()
    {
        Object[] range=new TimeChooseDialog
            (this,TimeChooseDialog.YEAR).getRange();

        try
            {
                ResultSet r;
                Vector<Report> reports=new Vector<Report>();

                r=kasseStmt.executeQuery(new Object[]{kasse});
                if (!r.next()) throw new SQLException();
                String kasseStr=r.getString(1);

                r=kontenStmt.executeQuery(new Object[]{kasse});
                while (r.next())
                    reports.add(reportKonto(r.getObject(1),kasseStr,range));

                Report.printAndPreviewPdf(Main.mainFrame, reports, "foxda - Kassenliste aller Konten");
            }
        catch (SQLException e) { Log.log(e); }
    }

    protected EditDialog createEditDialog()
    {
        EditDialog d=new KontenEditD(this);
        firePropertyChange("kasse",null,kasse);
        return d;
    }

    private static final VPreparedStatement tableStmt=new VPreparedStatement(
        "SELECT k.id,k.kuerzel,k.name,SUM(b.betrag) "+
        "FROM konten k,belege b "+
        "WHERE k.kasse=? AND (b.id=0 OR "+
        "      (b.konto=k.id AND b.nummer>0 "+
        "       AND extract(year FROM b.datum)=?)) "+
        "GROUP BY k.id,k.kuerzel,k.name "+
        "ORDER BY k.kuerzel,k.id");
    protected void doUpdateDataFields()
    {
        try
            {
                getResultTable().setData(tableStmt.executeQuery(new Object[]{
                    kasse,searchJahr.getValue()}));
            }
        catch (SQLException e) { Log.log(e); }
    }
}
