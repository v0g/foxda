/*****************************************************************************\
*                                                                             *
*  FoxdaFoxda      Foxda      Foxda   Foxda  FoxdaFoxda          Foxda        *
*  Foxda        Foxda Foxda    Foxda Foxda   Foxda  Foxda     Foxda Foxda     *
*  FoxdaFoxda  Foxda   Foxda      Foxda      Foxda   Foxda   Foxda   Foxda    *
*  Foxda        Foxda Foxda    Foxda Foxda   Foxda  Foxda   FoxdaFoxdaFoxda   *
*  Foxda           Foxda      Foxda   Foxda  FoxdaFoxda    Foxda       Foxda  *
*                                                                             *
*******************************************************************************
*                   *                                     *                   *
*                   *   FILE:          BelegeListD.java   *                   *
*                   *                                     *                   *
*                    *************************************                    *
*                                                                             *
*                                                                             *
*      This program is free software; you can redistribute it and/or          *
*      modify it under the terms of the GNU General Public License as         *
*      published by the Free Software Foundation; either version 2 of         *
*      the License, or (at your option) any later version.                    *
*                                                                             *
*      This program is distributed in the hope that it will be useful,        *
*      but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the           *
*      GNU General Public License for more details.                           *
*                                                                             *
*      You should have received a copy of the GNU General Public License      *
*      along with this program; if not, write to the Free Software            *
*      Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.              *
*                                                                             *
*                                                                             *
\*****************************************************************************/
package foxda;
import java.lang.*;
import java.util.*;
import java.sql.*;
import java.awt.*;
import java.awt.event.*;
import java.beans.*;
import javax.swing.*;
import voji.log.*;
import voji.utils.*;
import voji.db.*;
import voji.report.*;

/**
 *
 */
public class BelegeListD extends ListDialog
{
    public static final String MODULE="kassen";

    protected Object kasse=null;

    public BelegeListD(Dialog parent)
    {
        super(parent);
    }

    public BelegeListD(Frame parent)
    {
        super(parent);
    }

    private Instance
        kontenD;
    private int search=0;

    public Dimension getPreferredSize()
    {
        return new Dimension(540,super.getPreferredSize().height);
    }

    protected void dialogInit()
    {
        JButton b;

        Main.checkModule(MODULE);

        super.dialogInit();
        setTitle("Belege");

        kontenD=new Instance(){
                public Object create()
                {
                    Object o=new KontenListD(BelegeListD.this);
                    firePropertyChange("kasse",null,kasse);
                    return o;
                }
            };

        getParent().addPropertyChangeListener("kasse",
                                              new PropertyChangeListener(){
                public void propertyChange(PropertyChangeEvent e)
                {
                    firePropertyChange(e.getPropertyName(),
                                       e.getOldValue(),e.getNewValue());
                    kasse=e.getNewValue();
                }
            });

        getResultTable().addColumn("Nummer",SwingConstants.RIGHT,20,
                                   new DataFormat(null));
        getResultTable().addColumn("Ausgaben",SwingConstants.RIGHT,50,
                                   Main.ausgabenFormat);
        getResultTable().addColumn("Einnahmen",SwingConstants.RIGHT,50,
                                   Main.einnahmenFormat);
        getResultTable().addColumn("Konto",SwingConstants.CENTER,5,
                                   new DataFormat(null));
        getResultTable().addColumn("Datum",SwingConstants.CENTER,50,
                                   new DataFormat(Main.dateFormat));
        getResultTable().addColumn("Zweck",SwingConstants.LEFT,150,
                                   new DataFormat(null));

        getSpecialPanel().setLayout(new GridLayout(1,0));

        b=new JButton("Kassenliste",Icons.PRINT);
        b.setMnemonic('L');
        b.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e)
                {
                    printListe();
                }
            });
        getSpecialPanel().add(b);

        b=new JButton("Konten",Icons.BOOK);
        b.setMnemonic('N');
        b.setEnabled(Main.isEnabledModule(KontenListD.MODULE));
        b.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e)
                {
                    ((JDialog)kontenD.get()).setVisible(true);
                }
            });
        getSpecialPanel().add(b);

        for (char c='0';c<='9';c++)
            getResultTable().getInputMap().
                put(KeyStroke.getKeyStroke(c),"s_digit");
        getResultTable().getActionMap().put("s_digit",new AbstractAction(){
                public void actionPerformed(ActionEvent e)
                {
                    search=search*10 + Integer.parseInt(e.getActionCommand());
                    updateDataFields();
                }
            });

        getResultTable().getInputMap().
            put(KeyStroke.getKeyStroke(KeyEvent.VK_BACK_SPACE,0),"s_back");
        getResultTable().getActionMap().put("s_back",new AbstractAction(){
                public void actionPerformed(ActionEvent e)
                {
                    search/=10;
                    updateDataFields();
                }
            });

        getResultTable().getInputMap().
            put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE,0),"s_esc");
        getResultTable().getActionMap().put("s_esc",new AbstractAction(){
                public void actionPerformed(ActionEvent e)
                {
                    search=0;
                    updateDataFields();
                }
            });

        getResultTable().addFocusListener(new FocusAdapter(){
                public void focusLost(FocusEvent e)
                {
                    search=0;
                    updateDataFields();
                }
            });

        pack();
    }

    private static final VPreparedStatement listStmt=new VPreparedStatement(
        "SELECT b.nummer,b.datum,b.betrag,k.kuerzel,k.name,b.zweck "+
        "FROM belege b,konten k "+
        "WHERE b.kasse=? AND b.nummer>0 AND k.id=b.konto "+
        "      AND datum BETWEEN ? AND ? "+
        "ORDER BY b.nummer,b.datum,b.id");
    private static final VPreparedStatement kasseStmt=new VPreparedStatement(
        "SELECT name "+
        "FROM kassen "+
        "WHERE id=?");
    private static final VPreparedStatement standStmt=new VPreparedStatement(
        "SELECT SUM(betrag) "+
        "FROM belege "+
        "WHERE kasse=? AND ("+
        "   (datum<?) "+
        "OR ((datum BETWEEN ? AND ?) AND (nummer=0)))");
    private static final VPreparedStatement ausgabenStmt=new VPreparedStatement(
        "SELECT -SUM(betrag) "+
        "FROM belege "+
        "WHERE kasse=? AND nummer>0 "+
        "      AND (datum BETWEEN ? AND ?) "+
        "      AND betrag<0");
    private static final VPreparedStatement einnahmenStmt=new VPreparedStatement(
        "SELECT SUM(betrag) "+
        "FROM belege "+
        "WHERE kasse=? AND nummer>0 "+
        "      AND (datum BETWEEN ? AND ?) "+
        "      AND betrag>=0");
    private void printListe()
    {
        Object[] range=new TimeChooseDialog
            (this,TimeChooseDialog.YEAR).getRange();

        Report rep=new Report(PageAttributes.OrientationRequestedType.LANDSCAPE);
        ReportComponent c;
        Font f=Font.decode("sans-8");

        try {
        ResultSet r;

        r=kasseStmt.executeQuery(new Object[]{kasse});
        if (!r.next()) throw new SQLException();
        rep.addHeader(new ReportSpace(0.5));
        c=new ReportComponent();
        c.add(new ReportText
              (r.getString(1),
               1,10.5,
               Font.decode("times-bold-24"),
               ReportText.CENTER));
        c.add(new ReportPageNumber
              ("Seite ","",
               0,11,
               Font.decode("sans-10")));
        rep.addHeader(c);
        rep.addHeader(new ReportSpace(0.1));
        rep.addHeader(new ReportText
                      (Main.dateFormat.format(range[0])+
                       " - "+
                       Main.dateFormat.format(range[1]),
                       1,10.5,
                       Font.decode("times-14"),
                       ReportText.CENTER));
        rep.addHeader(new ReportSpace(0.2));
        rep.addHeader(new ReportTable
                      (new String[]{
                          "Nr.","Datum","Ausgaben","Einnahmen","Stand",
                          "Konto","Zweck"},
                       0.5,new double[]{0.5,1.0,1.0,1.0,1.0,3.0,3.0},
                       Font.decode("sans-bold-10"),
                       ReportText.CENTER,ReportText.WORD,ReportFrame.SINGLE,
                       ReportComponent.CENTER));

        r=standStmt.executeQuery(new Object[]{
            kasse,
            range[0],
            range[0],range[1]});
        if (!r.next()) throw new SQLException();
        double stand=r.getDouble(1);
        rep.add(new ReportTable
                (new String[]{
                    "","","","",
                    U.format(Main.currencyFormat,stand),
                    "",""},
                 0.5,new double[]{0.5,1.0,1.0,1.0,1.0,3.0,3.0},
                 f,
                 new int[]{
                     ReportText.RIGHT,
                     ReportText.RIGHT,
                     ReportText.RIGHT,
                     ReportText.RIGHT,
                     ReportText.RIGHT,
                     ReportText.LEFT,
                     ReportText.LEFT},
                 ReportText.WORD,
                 ReportFrame.SINGLE,
                 ReportComponent.TOP));

        r=listStmt.executeQuery(new Object[]
            {kasse,range[0],range[1]});
        while (r.next())
            {
                double betrag=r.getDouble(3);
                String betragStr=U.format(Main.currencyFormat,
                                          Math.abs(betrag));
                stand+=betrag;
                rep.add(new ReportTable
                        (new String[]{
                            U.format(Main.numberFormat,r.getObject(1)),
                            U.format(Main.dateFormat,r.getDate(2)),
                            betrag<0 ? betragStr : "",
                            betrag>=0 ? betragStr : "",
                            U.format(Main.currencyFormat,stand),
                            r.getString(4)+" ("+r.getString(5)+")",
                            r.getString(6)},
                         0.5,new double[]{0.5,1.0,1.0,1.0,1.0,3.0,3.0},
                         f,
                         new int[]{
                             ReportText.RIGHT,
                             ReportText.RIGHT,
                             ReportText.RIGHT,
                             ReportText.RIGHT,
                             ReportText.RIGHT,
                             ReportText.LEFT,
                             ReportText.LEFT},
                         ReportText.WORD,
                         ReportFrame.SINGLE,
                         ReportComponent.TOP));
            }

        r=ausgabenStmt.executeQuery(new Object[]
            {kasse,range[0],range[1]});
        if (!r.next()) throw new SQLException();
        double sumA=r.getDouble(1);

        r=einnahmenStmt.executeQuery(new Object[]
            {kasse,range[0],range[1]});
        if (!r.next()) throw new SQLException();
        double sumE=r.getDouble(1);

        f=Font.decode("sans-10");

        c=new ReportComponent();
        c.add(new ReportText
              ("Gesamt:",
               0,2.0,
               f,ReportText.RIGHT,ReportText.NEWLINE,true));
        c.add(new ReportTable
              (new String[]{
                  U.format(Main.currencyFormat,sumA),
                  U.format(Main.currencyFormat,sumE)},
               2.0,new double[]{1.0,1.0},
               f,
               new int[]{
                   ReportText.RIGHT,
                   ReportText.RIGHT},
               ReportText.WORD,
               ReportFrame.SINGLE,
               ReportComponent.CENTER));
        rep.add(c);

        c=new ReportComponent();
        c.add(new ReportTable
              (new String[]{
                  U.format(Main.currencyFormat,sumE-sumA)},
               2.0,new double[]{2.0},
               f,
               new int[]{
                   ReportText.CENTER},
               ReportText.WORD,
               ReportFrame.SINGLE,
               ReportComponent.CENTER));
        rep.add(c);
        }
        catch (SQLException e) { Log.log(e); }

        rep.addBottom(new ReportSpace(0.5));
        Report.printAndPreviewPdf(Main.mainFrame, rep, "foxda - Kassenliste");
    }

    protected EditDialog createEditDialog()
    {
        EditDialog d=new BelegeEditD(this);
        firePropertyChange("kasse",null,kasse);
        return d;
    }

    private static final VPreparedStatement tableStmt=new VPreparedStatement(
        "SELECT b.id,b.nummer,b.betrag,b.betrag,k.kuerzel,b.datum,"+
        "       b.zweck "+
        "FROM belege b,konten k "+
        "WHERE b.kasse=? AND k.id=b.konto "+
        "ORDER BY b.datum DESC,b.id DESC");
    private static final VPreparedStatement searchStmt=new VPreparedStatement(
        "SELECT id "+
        "FROM belege "+
        "WHERE kasse=? AND nummer=?");
    private static final VPreparedStatement sumStmt=new VPreparedStatement(
        "SELECT SUM(betrag) "+
        "FROM belege "+
        "WHERE kasse=?");
    protected void doUpdateDataFields()
    {
        try
            {
                ResultSet r;

                getResultTable().setData(tableStmt.executeQuery(new Object[]{
                    kasse}));

                if (search>0)
                    {
                        setInfo("Suche nach Beleg-Nummer: "+search);
                        r=searchStmt.executeQuery(new Object[]{
                            kasse,search});
                        if (!r.next()) throw new SQLException();
                        getResultTable().setSelectedID(r.getInt(1));
                        return;
                    }

                r=sumStmt.executeQuery(new Object[]{kasse});
                if (!r.next()) throw new SQLException();
                double sum=r.getDouble(1);

                setInfo("Stand: "+Main.currencyFormat.format(sum));
            }
        catch (SQLException e) { Log.log(e); }
    }
}
