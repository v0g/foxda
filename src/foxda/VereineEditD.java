/*****************************************************************************\
*                                                                             *
*  FoxdaFoxda      Foxda      Foxda   Foxda  FoxdaFoxda          Foxda        *
*  Foxda        Foxda Foxda    Foxda Foxda   Foxda  Foxda     Foxda Foxda     *
*  FoxdaFoxda  Foxda   Foxda      Foxda      Foxda   Foxda   Foxda   Foxda    *
*  Foxda        Foxda Foxda    Foxda Foxda   Foxda  Foxda   FoxdaFoxdaFoxda   *
*  Foxda           Foxda      Foxda   Foxda  FoxdaFoxda    Foxda       Foxda  *
*                                                                             *
*******************************************************************************
*                   *                                     *                   *
*                   *   FILE:         VereineEditD.java   *                   *
*                   *                                     *                   *
*                    *************************************                    *
*                                                                             *
*                                                                             *
*      This program is free software; you can redistribute it and/or          *
*      modify it under the terms of the GNU General Public License as         *
*      published by the Free Software Foundation; either version 2 of         *
*      the License, or (at your option) any later version.                    *
*                                                                             *
*      This program is distributed in the hope that it will be useful,        *
*      but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the           *
*      GNU General Public License for more details.                           *
*                                                                             *
*      You should have received a copy of the GNU General Public License      *
*      along with this program; if not, write to the Free Software            *
*      Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.              *
*                                                                             *
*                                                                             *
\*****************************************************************************/
package foxda;
import java.lang.*;
import java.sql.*;
import java.awt.*;
import javax.swing.*;
import voji.log.*;
import voji.utils.*;
import voji.db.*;
import voji.ui.*;

/**
 *
 */
public class VereineEditD extends EditDialog
{
    public static final String MODULE="personen";

    public VereineEditD(Dialog parent)
    {
        super(parent);
    }

    public VereineEditD(Frame parent)
    {
        super(parent);
    }

    private JTextField
        dataKuerzel,
        dataName;
    private JComboBox<String>
        dataTyp;
    private JLookupComponent
        dataVerband;

    public Dimension getPreferredSize()
    {
        return new Dimension(400,super.getPreferredSize().height);
    }

    protected void dialogInit()
    {
        JLabel l;

        Main.checkModule(MODULE);

        super.dialogInit();
        setTitle("Vereinsdaten");

        getContentPanel().setLayout(new BorderLayout());

        JPanel labelPanel=new JPanel();
        labelPanel.setLayout(new GridLayout(0,1));
        getContentPanel().add(labelPanel,BorderLayout.WEST);

        JPanel inputPanel=new JPanel();
        inputPanel.setLayout(new GridLayout(0,1));
        getContentPanel().add(inputPanel,BorderLayout.CENTER);

        dataTyp=new JComboBox<String>(new String[]{
            "KZV",
            "KV",
            "LV"
        });
        dataTyp.setEditable(true);
        inputPanel.add(dataTyp);
        l=new JLabel(" Typ: ",JLabel.RIGHT);
        l.setDisplayedMnemonic('T');
        l.setLabelFor(dataTyp);
        labelPanel.add(l);

        dataKuerzel=new JTextField();
        inputPanel.add(dataKuerzel);
        l=new JLabel(" Kürzel: ",JLabel.RIGHT);
        l.setDisplayedMnemonic('K');
        l.setLabelFor(dataKuerzel);
        labelPanel.add(l);

        dataName=new JTextField();
        inputPanel.add(dataName);
        l=new JLabel(" Name: ",JLabel.RIGHT);
        l.setDisplayedMnemonic('N');
        l.setLabelFor(dataName);
        labelPanel.add(l);

        dataVerband=new JLookupComponent
            (new Instance(VereineListD.class,new Object[]{this}),
             null,
             "SELECT kuerzel "+
             "FROM vereine "+
             "WHERE id=?",
             'V');
        inputPanel.add(dataVerband);
        l=new JLabel(" Verband: ",JLabel.RIGHT);
        l.setDisplayedMnemonic('V');
        labelPanel.add(l);

        pack();
    }

    protected void initComponents()
    {
        dataKuerzel.selectAll();
        dataName.selectAll();
        dataTyp.requestFocus();
    }

    private static final VPreparedStatement selectStmt=new VPreparedStatement(
        "SELECT typ,kuerzel,name,verband "+
        "FROM vereine "+
        "WHERE id=?");
    protected void selectData(int id)
    {
        try
            {
                int i=1;
                ResultSet r=selectStmt.executeQuery(new Object[]{
                    id});
                if (!r.next()) throw new SQLException();
                dataTyp.setSelectedItem(r.getObject(i++));
                dataKuerzel.setText(r.getString(i++));
                dataName.setText(r.getString(i++));
                dataVerband.setSelectedID(r.getInt(i++));
            }
        catch (SQLException e) { Log.log(e); }
    }

    private static final VPreparedStatement idStmt=new VPreparedStatement(
        "SELECT MAX(id)+1 "+
        "FROM vereine");
    private static final VPreparedStatement insertStmt=new VPreparedStatement(
        "INSERT INTO vereine "+
        "(typ,kuerzel,name,verband,id) "+
        "VALUES (?,?,?,?,?)");
    protected int insertData()
    {
        try
            {
                int id;
                ResultSet r=idStmt.executeQuery(new Object[]{});
                if (!r.next()) throw new SQLException();
                id=r.getInt(1);
                insertStmt.executeUpdate(new Object[]{
                    dataTyp.getSelectedItem(),
                    dataKuerzel.getText(),
                    dataName.getText(),
                    dataVerband.getSelectedID(),
                    id});
                return id;
            }
        catch (SQLException e) { Log.log(e); }
        return -1;
    }

    private static final VPreparedStatement updateStmt=new VPreparedStatement(
        "UPDATE vereine SET "+
        "typ=?,"+
        "kuerzel=?,"+
        "name=?,"+
        "verband=? "+
        "WHERE id=?");
    protected void updateData(int id)
    {
        try
            {
                updateStmt.executeUpdate(new Object[]{
                    dataTyp.getSelectedItem(),
                    dataKuerzel.getText(),
                    dataName.getText(),
                    dataVerband.getSelectedID(),
                    id});
            }
        catch (SQLException e) { Log.log(e); }
    }

    private static final VPreparedStatement deleteStmt=new VPreparedStatement(
        "DELETE FROM vereine "+
        "WHERE id=?");
    protected void deleteData(int id)
    {
        try
            {
                deleteStmt.executeUpdate(new Object[]{
                    id});
            }
        catch (SQLException e) { Log.log(e); }
    }
}
