/*****************************************************************************\
*                                                                             *
*  FoxdaFoxda      Foxda      Foxda   Foxda  FoxdaFoxda          Foxda        *
*  Foxda        Foxda Foxda    Foxda Foxda   Foxda  Foxda     Foxda Foxda     *
*  FoxdaFoxda  Foxda   Foxda      Foxda      Foxda   Foxda   Foxda   Foxda    *
*  Foxda        Foxda Foxda    Foxda Foxda   Foxda  Foxda   FoxdaFoxdaFoxda   *
*  Foxda           Foxda      Foxda   Foxda  FoxdaFoxda    Foxda       Foxda  *
*                                                                             *
*******************************************************************************
*                   *                                     *                   *
*                   *   FILE:       HSAnlagenEditD.java   *                   *
*                   *                                     *                   *
*                    *************************************                    *
*                                                                             *
*                                                                             *
*      This program is free software; you can redistribute it and/or          *
*      modify it under the terms of the GNU General Public License as         *
*      published by the Free Software Foundation; either version 2 of         *
*      the License, or (at your option) any later version.                    *
*                                                                             *
*      This program is distributed in the hope that it will be useful,        *
*      but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the           *
*      GNU General Public License for more details.                           *
*                                                                             *
*      You should have received a copy of the GNU General Public License      *
*      along with this program; if not, write to the Free Software            *
*      Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.              *
*                                                                             *
*                                                                             *
\*****************************************************************************/
package foxda;
import java.lang.*;
import java.util.*;
import java.sql.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import voji.log.*;
import voji.utils.*;
import voji.db.*;
import voji.ui.*;

/**
 *
 */
public class HSAnlagenEditD extends JDialog
{
    public static final String MODULE="hskasse";

    public HSAnlagenEditD(Dialog parent)
    {
        super(parent);
    }

    public HSAnlagenEditD(Frame parent)
    {
        super(parent);
    }

    private JFormatTextField
        dataGesBetrag,
        dataAJahr,
        dataJahre,
        dataBetrag;
    private JComboBox<String>
        dataTyp;
    private JResultTable
        dataAnlagen;
    private JLabel
        infoLabel;
    protected int currentID=-1;

    protected void dialogInit()
    {
        JLabel l;
        JPanel p;
        JButton b;

        Main.checkModule(MODULE);

        super.dialogInit();
        setTitle("HS-Anlagedaten");
        setModal(true);

        getContentPane().setLayout(new BorderLayout());

        JPanel contentPanel=new JPanel();
        contentPanel.setLayout(new BorderLayout());
        getContentPane().add(contentPanel,BorderLayout.NORTH);

        JPanel labelPanel=new JPanel();
        labelPanel.setLayout(new GridLayout(0,1));
        contentPanel.add(labelPanel,BorderLayout.WEST);

        JPanel inputPanel=new JPanel();
        inputPanel.setLayout(new GridLayout(0,1));
        contentPanel.add(inputPanel,BorderLayout.CENTER);

        dataGesBetrag=new JFormatTextField(Main.numberFormat);
        dataGesBetrag.setEditable(false);
        inputPanel.add(dataGesBetrag);
        l=new JLabel(" Gesamt-Betrag: ",JLabel.RIGHT);
        l.setDisplayedMnemonic('B');
        l.setLabelFor(dataGesBetrag);
        labelPanel.add(l);

        dataAJahr=new JFormatTextField(Main.simpleNumberFormat);
        dataAJahr.setEditable(false);
        inputPanel.add(dataAJahr);
        l=new JLabel(" Anfangs-Jahr: ",JLabel.RIGHT);
        l.setDisplayedMnemonic('A');
        l.setLabelFor(dataAJahr);
        labelPanel.add(l);

        dataJahre=new JFormatTextField(Main.simpleNumberFormat);
        dataJahre.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e)
                {
                    createAnlagen();
                }
            });
        inputPanel.add(dataJahre);
        l=new JLabel(" Anzahl der Jahre: ",JLabel.RIGHT);
        l.setDisplayedMnemonic('J');
        l.setLabelFor(dataJahre);
        labelPanel.add(l);

        dataTyp=new JComboBox<String>(new String[]{
            "Linear"
        });
        dataTyp.setEditable(true);
        inputPanel.add(dataTyp);
        l=new JLabel(" Typ: ",JLabel.RIGHT);
        l.setDisplayedMnemonic('T');
        l.setLabelFor(dataTyp);
        labelPanel.add(l);

        b=new JButton("Erstellen");
        b.setMnemonic('E');
        b.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e)
                {
                    createAnlagen();
                }
            });
        inputPanel.add(b,BorderLayout.SOUTH);
        labelPanel.add(new JLabel());

        JPanel tablePanel=new JPanel();
        tablePanel.setLayout(new BorderLayout());
        tablePanel.setBorder(BorderFactory.createCompoundBorder(
            BorderFactory.createEmptyBorder(8,8,8,8),
            BorderFactory.createLoweredBevelBorder()));
        getContentPane().add(tablePanel,BorderLayout.CENTER);

        dataAnlagen=new JResultTable();
        dataAnlagen.addColumn("Jahr",SwingConstants.RIGHT,20,
                              new DataFormat(null));
        dataAnlagen.addColumn("Betrag",SwingConstants.RIGHT,30,
                              new DataFormat(Main.currencyFormat));
        dataAnlagen.getInputMap().
            put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER,0),"edit");
        dataAnlagen.getActionMap().put("edit",new AbstractAction(){
                public void actionPerformed(ActionEvent e)
                {
                    updateBetrag();
                    dataBetrag.requestFocus();
                }
            });
        tablePanel.add(new JScrollPane(dataAnlagen),BorderLayout.CENTER);

        JPanel underTablePanel=new JPanel();
        underTablePanel.setLayout(new BorderLayout());
        tablePanel.add(underTablePanel,BorderLayout.SOUTH);

        infoLabel=new JLabel("",JLabel.RIGHT);
        underTablePanel.add(infoLabel,BorderLayout.CENTER);

        JPanel editPanel=new JPanel();
        editPanel.setLayout(new BorderLayout());
        editPanel.setBorder(BorderFactory.createRaisedBevelBorder());
        underTablePanel.add(editPanel,BorderLayout.EAST);

        dataBetrag=new JFormatTextField(Main.numberFormat);
        dataBetrag.setColumns(8);
        dataBetrag.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e)
                {
                    change();
                }
            });
        editPanel.add(dataBetrag,BorderLayout.CENTER);

        JPanel actionPanel=new JPanel();
        actionPanel.setLayout(new GridLayout(1,0));
        editPanel.add(actionPanel,BorderLayout.EAST);

        b=new JButton(Icons.NEW);
        b.setToolTipText("Hinzufügen");
        b.setMnemonic(KeyEvent.VK_INSERT);
        b.setMargin(new Insets(0,0,0,0));
        b.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e)
                {
                    insert();
                }
            });
        actionPanel.add(b);

        b=new JButton(Icons.TRASH);
        b.setToolTipText("Löschen");
        b.setMnemonic(KeyEvent.VK_DELETE);
        b.setMargin(new Insets(0,0,0,0));
        b.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e)
                {
                    delete();
                }
            });
        actionPanel.add(b);

        JPanel closePanel=new JPanel();
        closePanel.setLayout(new GridLayout(1,0));
        closePanel.setBorder(BorderFactory.createRaisedBevelBorder());
        getContentPane().add(closePanel,BorderLayout.SOUTH);

        b=new JButton("OK");
        b.setMnemonic('O');
        b.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e)
                {
                    setVisible(false);
                }
            });
        closePanel.add(b);

        pack();
    }

    private static final VPreparedStatement idStmt=new VPreparedStatement(
        "SELECT MAX(id)+1 "+
        "FROM hsanlagen");
    private static final VPreparedStatement insertStmt=new VPreparedStatement(
        "INSERT INTO hsanlagen "+
        "(beleg,jahr,betrag,id) "+
        "VALUES (?,?,?,?)");

    private static final VPreparedStatement clearStmt=new VPreparedStatement(
        "DELETE FROM hsanlagen "+
        "WHERE beleg=?");
    protected void createAnlagen()
    {
        try
            {
                ResultSet r;
                int id;
                double sum;
                int jahr;
                int num;

                r=idStmt.executeQuery(new Object[]{});
                if (!r.next()) throw new SQLException();
                id=r.getInt(1);

                sum=((Number)dataGesBetrag.getObject()).doubleValue();
                jahr=((Number)dataAJahr.getObject()).intValue();
                num=((Number)dataJahre.getObject()).intValue();

                double[] betrag=new double[num];
                double v=Main.roundMoney(sum/num);
                for (int i=0;i<num-1;i++)
                    {
                        betrag[i]=v;
                        sum-=v;
                    }
                betrag[num-1]=sum;

                clearStmt.executeUpdate(new Object[]{
                    currentID});
                for (int i=0;i<num;i++)
                    {
                        insertStmt.executeUpdate(new Object[]{
                            currentID,
                            jahr+i,
                            betrag[i],
                            id+i});
                    }

                updateDataFields();
                dataAnlagen.requestFocus();
            }
        catch (SQLException e) { Log.log(e); }
    }

    private static final VPreparedStatement changeStmt=new VPreparedStatement(
        "UPDATE hsanlagen "+
        "SET betrag=? "+
        "WHERE id=?");
    protected void change()
    {
        try
            {
                changeStmt.executeUpdate(new Object[]{
                    dataBetrag.getObject(),
                    dataAnlagen.getSelectedID()});

                updateDataFields();
                dataAnlagen.requestFocus();
            }
        catch (SQLException e) { Log.log(e); }
    }

    private static final VPreparedStatement jahrStmt=new VPreparedStatement(
        "SELECT COUNT(*),MAX(jahr)+1 "+
        "FROM hsanlagen "+
        "WHERE beleg=?");
    protected void insert()
    {
        try
            {
                ResultSet r;
                int id;
                int count;
                Object jahr;

                r=idStmt.executeQuery(new Object[]{});
                if (!r.next()) throw new SQLException();
                id=r.getInt(1);

                r=jahrStmt.executeQuery(new Object[]{
                    currentID});
                if (!r.next()) throw new SQLException();
                count=r.getInt(1);
                jahr=r.getObject(2);

                insertStmt.executeUpdate(new Object[]{
                    currentID,
                    count==0 ? dataAJahr.getObject() : jahr,
                    dataBetrag.getObject(),
                    id});

                updateDataFields();
                dataAnlagen.setSelectedID(id);
                dataBetrag.requestFocus();
            }
        catch (SQLException e) { Log.log(e); }
    }

    private static final VPreparedStatement deleteStmt=new VPreparedStatement(
        "DELETE FROM hsanlagen "+
        "WHERE id=?");
    protected void delete()
    {
        try
            {
                deleteStmt.executeUpdate(new Object[]{
                    dataAnlagen.getSelectedID()});

                updateDataFields();
                dataAnlagen.requestFocus();
            }
        catch (SQLException e) { Log.log(e); }
    }

    private static final VPreparedStatement betragStmt=new VPreparedStatement(
        "SELECT betrag "+
        "FROM hsanlagen "+
        "WHERE id=?");
    protected void updateBetrag()
    {
        try
            {
                ResultSet r=betragStmt.executeQuery(new Object[]{
                    dataAnlagen.getSelectedID()});
                if (!r.next()) throw new SQLException();

                dataBetrag.setObject(r.getObject(1));
            }
        catch (SQLException e) { Log.log(e); }
    }

    private static final VPreparedStatement tableStmt=new VPreparedStatement(
        "SELECT id,jahr,betrag "+
        "FROM hsanlagen "+
        "WHERE beleg=? "+
        "ORDER BY jahr");
    private static final VPreparedStatement sumStmt=new VPreparedStatement(
        "SELECT SUM(betrag) "+
        "FROM hsanlagen "+
        "WHERE beleg=?");
    protected void updateDataFields()
    {
        try
            {
                ResultSet r;

                dataAnlagen.setData(tableStmt.executeQuery(new Object[]{
                    currentID}));

                r=sumStmt.executeQuery(new Object[]{
                    currentID});
                if (!r.next()) throw new SQLException();
                double sum=r.getDouble(1);
                double err=
                    ((Number)dataGesBetrag.getObject()).doubleValue()-sum;

                String b="";
                if (sum!=0)
                    {
                        b+="Gesamt: "+Main.currencyFormat.format(sum);
                        if (err!=0)
                            b+=" | Fehler: "+Main.currencyFormat.format(err);
                    }
                infoLabel.setText(" "+b+" ");
            }
        catch (SQLException e) { Log.log(e); }
    }

    private static final VPreparedStatement initStmt=new VPreparedStatement(
        "SELECT -(betrag-steuer),extract(year FROM datum),anlagetyp "+
        "FROM hskasse "+
        "WHERE id=?");
    private static final VPreparedStatement emptyStmt=new VPreparedStatement(
        "SELECT COUNT(*)>0 "+
        "FROM hsanlagen "+
        "WHERE beleg=?");
    private static final VPreparedStatement belegStmt=new VPreparedStatement(
        "UPDATE hskasse "+
        "SET anlage=?,anlagetyp=? "+
        "WHERE id=?");
    public void show(int id)
    {
        currentID=id;

        try
            {
                ResultSet r=initStmt.executeQuery(new Object[]{
                    currentID});
                if (!r.next()) throw new SQLException();
                int i=1;

                dataGesBetrag.setObject(r.getObject(i++));
                dataAJahr.setObject(r.getObject(i++));
                dataJahre.setObject(null);
                dataTyp.setSelectedItem(r.getObject(i++));
                dataBetrag.setObject(null);
            }
        catch (SQLException e) { Log.log(e); }

        updateDataFields();
        dataJahre.selectAll();
        dataJahre.requestFocus();

        super.setVisible(true);

        try
            {
                ResultSet r=emptyStmt.executeQuery(new Object[]{
                    currentID});
                if (!r.next()) throw new SQLException();
                int i=1;

                belegStmt.executeUpdate(new Object[]{
                    r.getBoolean(i++) ? 1 : 0,
                    dataTyp.getSelectedItem(),
                    currentID});
            }
        catch (SQLException e) { Log.log(e); }
    }

    public void setVisible(boolean visible)
    {
        if (visible) {
            show(-1);
        } else {
            super.setVisible(false);
        }
    }
}
