/*****************************************************************************\
*                                                                             *
*  FoxdaFoxda      Foxda      Foxda   Foxda  FoxdaFoxda          Foxda        *
*  Foxda        Foxda Foxda    Foxda Foxda   Foxda  Foxda     Foxda Foxda     *
*  FoxdaFoxda  Foxda   Foxda      Foxda      Foxda   Foxda   Foxda   Foxda    *
*  Foxda        Foxda Foxda    Foxda Foxda   Foxda  Foxda   FoxdaFoxdaFoxda   *
*  Foxda           Foxda      Foxda   Foxda  FoxdaFoxda    Foxda       Foxda  *
*                                                                             *
*******************************************************************************
*                   *                                     *                   *
*                   *   FILE:            MainFrame.java   *                   *
*                   *                                     *                   *
*                    *************************************                    *
*                                                                             *
*                                                                             *
*      This program is free software; you can redistribute it and/or          *
*      modify it under the terms of the GNU General Public License as         *
*      published by the Free Software Foundation; either version 2 of         *
*      the License, or (at your option) any later version.                    *
*                                                                             *
*      This program is distributed in the hope that it will be useful,        *
*      but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the           *
*      GNU General Public License for more details.                           *
*                                                                             *
*      You should have received a copy of the GNU General Public License      *
*      along with this program; if not, write to the Free Software            *
*      Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.              *
*                                                                             *
*                                                                             *
\*****************************************************************************/
package foxda;
import java.lang.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import voji.log.*;
import voji.utils.*;

/**
 * This is the main frame of foxda. Here are buttons to all the sub sections.
 */
public class MainFrame extends JFrame
{
    private Instance
        aboutD,
        personenD,
        vereineD,
        rassenD,
        farbenD,
        kaninchenD,
        kassenD,
        hskontenD,
        hskassenD,
        ueberweisungenD;

    public MainFrame()
    {
        super("Buchhaltung  (Foxda "+Version.VERSION+")");

        JButton b;
        JPanel p;
        JMenu m;
        JMenuItem i;

        aboutD=new Instance(AboutDialog.class,new Object[]{this});
        personenD=new Instance(PersonenListD.class,new Object[]{this});
        vereineD=new Instance(VereineListD.class,new Object[]{this});
        rassenD=new Instance(RassenListD.class,new Object[]{this});
        farbenD=new Instance(FarbenListD.class,new Object[]{this});
        kaninchenD=new Instance(KaninchenListD.class,new Object[]{this});
        kassenD=new Instance(KassenListD.class,new Object[]{this});
        hskassenD=new Instance(HSKassenListD.class,new Object[]{this});
        hskontenD=new Instance(HSKontenListD.class,new Object[]{this});
        ueberweisungenD=new Instance(UeberweisungenListD.class,new Object[]{this});

        addWindowListener(new WindowAdapter(){
                public void windowClosing(WindowEvent e)
                {
                    System.exit(0);
                }
            });
        setJMenuBar(new JMenuBar());

        m=new JMenu("Foxda");
        m.setMnemonic('F');
        getJMenuBar().add(m);

        i=new JMenuItem("Info");
        i.setMnemonic('I');
        i.setAccelerator(KeyStroke.getKeyStroke('I',InputEvent.CTRL_DOWN_MASK));
        i.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e)
                {
                    ((JDialog)aboutD.get()).setVisible(true);
                }
            });
        m.add(i);

        m.addSeparator();

        i=new JMenuItem("Beenden");
        i.setMnemonic('B');
        i.setAccelerator(KeyStroke.getKeyStroke('Q',InputEvent.CTRL_DOWN_MASK));
        i.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e)
                {
                    System.exit(0);
                }
            });
        m.add(i);

        m=new JMenu("Alles");
        m.setMnemonic('A');
        getJMenuBar().add(m);

        i=new JMenuItem("Personen");
        i.setMnemonic('P');
        i.setEnabled(Main.isEnabledModule(PersonenListD.MODULE));
        i.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e)
                {
                    ((JDialog)personenD.get()).setVisible(true);
                }
            });
        m.add(i);

        i=new JMenuItem("Vereine");
        i.setMnemonic('V');
        i.setEnabled(Main.isEnabledModule(VereineListD.MODULE));
        i.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e)
                {
                    ((JDialog)vereineD.get()).setVisible(true);
                }
            });
        m.add(i);

        i=new JMenuItem("Rassen");
        i.setMnemonic('R');
        i.setEnabled(Main.isEnabledModule(RassenListD.MODULE));
        i.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e)
                {
                    ((JDialog)rassenD.get()).setVisible(true);
                }
            });
        m.add(i);

        i=new JMenuItem("Rassen/Farben");
        i.setMnemonic('F');
        i.setEnabled(Main.isEnabledModule(FarbenListD.MODULE));
        i.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e)
                {
                    ((JDialog)farbenD.get()).setVisible(true);
                }
            });
        m.add(i);

        i=new JMenuItem("Kaninchen");
        i.setMnemonic('K');
        i.setEnabled(Main.isEnabledModule(KaninchenListD.MODULE));
        i.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e)
                {
                    ((JDialog)kaninchenD.get()).setVisible(true);
                }
            });
        m.add(i);

        i=new JMenuItem("Kassen");
        i.setMnemonic('A');
        i.setEnabled(Main.isEnabledModule(KassenListD.MODULE));
        i.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e)
                {
                    ((JDialog)kassenD.get()).setVisible(true);
                }
            });
        m.add(i);

        i=new JMenuItem("HS-Konten");
        i.setMnemonic('O');
        i.setEnabled(Main.isEnabledModule(HSKontenListD.MODULE));
        i.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e)
                {
                    ((JDialog)hskontenD.get()).setVisible(true);
                }
            });
        m.add(i);

        i=new JMenuItem("HS-Kasse");
        i.setMnemonic('H');
        i.setEnabled(Main.isEnabledModule(HSKassenListD.MODULE));
        i.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e)
                {
                    ((JDialog)hskassenD.get()).setVisible(true);
                }
            });
        m.add(i);

        i=new JMenuItem("Überweisungen");
        i.setMnemonic('W');
        i.setEnabled(Main.isEnabledModule(UeberweisungenListD.MODULE));
        i.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e)
                {
                    ((JDialog)ueberweisungenD.get()).setVisible(true);
                }
            });
        m.add(i);

        m=new JMenu("Einstellungen");
        m.setMnemonic('E');
        getJMenuBar().add(m);

        JTabbedPane tab=new JTabbedPane();
        getContentPane().add(tab);

        JPanel startPanel=new JPanel();
        startPanel.setLayout(new BorderLayout());
        tab.addTab("Start",startPanel);

        JPanel buttonPanel=new JPanel();
        buttonPanel.setLayout(new GridLayout(2,0));
        startPanel.add(buttonPanel,BorderLayout.CENTER);

        b=new JButton("Zuchtbuch",
                      new ImageIcon(Main.getProperty("foxda.main.kaninchen",
                                                     "icon-kaninchen.png")));
        b.setMnemonic('Z');
        b.setVerticalTextPosition(JButton.BOTTOM);
        b.setHorizontalTextPosition(JButton.CENTER);
        b.setEnabled(Main.isEnabledModule(KaninchenListD.MODULE));
        b.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e)
                {
                    ((JDialog)kaninchenD.get()).setVisible(true);
                }
            });
        buttonPanel.add(b);

        b=new JButton("Kassen",
                      new ImageIcon(Main.getProperty("foxda.main.kassen",
                                                     "icon-kassen.png")));
        b.setMnemonic('K');
        b.setVerticalTextPosition(JButton.BOTTOM);
        b.setHorizontalTextPosition(JButton.CENTER);
        b.setEnabled(Main.isEnabledModule(KassenListD.MODULE));
        b.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e)
                {
                    ((JDialog)kassenD.get()).setVisible(true);
                }
            });
        buttonPanel.add(b);

        b=new JButton("Überweisungen",
                      new ImageIcon(Main.getProperty("foxda.main.ueberweisungen",
                                                     "icon-ueberweisungen.png")));
        b.setMnemonic('W');
        b.setVerticalTextPosition(JButton.BOTTOM);
        b.setHorizontalTextPosition(JButton.CENTER);
        b.setEnabled(Main.isEnabledModule(UeberweisungenListD.MODULE));
        b.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e)
                {
                    ((JDialog)ueberweisungenD.get()).setVisible(true);
                }
            });
        buttonPanel.add(b);

        b=new JButton("Mitgliederverzeichnis",
                      new ImageIcon(Main.getProperty("foxda.main.personen",
                                                     "icon-personen.png")));
        b.setMnemonic('M');
        b.setVerticalTextPosition(JButton.BOTTOM);
        b.setHorizontalTextPosition(JButton.CENTER);
        b.setEnabled(Main.isEnabledModule(PersonenListD.MODULE));
        b.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e)
                {
                    ((JDialog)personenD.get()).setVisible(true);
                }
            });
        buttonPanel.add(b);

        b=new JButton("Betriebskasse",
                      new ImageIcon(Main.getProperty("foxda.main.hskasse",
                                                     "icon-hskasse.png")));
        b.setMnemonic('B');
        b.setVerticalTextPosition(JButton.BOTTOM);
        b.setHorizontalTextPosition(JButton.CENTER);
        b.setEnabled(Main.isEnabledModule(HSKassenListD.MODULE));
        b.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e)
                {
                    ((JDialog)hskassenD.get()).setVisible(true);
                }
            });
        buttonPanel.add(b);

        b=new JButton("Beenden");
        b.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e)
                {
                    System.exit(0);
                }
            });
        startPanel.add(b,BorderLayout.SOUTH);

        JTextArea t=new JTextArea();
        t.setEditable(false);
        new JLogTextArea(t);
        tab.addTab("Log",new JScrollPane(t));

        tab.addTab("Test",new TestSection());

        tab.addTab("DB",new DBSection());

        pack();
    }

    public Dimension getPreferredSize()
    {
        return new Dimension(600,400);
    }
}
