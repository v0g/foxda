/*****************************************************************************\
*                                                                             *
*  FoxdaFoxda      Foxda      Foxda   Foxda  FoxdaFoxda          Foxda        *
*  Foxda        Foxda Foxda    Foxda Foxda   Foxda  Foxda     Foxda Foxda     *
*  FoxdaFoxda  Foxda   Foxda      Foxda      Foxda   Foxda   Foxda   Foxda    *
*  Foxda        Foxda Foxda    Foxda Foxda   Foxda  Foxda   FoxdaFoxdaFoxda   *
*  Foxda           Foxda      Foxda   Foxda  FoxdaFoxda    Foxda       Foxda  *
*                                                                             *
*******************************************************************************
*                   *                                     *                   *
*                   *   FILE:       KaninchenListD.java   *                   *
*                   *                                     *                   *
*                    *************************************                    *
*                                                                             *
*                                                                             *
*      This program is free software; you can redistribute it and/or          *
*      modify it under the terms of the GNU General Public License as         *
*      published by the Free Software Foundation; either version 2 of         *
*      the License, or (at your option) any later version.                    *
*                                                                             *
*      This program is distributed in the hope that it will be useful,        *
*      but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the           *
*      GNU General Public License for more details.                           *
*                                                                             *
*      You should have received a copy of the GNU General Public License      *
*      along with this program; if not, write to the Free Software            *
*      Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.              *
*                                                                             *
*                                                                             *
\*****************************************************************************/
package foxda;
import java.lang.*;
import java.util.*;
import java.sql.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import voji.log.*;
import voji.utils.*;
import voji.db.*;
import voji.report.*;
import voji.ui.*;

/**
 *
 */
public class KaninchenListD extends ListDialog
{
    public static final String MODULE="kaninchen";

    public KaninchenListD(Dialog parent)
    {
        super(parent);
    }

    public KaninchenListD(Frame parent)
    {
        super(parent);
    }

    private Instance
        personenD,
        deckscheinD;
    private JTextField
        searchTR;
    private JFormatTextField
        searchTL_monat,
        searchTL_jahr,
        searchTL_nummer;
    private JComboBox<Object>
        searchRasse,
        searchGeschlecht;
    private JSpinner
        searchJahrB,
        searchJahrE;

    private ImageIcon
        iconVKSK,
        imageStammbaumStart,
        imageStammbaumLeaf,
        imageStammbaumNode,
        imageStammbaumLine,
        imageStammbaumLeftUp,
        imageStammbaumLeftDown,
        imageStammbaumLeftT,
        imageStammbaumRightDown,
        imageStammbaumRightUp;

    private static final VChoice ch=new VChoice(new Object[]{
        "",        "",
        "1,0",        "1,0",
        "0,1",        "0,1"
    });

    protected void dialogInit()
    {
        JButton b;
        Set<AWTKeyStroke> s;

        Main.checkModule(MODULE);

        iconVKSK=new ImageIcon
            (Main.getProperty("foxda.kaninchen.vksk.icon",
                              "icon-vksk.png"));

        imageStammbaumStart=new ImageIcon
            (Main.getProperty("foxda.kaninchen.stammbaum.start",
                              "stammbaum-start.png"));
        imageStammbaumLeaf=new ImageIcon
            (Main.getProperty("foxda.kaninchen.stammbaum.leaf",
                              "stammbaum-leaf.png"));
        imageStammbaumNode=new ImageIcon
            (Main.getProperty("foxda.kaninchen.stammbaum.node",
                              "stammbaum-node.png"));
        imageStammbaumLine=new ImageIcon
            (Main.getProperty("foxda.kaninchen.stammbaum.line",
                              "stammbaum-line.png"));
        imageStammbaumLeftUp=new ImageIcon
            (Main.getProperty("foxda.kaninchen.stammbaum.left.up",
                              "stammbaum-left-up.png"));
        imageStammbaumLeftDown=new ImageIcon
            (Main.getProperty("foxda.kaninchen.stammbaum.left.down",
                              "stammbaum-left-down.png"));
        imageStammbaumLeftT=new ImageIcon
            (Main.getProperty("foxda.kaninchen.stammbaum.left.T",
                              "stammbaum-left-T.png"));
        imageStammbaumRightDown=new ImageIcon
            (Main.getProperty("foxda.kaninchen.stammbaum.right.down",
                              "stammbaum-right-down.png"));
        imageStammbaumRightUp=new ImageIcon
            (Main.getProperty("foxda.kaninchen.stammbaum.right.up",
                              "stammbaum-right-up.png"));

        specialButton=new JButton(Icons.EDIT);
        specialButton.setToolTipText("Deckscheine eintragen");
        specialButton.setMnemonic(KeyEvent.VK_D);

        super.dialogInit();
        setTitle("Kaninchen");

        personenD=new Instance(PersonenListD.class,new Object[]{this});
        deckscheinD=new Instance(DeckscheinDialog.class,new Object[]{this});

        getResultTable().addColumn("Rasse",SwingConstants.LEFT,150,
                                   Main.rassenFormat);
        getResultTable().addColumn("TR",SwingConstants.LEFT,50,
                                   new DataFormat(null));
        getResultTable().addColumn("TL",SwingConstants.LEFT,50,
                                   Main.tlFormat);
        getResultTable().addColumn("Geschl.",SwingConstants.CENTER,25,
                                   new DataFormat(null));
        getResultTable().addColumn("Wurfdatum",SwingConstants.CENTER,75,
                                   new DataFormat(Main.dateFormat));
        getResultTable().addColumn("Züchter",SwingConstants.LEFT,150,
                                   Main.zuechterFormat);

        getSpecialPanel().setLayout(new GridLayout(1,0));

        b=new JButton("Stammbaum",Icons.PRINT);
        b.setMnemonic('S');
        b.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e)
                {
                    printStammbaum(false);
                }
            });
        getSpecialPanel().add(b);

        b=new JButton("Stammbaum (Kurz)",Icons.PRINT);
        b.setMnemonic('K');
        b.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e)
                {
                    printStammbaum(true);
                }
            });
        getSpecialPanel().add(b);

        b=new JButton("Stammbaum (VKSK)",Icons.PRINT);
        b.setMnemonic('V');
        b.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e)
                {
                    printStammbaumVKSK();
                }
            });
        getSpecialPanel().add(b);


        b=new JButton("Züchterliste",Icons.BOOK);
        b.setMnemonic('Z');
        b.setEnabled(Main.isEnabledModule(PersonenListD.MODULE));
        b.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e)
                {
                    ((JDialog)personenD.get()).setVisible(true);
                }
            });
        getSpecialPanel().add(b);

        searchRasse=new JComboBox<Object>();
        searchRasse.setEditable(true);
        searchRasse.addItemListener(new ItemListener(){
                public void itemStateChanged(ItemEvent e)
                {
                    updateDataFields();
                }
            });
        searchRasse.getEditor().addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e)
                {
                    updateComponents();
                }
            });
        addSearchLabel("Rasse",'R',searchRasse.getEditor().getEditorComponent());
        getSearchPanel().add(searchRasse);

        searchTR=new JTextField(5);
        searchTR.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e)
                {
                    updateComponents();
                }
            });
        addSearchLabel("TR",'T',searchTR);
        getSearchPanel().add(searchTR);

        searchTL_monat=new JFormatTextField(Main.numberFormat);
        searchTL_monat.setColumns(2);
        s=new HashSet<AWTKeyStroke>(searchTL_monat.getFocusTraversalKeys
                                    (KeyboardFocusManager.FORWARD_TRAVERSAL_KEYS));
        s.add(KeyStroke.getKeyStroke(KeyEvent.VK_PERIOD,0));
        searchTL_monat.setFocusTraversalKeys
            (KeyboardFocusManager.FORWARD_TRAVERSAL_KEYS,s);
        searchTL_monat.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e)
                {
                    updateComponents();
                }
            });
        addSearchLabel("TL",'L',searchTL_monat);
        getSearchPanel().add(searchTL_monat);

        searchTL_jahr=new JFormatTextField(Main.numberFormat);
        searchTL_jahr.setColumns(1);
        s=new HashSet<AWTKeyStroke>(searchTL_jahr.getFocusTraversalKeys
                                    (KeyboardFocusManager.FORWARD_TRAVERSAL_KEYS));
        s.add(KeyStroke.getKeyStroke(KeyEvent.VK_PERIOD,0));
        searchTL_jahr.setFocusTraversalKeys
            (KeyboardFocusManager.FORWARD_TRAVERSAL_KEYS,s);
        searchTL_jahr.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e)
                {
                    updateComponents();
                }
            });
        getSearchPanel().add(new JLabel("."));
        getSearchPanel().add(searchTL_jahr);

        searchTL_nummer=new JFormatTextField(Main.numberFormat);
        searchTL_nummer.setColumns(3);
        searchTL_nummer.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e)
                {
                    updateComponents();
                }
            });
        getSearchPanel().add(new JLabel("."));
        getSearchPanel().add(searchTL_nummer);

        searchGeschlecht=new JComboBox<Object>(ch.elements());
        searchGeschlecht.addItemListener(new ItemListener(){
                public void itemStateChanged(ItemEvent e)
                {
                    updateDataFields();
                }
            });
        addSearchLabel("Geschl.",'G',searchGeschlecht);
        getSearchPanel().add(searchGeschlecht);

        searchJahrB=new JSpinner();
        searchJahrB.setValue(Calendar.getInstance().
                             get(Calendar.YEAR)-9);
        searchJahrB.setEditor(new JSpinner.NumberEditor(searchJahrB,"####"));
        searchJahrB.addChangeListener(new ChangeListener(){
                public void stateChanged(ChangeEvent e)
                {
                    updateDataFields();
                }
            });
        addSearchLabel("Jahre",'J',searchJahrB);
        getSearchPanel().add(searchJahrB);

        searchJahrE=new JSpinner();
        searchJahrE.setValue(Calendar.getInstance().
                             get(Calendar.YEAR));
        searchJahrE.setEditor(new JSpinner.NumberEditor(searchJahrE,"####"));
        searchJahrE.addChangeListener(new ChangeListener(){
                public void stateChanged(ChangeEvent e)
                {
                    updateDataFields();
                }
            });
        getSearchPanel().add(new JLabel("-"));
        getSearchPanel().add(searchJahrE);

        pack();
    }

    public void setSearchGeschlecht(String geschlecht)
    {
        int i=ch.v2i(geschlecht);
        if (i>=0) searchGeschlecht.setSelectedIndex(i);
    }

    private static final VPreparedStatement elternStmt=new VPreparedStatement(
        "SELECT k.vater,k.mutter,"+
        "       k.tr,k.tl_monat,tl_jahr,tl_nummer,k.geschlecht,"+
        "       r.kuerzel,f.farbe "+
        "FROM kaninchen k,farben f,rassen r "+
        "WHERE k.id=? AND f.id=k.farbe AND r.id=f.rasse");

    private void reportStammbaumSimple(ReportComponent comp,int id,int depth)
    {
        final double max_depth=4;
        final double width=7.5/(max_depth+1);
        final double pos=0.5+width*depth;

        if (depth>max_depth) return;

        try
            {
                int i=1;
                ResultSet r=elternStmt.executeQuery(new Object[]
                    {id});
                if (!r.next()) throw new SQLException();

                int v=r.getInt(i++);
                int m=r.getInt(i++);
                String taeto=Main.taetoFormat.format(r,i++);
                i++; // tl_monat
                i++; // tl_jahr
                i++; // tl_nummer
                String gesch=r.getString(i++);
                String rasse=Main.rassenFormat.format(r,i++);
                i++; // farbe

                ReportComponent c=new ReportFrame(pos,pos+width);
                c.add(new ReportSpace(0.5));
                c.add(new ReportText
                      (id==0
                       ? "unbekannt"
                       : taeto+"  ("+gesch+")\n"+rasse,
                       pos,pos+width,
                       Font.decode("times-8"),
                       ReportText.CENTER,
                       ReportText.WORD,
                       true),
                      ReportComponent.CENTER);

                ReportList l=new ReportList();
                reportStammbaumSimple(l,v,depth+1);
                reportStammbaumSimple(l,m,depth+1);
                c.add(l);

                comp.add(c);
            }
        catch (SQLException e) { Log.log(e); }
    }

    private void reportStammbaumNormal(ReportComponent comp,int id,String path,
                                       boolean shorten)
    {
        final int max_depth=3;
        final double left=0.5;
        final double prop=1.0/2.5; /* widthNode : widthLine */
        final double widthNode=7.5/(max_depth*(prop+1)+1);
        final double widthLine=widthNode*prop;
        final double height=1.0*widthLine;
        final int depth=path.length();

        if (depth>max_depth) return;

        if (shorten && id==0) return;

        try
            {
                int i=1;
                ResultSet r=elternStmt.executeQuery(new Object[]
                    {id});
                if (!r.next()) throw new SQLException();

                int v=r.getInt(i++);
                int m=r.getInt(i++);
                String taeto=Main.taetoFormat.format(r,i++);
                i++; // tl_monat
                i++; // tl_jahr
                i++; // tl_nummer
                String gesch=r.getString(i++);
                String rasse=Main.rassenFormat.format(r,i++);
                i++; // farbe

                ReportComponent c=new ReportComponent();
                double x=left;
                for (i=1;i<=depth;i++)
                    {
                        ImageIcon imageLine=null;
                        if (i==depth)
                            {
                                if (path.charAt(i-1)=='m')
                                    imageLine=imageStammbaumRightUp;
                                else
                                    imageLine=imageStammbaumRightDown;
                            }
                        else
                            {
                                if (path.charAt(i-1)!=path.charAt(i))
                                    imageLine=imageStammbaumLine;
                            }

                        x+=widthNode;
                        if (imageLine!=null)
                        c.add(new ReportImage
                              (imageLine,
                               x,0.0,
                               widthLine,height));
                        x+=widthLine;
                    }

                ImageIcon imageNode=null;
                ImageIcon imageLine=null;
                if (depth==max_depth || (shorten && v==0 && m==0))
                    {
                        imageNode=imageStammbaumLeaf;
                    }
                else
                    {
                        if (depth==0)
                            imageNode=imageStammbaumStart;
                        else
                            imageNode=imageStammbaumNode;

                        if (shorten && v==0)
                            imageLine=imageStammbaumLeftUp;
                        else if (shorten && m==0)
                            imageLine=imageStammbaumLeftDown;
                        else
                            imageLine=imageStammbaumLeftT;
                    }

                c.add(new ReportImage
                      (imageNode,
                       x,0.0,
                       widthNode,height));
                c.add(new ReportText
                      (id==0
                       ? "unbekannt"
                       : taeto+"  ("+gesch+")\n"+rasse,
                       x,x+widthNode,
                       Font.decode("times-8"),
                       ReportText.CENTER,
                       ReportText.WORD,
                       true),
                      ReportComponent.CENTER);
                x+=widthNode;
                if (imageLine!=null)
                c.add(new ReportImage
                      (imageLine,
                       x,0.0,
                       widthLine,height));

                ReportList l=new ReportList();
                reportStammbaumNormal(l,v,path+"v",shorten);
                l.add(c);
                reportStammbaumNormal(l,m,path+"m",shorten);

                comp.add(l);
            }
        catch (SQLException e) { Log.log(e); }
    }

    private static final VPreparedStatement tierStmt=new VPreparedStatement(
        "SELECT r.name,f.farbe,"+
        "       k.tr,k.tl_monat,k.tl_jahr,k.tl_nummer,"+
        "       k.geschlecht,k.geburt,"+
        "       z.nachname,z.vorname,z2.nachname,z2.vorname,"+
        "       z.strassenr,z.plz,z.ort,"+
        "       v.typ,v.kuerzel,v.name,"+
        "       t.nachname,t.vorname "+
        "FROM kaninchen k,rassen r,farben f,"+
        "     personen z,personen z2,vereine v,personen t "+
        "WHERE k.id=? AND f.id=k.farbe AND r.id=f.rasse AND "+
        "      z.id=k.zuechter AND z2.id=k.zuechter2 AND "+
        "      v.id=z.verein AND t.id=k.taeto_m");
    private void printStammbaum(boolean shorten)
    {
        int id=getSelectedID();

        Report rep=new Report(PageAttributes.OrientationRequestedType.PORTRAIT);
        ReportComponent c;
        Font f=Font.decode("sans-8");

        rep.addHeader(new ReportSpace(0.4));
        rep.addHeader(new ReportTable
                      (new String[]{"Abstammungsnachweis"},
                       0.5,new double[]{7.5},
                       Font.decode("times-bold-20"),
                       ReportText.CENTER,
                       ReportText.NEWLINE,
                       ReportFrame.BOLD,
                       ReportComponent.CENTER));

        try
            {
                int i=1;
                ResultSet r=tierStmt.executeQuery(new Object[]
                    {id});
                if (!r.next()) throw new SQLException();

                String rasse=Main.rassenFormat.format(r,i++);
                i++; // farbe
                String taeto=Main.taetoFormat.format(r,i++);
                i++; // tl_monat
                i++; // tl_jahr
                i++; // tl_nummer
                String gesch=r.getString(i++);
                String geburt=U.format(Main.dateFormat,r.getObject(i++));
                String zuechter=Main.zuechterFormat.format(r,i++);
                i++; // z.vorname
                i++; // z2.nachname
                i++; // z2.vorname
                String strassenr=r.getString(i++);
                String plz_ort=r.getString(i++)+" "+r.getString(i++);
                String verein_k=r.getString(i++)+" "+r.getString(i++);
                String verein=r.getString(i++);
                String taeto_m=Main.personenFormat.format(r,i++);
                i++; // vorname

                String[][] infoL=new String[][]{
                    {"Rasse",          rasse},
                    {"Täto-Nummer",    taeto},
                    {"Geschlecht",     gesch},
                    {"Wurfdatum",      geburt},
                    {"Tätowiermeister",taeto_m},
                };
                String[][] infoR=new String[][]{
                    {"Züchter",        zuechter+"\n"+
                                       strassenr+"\n"+
                                       plz_ort},
                    {"Verein",         verein_k+"\n"+
                                       verein},
                };
                ReportComponent l;
                c=new ReportComponent();

                l=new ReportList();
                for (int j=0;j<infoL.length;j++)
                    {
                        ReportComponent t=new ReportComponent();
                        t.add(new ReportText
                              (infoL[j][0]+": ",
                               0.5,1.5,
                               Font.decode("times-8"),
                               ReportText.LEFT));
                        t.add(new ReportText
                              (infoL[j][1],
                               1.5,4.0,
                               Font.decode("times-bold-8"),
                               ReportText.LEFT));
                        l.add(t);
                    }
                c.add(l);

                l=new ReportList();
                for (int j=0;j<infoR.length;j++)
                    {
                        ReportComponent t=new ReportComponent();
                        t.add(new ReportText
                              (infoR[j][0]+":",
                               4.5,5.0,
                               Font.decode("times-8"),
                               ReportText.LEFT));
                        t.add(new ReportText
                              (infoR[j][1],
                               5.0,8.0,
                               Font.decode("times-bold-8"),
                               ReportText.LEFT));
                        l.add(t);
                    }
                c.add(l);

                rep.add(new ReportSpace(0.1));
                rep.add(c);
                rep.add(new ReportSpace(0.1));
                rep.add(new ReportFrame
                        (0.5,8.0,ReportFrame.BOLD));
            }
        catch (SQLException e) { Log.log(e); }

        rep.add(new ReportSpace(0.1));
        c=new ReportComponent();
        reportStammbaumNormal(c,id,"",shorten);
        rep.add(c);

        rep.addBottom(new ReportFrame
                      (0.5,8.0,ReportFrame.BOLD));
        rep.addBottom(new ReportSpace(0.1));
        c=new ReportComponent();
        c.add(new ReportText
              ("Alle Angaben wurden überprüft und sind korrekt.\n"+
               "\n"+
               "..........................................................."+
               "........, den "+
               U.format(Main.dateFormat,new java.util.Date()),
               0.5,3.5,
               Font.decode("times-6"),
               ReportText.LEFT,ReportText.WORD,false),
              ReportComponent.BOTTOM);
        c.add(new ReportText
              ("..........................................................."+
               "........\n"+
               "Unterschrift des Zuchtbuchführers",
               3.5,5.75,
               Font.decode("times-6"),
               ReportText.CENTER,ReportText.NEWLINE,false),
              ReportComponent.BOTTOM);
        c.add(new ReportText
              ("..........................................................."+
               "........\n"+
               "Unterschrift des Züchters",
               5.75,8.0,
               Font.decode("times-6"),
               ReportText.CENTER,ReportText.NEWLINE,false),
              ReportComponent.BOTTOM);
        rep.addBottom(c);
        rep.addBottom(new ReportSpace(0.5));
        Report.printAndPreviewPdf(Main.mainFrame, rep, "foxda - Kaninchen-Stammbaum");
    }

    private static final VPreparedStatement vereinStmt=new VPreparedStatement(
        "SELECT v.typ,v.kuerzel,v.name "+
        "FROM kaninchen k,personen p,vereine v "+
        "WHERE k.id=? AND p.id=k.zuechter AND v.id=p.verein AND v.id!=0");
    private void printStammbaumVKSK()
    {
        int id=getSelectedID();

        Report rep=new Report(PageAttributes.OrientationRequestedType.PORTRAIT);
        ReportComponent c;

        rep.addHeader(new ReportSpace(0.5));
        c=new ReportComponent();
        c.add(new ReportImage
              (iconVKSK,
               0.5,0,
               0.4,0.4));
        c.add(new ReportText
              ("Verband der Kleingärtner, Siedler und Kleintierzüchter "+
               "- Zentralvorstand -\n"+
               "Abteilung Kleintierzucht I",
               1.5,8.0,
               Font.decode("times-10"),
               ReportText.RIGHT),
              ReportComponent.CENTER);
        rep.addHeader(c);
        rep.addHeader(new ReportSpace(0.1));
        rep.addHeader(new ReportFrame
                      (0.5,8.0,ReportFrame.BOLD));
        rep.addHeader(new ReportSpace(0.3));
        rep.addHeader(new ReportText
                      ("Abstammungsnachweis",
                       0.5,8.0,
                       Font.decode("times-bold-20"),
                       ReportText.CENTER));
        rep.addHeader(new ReportSpace(0.1));

        c=new ReportComponent();
        reportStammbaumSimple(c,id,0);
        rep.add(c);

        String sparte="";
        try
            {
                int i=1;
                ResultSet r=vereinStmt.executeQuery(new Object[]
                    {id});
                if (!r.next()) throw new SQLException();

                sparte=
                    "bin Mitglied der Sparte \""+
                    r.getString(i++)+" "+
                    r.getString(i++)+" - "+
                    r.getString(i++)+
                    "\" und ";
            }
        catch (SQLException e) { Log.log(e); }

        rep.addBottom(new ReportText
                      ("Ich "+sparte+
                       "versichere hiermit, daß ich Zuchtbuch führe "+
                       "nach den Vorschriften der Fachrichtung "+
                       "Rassekaninchenzüchter. Das Tier ist ordnungsgemäß "+
                       "gekennzeichnet unter Vorlegung des vorgeschriebenen "+
                       "Deckscheines.\n"+
                       "\n"+
                       "Ort: .............................................."+
                       "......................................, den "+
                       U.format(Main.dateFormat,new java.util.Date())+"\n"+
                       "\n"+
                       "Die Eintragung im Spartenzuchtbuch ist nach Prüfung "+
                       "der Unterlagen (Deckschein) vorgenommen worden.",
                       0.5,8.0,
                       Font.decode("times-7"),
                       ReportText.LEFT,
                       ReportText.WORD,
                       false));
        rep.addBottom(new ReportSpace(0.3));
        c=new ReportComponent();
        c.add(new ReportText
              ("..........................................................."+
               "........\n"+
               "Unterschrift des Zuchtbuchführers\n"+
               "oder Spartenvorsitzenden",
               0.5,3.0,
               Font.decode("times-6"),
               ReportText.NEWLINE,ReportText.WORD,false),
              ReportComponent.CENTER);
        c.add(new ReportText
              ("Spartenstempel",
               3.0,5.5,
               Font.decode("times-6"),
               ReportText.CENTER,ReportText.NEWLINE,false),
              ReportComponent.CENTER);
        c.add(new ReportText
              ("..........................................................."+
               "........\n"+
               "Unterschrift des Züchters",
               5.5,8.0,
               Font.decode("times-6"),
               ReportText.CENTER,ReportText.NEWLINE,false),
              ReportComponent.CENTER);
        rep.addBottom(c);
        rep.addBottom(new ReportSpace(0.5));
        Report.printAndPreviewPdf(Main.mainFrame, rep, "foxda - Kaninchen-Stammbaum (VKSK)");
    }

    protected EditDialog createEditDialog()
    {
        return new KaninchenEditD(this);
    }

    protected void initComponents()
    {
        searchRasse.requestFocus();
        super.initComponents();
    }

    protected void doUpdateComponents()
    {
        super.doUpdateComponents();
        searchRasse.getEditor().selectAll();
        searchTR.selectAll();
        searchTL_monat.selectAll();
        searchTL_jahr.selectAll();
        searchTL_nummer.selectAll();
    }

    private static final VPreparedStatement rassenStmt=new VPreparedStatement(
        "SELECT kuerzel "+
        "FROM rassen "+
        "ORDER BY kuerzel");
    private static final VPreparedStatement tableStmt=new VPreparedStatement(
        "SELECT k.id,r.kuerzel,f.farbe,k.tr,"+
        "k.tl_monat,k.tl_jahr,k.tl_nummer,"+
        "k.geschlecht,k.geburt,z.nachname,z.vorname,z2.nachname,z2.vorname "+
        "FROM kaninchen k,farben f,rassen r,personen z,personen z2 "+
        "WHERE k.farbe=f.id AND f.rasse=r.id AND "+
        "      k.zuechter=z.id AND k.zuechter2=z2.id AND "+
        "((k.id=0) OR (r.kuerzel=? AND k.geschlecht LIKE ? AND k.tr LIKE ? "+
        "AND k.tl_monat::TEXT LIKE ? AND k.tl_jahr::TEXT LIKE ? AND k.tl_nummer::TEXT LIKE ? "+
        "AND (extract(year FROM k.geburt) BETWEEN ? AND ?))) "+
        "ORDER BY k.id!=0,extract(year FROM k.geburt),"+
        "k.tr,f.sid,k.geburt,k.tl_nummer "+
        "LIMIT ?");
    protected void doUpdateDataFields()
    {
        try
            {
                Object rasse=searchRasse.getEditor().getItem();

                ResultSet r=rassenStmt.executeQuery(new Object[]{});
                Vector<Object> v=new Vector<Object>();
                while (r.next()) v.add(r.getObject(1));
                searchRasse.setModel(new DefaultComboBoxModel<Object>(v));
                searchRasse.setSelectedItem(rasse);

                getResultTable().setData(tableStmt.executeQuery(new Object[]{
                    rasse,
                    ch.i2v(searchGeschlecht.getSelectedIndex())+"%",
                    searchTR.getText()+"%",
                    U.format(null,searchTL_monat.getObject(),"%"),
                    U.format(null,searchTL_jahr.getObject(),"%"),
                    U.format(null,searchTL_nummer.getObject(),"%"),
                    searchJahrB.getValue(),
                    searchJahrE.getValue(),
                    Integer.valueOf(Main.getProperty("foxda.kaninchen.limit",
                                                     "256"))}));
            }
        catch (SQLException e) { Log.log(e); }
    }

    protected void doSpecial()
    {
        int id;
        do
            {
                id=((DeckscheinDialog)deckscheinD.get()).showInsert();
                updateComponents();
                getResultTable().setSelectedID(id);
            }
        while (id>=0);
    }
}
