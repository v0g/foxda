/*****************************************************************************\
*                                                                             *
*  FoxdaFoxda      Foxda      Foxda   Foxda  FoxdaFoxda          Foxda        *
*  Foxda        Foxda Foxda    Foxda Foxda   Foxda  Foxda     Foxda Foxda     *
*  FoxdaFoxda  Foxda   Foxda      Foxda      Foxda   Foxda   Foxda   Foxda    *
*  Foxda        Foxda Foxda    Foxda Foxda   Foxda  Foxda   FoxdaFoxdaFoxda   *
*  Foxda           Foxda      Foxda   Foxda  FoxdaFoxda    Foxda       Foxda  *
*                                                                             *
*******************************************************************************
*                   *                                     *                   *
*                   *   FILE:         VereineListD.java   *                   *
*                   *                                     *                   *
*                    *************************************                    *
*                                                                             *
*                                                                             *
*      This program is free software; you can redistribute it and/or          *
*      modify it under the terms of the GNU General Public License as         *
*      published by the Free Software Foundation; either version 2 of         *
*      the License, or (at your option) any later version.                    *
*                                                                             *
*      This program is distributed in the hope that it will be useful,        *
*      but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the           *
*      GNU General Public License for more details.                           *
*                                                                             *
*      You should have received a copy of the GNU General Public License      *
*      along with this program; if not, write to the Free Software            *
*      Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.              *
*                                                                             *
*                                                                             *
\*****************************************************************************/
package foxda;
import java.lang.*;
import java.util.*;
import java.sql.*;
import java.awt.*;
import java.awt.event.*;
import java.beans.*;
import javax.swing.*;
import voji.log.*;
import voji.utils.*;
import voji.db.*;
import voji.report.*;

/**
 *
 */
public class VereineListD extends ListDialog
{
    public static final String MODULE="personen";

    public VereineListD(Dialog parent)
    {
        super(parent);
    }

    public VereineListD(Frame parent)
    {
        super(parent);
    }

    private String search="";

    public Dimension getPreferredSize()
    {
        return new Dimension(630,super.getPreferredSize().height);
    }

    protected void dialogInit()
    {
        JButton b;

        Main.checkModule(MODULE);

        super.dialogInit();
        setTitle("Vereine");

        getResultTable().addColumn("Typ",SwingConstants.LEFT,5,
                                   new DataFormat(null));
        getResultTable().addColumn("Kürzel",SwingConstants.LEFT,10,
                                   new DataFormat(null));
        getResultTable().addColumn("Name",SwingConstants.LEFT,350,
                                   new DataFormat(null));
        getResultTable().addColumn("Verband",SwingConstants.LEFT,10,
                                   new DataFormat(null));

        getSpecialPanel().setLayout(new GridLayout(1,0));

        b=new JButton("Mitglieder",Icons.PRINT);
        b.setMnemonic('M');
        b.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e)
                {
                    printMitglieder();
                }
            });
        getSpecialPanel().add(b);

        b=new JButton("Verband",Icons.PRINT);
        b.setMnemonic('V');
        b.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e)
                {
                    printVerband();
                }
            });
        getSpecialPanel().add(b);

        b=new JButton("Gesamtliste",Icons.PRINT);
        b.setMnemonic('G');
        b.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e)
                {
                    printAll();
                }
            });
        getSpecialPanel().add(b);

        for (char c='0';c<='9';c++)
            getResultTable().getInputMap().
                put(KeyStroke.getKeyStroke(c),"s_char");
        for (char c='A';c<='Z';c++)
            getResultTable().getInputMap().
                put(KeyStroke.getKeyStroke(c),"s_char");
        for (char c='a';c<='z';c++)
            getResultTable().getInputMap().
                put(KeyStroke.getKeyStroke(c),"s_char");
        getResultTable().getActionMap().put("s_char",new AbstractAction(){
                public void actionPerformed(ActionEvent e)
                {
                    search=search+e.getActionCommand();
                    updateDataFields();
                }
            });

        getResultTable().getInputMap().
            put(KeyStroke.getKeyStroke(KeyEvent.VK_BACK_SPACE,0),"s_back");
        getResultTable().getActionMap().put("s_back",new AbstractAction(){
                public void actionPerformed(ActionEvent e)
                {
                    int l=search.length();
                    if (l>0) search=search.substring(0,l-1);
                    updateDataFields();
                }
            });

        getResultTable().getInputMap().
            put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE,0),"s_esc");
        getResultTable().getActionMap().put("s_esc",new AbstractAction(){
                public void actionPerformed(ActionEvent e)
                {
                    search="";
                    updateDataFields();
                }
            });

        getResultTable().addFocusListener(new FocusAdapter(){
                public void focusLost(FocusEvent e)
                {
                    search="";
                    updateDataFields();
                }
            });

        pack();
    }

    private static final VPreparedStatement vereinStmt=new VPreparedStatement(
        "SELECT name "+
        "FROM vereine "+
        "WHERE id=?");

    private static final VPreparedStatement mitgliederStmt=new VPreparedStatement(
        "SELECT p.nachname,p.vorname,p.strassenr,p.plz,p.ort,p.telefon,"+
        "       p.kennung,p.funktionen,p.eintritt,p.geburt,p.rassen "+
        "FROM personen p "+
        "WHERE p.id!=0 AND p.verein=? "+
        "ORDER BY p.nachname,p.vorname,p.id");
    private static final VPreparedStatement mitgliederCountStmt=new VPreparedStatement(
        "SELECT COUNT(*)"+
        "FROM personen "+
        "WHERE id!=0 AND verein=?");
    private void printMitglieder()
    {
        Object verein=getSelectedID();

        Report rep=new Report(PageAttributes.OrientationRequestedType.LANDSCAPE);
        ReportComponent c;
        Font f=Font.decode("sans-8");

        try {
        ResultSet r;

        r=vereinStmt.executeQuery(new Object[]{verein});
        if (!r.next()) throw new SQLException();
        rep.addHeader(new ReportSpace(0.5));
        c=new ReportComponent();
        c.add(new ReportText
              (r.getString(1),
               1,10.5,
               Font.decode("times-bold-24"),
               ReportText.CENTER));
        c.add(new ReportPageNumber
              ("Seite ","",
               0,11,
               Font.decode("sans-10")));
        rep.addHeader(c);
        rep.addHeader(new ReportSpace(0.2));
        rep.addHeader(new ReportTable
                      (new String[]{
                          "Name","Adresse","Telefon","Kennung/Funktion",
                          "Eintritt","Geb.-Datum","Rassen"},
                       0.5,new double[]{2.0,2.0,1.5,1.5,1.0,1.0,1.5},
                       Font.decode("sans-bold-10"),
                       ReportText.CENTER,ReportText.NEWLINE,ReportFrame.SINGLE,
                       ReportComponent.CENTER));

        r=mitgliederStmt.executeQuery(new Object[]{verein});
        while (r.next())
            {
                rep.add(new ReportTable
                        (new String[]{
                            r.getString(1)+", "+r.getString(2),
                            r.getString(3)+"\n"+
                                r.getString(4)+" "+r.getString(5),
                            r.getString(6),
                            r.getString(7)+"\n"+
                                r.getString(8),
                            U.format(Main.dateFormat,r.getDate(9)),
                            U.format(Main.dateFormat,r.getDate(10)),
                            r.getString(11)},
                         0.5,new double[]{2.0,2.0,1.5,1.5,1.0,1.0,1.5},
                         f,
                         new int[]{
                             ReportText.LEFT,
                             ReportText.LEFT,
                             ReportText.LEFT,
                             ReportText.LEFT,
                             ReportText.RIGHT,
                             ReportText.RIGHT,
                             ReportText.LEFT},
                         ReportText.WORD,ReportFrame.SINGLE,
                         ReportComponent.CENTER));
            }

        r=mitgliederCountStmt.executeQuery(new Object[]{verein});
        if (!r.next()) throw new SQLException();
        rep.add(new ReportTable
                (new String[]{
                    "Insgesamt: "+
                    U.format(Main.simpleNumberFormat,r.getObject(1))},
                 0.5,new double[]{10.5},
                 Font.decode("sans-8"),
                 new int[]{
                     ReportText.CENTER},
                 ReportText.WORD,
                 ReportFrame.SINGLE,
                 ReportComponent.CENTER));
        }
        catch (SQLException e) { Log.log(e); }

        rep.addBottom(new ReportSpace(0.5));
        Report.printAndPreviewPdf(Main.mainFrame, rep, "foxda - Mitgliederliste");
    }

    private static final VPreparedStatement vereineStmt=new VPreparedStatement(
        "SELECT typ,kuerzel,name "+
        "FROM vereine "+
        "WHERE id!=0 AND verband=? "+
        "ORDER BY kuerzel,id");
    private static final VPreparedStatement vereineCountStmt=new VPreparedStatement(
        "SELECT COUNT(*) "+
        "FROM vereine "+
        "WHERE id!=0 AND verband=?");
    private void printVerband()
    {
        Object verband=getSelectedID();

        Report rep=new Report(PageAttributes.OrientationRequestedType.LANDSCAPE);
        ReportComponent c;
        Font f=Font.decode("sans-12");

        try {
        ResultSet r;

        r=vereinStmt.executeQuery(new Object[]{verband});
        if (!r.next()) throw new SQLException();
        rep.addHeader(new ReportSpace(0.5));
        c=new ReportComponent();
        c.add(new ReportText
              (r.getString(1),
               1,10.5,
               Font.decode("times-bold-24"),
               ReportText.CENTER));
        c.add(new ReportPageNumber
              ("Seite ","",
               0,11,
               Font.decode("sans-10")));
        rep.addHeader(c);
        rep.addHeader(new ReportSpace(0.2));
        rep.addHeader(new ReportTable
                      (new String[]{
                          "Kürzel","Vollständige Bezeichnung"},
                       0.5,new double[]{1.0,9.5},
                       Font.decode("sans-bold-10"),
                       ReportText.CENTER,ReportText.WORD,ReportFrame.SINGLE,
                       ReportComponent.CENTER));

        r=vereineStmt.executeQuery(new Object[]{verband});
        while (r.next())
            {
                rep.add(new ReportTable
                        (new String[]{
                            r.getString(1)+" "+r.getString(2),
                            r.getString(3)},
                         0.5,new double[]{1.0,9.5},
                         f,
                         new int[]{
                             ReportText.LEFT,
                             ReportText.LEFT},
                         ReportText.WORD,
                         ReportFrame.SINGLE,
                         ReportComponent.CENTER));
            }

        r=vereineCountStmt.executeQuery(new Object[]{verband});
        if (!r.next()) throw new SQLException();
        rep.add(new ReportTable
                (new String[]{
                    "Insgesamt: "+
                    U.format(Main.simpleNumberFormat,r.getObject(1))},
                 0.5,new double[]{10.5},
                 Font.decode("sans-8"),
                 new int[]{
                     ReportText.CENTER},
                 ReportText.WORD,
                 ReportFrame.SINGLE,
                 ReportComponent.CENTER));
        }
        catch (SQLException e) { Log.log(e); }

        rep.addBottom(new ReportSpace(0.5));
        Report.printAndPreviewPdf(Main.mainFrame, rep, "foxda - Vereine eines Verbandes");
    }

    private static final VPreparedStatement listStmt=new VPreparedStatement(
        "SELECT v.typ,v.kuerzel,v.name,b.typ,b.kuerzel "+
        "FROM vereine v,vereine b "+
        "WHERE v.id!=0 AND v.verband=b.id "+
        "ORDER BY b.kuerzel,b.id,v.kuerzel,v.id");
    private static final VPreparedStatement listCountStmt=new VPreparedStatement(
        "SELECT COUNT(*) "+
        "FROM vereine "+
        "WHERE id!=0");
    private void printAll()
    {
        Report rep=new Report(PageAttributes.OrientationRequestedType.LANDSCAPE);
        ReportComponent c;
        Font f=Font.decode("sans-12");

        try {
        ResultSet r;

        rep.addHeader(new ReportSpace(0.5));
        c=new ReportComponent();
        c.add(new ReportText
              ("Vereine und Verbände",
               1,10.5,
               Font.decode("times-bold-24"),
               ReportText.CENTER));
        c.add(new ReportPageNumber
              ("Seite ","",
               0,11,
               Font.decode("sans-10")));
        rep.addHeader(c);
        rep.addHeader(new ReportSpace(0.2));
        rep.addHeader(new ReportTable
                      (new String[]{
                          "Kürzel","Vollständige Bezeichnung","Verband"},
                       0.5,new double[]{1.0,8.5,1.0},
                       Font.decode("sans-bold-10"),
                       ReportText.CENTER,ReportText.WORD,ReportFrame.SINGLE,
                       ReportComponent.CENTER));

        r=listStmt.executeQuery(new Object[]{});
        while (r.next())
            {
                rep.add(new ReportTable
                        (new String[]{
                            r.getString(1)+" "+r.getString(2),
                            r.getString(3),
                            r.getString(4)+" "+r.getString(5)},
                         0.5,new double[]{1.0,8.5,1.0},
                         f,
                         new int[]{
                             ReportText.LEFT,
                             ReportText.LEFT,
                             ReportText.LEFT},
                         ReportText.WORD,
                         ReportFrame.SINGLE,
                         ReportComponent.CENTER));
            }

        r=listCountStmt.executeQuery(new Object[]{});
        if (!r.next()) throw new SQLException();

        f=Font.decode("sans-8");

        rep.add(new ReportTable
                (new String[]{
                    "Insgesamt: "+
                    U.format(Main.simpleNumberFormat,r.getObject(1))},
                 0.5,new double[]{10.5},
                 f,
                 new int[]{
                     ReportText.CENTER},
                 ReportText.WORD,
                 ReportFrame.SINGLE,
                 ReportComponent.CENTER));
        }
        catch (SQLException e) { Log.log(e); }

        rep.addBottom(new ReportSpace(0.5));
        Report.printAndPreviewPdf(Main.mainFrame, rep, "foxda - Vereine und Verbände");
    }

    protected EditDialog createEditDialog()
    {
        return new VereineEditD(this);
    }

    private static final VPreparedStatement tableStmt=new VPreparedStatement(
        "SELECT v.id,v.typ,v.kuerzel,v.name,b.kuerzel "+
        "FROM vereine v,vereine b "+
        "WHERE v.verband=b.id "+
        "ORDER BY b.kuerzel,b.id,v.kuerzel,v.id");
    private static final VPreparedStatement searchStmt=new VPreparedStatement(
        "SELECT id "+
        "FROM vereine v "+
        "WHERE kuerzel LIKE ? "+
        "ORDER BY kuerzel,id");
    protected void doUpdateDataFields()
    {
        try
            {
                ResultSet r;

                getResultTable().
                    setData(tableStmt.executeQuery(new Object[]{}));

                if (search.length()>0)
                    {
                        setInfo("Suche nach Verein: "+search);
                        r=searchStmt.executeQuery(new Object[]{
                            search+"%"});
                        if (!r.next()) throw new SQLException();
                        getResultTable().setSelectedID(r.getInt(1));
                        return;
                    }

                setInfo("");
            }
        catch (SQLException e) { Log.log(e); }
    }
}
