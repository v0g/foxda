/*****************************************************************************\
*                                                                             *
*  FoxdaFoxda      Foxda      Foxda   Foxda  FoxdaFoxda          Foxda        *
*  Foxda        Foxda Foxda    Foxda Foxda   Foxda  Foxda     Foxda Foxda     *
*  FoxdaFoxda  Foxda   Foxda      Foxda      Foxda   Foxda   Foxda   Foxda    *
*  Foxda        Foxda Foxda    Foxda Foxda   Foxda  Foxda   FoxdaFoxdaFoxda   *
*  Foxda           Foxda      Foxda   Foxda  FoxdaFoxda    Foxda       Foxda  *
*                                                                             *
*******************************************************************************
*                   *                                     *                   *
*                   *   FILE:          KontenEditD.java   *                   *
*                   *                                     *                   *
*                    *************************************                    *
*                                                                             *
*                                                                             *
*      This program is free software; you can redistribute it and/or          *
*      modify it under the terms of the GNU General Public License as         *
*      published by the Free Software Foundation; either version 2 of         *
*      the License, or (at your option) any later version.                    *
*                                                                             *
*      This program is distributed in the hope that it will be useful,        *
*      but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the           *
*      GNU General Public License for more details.                           *
*                                                                             *
*      You should have received a copy of the GNU General Public License      *
*      along with this program; if not, write to the Free Software            *
*      Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.              *
*                                                                             *
*                                                                             *
\*****************************************************************************/
package foxda;
import java.lang.*;
import java.util.*;
import java.sql.*;
import java.awt.*;
import java.beans.*;
import javax.swing.*;
import voji.log.*;
import voji.db.*;
import voji.ui.*;

/**
 *
 */
public class KontenEditD extends EditDialog
{
    public static final String MODULE="kassen";

    protected Object kasse=null;

    public KontenEditD(Dialog parent)
    {
        super(parent);
    }

    public KontenEditD(Frame parent)
    {
        super(parent);
    }

    private JTextField
        dataKuerzel,
        dataName;

    public Dimension getPreferredSize()
    {
        return new Dimension(400,super.getPreferredSize().height);
    }

    protected void dialogInit()
    {
        JLabel l;

        Main.checkModule(MODULE);

        super.dialogInit();
        setTitle("Kontodaten");

        getParent().addPropertyChangeListener("kasse",
                                              new PropertyChangeListener(){
                    public void propertyChange(PropertyChangeEvent e)
                {
                    firePropertyChange(e.getPropertyName(),
                                       e.getOldValue(),e.getNewValue());
                    kasse=e.getNewValue();
                }
            });

        getContentPanel().setLayout(new BorderLayout());

        JPanel labelPanel=new JPanel();
        labelPanel.setLayout(new GridLayout(0,1));
        getContentPanel().add(labelPanel,BorderLayout.WEST);

        JPanel inputPanel=new JPanel();
        inputPanel.setLayout(new GridLayout(0,1));
        getContentPanel().add(inputPanel,BorderLayout.CENTER);

        dataKuerzel=new JTextField();
        inputPanel.add(dataKuerzel);
        l=new JLabel(" Kürzel: ",JLabel.RIGHT);
        l.setDisplayedMnemonic('K');
        l.setLabelFor(dataKuerzel);
        labelPanel.add(l);

        dataName=new JTextField();
        inputPanel.add(dataName);
        l=new JLabel(" Name: ",JLabel.RIGHT);
        l.setDisplayedMnemonic('N');
        l.setLabelFor(dataName);
        labelPanel.add(l);

        pack();
    }

    protected void initComponents()
    {
        dataKuerzel.selectAll();
        dataName.selectAll();
        dataKuerzel.requestFocus();
    }

    private static final VPreparedStatement selectStmt=new VPreparedStatement(
        "SELECT kuerzel,name "+
        "FROM konten "+
        "WHERE id=?");
    protected void selectData(int id)
    {
        try
            {
                int i=1;
                ResultSet r=selectStmt.executeQuery(new Object[]{
                    id});
                if (!r.next()) throw new SQLException();
                dataKuerzel.setText(r.getString(i++));
                dataName.setText(r.getString(i++));
            }
        catch (SQLException e) { Log.log(e); }
    }

    private static final VPreparedStatement idStmt=new VPreparedStatement(
        "SELECT MAX(id)+1 "+
        "FROM konten");
    private static final VPreparedStatement insertStmt=new VPreparedStatement(
        "INSERT INTO konten "+
        "(kuerzel,name,kasse,id) "+
        "VALUES (?,?,?,?)");
    protected int insertData()
    {
        try
            {
                int id;
                ResultSet r=idStmt.executeQuery(new Object[]{});
                if (!r.next()) throw new SQLException();
                id=r.getInt(1);
                insertStmt.executeUpdate(new Object[]{
                    dataKuerzel.getText(),
                    dataName.getText(),
                    kasse,
                    id});
                return id;
            }
        catch (SQLException e) { Log.log(e); }
        return -1;
    }

    private static final VPreparedStatement updateStmt=new VPreparedStatement(
        "UPDATE konten SET "+
        "kuerzel=?,"+
        "name=? "+
        "WHERE id=?");
    protected void updateData(int id)
    {
        try
            {
                updateStmt.executeUpdate(new Object[]{
                    dataKuerzel.getText(),
                    dataName.getText(),
                    id});
            }
        catch (SQLException e) { Log.log(e); }
    }

    private static final VPreparedStatement deleteStmt=new VPreparedStatement(
        "DELETE FROM konten "+
        "WHERE id=?");
    protected void deleteData(int id)
    {
        try
            {
                deleteStmt.executeUpdate(new Object[]{
                    id});
            }
        catch (SQLException e) { Log.log(e); }
    }
}
