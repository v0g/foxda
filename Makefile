JAVAFILES=$(wildcard \
            foxda/*.java \
           )

CLASSFILES=$(JAVAFILES:.java=.class)

IMAGES = icon-hskasse.png \
         icon-kaninchen.png \
         icon-kassen.png \
         icon-personen.png \
         icon-ueberweisungen.png \
         \
         stammbaum-start.png \
         stammbaum-leaf.png \
         stammbaum-node.png \
         \
         stammbaum-line.png \
         stammbaum-left-up.png \
         stammbaum-left-down.png \
         stammbaum-left-T.png \
         stammbaum-right-up.png \
         stammbaum-right-down.png \

JAVAC = javac

JAVAC_FLAGS = -deprecation -Xlint:unchecked

# main targets
.PHONY: all
all: $(IMAGES) foxda.ini voji.jar foxda.jar

.PHONY: clean
clean:
	rm -rf .build $(IMAGES) voji.jar foxda.jar

# configuration
foxda.ini:
	cp foxda-DEFAULT.ini $@

# compile
voji.jar: $(wildcard src/voji/*/*.java src/voji/*/*/*.java)
	rm -rf   .build/voji
	mkdir -p .build/voji
	$(JAVAC) $(JAVAC_FLAGS) -d .build/voji $^
	jar cf voji.jar -C .build/voji .

foxda.jar: voji.jar $(wildcard src/foxda/*.java)
	rm -rf   .build/foxda
	mkdir -p .build/foxda
	$(JAVAC) $(JAVAC_FLAGS) -Werror -classpath voji.jar -d .build/foxda $(filter %.java,$^)
	jar cf foxda.jar -C .build/foxda .

# image transformation
%-down.pnm: %-up-vflip.pnm
	cp $< $@

%-T-up.pnm: %-up.pnm
	pnmcut -bottom $$(echo $$(pnmfile $< | cut -d' ' -f5)/2-1 | bc) $< > $@

%-T.pnm: %-T-up.pnm %-T-down.pnm
	pnmcat -tb $+ > $@

%-right-up.pnm: %-left-up.pnm
	pnmflip -lr $< > $@

%-node.pnm: %-leaf-left.pnm %-leaf-left-hflip.pnm
	pnmcat -lr $+ > $@

%-start.pnm: %-leaf-hflip.pnm
	cp $< $@

# general image transformation
stammbaum-%.pnm: src/images/stammbaum-%.pnm
	cp $< $@

icon-%.png: src/images/icon-%.png
	cp $< $@

%.png: %.pnm
	pnmtopng -compression 9 -force $< > $@

%-hflip.pnm: %.pnm
	pnmflip -lr $< > $@

%-vflip.pnm: %.pnm
	pnmflip -tb $< > $@

%-left.pnm: %.pnm
	pnmcut -right $$(echo $$(pnmfile $< | cut -d' ' -f3)/2-1 | bc) $< > $@

%-right.pnm: %.pnm
	pnmcut -left $$(echo $$(pnmfile $< | cut -d' ' -f3)/2-1 | bc) $< > $@
